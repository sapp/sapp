package nl.wur.ssb.diamond;

import life.gbol.domain.AnnotationResult;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.Protein;
import life.gbol.domain.ProvenanceAnnotation;
import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.FASTA;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.InputOutput.Output;
import nl.wur.ssb.conversion.options.CommandOptionsFormatConversion;
import nl.wur.ssb.conversion.rdfconversion.Conversion;
import org.apache.jena.ext.com.google.common.io.Files;
import org.apache.jena.rdf.model.Resource;
import org.apache.log4j.Logger;
import org.purl.ontology.bibo.domain.Document;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

public class Diamond extends SequenceBuilder {
    private static final Logger logger = Logger.getLogger(Diamond.class);
    static CommandOptions options;

    public Diamond(String[] args) throws Exception {
        super(null, "http://gbol.life/0.1/");

        options = new CommandOptions(args);

        this.domain = options.domain;

        // Retrieval of proteins, valid for COG and SwissProt..
        List<File> files = FASTA.create(options, "protein");

        if (files.size() > 0) {
            // DIAMOND
            blast(files);
        } else {
            throw new Exception("No protein sequences found");
        }

        // Converts to HDT should change name
        String[] args2 = {"-convert", "-i", options.output + ".nt", "-o", options.output.getAbsolutePath()};
        Conversion.app(new CommandOptionsFormatConversion(args2));
        // Remove NT file
        new File(options.output + ".nt").delete();
    }

    public void blast(List<File> files) throws Exception {
        logger.info("Starting blast");

        HashSet<File> outputFiles = new HashSet<>();

        for (File fastaFile : files) {
            logger.debug("Database: " + options.db);

            if (options.resultFile == null) {
                options.resultFile = new File(fastaFile + ".diamond.blast");
            }

            String cmd = options.binary + " blastp --outfmt 6 qseqid salltitles pident length mismatch gaps qstart qend sstart send evalue bitscore full_sseq --no-auto-append -d " + options.db + " -k 25 -q " + fastaFile + " -o " + options.resultFile + " -t ./";

            logger.debug("Executing: " + cmd);

            Execute(cmd);

            outputFiles.addAll(bestHitParser());

            // Reset to null to get new result name
            options.resultFile = null;
        }
        this.domain.closeAndDelete();

        // Merge all files
        File ntFile = new File(options.output + ".nt");
        FileOutputStream target = new FileOutputStream(ntFile);
        for (File outputFile : outputFiles) {
            FileInputStream source = new FileInputStream(outputFile);
            byte[] buf = new byte[8192];
            int length;
            while ((length = source.read(buf)) > 0) {
                target.write(buf, 0, length);
            }
            source.close();
        }
        target.close();
    }

//    private String buid(File fasta) throws IOException {
//            // Initialise logger
//            logger.debug("Build stage...");
//
//            int extLoc = fasta.getAbsolutePath().lastIndexOf(".");
//            File output = new File(fasta.getAbsolutePath().substring(0, extLoc) + ".dmnd");
//
//            String diamond = nl.wur.ssb.RDFSimpleCon.Util.prepareBinaryFromJar("binaries/{OS}/diamond");
//            new File(diamond).setExecutable(true);
//            String command = diamond + " makedb --no-auto-append --in " + fasta + " --db " + output;
//            logger.debug(command);
//            new ExecCommand(command);
//            return output.getAbsolutePath();
//    }


    public HashSet<File> bestHitParser() throws Exception {
        List<String> dbCollection = new ArrayList<>();
        logger.debug("Reading: " + options.resultFile);
        Scanner results = new Scanner(options.resultFile);
        int lines = 0;
        HashSet<File> outputFiles = new HashSet<>();
        while (results.hasNextLine()) {
            lines++;
            if (lines % 1000 == 0) {
                logger.info("Parsed " + lines + " lines");

                // So we dont check it every line...
                if (this.domain.getRDFSimpleCon().getModel().size() > 100000) {
                    // Create new domain object
                    logger.info("Syncing intermediate results (" + this.domain.getRDFSimpleCon().getModel().size() + " triples) to: " + options.output);
                    options.domain = this.domain;
                    // Save.save(options.domain, options.output);
                    File output = new File(options.resultFile + "_" + lines + ".nt");
                    outputFiles.add(output);
                    Output.save(options.domain, output);
                    // See if it can delete it
                    this.domain.closeAndDelete();

                    // Trying memory store
                    // File diskstore = Files.createTempDir();

                    this.domain = new Domain("file://" + Files.createTempDir().getAbsolutePath());
                    options.domain = this.domain;
                    options.annotResult = this.domain.make(AnnotationResult.class, options.annotResultIRI);
                }
            }

            String restultLine = results.nextLine();
            String[] resultLines = restultLine.split("\t");
            String query_id = resultLines[0].trim();
            String subject_id = resultLines[1].split(" ")[0];
            String description = "";
            if (resultLines[1].split(" ", 2).length > 1) {
                description = resultLines[1].split(" ", 2)[1];
            }
            float perc_identity = Float.parseFloat(resultLines[2].trim());
            int alignment_length = Integer.parseInt(resultLines[3].trim());
            int mismatches = Integer.parseInt(resultLines[4].trim());
            int gaps = Integer.parseInt(resultLines[5].trim());
            int q_start = Integer.parseInt(resultLines[6].trim());
            int q_end = Integer.parseInt(resultLines[7].trim());
            int s_start = Integer.parseInt(resultLines[8].trim());
            int s_end = Integer.parseInt(resultLines[9].trim());
            double evalue = Double.parseDouble(resultLines[10].trim());
            float bitscore = Float.parseFloat(resultLines[11].trim());
            String sequence = resultLines[12];

            dbCollection.add(subject_id);

            // Protein Object is created
            Protein protein = this.domain.make(Protein.class, query_id);

            // Protein Feature (a blast hit) is created...
            life.gbol.domain.ProteinHomology proteinFeature = this.domain.make(life.gbol.domain.ProteinHomology.class, protein.getResource().getURI() + "/blast/" + subject_id.replace("http://gbol.life/0.1/protein/", "").replace("|", "_") + "/" + q_start + "-" + q_end + "/" + s_start + "-" + s_end);

            this.featureURI = proteinFeature.getResource().getURI().replaceAll("/$", "") + "/";

            proteinFeature.setTargetRegion(this.makeRegion(s_start, s_end, "region", proteinFeature));
            proteinFeature.setLocation(this.makeRegion(q_start, q_end, "region", proteinFeature));

            FeatureProvenance featureprov = this.domain.make(FeatureProvenance.class, proteinFeature.getResource().getURI() + "/feature/prov");

            this.domain.disableCheck();
            featureprov.setOrigin(options.annotResult);
            this.domain.enableCheck();

            proteinFeature.addProvenance(featureprov);

            /*
             * Setting the blast results
             */

            // nl.systemsbiology.semantics.sapp.domain.Blast blastResult = this.domain.make(nl.systemsbiology.semantics.sapp.domain.Blast.class, proteinFeature.getResource().getURI() + "/blast/prov");

            ProvenanceAnnotation provannot = this.domain.make(ProvenanceAnnotation.class, proteinFeature.getResource().getURI() + "/blast/prov");
            Resource resource = provannot.getResource();

            resource.addLiteral(createProperty("http://gbol.life/0.1/perc_identity"), perc_identity);
            resource.addLiteral(createProperty("http://gbol.life/0.1/alignment_length"), alignment_length);
            resource.addLiteral(createProperty("http://gbol.life/0.1/mismatches"), mismatches);
            resource.addLiteral(createProperty("http://gbol.life/0.1/gaps"), gaps);
            resource.addLiteral(createProperty("http://gbol.life/0.1/evalue"), evalue);
            resource.addLiteral(createProperty("http://gbol.life/0.1/bitscore"), bitscore);

            Document document = this.domain.make(Document.class, "https://doi.org/10.1038/nmeth.3176");
            document.setAbstract("The alignment of sequencing reads against a protein reference database is a major computational bottleneck in metagenomics and data-intensive evolutionary projects. Although recent tools offer improved performance over the gold standard BLASTX, they exhibit only a modest speedup or low sensitivity. We introduce DIAMOND, an open-source algorithm based on double indexing that is 20,000 times faster than BLASTX on short reads and has a similar degree of sensitivity.");
            document.setDateAccepted(LocalDateTime.parse("2015-01-01T00:00:01"));
            document.setTitle("Fast and sensitive protein alignment using DIAMOND");
            provannot.setReference(document);
            featureprov.setAnnotation(provannot);

            /*
             * Creating orthologous pair...
             */

            Protein homologousTo = this.domain.make(Protein.class, "http://gbol.life/0.1/protein/" + Generic.checksum(sequence, "SHA-384"));
            homologousTo.setSequence(sequence);
            homologousTo.addAccession(subject_id.split(" ")[0]);

            proteinFeature.setHomologousTo(homologousTo.getResource().getURI());
            proteinFeature.setHomologousToDesc(description);
            // System.err.println(protein.getResource().getURI() + " feature " + proteinFeature.getResource().getURI());
            protein.addFeature(proteinFeature);
        }

        // Final flush
        File output = new File(options.resultFile + "_" + lines + ".nt");
        outputFiles.add(output);
        Output.save(options.domain, output);

        logger.info("Parsed " + lines + " lines");
        results.close();

        options.domain = this.domain;
        return outputFiles;
    }

    private void Execute(String command) throws Exception {
        // TODO return version...
        logger.info(command);
        ExecCommand result = new ExecCommand(command);

        logger.info("Stdout: " + result.getOutput());

        if (result.getExit() > 0) {
            logger.error("Stderr: " + result.getError());
            logger.info("Exit code: " + result.getExit());
            throw new Exception("Execution failed");
        }
    }
}
