package nl.wur.ssb.diamond;

import com.beust.jcommander.*;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import nl.wur.ssb.SappGeneric.ImportProv;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Parameters(commandDescription = "Available options: ")

public class CommandOptions extends CommandOptionsGeneric {

    @Parameter(names = {"-c", "-cores"}, description = "Numbers of cores to use")
    public int cores = 1;
    @Parameter(names = {"-binary"}, description = "Diamond executable")
    public String binary = "diamond";
    @Parameter(names = {"-diamond"}, description = "Application: Running in diamond blast mode")
    public boolean diamond;
    AnnotationResult annotResult;
    @Parameter(names = {"--max-target-seqs", "-k"}, description = "maximum number of target sequences to report alignments for")
    int targets = 1;
    @Parameter(names = {"-db", "-database"}, description = "Path to diamond database")
    String db;
    @Parameter(names = {"-r", "-resultFile"}, description = "Diamond result file")
    File resultFile;
    @Parameter(names = {"-force"}, description = "Force overwrite outputfile")
    boolean force;
    String repository = "https://gitlab.com/sapp/Annotation/Blast";
    LocalDateTime starttime = LocalDateTime.now();
    String toolversion = "0.9.9";
    String tool = "Diamond";
    String commandLine;
    String annotResultIRI = "http://gbol.life/0.1/" + UUID.randomUUID();

    CommandOptions(String[] args) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            // TODO make this default in all modules
            // Setting the AnnotationProvenance

            ImportProv origin;
            if (this.input != null) {
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
                origin = new ImportProv(domain, List.of(this.input.getAbsolutePath()), this.output, this.commandLine, this.tool, this.toolversion, this.repository, null, this.starttime, LocalDateTime.now());
            } else if (this.endpoint != null) {
                this.domain = new Domain(this.endpoint.toString());
                if (this.username != null) {
                    this.domain.getRDFSimpleCon().setAuthen(this.username, this.password);
                }
                origin = new ImportProv(domain, Collections.singletonList(this.endpoint.toString()), this.output, this.commandLine, this.tool, this.toolversion, this.repository, null, this.starttime, LocalDateTime.now());
            } else {
                throw new MissingArgumentException("Use -input or -endpoint");
            }


            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
            annotResultIRI = annotResult.getResource().getURI();
            origin.linkEntity(annotResult);

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }

    public class FileListConverter implements IStringConverter<List<String>> {
        @Override
        public List<String> convert(String args) {
            return Arrays.asList(args.split(","));

        }
    }
}
