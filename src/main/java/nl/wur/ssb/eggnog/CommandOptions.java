package nl.wur.ssb.eggnog;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import nl.wur.ssb.SappGeneric.ImportProv;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;

@Parameters(commandDescription = "Available options: ")
public class CommandOptions extends CommandOptionsGeneric {

    final String description = "Eggnog annotation module of SAPP";
    private final String repository = "https://gitlab.com/sapp/annotation/eggnog";
    public String annotResultIRI;
    @Parameter(names = {"-eggnog"}, description = "Application: Running in EggNOG blast mode")
    public boolean eggnog;
    public AnnotationResult annotResult;
    String tool = "EggNOG";
    String toolversion = "";
    @Parameter(names = {"-path"}, description = "Path to the EggNOG program folder")
    File path;
    @Parameter(names = {"-r", "-resultFile"}, description = "EggNOG result file")
    File resultFile;
    private String commandLine;
    @Parameter(names = "-starttime", description = "Start time of code", hidden = true)
    private LocalDateTime starttime = LocalDateTime.now();

    // TODO this is the default CommandOptions code ...
    public CommandOptions(String args[]) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            if (this.help || args.length == 0)
                throw new ParameterException("");

            ImportProv origin;
            if (this.input != null) {
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
                origin = new ImportProv(domain, Arrays.asList(this.input.getAbsolutePath()), this.output, this.commandLine, this.tool, this.toolversion, this.repository, null, this.starttime, LocalDateTime.now());
            } else if (this.endpoint != null) {
                this.domain = new Domain(this.endpoint.toString());
                if (this.username != null) {
                    this.domain.getRDFSimpleCon().setAuthen(this.username, this.password);
                }
                origin = new ImportProv(domain, Arrays.asList(this.endpoint.toString()), this.output, this.commandLine, this.tool, this.toolversion, this.repository, null, this.starttime, LocalDateTime.now());
            } else {
                throw new MissingArgumentException("Use -input or -endpoint");
            }

            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
            annotResultIRI = annotResult.getResource().getURI();
            origin.linkEntity(annotResult);

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
