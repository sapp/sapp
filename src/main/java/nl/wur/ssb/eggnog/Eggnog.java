package nl.wur.ssb.eggnog;

import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.SappGeneric.ExecCommand;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Xrefs;
import nl.wur.ssb.interproscan.InterProScan;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.purl.ontology.bibo.domain.Document;
import org.rdfhdt.hdt.util.StringUtil;

import java.io.File;
import java.util.*;

import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

public class Eggnog extends SequenceBuilder {

    final UUID xrefprovID = UUID.randomUUID();
    private final Logger logger = LogManager.getLogger(Eggnog.class);
    private XRefProvenance xrefProv;
    private CommandOptions arguments;

    public Eggnog(String[] args) throws Exception {
        super(null, "http://gbol.life/0.1/");

        arguments = new CommandOptions(args);

        this.domain = arguments.domain;

        this.rootIRI = "http://gbol.life/0.1/" + xrefprovID + "/";

        xrefProv = arguments.domain.make(XRefProvenance.class, rootIRI + "XRefProv");

        if (arguments.resultFile != null)
            parser(arguments.resultFile);
        else {
            List<File> proteinFiles = Generic.getProteinsFromRDF(arguments.domain, arguments.input.getName());
            for (File proteinFile : proteinFiles) {
                execute(arguments, proteinFile);
            }
        }

        logger.info("Results are saved in: " + arguments.output.getAbsolutePath());
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(arguments.domain, arguments.output);

        arguments.domain.close();
    }

    private void execute(CommandOptions arguments, File proteinFile) throws Exception {
        // Path to the application
        if (arguments.path.isDirectory()) {
            arguments.path = new File(arguments.path + "/emapper.py");
        }
        // Set execution arguments
        String[] args = {arguments.path.getAbsolutePath(), "-i", proteinFile.getAbsolutePath(), "-o", proteinFile.getAbsolutePath() + ".eggnog"};
        String command = StringUtils.join(args, " ");
        if (Util.getOs().equals("linux") && !new File(proteinFile + ".eggnog").exists()) {
            logger.info("Command: " + command);
            nl.wur.ssb.RDFSimpleCon.ExecCommand result = new nl.wur.ssb.RDFSimpleCon.ExecCommand(command);
            if (result.getExit() > 0) {
                logger.error(result.getError());
                logger.info(result.getOutput());
                throw new Exception("Execution failed");
            } else {
                logger.info(result.getOutput());
            }
        } else {
            logger.error("Other operating system beside linux not supported by eggnog at the moment.");
        }

        // emapper.py --cpu 20 --mp_start_method forkserver --data_dir /dev/shm/ -o out --output_dir /emapper_web_jobs/emapper_jobs/user_data/MM_kyyqy87s --temp_dir /emapper_web_jobs/emapper_jobs/user_data/MM_kyyqy87s --override -m diamond --dmnd_ignore_warnings --dmnd_algo ctg -i /emapper_web_jobs/emapper_jobs/user_data/MM_kyyqy87s/queries.fasta --evalue 0.001 --score 60 --pident 40.0 --query_cover 20.0 --subject_cover 20.0 --itype proteins --tax_scope auto --target_orthologs all --go_evidence non-electronic --pfam_realign none --report_orthologs --decorate_gff yes --excel > /emapper_web_jobs/emapper_jobs/user_data/MM_kyyqy87s/emapper.out 2> /emapper_web_jobs/emapper_jobs/user_data/MM_kyyqy87s/emapper.err
        File outputFile = new File(proteinFile + ".eggnog.emapper.annotations");
        parser(outputFile);
    }

    private void parser(File file) throws Exception {
        logger.info("Parsing " + file);

        // HashMap<String, String> locusLookup = Generic.getLocusTags(arguments.domain);

        Scanner scanner = new Scanner(file);

        int annotationParsed = 0;
        ArrayList<String> header = new ArrayList<>();
        while (scanner.hasNext()) {
            annotationParsed++;
            if (annotationParsed % 100 == 0)
                System.out.print("Annotations parsed: " + annotationParsed + "\r");

            String line = scanner.nextLine();

            if (line.startsWith("#query")) {
                header.addAll(List.of(line.replaceAll("^#", "").split("\t")));
                continue;
            } else if (line.startsWith("#")) {
                continue;
            }

            String[] lineArray = line.split("\t");

            //#query_name
//            String query_name = lineArray[0]; // done
//            String seed_eggNOG_ortholog = lineArray[1]; // done
//            String seed_ortholog_evalue = lineArray[2]; // done
//            String seed_ortholog_score = lineArray[3]; // done
//            String best_tax_level = lineArray[4];
//            String Preferred_name = lineArray[5];
//            String GOs = lineArray[6]; // xref
//            String EC = lineArray[7]; // xref
//            String KEGG_ko = lineArray[8]; // xref
//            String KEGG_Pathway = lineArray[9]; //xref
//            String KEGG_Module = lineArray[10]; //xref
//            String KEGG_Reaction = lineArray[11]; //xref
//            String KEGG_rclass = lineArray[12]; //xref
//            String BRITE = lineArray[13]; //xref
//            String KEGG_TC = lineArray[14]; //xref
//            String CAZy = lineArray[15]; //xref
//            String BiGG_Reaction = lineArray[16]; //xref

            String proteinURI = lineArray[header.indexOf("query")];

            if (proteinURI == null) {
                throw new Exception("Null found!? for " + file);
            }

            Protein proteinRDF = arguments.domain.make(life.gbol.domain.Protein.class, proteinURI);

//          #query	seed_ortholog	evalue	score	eggNOG_OGs	max_annot_lvl	COG_category	Description	Preferred_name	GOs	EC	KEGG_ko	KEGG_Pathway	KEGG_Module	KEGG_Reaction	KEGG_rclass	BRITE	KEGG_TC	CAZy	BiGG_Reaction	PFAMs

            // Convert to RDF! - protein URI + seed_eggNOG_ortholog identifier
            ProteinDomain proteinDomain = arguments.domain.make(ProteinDomain.class, proteinRDF.getResource().getURI() + "/" + lineArray[header.indexOf("seed_ortholog")]);

            // Annotation result
            AnnotationResult annotationResult = arguments.domain.make(AnnotationResult.class, arguments.annotResultIRI);

            // Provenance
            String version = "1.0-MGnify";
            String library = "eggNOG";

            FeatureProvenance featureprov = arguments.domain.make(life.gbol.domain.FeatureProvenance.class, proteinDomain.getResource().getURI() + "/" + library + "/" + version);

            ProvenanceAnnotation provannot = arguments.domain.make(ProvenanceAnnotation.class, proteinDomain.getResource().getURI() + "/" + library + "/" + version + "/prov"); // scoretry {
            Resource resource = provannot.getResource();

            if (lineArray[header.indexOf("evalue")].contains(";")) {
                resource.addLiteral(createProperty("http://gbol.life/0.1/evalue"), lineArray[header.indexOf("evalue")].split(";")[1]);
            } else {
                resource.addLiteral(createProperty("http://gbol.life/0.1/evalue"), Double.valueOf(lineArray[header.indexOf("evalue")]));
            }

            if (lineArray[header.indexOf("score")].contains(";"))
                resource.addLiteral(createProperty("http://gbol.life/0.1/score"), Double.valueOf(lineArray[header.indexOf("score")].split(";")[1]));
            else {
                resource.addLiteral(createProperty("http://gbol.life/0.1/score"), Double.valueOf(lineArray[header.indexOf("score")]));
            }

            Document document = domain.make(Document.class, "https://blabla.com");
            document.setTitle("EGNOGG BLA");
            document.setAbstract("ABSTRACT HERE");
            // document.setDateAccepted(LocalDate.parse("2020-11-06").atStartOfDay());
            document.setDoi("DOI HERE");
            provannot.setReference(document);

            featureprov.setAnnotation(provannot);
            featureprov.setOrigin(annotationResult);
            proteinDomain.addProvenance(featureprov);

            XRefProvenance xrefprov = arguments.domain.make(life.gbol.domain.XRefProvenance.class, featureprov.getResource().getURI() + "/xrefprov");
            xrefprov.setOrigin(annotationResult);

            for (String go_annotation : lineArray[header.indexOf("GOs")].split(",")) {
                if (go_annotation.replaceAll("-", "").isEmpty()) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "go", version, go_annotation, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ec_annotation : lineArray[header.indexOf("EC")].split(",")) {
                if (ec_annotation.replaceAll("-", "").isEmpty()) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "ec", version, ec_annotation, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : lineArray[header.indexOf("KEGG_ko")].split(",")) {
                if (ko.replaceAll("-", "").isEmpty()) continue;
                ko = ko.replaceAll("ko:", "");
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.orthology", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : lineArray[header.indexOf("KEGG_Module")].split(",")) {
                if (ko.replaceAll("-", "").isEmpty()) continue;
                ko = ko.replaceAll("ko:", "");
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.module", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : lineArray[header.indexOf("KEGG_Pathway")].split(",")) {
                if (ko.replaceAll("-", "").isEmpty()) continue;
                ko = ko.replaceAll("ko:", "");
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.pathway", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : lineArray[header.indexOf("KEGG_Reaction")].split(",")) {
                if (ko.replaceAll("-", "").isEmpty()) continue;
                ko = ko.replaceAll("ko:", "");
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.reaction", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : lineArray[header.indexOf("KEGG_rclass")].split(",")) {
                if (ko.replaceAll("-", "").isEmpty()) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.rclass", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : lineArray[header.indexOf("BRITE")].split(",")) {
                if (ko.replaceAll("-", "").isEmpty()) continue;
                ko = ko.replaceAll("ko:", "");
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "brite", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String ko : lineArray[header.indexOf("KEGG_TC")].split(",")) {
                if (ko.replaceAll("-", "").isEmpty()) continue;
                ko = ko.replaceAll("ko:", "");
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "kegg.tc", version, ko, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String cazy : lineArray[header.indexOf("CAZy")].split(",")) {
                if (cazy.replaceAll("-", "").isEmpty()) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "cazy", version, cazy, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }

            for (String bigg : lineArray[header.indexOf("BiGG_Reaction")].split(",")) {
                if (bigg.replaceAll("-", "").isEmpty()) continue;
                XRef xref = Xrefs.create(XRef.class, arguments.domain, xrefprov, "bigg.reaction", version, bigg, null);
                xrefProv.setOrigin(annotationResult);
                xref.setProvenance(xrefProv);
                proteinDomain.addXref(xref);
            }
        }
    }
}
