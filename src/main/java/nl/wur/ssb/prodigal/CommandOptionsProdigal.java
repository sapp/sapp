package nl.wur.ssb.prodigal;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.SappGeneric.Store;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsProdigal {
    public final String tool = "prodigal";
    public final String toolversion = "2.6.3";
    final String repository = "https://gitlab.com/sapp/sapp";
    public String commandLine;
    public AnnotationResult annotResult;
    public LocalDateTime starttime = LocalDateTime.now();
    public Domain domain = Store.createMemoryStore();
    public String annotResultIRI;
    @Parameter(names = {"--help"})
    public boolean help = false;
    @Parameter(names = {"-i", "-input"}, description = "Input file", required = true)
    public File input;
    @Parameter(names = {"-o", "-output"}, description = "Output file", required = true)
    public File output;
    @Parameter(names = {"-debug"}, description = "Debug mode")
    public boolean debug;
    @Parameter(names = {"-c", "-codon"}, description = "Codon table used (overwrites genetic table in RDF data)")
    public int codon = -1;
    @Parameter(names = {"-m", "-meta"}, description = "meta genome analysis")
    public boolean meta;
    @Parameter(names = {"-faa"}, description = "Protein fasta file (skips prodigal prediction)")
    public File faa;
    @Parameter(names = {"-ffa"}, description = "Nucleotide fasta file (skips prodigal prediction)")
    public File ffn;
    @Parameter(names = {"-gff"}, description = "Gene gff file (skips prodigal prediction)")
    public File gff;
    @Parameter(names = {"-prodigal"}, description = "Running in Prodigal mode")
    boolean prodigal;

    public CommandOptionsProdigal(String[] args) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);

            ImportProv origin = new ImportProv(domain, List.of(this.input.getAbsolutePath()), this.output, this.commandLine, this.tool, this.toolversion, this.repository, null, this.starttime, LocalDateTime.now());
            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
            annotResultIRI = annotResult.getResource().getURI();
            origin.linkEntity(annotResult);

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
