package nl.wur.ssb.prodigal;

import life.gbol.domain.Protein;
import life.gbol.domain.ProvenanceAnnotation;
import life.gbol.domain.Region;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import nl.wur.ssb.SappGeneric.ExecCommand;
import nl.wur.ssb.SappGeneric.FASTA;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.Generic;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.purl.ontology.bibo.domain.Document;

import java.io.File;
import java.io.FileReader;
import java.util.*;

import static nl.wur.ssb.SappGeneric.Generic.prepareBinaryFromJar;
import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

public class Prodigal extends SequenceBuilder {

    private static final Logger logger = LogManager.getLogger();

    public Prodigal(String[] args) throws Exception {
        super(null, "");

        CommandOptionsProdigal arguments = new CommandOptionsProdigal(args);

        this.domain = arguments.domain;


        // Fix for generic
        CommandOptionsGeneric commandOptionsGeneric = new CommandOptionsGeneric();
        commandOptionsGeneric.input = arguments.input;
        commandOptionsGeneric.domain = arguments.domain;

        List<File> fastas = FASTA.create(commandOptionsGeneric, "genome");

        // If GFF file is given, skip gene prediction
        if (arguments.gff != null && fastas.size() == 1) {
            this.geneParser(arguments, fastas.get(0));
            return;
        }


        String prodigal = prepareBinaryFromJar("binaries/{OS}/prodigal/2.6.3/prodigal");
        String[] cmd;

        // Get codon table
        if (arguments.codon <= 0) {
            logger.info("Obtaining codon information from RDF file");
            arguments.codon = domain.getRDFSimpleCon().runQuerySingleRes("getTranslTable.txt", true).getLitInt("table");
        }

        logger.info("Translation table set to " + arguments.codon);

        // For each file ...
        for (File fasta : fastas) {
            logger.info("Analysing: " + fasta);
            // Obtaining GFF scores and delete on exit
            if (new File(fasta + "_genecaller.gff").exists()) {
                logger.info(fasta + "_genecaller.gff exists... skipping gene prediction");
            } else if (arguments.meta) {
                String runType = "meta";
                cmd = new String[]{prodigal, "-q", "-f", "gff", "-o", fasta + "_genecaller.gff", "-a", fasta + "_proteins.fasta", "-p", runType, "-g", String.valueOf(arguments.codon), "-d", fasta + "_nucleotide.fasta", "-i", fasta.getAbsolutePath()};
                logger.info(StringUtils.join(cmd, " "));
                new ExecCommand(cmd);
            } else {
                String runType = "single";
                cmd = new String[]{prodigal, "-q", "-f", "gff", "-o", fasta + "_genecaller.gff", "-a", fasta + "_proteins.fasta", "-p", runType, "-g", String.valueOf(arguments.codon), "-t", fasta + "_genecaller.trn", "-d", fasta + "_nucleotide.fasta", "-i", fasta.getAbsolutePath()};
                // Create training file
                logger.info(StringUtils.join(cmd, " "));
                new ExecCommand(cmd);
                // Create result file
                cmd = new String[]{prodigal, "-q", "-f", "gff", "-o", fasta + "_genecaller.gff", "-a", fasta + "_proteins.fasta", "-p", runType, "-g", String.valueOf(arguments.codon), "-t", fasta + "_genecaller.trn", "-d", fasta + "_nucleotide.fasta", "-i", fasta.getAbsolutePath()};
                logger.info(StringUtils.join(cmd, " "));
                new ExecCommand(cmd);
            }
            this.geneParser(arguments, fasta);
        }

        // When HDT input was given, HD T output is given all else gz ntriples
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(domain, arguments.output);
    }

    public void geneParser(CommandOptionsProdigal arguments, File fasta) throws Exception {
        // TODO Can be extracted from the GFF header...
        Map<String, String> geneMap = new HashMap<>();
        Map<String, String> gffMap = new HashMap<>();
        Map<String, String> proteinMap = new HashMap<>();

        String prefix = fasta.getName().split("__#__")[0];

//        String name = arguments.input.getName();

        logger.info("Building gff dictionary");

        Scanner gffs;
        Scanner genes;
        Scanner proteins;

        if (arguments.gff != null) {
            gffs = new Scanner(new FileReader(arguments.gff));
            genes = new Scanner(new FileReader(arguments.ffn));
            proteins = new Scanner(new FileReader(arguments.faa));
        } else {
            gffs = new Scanner(new FileReader(fasta + "_genecaller.gff"));
            genes = new Scanner(new FileReader(fasta + "_nucleotide.fasta"));
            proteins = new Scanner(new FileReader(fasta + "_proteins.fasta"));
        }

        while (gffs.hasNextLine()) {
            String line = gffs.nextLine();
            if (!line.startsWith("#")) {
                if (line.contains("ID=")) {
                    String key = line.split("ID=")[1].split(";")[0];
                    gffMap.put(key, line.strip().replace("/note=\"", ""));
                }
            }
        }

        logger.info("Building gene dictionary");
        String sequence;
        String header = null;
        while (genes.hasNext()) {
            String line = genes.nextLine();
            if (line.startsWith(">")) {
                header = line;
                geneMap.put(header, "");
            } else {
                sequence = geneMap.get(header) + line;
                geneMap.put(header, sequence);
            }
        }
        genes.close();

        logger.info("Building protein dictionary");

        while (proteins.hasNext()) {
            String line = proteins.nextLine();
            if (line.startsWith(">")) {
                header = line;
                proteinMap.put(header, "");
            } else {
                sequence = proteinMap.get(header) + line;
                proteinMap.put(header, sequence);
            }
        }
        proteins.close();

        life.gbol.domain.NASequence dnasequence;

        // If there are more types known we need to obtain the information for each contig
        Iterator<ResultLine> results = domain.getRDFSimpleCon().runQuery("getContigTypes.txt", true).iterator();

        // Setting up type dictionary
        // Stores the contig types and genome url lookup
        HashMap<String, String> seqTypes = new HashMap<>();
        HashMap<String, String> genomeToAccession = new HashMap<>();
        while (results.hasNext()) {
            ResultLine result = results.next();
            // Lookup allowing the use of http or accession identifier
            seqTypes.put(result.getIRI("seqobject"), result.getIRI("type"));
            seqTypes.put(result.getLitString("accession"), result.getIRI("type"));
            genomeToAccession.put(result.getLitString("accession"), result.getIRI("seqobject"));
        }

        // Get locus tags and add locus tag if it does not exists...
        Iterator<ResultLine> locusLookup = domain.getRDFSimpleCon().runQuery("getAllLocusTag.txt", true).iterator();

        HashMap<String, String> locusLookupHash = new HashMap<>();
        while (locusLookup.hasNext()) {
            ResultLine next = locusLookup.next();
            String gene = next.getIRI("gene");
            String locus = next.getLitString("locus");
            locusLookupHash.put(gene, locus);
        }

        logger.info("Parsing gene dictionary");

        int count = 0;
        int locusCount = 1;
        for (String key : geneMap.keySet()) {
            count = count + 1;
            if (count % 100 == 0)
                System.out.print("Parsed " + count + " genes of " + geneMap.size() + " database size " + domain.getRDFSimpleCon().getModel().size() + "\r");

            // Causing issues...
//            if (count % 10000 == 0) {
//                logger.info("Writing " + domain.getRDFSimpleCon().getModel().size() + " triples to: " + arguments.output);
//                arguments.domain = domain;
//                Save.save(arguments);
//                domain.closeAndDelete();
//                domain = Store.createMemoryStore();
//            }

            String[] elements = key.split(" ");
            long pos1 = Integer.parseInt(elements[2]);
            long pos2 = Integer.parseInt(elements[4]);
            long begin;
            long end;

            if (pos1 < pos2) {
                begin = pos1;
                end = pos2;
            } else {
                begin = pos2;
                end = pos1;
            }

            String strand = elements[6];

            boolean isComplement;
            if (strand.matches("-1"))
                isComplement = true;
            else if (strand.matches("1"))
                isComplement = false;
            else
                throw new Exception("Formatting of strand is wrong");

            /*
             * Obtaining prodigal information...
             */

            // ID=1_83;partial=00;start_type=ATG;rbs_motif=GGA/GAG/AGG;rbs_spacer=5-10bp;gc_cont=0.239

            String information = elements[8];
            String ID = information.split(";")[0].split("=")[1];
            String gffEntry = gffMap.get(ID);

            // logger.debug(ID + " <> " + gffEntry);

            // Format with GFF tab separated version 3
            String gffParameters;
            if (gffEntry.contains("\t")) {
                gffParameters = gffEntry.split("\t")[8];
            } else {
                // Other format of GFF
                gffParameters = gffEntry;
            }


            // ID=1_1324;partial=00;start_type=ATG;rbs_motif=None;rbs_spacer=None;gc_cont=0.720;conf=100.00;score=95.71;
            // cscore=98.42;sscore=-2.71;rscore=-4.48;uscore=-2.11;tscore=4.54;

            HashMap<String, String> gffParametersHash = new HashMap<>();
            for (String entry : gffParameters.split(";")) {
                if (entry.contains("="))
                    gffParametersHash.put(entry.split("=")[0], entry.split("=")[1]);
            }

//            String conf = gffParameters.split(";")[6].split("=")[1];
//            String score = gffParameters.split(";")[7].split("=")[1];
//            String cscore = gffParameters.split(";")[8].split("=")[1];
//
//            String sscore = gffParameters.split(";")[9].split("=")[1];
//            String rscore = gffParameters.split(";")[10].split("=")[1];
//            String uscore = gffParameters.split(";")[11].split("=")[1];
//            String tscore = gffParameters.split(";")[12].split("=")[1];
//
            String partial = gffParametersHash.get("partial");
            boolean leftPartial = false;
            boolean rightPartial = false;
            if (partial.startsWith("1")) {
                leftPartial = true;
            }
            if (partial.endsWith("1")) {
                rightPartial = true;
            }

//            String start_type = information.split(";")[2].split("=")[1];
//            String rbs_motif = information.split(";")[3].split("=")[1];
//            String rbs_spacer = information.split(";")[4].split("=")[1];
//            String gc_cont = information.split(";")[5].split("=")[1];

            String proteinSequence = proteinMap.get(key).replaceAll("\\*$", "");
            String geneSequence = geneMap.get(key);

            String geneChecksum = Generic.checksum(geneSequence, "SHA-384");

            String proteinChecksum = Generic.checksum(proteinSequence, "SHA-384");

            Protein proteinobject = domain.make(Protein.class, "http://gbol.life/0.1/protein/" + proteinChecksum);

            proteinobject.setSha384(proteinChecksum);
            proteinobject.setSequence(proteinSequence);

            /*
             * Creation of the genome object...
             */

            String genome = key.split(" ")[0].replace(">", "").replaceAll("_[0-9]+$", "");

//            String seqType;
//            if (seqTypes.containsKey("all")) {
//                seqType = seqTypes.get("all");
//            } else {
//                seqType = seqTypes.get(genome);
//            }

            // Not likely a valid url so using genome accession to get the url
            if (!genome.startsWith("http"))
                genome = genomeToAccession.get(genome);

            String seqType = seqTypes.get(genome);

            if (seqType == null) {
                dnasequence = domain.make(life.gbol.domain.Contig.class, genome);
                logger.debug("Unknown genome format detected... Using contig instead");
            } else if (seqType.toLowerCase().contains("scaffold")) {
                dnasequence = domain.make(life.gbol.domain.Scaffold.class, genome);
            } else if (seqType.toLowerCase().contains("contig")) {
                dnasequence = domain.make(life.gbol.domain.Contig.class, genome);
            } else if (seqType.toLowerCase().contains("chromosome")) {
                dnasequence = domain.make(life.gbol.domain.Chromosome.class, genome);
            } else if (seqType.toLowerCase().contains("plasmid")) {
                dnasequence = domain.make(life.gbol.domain.Plasmid.class, genome);
            } else {
                dnasequence = domain.make(life.gbol.domain.Contig.class, genome);
                logger.debug("Unknown genome format detected... Using contig instead");
            }

            // Setting the "rootURI"
            this.featureURI = dnasequence.getResource().getURI();
            /*
             * Creation of gene...
             */

            life.gbol.domain.Gene genefeature = domain.make(life.gbol.domain.Gene.class, dnasequence.getResource().getURI() + "/gene/" + begin + "-" + end);

            Region region = this.makeRegion(begin, end, isComplement, false, leftPartial, rightPartial, genefeature);

            // App.logger.info(isComplement);
            genefeature.setLocation(region);

            /*
             * Creation of prodigal provenance
             */

            life.gbol.domain.FeatureProvenance featureprov = domain.make(life.gbol.domain.FeatureProvenance.class, genefeature.getResource().getURI() + "/" + arguments.tool + "/" + arguments.toolversion);

            ProvenanceAnnotation provannot = domain.make(ProvenanceAnnotation.class, genefeature.getResource().getURI() + "/" + arguments.tool + "/" + arguments.toolversion + "/prov");
            Resource resource = provannot.getResource();
            resource.addLiteral(createProperty("http://gbol.life/0.1/gc_cont"), gffParametersHash.get("gc_cont"));
            resource.addLiteral(createProperty("http://gbol.life/0.1/rbs_motif"), gffParametersHash.get("rbs_motif"));
            resource.addLiteral(createProperty("http://gbol.life/0.1/rbs_spacer"), gffParametersHash.get("rbs_spacer"));
            resource.addLiteral(createProperty("http://gbol.life/0.1/start_type"), gffParametersHash.get("start_type"));
            resource.addLiteral(createProperty("http://gbol.life/0.1/score"), Float.valueOf(gffParametersHash.get("score")));
            resource.addLiteral(createProperty("http://gbol.life/0.1/conf"), Float.valueOf(gffParametersHash.get("conf")));
            resource.addLiteral(createProperty("http://gbol.life/0.1/cscore"), Float.valueOf(gffParametersHash.get("cscore")));
            resource.addLiteral(createProperty("http://gbol.life/0.1/rscore"), Float.valueOf(gffParametersHash.get("rscore")));
            resource.addLiteral(createProperty("http://gbol.life/0.1/sscore"), Float.valueOf(gffParametersHash.get("sscore")));
            resource.addLiteral(createProperty("http://gbol.life/0.1/tscore"), Float.valueOf(gffParametersHash.get("tscore")));
            resource.addLiteral(createProperty("http://gbol.life/0.1/uscore"), Float.valueOf(gffParametersHash.get("uscore")));

            Document reference = domain.make(Document.class, "http://doi.org");
            provannot.setReference(reference);
            featureprov.setAnnotation(provannot);
            domain.disableCheck();
            featureprov.setOrigin(arguments.annotResult);
            domain.enableCheck();
            genefeature.addProvenance(featureprov);

            if (locusLookupHash.containsKey(genefeature.getResource().getURI())) {
                String locus = locusLookupHash.get(genefeature.getResource().getURI());
                genefeature.setLocusTag(locus);
            } else {
                String locus = prefix + "_" + locusCount;
                genefeature.setLocusTag(locus);

                if (locusCount == 1) {
                    locusCount = 5;
                } else {
                    locusCount = locusCount + 5;
                }
            }


//            // Add locus tag if it does not exists...
//            Iterable<ResultLine> locusLookup = null;
//
//            if (arguments.endpoint != null) {
//                locusLookup = domain.getRDFSimpleCon().runQuery("getLocusTagFromGeneIRI.txt", false, genefeature.getResource().getURI());
//            } else if (arguments.input != null) {
//                try {
//                    locusLookup = domain.getRDFSimpleCon().runQuery(arguments.hdt, "getLocusTagFromGeneIRI.txt", genefeature.getResource().getURI());
//                } catch (NullPointerException e) {
//
//                }
//            }
//
//            if (locusLookup != null && locusLookup.iterator().hasNext()) {
//                String locus = locusLookup.iterator().next().getLitString("locus");
//                genefeature.setLocusTag(locus);
//            } else {
//                String locus = prefix + "_" + locusCount;
//                genefeature.setLocusTag(locus);
//
//                if (locusCount == 1) {
//                    locusCount = 5;
//                } else {
//                    locusCount = locusCount + 5;
//                }
//            }
            // Linking the gene to the dnaobject
            dnasequence.addFeature(genefeature);

            /*
             * Creating the mRNA object with single exon
             */

            life.gbol.domain.mRNA mrna = domain.make(life.gbol.domain.mRNA.class, dnasequence.getResource().getURI() + "/mrna/" + begin + "-" + end);

            life.gbol.domain.Exon exon = domain.make(life.gbol.domain.Exon.class, dnasequence.getResource().getURI() + "/exon/" + begin + "-" + end);

            // Setting positions
            region = this.makeRegion(begin, end, isComplement, false, leftPartial, rightPartial, exon);
            exon.setLocation(region);
            exon.addProvenance(featureprov);

            life.gbol.domain.ExonList exonlist = domain.make(life.gbol.domain.ExonList.class, dnasequence.getResource().getURI() + "/exonlist/" + begin + "-" + end);

            exonlist.addExon(exon);

            /*
             * Creating the CDS
             */

            life.gbol.domain.CDS cds = domain.make(life.gbol.domain.CDS.class, dnasequence.getResource().getURI() + "/cds/" + begin + "-" + end);

            // Changing CDS location to 1, end of gene
            region = this.makeRegion(1, geneSequence.length(), "cds", cds);
            cds.setLocation(region);
            proteinobject.setLength((long) proteinSequence.length());
            cds.setProtein(proteinobject);
            cds.addProvenance(featureprov);
            mrna.addFeature(cds);
            mrna.setSha384(geneChecksum);
            mrna.setExonList(exonlist);
            mrna.setSequence(geneSequence);
            mrna.setLength((long) geneSequence.length());
            // Adding the exon to the gene as well..
            genefeature.addExon(exon);

            genefeature.addTranscript(mrna);

            dnasequence.setTranslTable(arguments.codon);
        }

        // Saves to last batch to output file using compression
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(domain, arguments.output);

        // Remove temp files
        if (!arguments.debug) {
            new File(fasta + "_proteins.fasta").deleteOnExit();
            new File(fasta + "_nucleotide.fasta").deleteOnExit();
            new File(fasta + "_genecaller.gff").deleteOnExit();
            fasta.deleteOnExit();
        }
    }
}
