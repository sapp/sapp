package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Result {
    String sequence;
    String md5;
    List<Match> matches;
    List<Xref> xref;

    @JsonProperty("sequence")
    public String getSequence() {
        return this.sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    @JsonProperty("md5")
    public String getMd5() {
        return this.md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    @JsonProperty("matches")
    public List<Match> getMatches() {
        return this.matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    @JsonProperty("xref")
    public List<Xref> getXref() {
        return this.xref;
    }

    public void setXref(List<Xref> xref) {
        this.xref = xref;
    }
}
