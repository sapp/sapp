package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Location {
    int start;
    int end;
    int hmmStart;
    int hmmEnd;
    int hmmLength;
    String hmmBounds;
    double evalue;
    double score;
    int envelopeStart;
    int envelopeEnd;
    boolean postProcessed;
    List<LocationFragment> locationFragments;
    List<Site> sites;
    double pvalue;
    int motifNumber;
    String alignment;
    String level;
    String cigarAlignment;
    String sequenceFeature;

    String representative;

    @JsonProperty("representative")
    public String getRepresentative() {
        return this.representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    @JsonProperty("start")
    public int getStart() {
        return this.start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    @JsonProperty("end")
    public int getEnd() {
        return this.end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    @JsonProperty("hmmStart")
    public int getHmmStart() {
        return this.hmmStart;
    }

    public void setHmmStart(int hmmStart) {
        this.hmmStart = hmmStart;
    }

    @JsonProperty("hmmEnd")
    public int getHmmEnd() {
        return this.hmmEnd;
    }

    public void setHmmEnd(int hmmEnd) {
        this.hmmEnd = hmmEnd;
    }

    @JsonProperty("hmmLength")
    public int getHmmLength() {
        return this.hmmLength;
    }

    public void setHmmLength(int hmmLength) {
        this.hmmLength = hmmLength;
    }

    @JsonProperty("hmmBounds")
    public String getHmmBounds() {
        return this.hmmBounds;
    }

    public void setHmmBounds(String hmmBounds) {
        this.hmmBounds = hmmBounds;
    }

    @JsonProperty("evalue")
    public double getEvalue() {
        return this.evalue;
    }

    public void setEvalue(double evalue) {
        this.evalue = evalue;
    }

    @JsonProperty("score")
    public double getScore() {
        return this.score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @JsonProperty("envelopeStart")
    public int getEnvelopeStart() {
        return this.envelopeStart;
    }

    public void setEnvelopeStart(int envelopeStart) {
        this.envelopeStart = envelopeStart;
    }

    @JsonProperty("envelopeEnd")
    public int getEnvelopeEnd() {
        return this.envelopeEnd;
    }

    public void setEnvelopeEnd(int envelopeEnd) {
        this.envelopeEnd = envelopeEnd;
    }

    @JsonProperty("postProcessed")
    public boolean getPostProcessed() {
        return this.postProcessed;
    }

    public void setPostProcessed(boolean postProcessed) {
        this.postProcessed = postProcessed;
    }

    @JsonProperty("location-fragments")
    public List<LocationFragment> getLocationFragments() {
        return this.locationFragments;
    }

    public void setLocationFragments(List<LocationFragment> locationFragments) {
        this.locationFragments = locationFragments;
    }

    @JsonProperty("sites")
    public List<Site> getSites() {
        return this.sites;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }

    @JsonProperty("pvalue")
    public double getPvalue() {
        return this.pvalue;
    }

    public void setPvalue(double pvalue) {
        this.pvalue = pvalue;
    }

    @JsonProperty("motifNumber")
    public int getMotifNumber() {
        return this.motifNumber;
    }

    public void setMotifNumber(int motifNumber) {
        this.motifNumber = motifNumber;
    }

    @JsonProperty("alignment")
    public String getAlignment() {
        return this.alignment;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    @JsonProperty("level")
    public String getLevel() {
        return this.level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @JsonProperty("cigarAlignment")
    public String getCigarAlignment() {
        return this.cigarAlignment;
    }

    public void setCigarAlignment(String cigarAlignment) {
        this.cigarAlignment = cigarAlignment;
    }

    @JsonProperty("sequence-feature")
    public String getSequenceFeature() {
        return this.sequenceFeature;
    }

    public void setSequenceFeature(String sequenceFeature) {
        this.sequenceFeature = sequenceFeature;
    }

}
