package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Signature {
    String accession;
    String name;
    String description;
    SignatureLibraryRelease signatureLibraryRelease;
    Entry entry;

    @JsonProperty("accession")
    public String getAccession() {
        return this.accession;
    }

    public void setAccession(String accession) {
        this.accession = accession;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("signatureLibraryRelease")
    public SignatureLibraryRelease getSignatureLibraryRelease() {
        return this.signatureLibraryRelease;
    }

    public void setSignatureLibraryRelease(SignatureLibraryRelease signatureLibraryRelease) {
        this.signatureLibraryRelease = signatureLibraryRelease;
    }

    @JsonProperty("entry")
    public Entry getEntry() {
        return this.entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }
}
