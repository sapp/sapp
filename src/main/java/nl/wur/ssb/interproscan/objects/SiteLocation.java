package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SiteLocation {
    int start;
    int end;
    String residue;

    @JsonProperty("start")
    public int getStart() {
        return this.start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    @JsonProperty("end")
    public int getEnd() {
        return this.end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    @JsonProperty("residue")
    public String getResidue() {
        return this.residue;
    }

    public void setResidue(String residue) {
        this.residue = residue;
    }
}
