package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Root {
    String interproscanVersion;
    List<Result> results;

    @JsonProperty("interproscan-version")
    public String getInterproscanVersion() {
        return this.interproscanVersion;
    }

    public void setInterproscanVersion(String interproscanVersion) {
        this.interproscanVersion = interproscanVersion;
    }

    @JsonProperty("results")
    public List<Result> getResults() {
        return this.results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
