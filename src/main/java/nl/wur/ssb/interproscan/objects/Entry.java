package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Entry {
    String accession;
    String name;
    String description;
    String type;
    List<GoXRef> goXRefs;
    List<PathwayXRef> pathwayXRefs;

    @JsonProperty("accession")
    public String getAccession() {
        return this.accession;
    }

    public void setAccession(String accession) {
        this.accession = accession;
    }

    @JsonProperty("name")
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("type")
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("goXRefs")
    public List<GoXRef> getGoXRefs() {
        return this.goXRefs;
    }

    public void setGoXRefs(List<GoXRef> goXRefs) {
        this.goXRefs = goXRefs;
    }

    @JsonProperty("pathwayXRefs")
    public List<PathwayXRef> getPathwayXRefs() {
        return this.pathwayXRefs;
    }

    public void setPathwayXRefs(List<PathwayXRef> pathwayXRefs) {
        this.pathwayXRefs = pathwayXRefs;
    }
}
