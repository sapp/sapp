package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Match {
    Signature signature;
    List<Location> locations;
    double evalue;
    double score;
    String modelAc;
    String familyName;
    String graphscan;
    Object scope;

    @JsonProperty("signature")
    public Signature getSignature() {
        return this.signature;
    }

    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    @JsonProperty("locations")
    public List<Location> getLocations() {
        return this.locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    @JsonProperty("evalue")
    public double getEvalue() {
        return this.evalue;
    }

    public void setEvalue(double evalue) {
        this.evalue = evalue;
    }

    @JsonProperty("score")
    public double getScore() {
        return this.score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @JsonProperty("model-ac")
    public String getModelAc() {
        return this.modelAc;
    }

    public void setModelAc(String modelAc) {
        this.modelAc = modelAc;
    }

    @JsonProperty("familyName")
    public String getFamilyName() {
        return this.familyName;
    }

    public void setFamilyName(String familyName) {
        this.modelAc = familyName;
    }

    @JsonProperty("graphscan")
    public String getGraphscan() {
        return this.graphscan;
    }

    public void setGraphscan(String graphscan) {
        this.graphscan = graphscan;
    }

    @JsonProperty("scope")
    public Object getScope() {
        return this.scope;
    }

    public void setScope(Object scope) {
        this.scope = scope;
    }

}
