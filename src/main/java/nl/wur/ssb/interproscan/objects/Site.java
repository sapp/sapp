package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Site {
    String description;
    int numLocations;
    List<SiteLocation> siteLocations;
    String label;
    String group;
    int hmmStart;
    int hmmEnd;

    @JsonProperty("description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("numLocations")
    public int getNumLocations() {
        return this.numLocations;
    }

    public void setNumLocations(int numLocations) {
        this.numLocations = numLocations;
    }

    @JsonProperty("siteLocations")
    public List<SiteLocation> getSiteLocations() {
        return this.siteLocations;
    }

    public void setSiteLocations(List<SiteLocation> siteLocations) {
        this.siteLocations = siteLocations;
    }

    @JsonProperty("label")
    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @JsonProperty("group")
    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @JsonProperty("hmmStart")
    public int getHmmStart() {
        return this.hmmStart;
    }

    public void setHmmStart(int hmmStart) {
        this.hmmStart = hmmStart;
    }

    @JsonProperty("hmmEnd")
    public int getHmmEnd() {
        return this.hmmEnd;
    }

    public void setHmmEnd(int hmmEnd) {
        this.hmmEnd = hmmEnd;
    }

}



