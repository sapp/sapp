package nl.wur.ssb.interproscan.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LocationFragment {
    int start;
    int end;
    String dcStatus;

    @JsonProperty("start")
    public int getStart() {
        return this.start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    @JsonProperty("end")
    public int getEnd() {
        return this.end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    @JsonProperty("dc-status")
    public String getDcStatus() {
        return this.dcStatus;
    }

    public void setDcStatus(String dcStatus) {
        this.dcStatus = dcStatus;
    }
}
