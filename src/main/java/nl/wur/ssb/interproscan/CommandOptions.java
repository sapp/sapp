package nl.wur.ssb.interproscan;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.time.LocalDateTime;

@Parameters(commandDescription = "Available options: ")
public class CommandOptions extends CommandOptionsGeneric {
    public final String tool = "InterProScan";
    public final String repository = "https://gitlab.com/sapp/annotation/interproscan";
    private final Logger logger = LogManager.getLogger(CommandOptions.class);
    public String toolversion;
    public String commandLine;
    public AnnotationResult annotResult;

    @Parameter(names = {"-interpro"}, description = "Running in InterProScan mode")
    boolean interpro;

    @Parameter(names = {"-path"}, description = "Path to the interproscan program folder")
    File path = new File("./interpro");

    @Parameter(names = {"-include-reactome"}, description = "Due to its size reactome identifiers are excluded by default")
    boolean reactome;

    @Parameter(names = {"-r", "-resultFile"}, description = "Interproscan (json/tsv) result file")
    File resultFile;

    @Parameter(names = {"-dp", "-disableprecalc"}, description = "Disables precalculation lookup")
    boolean disable;

    @Parameter(names = {"-f", "-force"}, description = "Forces to overwrite json output file and recompute")
    boolean overwrite;

    @Parameter(names = {"-a", "-applications"}, description = "Comma separated list of applications that can be used")
    String app;

    @Parameter(names = {"-T", "-tempdir"}, description = "Temporary folder location")
    String tmpdir;

    @Parameter(names = {"-cpu"}, description = "Number of cpu cores interpro needs to run on (minimum of 5 needed)")
    int CPU = 5;

    @Parameter(names = "-starttime", description = "Start time of code", hidden = true)
    LocalDateTime starttime = LocalDateTime.now();

    public CommandOptions(String[] args) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            if (this.help || args.length == 0)
                throw new ParameterException("");

            // Set tool version based on interproscan folder name
            this.toolversion = this.path.getName();

            if (this.input == null && this.endpoint == null)
                throw new ParameterException("Parameter -input or -endpoint is required");


            String[] files = new String[0];

            if (this.input != null) {
                logger.info("Loading data from file");
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
            }

            if (this.endpoint != null) {
                this.domain = new Domain(this.endpoint.toString());
                if (this.username != null) {
                    this.domain.getRDFSimpleCon().setAuthen(this.username, this.password);
                }
            }

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
