package nl.wur.ssb.interproscan;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.SappGeneric.Xrefs;
import nl.wur.ssb.interproscan.objects.Location;
import nl.wur.ssb.interproscan.objects.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.*;
import java.util.zip.GZIPInputStream;

import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

public class InterProScan extends SequenceBuilder {

    final UUID xrefprovID = UUID.randomUUID();
    private final XRefProvenance xrefProv;
    private final CommandOptions arguments;
    private final File name;
    private final Logger logger = LogManager.getLogger(InterProScan.class);
    private int reactome_count = 0;

    public InterProScan(String[] args) throws Exception {
        super(null, "http://gbol.life/0.1/");

        arguments = new CommandOptions(args);

        this.domain = arguments.domain;

        if (arguments.input != null)
            name = arguments.input;
        else
            name = new File(new File(arguments.endpoint.toString()).getName());

        this.rootIRI = "http://gbol.life/0.1/" + xrefprovID + "/";
        xrefProv = arguments.domain.make(XRefProvenance.class, rootIRI + "XRefProv");

        // xrefProv.setOrigin(arguments.annotResult);

        if (arguments.debug)
            logger.atLevel(Level.DEBUG);

        // If no sequences are found the analysis is obviously skipped
        // Everything is now from loadRDF as we have set a limit to 5.000 per
        // analysis run...
        if (arguments.resultFile == null) {
            List<File> proteinFiles = Generic.getProteinsFromRDF(arguments.domain, name.getName());
            execute(proteinFiles);
        } else {
            // Process result file
            // Check if gzip
            if (arguments.resultFile.getName().endsWith(".json.gz")) {
                logger.info("Processing JSON GZIP file");
                jsonParser(arguments.resultFile);
            } else if (arguments.resultFile.getName().endsWith(".json")) {
                logger.info("Processing JSON file");
                jsonParser(arguments.resultFile);
            } else if (arguments.resultFile.getName().endsWith(".tsv")) {
                logger.info("Processing TSV file");
                tsvParser(arguments.resultFile);
            } else {
                throw new FileNotFoundException("File not found with tsv or json extension");
            }
        }

        logger.info("Results are saved in: " + arguments.output.getAbsolutePath());
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(arguments.domain, arguments.output);

        arguments.domain.close();
    }

    private void execute(List<File> proteinFiles) throws Exception {
        for (File proteinFile : proteinFiles) {
            if (proteinFile.length() == 0) {
                throw new IllegalArgumentException("Your input file " + proteinFile + " seems to have no proteins. Check your input .hdt file for proteins and add them in case of absence.");
            }
            interproscan(proteinFile);
            if (!new File(proteinFile + "_interpro.json.nt.gz").exists()) {
                jsonParser(proteinFile);
            }
        }
    }

    private void interproscan(File proteinFile) throws Exception {
        File jsonFile = new File(proteinFile + "_interpro.json");
        if (jsonFile.exists() && !arguments.overwrite) {
            logger.info("JSON file already exists, skipping computing for " + jsonFile);
            return;
        }

        String INTERPRO = arguments.path + "/interproscan.sh";

        if (arguments.tmpdir != null) {
            File tmpdir = new File(arguments.tmpdir);
            if (!tmpdir.exists()) {
                tmpdir.mkdir();
            }
            INTERPRO = INTERPRO + " --tempdir " + arguments.tmpdir + "/";
        }

        // Minimum of 5 threads needed
        if (arguments.CPU < 5) {
            INTERPRO = INTERPRO + " -cpu " + 5;
        } else {
            INTERPRO = INTERPRO + " -cpu " + arguments.CPU;
        }
        String command = INTERPRO;

        command += " --input " + proteinFile;
        command += " --goterms ";
        command += " --pathways ";
        command += " --iprlookup ";
        command += " --formats json ";
        command += " --seqtype p ";
        command += "--outfile " + proteinFile + "_interpro.json";

        if (arguments.app != null) {
            command += " --applications " + arguments.app;
        }
        if (arguments.disable) {
            command += " --disable-precalc ";
        }

        if (Util.getOs() == "linux" && !new File(proteinFile + "_interpro.json").exists()) {
            logger.info(command);
            ExecCommand result = new ExecCommand(command);
            if (result.getExit() > 0) {
                logger.error(result.getError());
                logger.info(result.getOutput());
                throw new Exception("Execution failed");
            }
        } else {
            logger.error("Other operating system beside linux not supported by interproscan. For more information: http://www.ebi.ac.uk/interpro/");
        }
    }

    private void tsvParser(File resultFile) throws Exception {

        // Provenance section start
        ImportProv origin = new ImportProv(domain, List.of(arguments.input.getAbsolutePath()), arguments.output, arguments.commandLine, arguments.tool, arguments.toolversion, arguments.repository, null, arguments.starttime, LocalDateTime.now());
        arguments.annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
        origin.linkEntity(arguments.annotResult);
        // Provenance section end

        // Generic xref provenance
        XRefProvenance xRefProvenance = domain.make(XRefProvenance.class, "http://gbol.life/0.1/interproscan/" + arguments.toolversion + "/prov/xref");
        xRefProvenance.setOrigin(arguments.annotResult);


        // Obtain locus tag mapping (first column)
        HashMap<String, String> locusLookup = Generic.getLocusTags(arguments.domain);

        int count = 0;
        Scanner scanner = new Scanner(resultFile);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] lineSplit = line.split("\t", -1);

            String locus_tag = lineSplit[0];
            String md5 = lineSplit[1];
            String length = lineSplit[2];
            String analysis = lineSplit[3];
            String signature_accession = lineSplit[4];
            String signature_description = lineSplit[5];
            String start = lineSplit[6];
            String stop = lineSplit[7];
            String score = lineSplit[8];
            String status = lineSplit[9];
            String date = lineSplit[10];
            String interpro_accession = lineSplit[11];
            String interpro_description = "";
            String gos = "";
            String pathways = "";
            if (interpro_accession.length() > 1) {// meaning there is a description we can check for go and pathway
                interpro_description = lineSplit[12];
                gos = lineSplit[13].replaceAll("-", "");
                pathways = lineSplit[14];
            }
            count = count + 1;
            if (arguments.debug && count % 1000 == 0) {
                logger.debug("Processed " + count + " entries with " + domain.getRDFSimpleCon().getModel().size() + " statements");
            }
            if (!scanner.hasNextLine()) {
                logger.info("Processed " + count + " entries with " + domain.getRDFSimpleCon().getModel().size() + " statements");
            }
            Protein protein = domain.make(Protein.class, locusLookup.get(locus_tag));
            ProteinDomain proteinDomain = domain.make(ProteinDomain.class, locusLookup.get(locus_tag) + "/" + signature_accession + "/" + start + "-" + stop);
            // Signature as xref
            XRef xref = Xrefs.create(XRef.class, domain, xRefProvenance, analysis, "0.0", signature_accession, null);
            proteinDomain.addXref(xref);
            proteinDomain.setSignature(xref.getResource().getURI());

            // Signature information
            if (signature_accession.length() > 0) {
                proteinDomain.addAccession(signature_accession);
            }

//            if (match.getSignature().getName() != null) {
//                proteinDomain.setStandardName(match.getSignature().getName());
//            }
            if (signature_description.length() > 0) {
                proteinDomain.setSignatureDesc(signature_description);
            }

            // Domain location
            proteinDomain.setLocation(this.makeRegion(Long.parseLong(start), Long.parseLong(stop), true, false, false));

            // Set provenance
            FeatureProvenance featureProvenance = domain.make(FeatureProvenance.class, proteinDomain.getResource().getURI() + "/" + analysis + "/" + "0.0");
            featureProvenance.setOrigin(arguments.annotResult);

            // Add score like information
            ProvenanceApplication provenanceApplication = domain.make(ProvenanceApplication.class, featureProvenance.getResource().getURI() + "/prov");
//            provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/evalue"), evalue);
            provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/score"), score);
//            provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/pvalue"), location.getPvalue());

            // IPR Xrefs from entry
            if (interpro_accession.length() > 0) {
                xref = Xrefs.create(XRef.class, domain, xRefProvenance, "interproscan", arguments.toolversion, interpro_accession, null);
                if (interpro_description.length() > 0)
                    proteinDomain.setFunction(interpro_description);
                proteinDomain.addXref(xref);
            }

            if (gos.length() > 0) {
                for (String go : gos.split("\\|")) {
                    XRef xRef = Xrefs.create(domain, xRefProvenance, "go", go);
                    proteinDomain.addXref(xRef);
                }
                if (pathways.length() > 0) {
                    for (String pathway : pathways.split("\\|")) {
                        if (pathway.length() == 1) continue;
                        if (pathway.toLowerCase().startsWith("reactome")) {
                            reactome_count++;
                            if (!arguments.reactome)
                                continue;
                        }
                        String[] pathwaySplit = pathway.split(":");

                        if (pathwaySplit.length != 2)
                            System.err.println("CHECK: " + pathway);
                        String database = pathwaySplit[0].strip();
                        String identifier = pathwaySplit[1].strip();
                        xref = Xrefs.create(domain, xRefProvenance, database, identifier);
                        proteinDomain.addXref(xref);

                    }
                }

                // Couple back to parent object
                featureProvenance.setAnnotation(provenanceApplication);
                // Assign provenance
                proteinDomain.addProvenance(featureProvenance);
                // proteinDomain.addXref(this.createXRef(""));
                // Assign domain to protein
                protein.addFeature(proteinDomain);
            }


            //            if (result.getXref().size() == 1) {
//                // Create protein object
//                Protein protein = domain.make(Protein.class, result.getXref().get(0).getId());
//                // iterate through all the matches for a given protein
//                for (Match match : result.getMatches()) {
//                    // Each match is a new ProteinFeature
//                    for (Location location : match.getLocations()) {


//                        // Create domain feature
//                        ProteinDomain proteinDomain = domain.make(ProteinDomain.class, protein.getResource().getURI() + "/" + match.getSignature().getAccession() + "/" + location.getStart() + "-" + location.getEnd());
//                        // Signature as xref
//                        XRef xref = Xrefs.create(XRef.class, domain, xRefProvenance, match.getSignature().getSignatureLibraryRelease().getLibrary(), match.getSignature().getSignatureLibraryRelease().getVersion(), match.getSignature().getAccession(), null);
//                        proteinDomain.addXref(xref);
//                        proteinDomain.setSignature(xref.getResource().getURI());
//
//                        // Signature information
//                        if (match.getSignature().getAccession() != null) {
//                            proteinDomain.addAccession(match.getSignature().getAccession());
//                        }
//                        if (match.getSignature().getName() != null) {
//                            proteinDomain.setStandardName(match.getSignature().getName());
//                        }
//                        if (match.getSignature().getDescription() != null) {
//                            proteinDomain.setSignatureDesc(match.getSignature().getDescription());
//                        }
//
//                        // Domain location
//                        proteinDomain.setLocation(this.makeRegion(location.getStart(), location.getEnd(), true, false, false));
//
//                        // Set provenance
//                        FeatureProvenance featureProvenance = domain.make(FeatureProvenance.class, proteinDomain.getResource().getURI() + "/" + match.getSignature().getSignatureLibraryRelease().getLibrary() + "/" + match.getSignature().getSignatureLibraryRelease().getVersion());
//                        featureProvenance.setOrigin(arguments.annotResult);
//
//                        // Add score like information
//                        ProvenanceApplication provenanceApplication = domain.make(ProvenanceApplication.class, featureProvenance.getResource().getURI() + "/prov");
//                        provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/evalue"), match.getEvalue());
//                        provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/score"), match.getScore());
//                        provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/pvalue"), location.getPvalue());
//
//                        // IPR Xrefs from entry
//                        if (match.getSignature().getEntry() != null) {
//                            Entry entry = match.getSignature().getEntry();
//                            // IPR number as xref
//                            xref = Xrefs.create(XRef.class, domain, xRefProvenance, "interproscan", arguments.toolversion, entry.getAccession(), null);
//                            if (entry.getDescription() != null)
//                                proteinDomain.setFunction(entry.getDescription());
//                            if (entry.getName() != null) {
//                                proteinDomain.addSecondarySignature(entry.getName());
//                            }
//                            proteinDomain.addXref(xref);
//
//                            // System.err.println(entry.getGoXRefs().size());
//                            for (GoXRef goXRef : entry.getGoXRefs()) {
//                                XRef xRef = Xrefs.create(domain, xRefProvenance, "go", goXRef.getId());
//                                proteinDomain.addXref(xRef);
//                            }
//                            // System.err.println(entry.getPathwayXRefs().size());
//                            for (PathwayXRef pathwayXRef : entry.getPathwayXRefs()) {
//                                if (pathwayXRef.getDatabaseName().toLowerCase(Locale.ROOT).contains("reactome"))
//                                    continue;
//                                xref = Xrefs.create(domain, xRefProvenance, pathwayXRef.getDatabaseName(), pathwayXRef.getId());
//                                proteinDomain.addXref(xref);
//                            }
//                        }
//
//                        // Couple back to parent object
//                        featureProvenance.setAnnotation(provenanceApplication);
//                        // Assign provenance
//                        proteinDomain.addProvenance(featureProvenance);
//                        // proteinDomain.addXref(this.createXRef(""));
//                        // Assign domain to protein
//                        protein.addFeature(proteinDomain);
//                    }
//                }
//            }
        }
    }

    private void jsonParser(File resultFile) throws Exception {
        String content;
        if (!resultFile.exists()) {
            File file = new File(resultFile + "_interpro.json");
            content = Files.readString(file.toPath());
        } else {
            // Check if gzip compression
            if (resultFile.getName().endsWith(".gz")) {
                GZIPInputStream gzip = new GZIPInputStream(Files.newInputStream(resultFile.toPath()));
                content = new String(gzip.readAllBytes());
            } else {
                content = Files.readString(resultFile.toPath());
            }
        }


        // JSON string mapper to object with several configuration options
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

        // https://json2csharp.com/json-to-pojo to generate the objects
        Root root = objectMapper.readValue(content, Root.class);

        logger.info("Successfully mapped " + root.getResults().size() + " items");

        // Provenance section start
        ImportProv origin = new ImportProv(domain, List.of(arguments.input.getAbsolutePath()), arguments.output, arguments.commandLine, arguments.tool, arguments.toolversion, arguments.repository, null, arguments.starttime, LocalDateTime.now());
        arguments.annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
        origin.linkEntity(arguments.annotResult);
        // Provenance section end

        // Generic xref provenance
        XRefProvenance xRefProvenance = domain.make(XRefProvenance.class, "http://gbol.life/0.1/interproscan/" + arguments.toolversion + "/prov/xref");
        xRefProvenance.setOrigin(arguments.annotResult);


        int count = 0;
        for (Result result : root.getResults()) {
            count = count + 1;
            if (arguments.debug && count % 10 == 0)
                logger.debug("Processed " + count + " entries with " + domain.getRDFSimpleCon().getModel().size() + " statements");

            if (result.getXref().size() == 1) {
                // Create protein object
                String id = result.getXref().get(0).getId();
                Protein protein;
                if (!id.contains("http")) {
                    // Calculate checksum from sequence
                    String proteinChecksum = Generic.checksum(result.getSequence(), "SHA-384");
                    protein = domain.make(Protein.class, "http://gbol.life/0.1/protein/" + proteinChecksum);
                } else {
                    protein = domain.make(Protein.class, result.getXref().get(0).getId());
                }
                // iterate through all the matches for a given protein
                for (Match match : result.getMatches()) {
                    // Each match is a new ProteinFeature
                    for (Location location : match.getLocations()) {
                        // Create domain feature
                        ProteinDomain proteinDomain = domain.make(ProteinDomain.class, protein.getResource().getURI() + "/" + match.getSignature().getAccession() + "/" + location.getStart() + "-" + location.getEnd());
                        // Signature as xref
                        XRef xref = Xrefs.create(XRef.class, domain, xRefProvenance, match.getSignature().getSignatureLibraryRelease().getLibrary(), match.getSignature().getSignatureLibraryRelease().getVersion(), match.getSignature().getAccession(), null);
                        proteinDomain.addXref(xref);
                        proteinDomain.setSignature(xref.getResource().getURI());

                        // Signature information
                        if (match.getSignature().getAccession() != null) {
                            proteinDomain.addAccession(match.getSignature().getAccession());
                        }
                        if (match.getSignature().getName() != null) {
                            proteinDomain.setStandardName(match.getSignature().getName());
                        }
                        if (match.getSignature().getDescription() != null) {
                            proteinDomain.setSignatureDesc(match.getSignature().getDescription());
                        }

                        // Domain location
                        proteinDomain.setLocation(this.makeRegion(location.getStart(), location.getEnd(), true, false, false));

                        // Set provenance
                        FeatureProvenance featureProvenance = domain.make(FeatureProvenance.class, proteinDomain.getResource().getURI() + "/" + match.getSignature().getSignatureLibraryRelease().getLibrary() + "/" + match.getSignature().getSignatureLibraryRelease().getVersion());
                        featureProvenance.setOrigin(arguments.annotResult);

                        // Add score like information
                        ProvenanceApplication provenanceApplication = domain.make(ProvenanceApplication.class, featureProvenance.getResource().getURI() + "/prov");
                        provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/evalue"), match.getEvalue());
                        provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/score"), match.getScore());
                        provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/pvalue"), location.getPvalue());

                        // IPR Xrefs from entry
                        if (match.getSignature().getEntry() != null) {
                            Entry entry = match.getSignature().getEntry();
                            // IPR number as xref
                            xref = Xrefs.create(XRef.class, domain, xRefProvenance, "interproscan", arguments.toolversion, entry.getAccession(), null);
                            if (entry.getDescription() != null)
                                proteinDomain.setFunction(entry.getDescription());
                            if (entry.getName() != null) {
                                proteinDomain.addSecondarySignature(entry.getName());
                            }
                            proteinDomain.addXref(xref);

                            // System.err.println(entry.getGoXRefs().size());
                            for (GoXRef goXRef : entry.getGoXRefs()) {
                                XRef xRef = Xrefs.create(domain, xRefProvenance, "go", goXRef.getId());
                                proteinDomain.addXref(xRef);
                            }
                            // System.err.println(entry.getPathwayXRefs().size());
                            for (PathwayXRef pathwayXRef : entry.getPathwayXRefs()) {
                                if (pathwayXRef.getDatabaseName().toLowerCase(Locale.ROOT).contains("reactome"))
                                    continue;
                                xref = Xrefs.create(domain, xRefProvenance, pathwayXRef.getDatabaseName(), pathwayXRef.getId());
                                proteinDomain.addXref(xref);
                            }
                        }

                        // Couple back to parent object
                        featureProvenance.setAnnotation(provenanceApplication);
                        // Assign provenance
                        proteinDomain.addProvenance(featureProvenance);
                        // proteinDomain.addXref(this.createXRef(""));
                        // Assign domain to protein
                        protein.addFeature(proteinDomain);
                    }
                }
            }
        }
    }
}
