package nl.wur.ssb.infernal;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.time.LocalDateTime;

@Parameters(commandDescription = "Available options: ")
public class CommandOptions extends CommandOptionsGeneric {

    public final String repository = "https://github.com/EddyRivasLab/infernal";
    // public String description = "INFERNAL prediction module from SAPP";
    public final LocalDateTime starttime = LocalDateTime.now();
    final String tool = "INFERNAL";

    @Parameter(names = {"-infernal"}, description = "Path to infernal binary")
    public boolean infernal = false;

    @Parameter(names = {"-cm"}, description = "Path to Rfam.cm file that is already indexed with cmpress", required = true)
    public File cm;

    @Parameter(names = {"-clanin"}, description = "Path to Rfam.clanin file", required = true)
    public File clanin;

    @Parameter(names = {"-cmscan"}, description = "Path to cmscan", required = true)
    public String cmscan;

    @Parameter(names = {"-cpu"}, description = "Number of cpu threads to use")
    public int cpu = 2;

    public String annotResultIRI;
    public String commandLine;
    public String toolversion;
    public int locus = 1;
    // final String sappversion =  nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptions.class)[0];
    // final String sappcommit = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptions.class)[1];
    AnnotationResult annotResult;

    CommandOptions(String[] args) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            if (this.help) {
                throw new ParameterException("");
            }

            // TODO make this default in all modules
            // Setting the AnnotationProvenance
            String[] files = new String[0];

            if (this.input != null) {
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
            } else if (this.endpoint != null) {
                this.domain = new Domain(this.endpoint.toString());
                if (this.username != null) {
                    this.domain.getRDFSimpleCon().setAuthen(this.username, this.password);
                }
            } else {
                throw new ParameterException("-input is missing");
            }


            if (this.input == null && this.endpoint == null) {
                throw new ParameterException("Required either -input or -endpoint");
            }

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help || args.length == 0) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.err.println("Warning, INFERNAL (cmscan) needs to be installed on the system by yourself check https://github.com/EddyRivasLab/infernal for more information");
            System.err.println("Warning, Rfam database needs to be available on the system by yourself check https://rfam.org for more information");
            System.err.println("Rfam.clanin: https://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.clanin");
            System.err.println("Rfam.cm: https://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.cm.gz");
            System.exit(exitCode);
        }
    }
}

