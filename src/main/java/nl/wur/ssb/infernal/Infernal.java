package nl.wur.ssb.infernal;

import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.FASTA;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.SappGeneric.Xrefs;
import org.apache.jena.rdf.model.Resource;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.ChromosomeSequence;
import org.biojava.nbio.core.sequence.Strand;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;

import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

public class Infernal extends SequenceBuilder {

    public final Logger logger = LogManager.getLogger(Infernal.class);
    private final CommandOptions arguments;
    private final HashMap<String, String> fastaLookup = new HashMap<>();
    private HashMap<String, String> seqTypes = new HashMap<>();

    public Infernal(String[] args) throws Exception {
        super(null, "");

        arguments = new CommandOptions(args);

        this.domain = arguments.domain;

        if (arguments.debug)
            logger.atLevel(Level.DEBUG);

        // Generate fasta files
        List<File> files = FASTA.create(arguments, "genome");

        infernalCaller(arguments, files);
        infernalParser(files);

        nl.wur.ssb.SappGeneric.InputOutput.Output.save(domain, arguments.output);
    }

    private void infernalParser(List<File> files) throws Exception {
        for (File fastaFile : files) {
            Scanner tabular = new Scanner(new FileReader(fastaFile + ".tblout"));
            while (tabular.hasNextLine()) {
                String line = tabular.nextLine();
                if (line.contains("Version:")) {
                    arguments.toolversion = line.split(" +")[2];
                    ArrayList<String> inputList = new ArrayList<>();
                    inputList.add(arguments.input.getAbsolutePath());
                    ImportProv origin = new ImportProv(domain, inputList, arguments.output, arguments.commandLine, arguments.tool, arguments.toolversion, arguments.repository, null, arguments.starttime, LocalDateTime.now());
                    arguments.annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
                    origin.linkEntity(arguments.annotResult);
                }
            }

            tabular = new Scanner(new FileReader(fastaFile + ".tblout"));

            while (tabular.hasNext()) {
                String line = tabular.nextLine();

                if (!line.startsWith("#")) {
                    String[] lineSplit = line.split(" +", 27);
                    String idx = lineSplit[0];
                    String target_name = lineSplit[1];
                    String accession = lineSplit[2];
                    String query_name = lineSplit[3];
                    // String accession = lineSplit[4];
                    String clan_name = lineSplit[5];
                    String mdl = lineSplit[6];
                    long mdl_from = Long.parseLong(lineSplit[7]);
                    long mdl_to = Long.parseLong(lineSplit[8]);
                    Long begin = Long.parseLong(lineSplit[9]);
                    Long end = Long.parseLong(lineSplit[10]);
                    String strandString = lineSplit[11].strip();
                    boolean strand;
                    if (strandString.contains("+")) {
                        strand = true;
                    } else if (strandString.contains("-")) {
                        strand = false;
                    } else {
                        throw new Exception("Strand type not recognised " + strandString);
                    }
                    String truncString = lineSplit[12];

                    boolean trunc;
                    trunc = !truncString.contains("no");

                    int pass = Integer.parseInt(lineSplit[13]);
                    float gc = Float.parseFloat(lineSplit[14]);
                    float bias = Float.parseFloat(lineSplit[15]);
                    double score = Double.parseDouble(lineSplit[16]);
                    double evalue = Double.parseDouble(lineSplit[17]);
                    String inc = lineSplit[18];
                    String olp = lineSplit[19];
                    String anyidx = lineSplit[20];
                    String afrct1 = lineSplit[21];
                    String afrct2 = lineSplit[22];
                    String winidx = lineSplit[23];
                    String wfrct1 = lineSplit[24];
                    String wfrct2 = lineSplit[25];
                    String description_of_target = lineSplit[26];

                    // Make entry
                    NASequence dnaobject = getGenomeInformation(domain, query_name);

                    // Setting the "rootURI" for the location iri's
                    this.featureURI = dnaobject.getResource().getURI();

                    Gene gene = domain.make(Gene.class, dnaobject.getResource().getURI() + "/gene/" + begin + "_" + end);

                    // CRISPR detection
                    if (description_of_target.contains("CRISPR RNA direct repeat element")) {
                        RepeatRegion repeat = domain.make(RepeatRegion.class, dnaobject.getResource().getURI() + "/crisprrepeat/" + begin + "-" + end);
                        Region region = this.makeRegion(begin, end, strand, false, false, false, gene);
                        repeat.setLocation(region);
                        repeat.setRptType(RepeatType.CRISPRRepeat);
                        repeat.setFunction(description_of_target);

                        // Prov
                        FeatureProvenance repeatProv = this.domain.make(FeatureProvenance.class, repeat.getResource().getURI() + "/" + arguments.tool + "/" + arguments.toolversion + "/featureProv");
                        repeatProv.setOrigin(arguments.annotResult);
                        repeat.addProvenance(repeatProv);
                        addLiterals(repeatProv.getResource(), mdl, mdl_from, mdl_to, trunc, pass, gc, bias, score, evalue, inc, olp, anyidx, afrct1, afrct2, winidx, wfrct1, wfrct2);

                        XRefProvenance xrefprov = domain.make(XRefProvenance.class, repeat.getResource().getURI() + "/xrefprov");
                        xrefprov.setOrigin(arguments.annotResult);

                        // RFAM accession xref
                        if (!accession.contains("-")) {
                            XRef xRef = Xrefs.create(XRef.class, domain, xrefprov, "rfam", arguments.toolversion, accession, null);
                            repeat.addXref(xRef);
                        }
                        // CLAN accession xref
                        if (!clan_name.contains("-")) {
                            XRef xRef = Xrefs.create(XRef.class, domain, xrefprov, "clan", arguments.toolversion, clan_name, null);
                            repeat.addXref(xRef);
                        }
                    } else {
                        Region region = this.makeRegion(begin, end, strand, false, false, false, gene);
                        gene.setLocation(region);

                        // Creation of exon and exonlist...
                        Exon exon = this.domain.make(Exon.class, dnaobject.getResource().getURI() + "/exon/" + begin + "_" + end);
                        exon.setLocation(region);

                        Transcript entry;
                        if (description_of_target.toLowerCase(Locale.ROOT).matches("trna")) {
                            tRNA trna = domain.make(tRNA.class, dnaobject.getResource().getURI() + "/trna/" + begin + "_" + end);
                            trna.setProduct(description_of_target);
                            entry = trna;
                        } else if (description_of_target.contains("ribosomal RNA")) {
                            rRNA rrna = domain.make(rRNA.class, dnaobject.getResource().getURI() + "/rrna/" + begin + "_" + end);
                            rrna.setProduct(description_of_target);
                            entry = rrna;
                        } else {
                            MiscRna miscRNA = domain.make(MiscRna.class, dnaobject.getResource().getURI() + "/miscrna/" + begin + "_" + end);
                            miscRNA.setProduct(description_of_target);
                            entry = miscRNA;
                        }

                        this.setSequence(entry, getSequence(query_name, begin, end, strand));

                        XRefProvenance xrefprov = domain.make(XRefProvenance.class, entry.getResource().getURI() + "/xrefprov");
                        xrefprov.setOrigin(arguments.annotResult);

                        // RFAM accession xref
                        if (!accession.contains("-")) {
                            XRef xRef = Xrefs.create(XRef.class, domain, xrefprov, "rfam", arguments.toolversion, accession, null);
                            entry.addXref(xRef);
                        }
                        // CLAN accession xref
                        if (!clan_name.contains("-")) {
                            XRef xRef = Xrefs.create(XRef.class, domain, xrefprov, "clan", arguments.toolversion, clan_name, null);
                            entry.addXref(xRef);
                        } else {
                            // Add provenance without ontology
                            FeatureProvenance geneProv = domain.make(FeatureProvenance.class, entry.getResource().getURI() + "/prov");
                            geneProv.setOrigin(arguments.annotResult);
                            gene.addProvenance(geneProv);
                            gene.addTranscript(entry);

                            Resource resource = geneProv.getResource();
                            addLiterals(resource, mdl, mdl_from, mdl_to, trunc, pass, gc, bias, score, evalue, inc, olp, anyidx, afrct1, afrct2, winidx, wfrct1, wfrct2);

                            // Locus tag check
                            try {
                                gene.getLocusTag();
                            } catch (RuntimeException e) {
                                gene.setLocusTag("Test");
                            }
                            dnaobject.addFeature(gene);
                        }
                    }
                }
            }
        }
    }

    private void addLiterals(Resource resource, String mdl, long mdl_from, long mdl_to, boolean trunc, int pass, float gc, float bias, double score, double evalue, String inc, String olp, String anyidx, String afrct1, String afrct2, String winidx, String wfrct1, String wfrct2) {

        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/mdl"), mdl);

        // Subregion
        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/mdl_from"), mdl_from);
        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/mdl_to"), mdl_to);

        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/trunc"), trunc);
        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/pass"), pass);
        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/gc"), gc);
        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/bias"), bias);
        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/score"), score);
        resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/evalue"), evalue);
        if (inc.contains("-"))
            resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/inc"), inc);
        if (!olp.contains("-"))
            resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/olp"), olp);
        if (!anyidx.contains("-"))
            resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/anyidx"), anyidx);
        if (!afrct1.contains("-"))
            resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/afrct1"), afrct1);
        if (!afrct2.contains("-"))
            resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/afrct2"), afrct2);
        if (!winidx.contains("-"))
            resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/winidx"), winidx);
        if (!wfrct1.contains("-"))
            resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/wfrct1"), wfrct1);
        if (!wfrct2.contains("-"))
            resource.addLiteral(createProperty("http://gbol.life/0.1/provenance/wfrct2"), wfrct2);

    }

    private String getSequence(String query, Long begin, Long end, boolean strand) throws CompoundNotFoundException {
        if (!fastaLookup.containsKey(query)) {
            try {
                ResultLine resultLine = domain.getRDFSimpleCon().runQuerySingleRes("getSequence.txt", true, query);
                String sequence = resultLine.getLitString("sequence");
                fastaLookup.put(query, sequence);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ChromosomeSequence seq = new ChromosomeSequence(fastaLookup.get(query).replaceAll("[^ATGC]", "N"));
        if (strand) {
            return seq.getSequenceAsString(begin.intValue(), end.intValue(), Strand.POSITIVE);
        } else {
            return seq.getSequenceAsString(end.intValue(), begin.intValue(), Strand.NEGATIVE);
        }
    }

    private NASequence getGenomeInformation(Domain domain, String genome) throws Exception {
        // If there are more types known we need to obtain the information for each contig
        Iterator<ResultLine> results = domain.getRDFSimpleCon().runQuery("getContigTypes.txt", true).iterator();

        if (seqTypes.keySet().size() == 0) {
            seqTypes = new HashMap<>();
            while (results.hasNext()) {
                ResultLine result = results.next();
                seqTypes.put(result.getIRI("seqobject"), result.getIRI("type"));
            }
        }


        String seqType = seqTypes.get(genome);

        NASequence dnasequence;
        if (seqType == null) {
            dnasequence = domain.make(Contig.class, genome);
            logger.debug("Unknown genome format detected... Using contig instead");
        } else if (seqType.toLowerCase().contains("scaffold")) {
            dnasequence = domain.make(Scaffold.class, genome);
        } else if (seqType.toLowerCase().contains("contig")) {
            dnasequence = domain.make(Contig.class, genome);
        } else if (seqType.toLowerCase().contains("chromosome")) {
            dnasequence = domain.make(Chromosome.class, genome);
        } else if (seqType.toLowerCase().contains("plasmid")) {
            dnasequence = domain.make(Plasmid.class, genome);
        } else {
            dnasequence = domain.make(Contig.class, genome);
            logger.debug("Unknown genome format detected... Using contig instead");
        }
        return dnasequence;
    }

    private void infernalCaller(CommandOptions arguments, List<File> files) throws Exception {
        for (File fastaFile : files) {
            // Obtain Z value
            Scanner scanner = new Scanner(fastaFile);
            long size = 0;
            while (scanner.hasNextLine()) {
                size = size + scanner.nextLine().length();
            }
            size = (size * 2) / 1000000;
            String cmd = arguments.cmscan + " -Z " + size + " --cpu " + arguments.cpu + " --cut_ga --rfam --nohmmonly --tblout " + fastaFile.getName() + ".tblout --fmt 2 --clanin " + arguments.clanin + " " + arguments.cm + " " + fastaFile + " > " + fastaFile.getName() + ".cmscan";

            logger.info(cmd);

            PrintWriter writer = new PrintWriter(fastaFile + "_infernal.sh", StandardCharsets.UTF_8);
            writer.println(cmd);
            writer.close();

            if (!new File(fastaFile.getName() + ".cmscan").exists()) {
                logger.info("Executing: " + fastaFile.getName() + ".cmscan");

                ExecCommand code = new ExecCommand("bash " + fastaFile + "_infernal.sh");
                if (code.getExit() > 0) {
                    logger.error(code.getOutput());
                    throw new Exception("Execution failed " + code.getError());
                }
            } else {
                logger.info(fastaFile + ".cmscan already exists... skipping calculation");
            }
        }
    }
}
