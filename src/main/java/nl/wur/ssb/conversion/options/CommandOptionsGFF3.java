package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.SappGeneric.ImportProv;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsGFF3 extends CommandOptionsConversion {

    public final String toolVersion = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsGFF3.class)[0];
    public final String toolCommit = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsGFF3.class)[1];
    @Parameter(names = {"-gff2rdf"}, description = "Input is GFF3 and output is RDF")
    public boolean gff2rdf;
    @Parameter(names = {"-gff2embl"}, description = "Input is GFF3 and output is embl")
    public boolean gff2embl;
    @Parameter(names = {"-codon"}, description = "Codon table used (for protein translation)", required = true)
    public int codon = -1;
    @Parameter(names = {"-f", "-fasta"}, description = "input corresponding fasta file", required = true)
    public File fasta;
    @Parameter(names = {"-sr", "-skip-remaining"}, description = "Skip fasta sequences without annotation")
    public boolean skip_remaining;
    @Parameter(names = {"-id", "-identifier"}, description = "Sample identifier", required = true)
    public String identifier;
    @Parameter(names = {"-agent"}, description = "IRI of the agent running the tool", hidden = true)
    public String userAgentIri;
    @Parameter(names = "-seqtype", description = "Sequence type (contig, scaffold, chromosome, plasmid, read)")
    public String seqtype = "contig";

    @Parameter(names = {"-topology"}, description = "Describing whether sequence is circular or linear (only applicable for genome fasta files)")
    public String topology;

    public int locusTag = 1;

    // Provenance section

    public AnnotationResult annotResult;


    public CommandOptionsGFF3(String[] args) throws Exception {
        try {
            // Building this
            new JCommander(this, args);

            this.commandLine = StringUtils.join(args, " ");

            /*
             * Provenance part
             */

            ImportProv origin = new ImportProv(domain, List.of(this.input.getAbsolutePath()), this.output, this.commandLine, this.toolName, this.toolVersion, this.repository, null, this.starttime, LocalDateTime.now());

            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
            origin.linkEntity(annotResult);

            ////////////////////////////////////////////////


//            JCommander jc = new JCommander(this, args);
//            this.commandLine = StringUtils.join(args, " ");
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
