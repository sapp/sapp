package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import nl.wur.ssb.SappGeneric.Generic;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.ext.com.google.common.hash.Hashing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Parameters(commandDescription = "Available options: ")
public class CommandOptionsFlatfile extends CommandOptionsConversion {

    public final String sappcommit = Generic.getVersion(CommandOptionsFlatfile.class)[1];
    public final String toolVersion = Generic.getVersion(CommandOptionsFlatfile.class)[0];

    @Parameter(names = {"-embl2rdf"}, description = "EMBL to RDF conversion")
    public boolean embl = false;

    @Parameter(names = {"-genbank2rdf"}, description = "GenBank to RDF conversion")
    public boolean genbank = false;

    @Parameter(names = {"-id", "-identifier"}, description = "Unique identifier for your sample (e.g. GCA_000001", required = true)
    public String identifier;

    @Parameter(names = {"-locus"}, description = "Locus counter for genes missing a locus_tag")
    public int locusTag = 1;

    @Parameter(names = {"-agent"}, description = "IRI of the agent running the tool")
    public String userAgentIri;

//    @Parameter(names = {"-if", "-importFiles"}, description = "Any ttl file that needs to include, aka a file describing the agent")
//    public List<File> importFiles = new ArrayList<>();

    @Parameter(names = {"-codon"}, description = "Codon table to use when transl_table AND protein sequences are not available")
    public int codon = -1;

    @Parameter(names = {"-fasta"}, description = "Fasta file supplied when EMBL/GenBank file contains assembly information instead of a sequence")
    public File fasta;

    @Parameter(names = {"-disableOverlap", "-dop"}, description = "Disables overlap detection e.g. when CDS falls within another Gene")
    public boolean overlapDetection = true;

    public int triples = 10000;

    @Parameter(names = {"-strict"}, description = "Performs strict conversions, e.g. locusTag mismatch etc... will throw an error")
    public boolean strict = false;

    // Provenance section
//    public final Domain domain = nl.wur.ssb.SappGeneric.Store.createDiskStore();
//    public AnnotationResult annotResult;
    public String annotURI = "http://gbol.life/0.1/" + UUID.randomUUID();


    public CommandOptionsFlatfile(String[] args) throws Exception {

        try {
            new JCommander(this, args);

            this.commandLine = StringUtils.join(args, " ");

            /**
             * Provenance part
             */

            List<String> md5s = new ArrayList<>();

            md5s.add(Generic.checksum(input, Hashing.md5()));


//            ImportProv origin = new nl.wur.ssb.SappGeneric.ImportProv(domain, Arrays.asList(files), this.output, this.commandLine, this.toolName, this.toolVersion, this.repository, null, this.starttime, LocalDateTime.now(), md5s);

//            annotResult = domain.make(AnnotationResult.class, annotURI);
//            origin.linkEntity(annotResult);

            ////////////////////////////////////////////////
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
