package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import org.apache.commons.lang.StringUtils;

import java.io.File;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsReduce extends CommandOptionsGeneric {
    @Parameter(names = {"-reduce"}, description = "Reducing the HDT file by removing sequences")
    public boolean reduce;

    @Parameter(names = {"-dir"}, description = "Directory containing multiple HDT files")
    public File dir;

    @Parameter(names = {"-recursive"}, description = "List files recursively in a given directory")
    public boolean recursive;

    public CommandOptionsReduce(String[] args) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            if (this.input != null) {
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
            }

            if (this.endpoint != null) {
                this.domain = new Domain(this.endpoint.toString());
                if (this.username != null) {
                    this.domain.getRDFSimpleCon().setAuthen(this.username, this.password);
                }
            }

            if (this.dir != null) {
                System.out.println("Using directory content");
            }
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}