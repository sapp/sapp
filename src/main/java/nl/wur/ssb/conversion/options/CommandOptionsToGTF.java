package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import org.apache.commons.lang.StringUtils;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsToGTF extends CommandOptionsGeneric {

//    @Parameter(names = {"-fout", "-fastaout"}, description = "output fasta file", required = true)
//    public File fasta;

//    @Parameter(names = {"-gout", "-gtfout"}, description = "output gtf file", required = true)
//    public File gtf;

    @Parameter(names = {"-rdf2gtf"}, description = "Converting HDT/RDF to GTF", required = true)
    public boolean rdf2gtf;


    public CommandOptionsToGTF(String[] args) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            if (this.input != null) {
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
            }

            if (this.endpoint != null) {
                this.domain = new Domain(this.endpoint.toString());
                if (this.username != null) {
                    this.domain.getRDFSimpleCon().setAuthen(this.username, this.password);
                }
            }
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
