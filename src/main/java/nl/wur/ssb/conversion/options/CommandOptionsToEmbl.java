package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsToEmbl extends CommandOptionsGeneric {

    private static final Logger logger = LogManager.getLogger(CommandOptionsToEmbl.class);

    @Parameter(names = {"-rdf2embl"}, description = "Converting HDT/RDF to EMBL", required = true)
    public boolean hdt2embl;
    @Parameter(names = {"-c", "-codon"}, description = "Codon table to use")
    public int translTable = -1;
    @Parameter(names = {"-s", "-sha"}, description = "Add the SHA384 key to protein")
    public boolean sha = false;
    @Parameter(names = {"-l", "-locus"}, description = "Regenerates locus tags using a given identifier")
    public String locus;

    public CommandOptionsToEmbl(String[] args) throws Exception {
        try {
            JCommander jc = new JCommander(this, args);

            if (this.input != null) {
                System.err.println("loading");
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
                System.err.println("finished");
            } else if (this.endpoint != null) {
                logger.info("Mounting endpoint");
                this.domain = new Domain(this.endpoint.toString());
                logger.info(this.domain.getRDFSimpleCon().getModel().size());
            }
            System.err.println("done?");
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
