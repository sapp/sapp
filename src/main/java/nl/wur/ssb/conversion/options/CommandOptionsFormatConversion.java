package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import java.io.File;

@Parameters(commandDescription = "Available options: ")

public class CommandOptionsFormatConversion {
    @Parameter(names = {"-convert"}, description = "Converting one RDF format to another RDF format")
    public static boolean convert;
    @Parameter(names = {"--prefix", "-p"}, description = "Prefix reducer: 'prefix@url prefi2x@url2'")
    public static String prefix = "gbol@http://gbol.life/0.1/ biopax@http://www.biopax.org/release/bp-level3.owl#";
    @Parameter(names = {"-i", "-input"}, description = "input file", required = true)
    public File input;
    @Parameter(names = {"-o", "-output"}, description = "Output file with new format extension", required = true)
    public File output;
    @Parameter(names = "-debug", description = "Debug mode", hidden = true)
    public boolean debug = false;
    @Parameter(names = "-split", description = "Size of the split in megabytes (set to 0 for no split)")
    public int split = 0;
    @Parameter(names = "--help")
    boolean help = false;

    public CommandOptionsFormatConversion(String[] args) {
        try {
            JCommander jc = new JCommander(this, args);
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
