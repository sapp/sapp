package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.SappGeneric.Store;
import org.apache.commons.lang.StringUtils;

import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsFasta2RDF extends CommandOptionsConversion {

    private final LocalDateTime starttime = LocalDateTime.now();
    public Domain domain;
    @Parameter(names = {"-id", "-identifier"}, description = "Unique identifier for your sample (e.g. GCA_000001", required = true)
    public String identifier;
    @Parameter(names = {"-codon"}, description = "Codon table used by the genome", required = true)
    public int codon = -1;
    @Parameter(names = {"-gene"}, description = "Fasta file consists of genes")
    public boolean gene = false;
    @Parameter(names = {"-protein"}, description = "Fasta file consists of proteins")
    public boolean protein = false;
    @Parameter(names = {"-genome"}, description = "Fasta file consists of genome sequences")
    public boolean genome = false;
    @Parameter(names = {"-translate"}, description = "Translate the input sequence to protein")
    public boolean translate = false;
    @Parameter(names = {"-contig"}, description = "Genome fasta file consists of contigs")
    public boolean contig = false;
    @Parameter(names = {"-scaffold"}, description = "Genome fasta file consists of scaffolds")
    public boolean scaffold = false;
    @Parameter(names = {"-chromosome"}, description = "Genome fasta file consists of complete chromosomes")
    public boolean chromosome = false;
    @Parameter(names = {"-topology"}, description = "Describing wether sequence is circular or linear (only applicable for genome fasta files)")
    public String topology = "linear";
    @Parameter(names = {"-stopcodon"}, description = "Will not raise an exception when stop codons are detected during translation")
    public boolean stopcodon = false;
    @Parameter(names = {"-length"}, description = "Minimum sequence length")
    public int length = 0;
    @Parameter(names = {"-jobs"}, description = "Number of spark jobs", hidden = true)
    public int jobs = 10;
    public AnnotationResult annotResult;
    @Parameter(names = {"-fasta2rdf"}, description = "Fasta to RDF/HDT conversion")
    boolean fasta2rdf = false;

    public CommandOptionsFasta2RDF(String[] args) throws Exception {
        // Building this
        try {
            JCommander jc = new JCommander(this, args);

            this.commandLine = StringUtils.join(args, " ");

            this.domain = Store.createMemoryStore();

            if (this.input == null && this.endpoint == null)
                throw new ParameterException("Parameter -input required");

            // Disk store which is the output
//            if (this.output.exists() && this.output.isDirectory()) {
//                System.err.println("Mounting folder " + this.output);
//                this.domain = new Domain("file://" + this.output);
//            } else if (this.output.exists()) {
//                throw new Exception("Output location is existing but not a JENA disk store " + this.output);
//            } else {
//                System.err.println("Making folder " + this.output + ".dir");
//                new File(this.output + ".dir").mkdirs();
//                this.domain = new Domain("file://" + this.output + ".dir");
//            }

            /*
             * Provenance part
             */

            String tool = "Conversion";
            String toolversion = "0.1";

            ImportProv origin = new ImportProv(domain, List.of(this.input.getAbsolutePath()), this.output, this.commandLine, tool, toolversion, this.repository, null, this.starttime, LocalDateTime.now());

            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
            // String annotResultIRI = annotResult.getResource().getURI();
            origin.linkEntity(annotResult);

//            String[] files = {this.input.getAbsolutePath()};

//            String toolVersion = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsFlatfile.class)[0];

//            ImportProv origin = new nl.wur.ssb.SappGeneric.ImportProv(domain, Arrays.asList(files), this.output, this.commandLine, this.toolName, toolVersion, this.repository, null, this.starttime, LocalDateTime.now());
//            annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());

//            origin.linkEntity(annotResult);

            //Ouput is needed for SPARK
            if (this.output == null) {
                this.output = Files.createTempFile("conversion", ".nt.gz").toFile();
                this.output.deleteOnExit();
            }

            ////////////////////////////////////////////////

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
