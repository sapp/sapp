package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

import java.io.File;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsRDF2FASTA {

    // Triple store
    public Domain domain;
    @Parameter(names = {"-i", "-input"}, description = "input GBOL RDF file", required = true)
    public File input;
    //    @Parameter(names = "-version", description = "SAPP version", hidden = true)
    public String version = "1.0.0";
    @Parameter(names = "-debug", description = "Debug mode")
    public boolean debug = false;
    @Parameter(names = {"-transcript"}, description = "Gene file location")
    public File gene;
    @Parameter(names = {"-trna"}, description = "tRNA file location")
    public File trna;
    @Parameter(names = {"-rrna"}, description = "rRNA file location")
    public File rrna;
    @Parameter(names = {"-protein"}, description = "Protein file location")
    public File protein;
    @Parameter(names = {"-genome"}, description = "Genome file location")
    public File genome;
    @Parameter(names = "--help")
    boolean help = false;
    @Parameter(names = {"-rdf2fasta"}, description = "GBOL to FASTA conversion")
    boolean rdf2fasta = false;


    public CommandOptionsRDF2FASTA(String[] args) {
        try {
            JCommander jc = new JCommander(this, args);

            this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
