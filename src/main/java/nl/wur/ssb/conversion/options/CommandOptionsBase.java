package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsBase {

    @Parameter(names = "--help")
    boolean help = false;

    @Parameter(names = {"-embl2rdf"}, description = "input format is embl")
    boolean embl2rdf = false;

    @Parameter(names = {"-genbank2rdf"}, description = "input format is genbank")
    boolean genbank2rdf = false;

    @Parameter(names = {"-fasta2rdf"}, description = "input format is fasta")
    boolean fasta2rdf = false;

    @Parameter(names = {"-rdf2fasta"}, description = "input format is fasta")
    boolean rdf2fasta = false;

    @Parameter(names = {"-rdf2embl"}, description = "input format is RDF to create EMBL files")
    boolean rdf2embl = false;

    @Parameter(names = {"-rdf2gtf"}, description = "input format is RDF to create GTF files")
    boolean rdf2gtf = false;

    @Parameter(names = {"-gtf2rdf"}, description = "input format is GTF to create GBOL files")
    boolean gtf2rdf = false;

    @Parameter(names = {"-gff2rdf"}, description = "input format is GFF3 to create GBOL files")
    boolean gff2rdf = false;

    @Parameter(names = {"-gff2embl"}, description = "input format is GFF3 to create EMBL files", hidden = true)
    boolean gff2embl = false;

    @Parameter(names = {"-convert"}, description = "Conversion of RDF to any other format (TTL/RDF/HDT/etc...) or folder (if no rdf extension is given)")
    boolean convert = false;

    @Parameter(names = {"-reduce"}, description = "Reduces the genomic HDT file by removing sequence information")
    boolean reduce = false;

    @Parameter(names = {"-merge"}, description = "Merging of HDT files")
    boolean merge = false;

    @Parameter(names = {"--error"}, description = "Still throw an error from internal call", hidden = true)
    boolean error = false;

    public CommandOptionsBase(String[] args) {
        try {
            JCommander jc = new JCommander(this, args);
            if (this.help)
                new JCommander(this).usage();

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help)
                exitCode = 0;
            if (this.error)
                exitCode = 64;
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
