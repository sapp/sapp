package nl.wur.ssb.conversion.options;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;

import java.time.LocalDateTime;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsConversion extends CommandOptionsGeneric {

    public final String toolName = "SAPP - File Conversion";
    public final String repository = "http://gitlab.com/sapp/conversion/conversion";
    @Parameter(names = {"-force"}, description = "Force complete run")
    public boolean force;
    public LocalDateTime starttime = LocalDateTime.now();
    public String commandLine;

    public CommandOptionsConversion() throws Exception {
    }
}
