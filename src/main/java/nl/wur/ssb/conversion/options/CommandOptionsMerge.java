package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import java.io.File;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsMerge {

    @Parameter(names = {"-m", "-merge"}, description = "Merge RDF/HDT files", required = true)
    public boolean merge;
    @Parameter(names = {"-i", "-input"}, description = "input files (comma separated)", required = true)
    public String input;
    @Parameter(names = {"-o", "-output"}, description = "output file", required = true)
    public File output;
    @Parameter(names = "-debug", description = "Debug mode")
    public boolean debug = false;
    @Parameter(names = "--help")
    boolean help = false;

    public CommandOptionsMerge(String[] args) {
        try {
            new JCommander(this, args);
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
