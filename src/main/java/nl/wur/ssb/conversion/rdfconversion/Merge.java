package nl.wur.ssb.conversion.rdfconversion;

import nl.wur.ssb.HDT.HDT;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.conversion.options.CommandOptionsMerge;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.exceptions.ParserException;
import org.rdfhdt.hdt.options.HDTSpecification;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Merge {
    public static String jarPath;
    public static RDFSimpleCon sappRDFsource;
    public static Domain domain;

    public static void app(CommandOptionsMerge arguments) throws IOException, ParserException {
        HDT hdt = new HDT();
        List<File> ntFiles = new ArrayList<File>();
        for (String hdtfile : arguments.input.split(",")) {
            File tmpFile = File.createTempFile("hdt", "nt");
            hdt.hdt2rdf(new File(hdtfile), tmpFile);
            ntFiles.add(tmpFile);
        }
        File mergedFile = File.createTempFile("hdt", "nt");
        MergeFiles(ntFiles, mergedFile);
        String baseURI = "http://gbol.life/0.1/";
        String inputType = "ntriples";
        String HDT = arguments.output.getAbsolutePath();
        org.rdfhdt.hdt.hdt.HDT hdtManager = org.rdfhdt.hdt.hdt.HDTManager.generateHDT(mergedFile.getAbsolutePath(), baseURI, RDFNotation.parse(inputType), new HDTSpecification(), null);
        System.out.println("HDTSAVE: " + HDT);
        hdtManager.saveToHDT(HDT, null);

        // Removing ntFiles
        for (File tmpFile : ntFiles) {
            tmpFile.delete();
        }
    }

    public static void MergeFiles(List<File> ntFiles, File mergedFile) {
        FileWriter fstream = null;
        BufferedWriter out = null;
        try {
            fstream = new FileWriter(mergedFile, true);
            out = new BufferedWriter(fstream);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        for (File f : ntFiles) {
            System.out.println("merging: " + f.getName() + " into " + mergedFile);
            FileInputStream fis;
            try {
                fis = new FileInputStream(f);
                BufferedReader in = new BufferedReader(new InputStreamReader(fis));
                String aLine;
                while ((aLine = in.readLine()) != null) {
                    out.write(aLine);
                    out.newLine();
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
