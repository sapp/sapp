package nl.wur.ssb.conversion.rdfconversion;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.InputOutput.Input;
import nl.wur.ssb.conversion.options.CommandOptionsFormatConversion;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.options.HDTSpecification;
import org.rdfhdt.hdt.tools.HDTCat;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.zip.GZIPOutputStream;

import static nl.wur.ssb.SappGeneric.InputOutput.Input.formatDetector;

public class Conversion {
    private static final Logger logger = LogManager.getLogger(Conversion.class);

    public static void app(CommandOptionsFormatConversion arguments) throws Exception {
        Input.RDFFormat rdfFormat = formatDetector(arguments.output);
        if (rdfFormat.equals(Input.RDFFormat.DIR)) {
            Domain domainFolder = new Domain("file://" + arguments.output);
            Domain domainFile = new Domain("file://" + arguments.input);
            domainFile.getRDFSimpleCon().getModel().listStatements().forEachRemaining(statement -> {
                domainFolder.getRDFSimpleCon().getModel().add(statement);
            });
            domainFile.close();
            domainFolder.close();
        } else {
            // Small file do this
            if (arguments.split == 0) {
                Domain domain = Input.load(arguments.input);
                nl.wur.ssb.SappGeneric.InputOutput.Output.save(domain, arguments.output);
            } else {
                logger.warn("Input file needs to be in NT format to perform the split function");
                BufferedReader buffered;
                if (arguments.input.getName().endsWith(".gz") || arguments.input.getName().endsWith("bz2")) {
                    buffered = getBufferedReaderForCompressedFile(arguments.input.getAbsolutePath());
                } else {
                    InputStream fileStream = new FileInputStream(arguments.input.getAbsolutePath());
                    Reader decoder = new InputStreamReader(fileStream, StandardCharsets.UTF_8);
                    buffered = new BufferedReader(decoder);
                }
                String content;
                int part = 1;
                int line = 0;

                File partFile = new File(arguments.input.getName() + ".part." + part + ".gz");
                if (!partFile.exists()) {
                    Writer writer = new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(partFile)), StandardCharsets.UTF_8);

                    while ((content = buffered.readLine()) != null) {
                        writer.write(content + "\n");
                        line = line + 1;
                        if (line == 1000000) {
                            // check size?
                            line = 0;
                            writer.flush();
                            // 50 MB to hdt conversion
                            if (partFile.length() / 1024 / 1024 > arguments.split) {
                                logger.info("Closing temp file " + partFile.getName() + " and converting to hdt");

                                writer.close();
                                part = part + 1;

                                HDT hdt = HDTManager.generateHDT(partFile.getAbsolutePath(), "http://example.com", RDFNotation.NTRIPLES, new HDTSpecification(), null);
                                hdt.saveToHDT(partFile.getAbsolutePath().replaceAll(".gz$", ".hdt"), null);

                                partFile = new File(arguments.input.getName() + ".part." + part + ".gz");
                                writer = new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(partFile)), StandardCharsets.UTF_8);
                            }
                        }
                    }
                    writer.close();
                } else {
                    logger.info("Part file " + partFile + " already exists assuming file has already been split");
                }


                HDTCat hdtCat = new HDTCat();

                ArrayList<File> hdtFiles = new ArrayList<>();

                Files.list(Path.of(".")).forEach(file -> {
                    if (file.toFile().getName().contains(arguments.input.getName() + ".part.") && file.toFile().getName().endsWith(".hdt")) {
                        hdtFiles.add(file.toFile());
                    }
                });

                File file = new File("empty.nt");
                file.createNewFile();
                HDT hdt1 = HDTManager.generateHDT(file.getAbsolutePath(), "uri", RDFNotation.NTRIPLES, new HDTSpecification(), null);
                hdt1.saveToHDT(arguments.output.getAbsolutePath(), null);
                file.delete();

                for (int i = 0; i < hdtFiles.size(); i++) {
                    logger.info("Merging HDT file number " + i + " out of " + hdtFiles.size());
                    File first = hdtFiles.get(i);
                    hdtCat.hdtInput1 = arguments.output.getAbsolutePath();
                    hdtCat.hdtInput2 = first.getAbsolutePath();
                    // All to output?
                    hdtCat.hdtOutput = arguments.output.getAbsolutePath() + ".tmp.hdt";
                    hdtCat.execute();

                    // Remove input file
                    arguments.output.delete();
                    // Move file to input file and merge next one
                    Files.move(Path.of(arguments.output.getAbsolutePath() + ".tmp.hdt"), arguments.output.toPath());
                }
            }
        }
    }

    public static BufferedReader getBufferedReaderForCompressedFile(String fileIn) throws FileNotFoundException, CompressorException {
        FileInputStream fin = new FileInputStream(fileIn);
        BufferedInputStream bis = new BufferedInputStream(fin);
        CompressorInputStream input = new CompressorStreamFactory().createCompressorInputStream(bis);
        BufferedReader br2 = new BufferedReader(new InputStreamReader(input));
        return br2;
    }
}

