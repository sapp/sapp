package nl.wur.ssb.conversion.togtf;

import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.conversion.options.CommandOptionsToGTF;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ToGTF {
    private static final Logger logger = LogManager.getLogger(ToGTF.class);
    private static final HashSet<String> ignoredFeatures = new HashSet<>();

    public static void app(CommandOptionsToGTF arguments) throws Exception {

        // SELECT DISTINCT ?naobject ?sequence ?accession ?description ?version ?scientificname ?lineage
        // ?dataclass ?division ?topology ?moleculetype
        Iterable<ResultLine> results;

        RDFSimpleCon rdfSimpleCon = arguments.domain.getRDFSimpleCon();

        // Get samples
        Iterable<ResultLine> samples;

        samples = rdfSimpleCon.runQuery("getSample.txt", false);

        // You can have multiple samples in a single rdf / endpoint
        // so if there is more than one sample this needs to be taken into account
        ArrayList<String> sampleIRIs = new ArrayList<>();
        for (ResultLine sample : samples) {
            String sampleIRI = sample.getIRI("sample");
            sampleIRIs.add(sampleIRI);
        }

        PrintWriter fastaWriter = new PrintWriter(arguments.output + ".fasta", UTF_8);
        PrintWriter gtfWriter = new PrintWriter(arguments.output + ".gtf", UTF_8);

        gtfWriter.write("##description: Annotation through GBOL - SAPP\n");
        gtfWriter.write("##provider: SAPP\n");
        gtfWriter.write("##contact: jasperkoehorst@gmail.com\n");
        gtfWriter.write("##format: gtf\n");
        gtfWriter.write("##date: 0000-00-00\n");
        gtfWriter.write("##Created by SAPP Conversion...\n");

        for (String sampleIRI : sampleIRIs) {
            String sampleName = "";
            if (sampleIRIs.size() > 1) {
//                sampleName = "_" + sampleIRI.replaceAll(".*/", "");
                logger.info("Multiple samples detected, merging results");
            }

            if (new File(arguments.output + sampleName + "SKIP FOR NOW").exists()) {
                logger.info("Fasta file already exists, skipping genome " + sampleName);
            } else {
                logger.info("Converting " + sampleIRI.replaceAll(".*/", ""));

                results = rdfSimpleCon.runQuery("getGenomeFasta.txt", true, sampleIRI);

                // Parses each dnaobject
                for (ResultLine dnaobject : results) {
                    String sequence = dnaobject.getLitString("sequence");
                    String accession = dnaobject.getLitString("locus");
                    String FASTA = ">" + accession + "\n" + sequence + "\n";
                    // Write to FASTA file...
                    fastaWriter.print(FASTA);

                    // SELECT DISTINCT ?feature ?ftype
                    String iri = dnaobject.getIRI("naobject");
                    String type = dnaobject.getIRI("type");

                    NASequence naSequence;
                    if (type.endsWith("/Contig")) {
                        naSequence = arguments.domain.make(Contig.class, iri);
                    } else if (type.endsWith("/Chromosome")) {
                        naSequence = arguments.domain.make(Chromosome.class, iri);
                    } else if (type.endsWith("/Scaffold")) {
                        naSequence = arguments.domain.make(Scaffold.class, iri);
                    } else if (type.endsWith("/Plasmid")) {
                        naSequence = arguments.domain.make(Plasmid.class, iri);
                    } else {
                        throw new Exception("Type not known yet " + type);
                    }

                    for (NAFeature feature : naSequence.getAllFeature()) {
                        ArrayList<String> geneArray = new ArrayList();
                        geneArray.add(accession);
                        geneArray.add("GBOL");

                        if (feature.getClassTypeIri().endsWith("/Gene")) {
                            geneArray.add("gene");

                            Gene gene = (Gene) feature;
                            if (gene.getLocation().getClassTypeIri().endsWith("/Region")) {
                                Region region = (Region) gene.getLocation();
                                ArrayList<String> regionArray = getRegion(region);
                                geneArray.addAll(regionArray);
                            } else {
                                throw new Exception("Type not known yet " + gene.getLocation().getClassTypeIri());
                            }

                            // Set the attributes
                            // Example: gene_id "ATCC64974_10"; gbkey "Gene"; gene_biotype "protein_coding"; locus_tag "ATCC64974_10";
                            String attribute = "gene_id \"" + gene.getLocusTag() + "\"; gbkey \"Gene\"; gene_biotype \"protein_coding\"; locus_tag \"" + gene.getLocusTag() + "\";";
                            geneArray.add(attribute);

                            // Write line to file
                            gtfWriter.println(StringUtils.join(geneArray, "\t"));

                            // Line array for gene is now finished, now for each transcript...
                            for (Transcript transcript : gene.getAllTranscript()) {
                                ArrayList<String> transcriptArray = new ArrayList<>();
                                transcriptArray.add(accession);
                                transcriptArray.add("GBOL");
                                transcriptArray.add("transcript");
                                transcriptArray.add(geneArray.get(3));
                                transcriptArray.add(geneArray.get(4));
                                transcriptArray.add(geneArray.get(5));
                                transcriptArray.add(geneArray.get(6));
                                transcriptArray.add(geneArray.get(7));
                                gtfWriter.println(StringUtils.join(transcriptArray, "\t"));

                                // Parsing the exons
                                int exonCounter = 0;
                                for (Exon exon : transcript.getExonList().getAllExon()) {
                                    exonCounter += 1;
                                    ArrayList<String> exonArray = new ArrayList<>();
                                    exonArray.add(accession);
                                    exonArray.add("GBOL");
                                    exonArray.add("exon");
                                    ArrayList<String> regionArray = getRegion((Region) exon.getLocation());
                                    exonArray.addAll(regionArray);

                                    attribute = "gene_id \"" + gene.getLocusTag() + "\"; gbkey \"mRNA\"; locus_tag \"" + gene.getLocusTag() + "\"; exon_number \"" + exonCounter + "\";";
                                    exonArray.add(attribute);
                                    gtfWriter.println(StringUtils.join(exonArray, "\t"));
                                }
                            }
                        } else if (feature.getClassTypeIri().endsWith("/MobileElement")) {
                            // TODO
//                            ArrayList<String> mobileElementArray = new ArrayList<>();
//                            mobileElementArray.add(accession);
//                            mobileElementArray.add("GBOL");
//                            mobileElementArray.add("mobileElement");
//                            ArrayList<String> regionArray = getRegion((Region) feature.getLocation());
//                            mobileElementArray.addAll(regionArray);
                        } else if (feature.getClassTypeIri().endsWith("/RepeatRegion")) {
                            // TODO
                        } else if (feature.getClassTypeIri().endsWith("/ReplicationOrigin")) {
                            // TODO
                        } else if (feature.getClassTypeIri().endsWith("/Exon")) {
                            // Ignored...
                        } else if (feature.getClassTypeIri().endsWith("/AssemblyGap")) {
                            // Ignored
                        } else if (feature.getClassTypeIri().endsWith("/Intron")) {
                            // Ignored
                        } else {
                            String ignoredFeature = feature.getClassTypeIri().replaceAll(".*/", "").strip();
                            if (!ignoredFeatures.contains(ignoredFeature)) {
                                logger.error(ignoredFeature + " not yet supported");
                                ignoredFeatures.add(ignoredFeature);
                            }
                        }
                    }
                }
            }
        }
        fastaWriter.close();
        gtfWriter.close();
    }

    private static ArrayList<String> getRegion(Region region) throws Exception {
        ArrayList<String> regionArray = new ArrayList<>();
        // Set begin
        regionArray.add(String.valueOf(getPosition(region.getBegin())));
        regionArray.add(String.valueOf(getPosition(region.getEnd())));
        regionArray.add(".");
        // Set strand
        if (region.getStrand().getIRI().endsWith("ForwardStrandPosition")) {
            regionArray.add("+");
        } else {
            regionArray.add("-");
        }
        // Set frame after strand
        regionArray.add(".");
        return regionArray;
    }

    private static Long getPosition(Position position) throws Exception {
        if (position.getClassTypeIri().endsWith("/ExactPosition")) {
            ExactPosition exactPosition = (ExactPosition) position;
            return exactPosition.getPosition();
        } else if (position.getClassTypeIri().endsWith("/BeforePosition")) {
            BeforePosition beforePosition = (BeforePosition) position;
            return beforePosition.getPosition();
        } else if (position.getClassTypeIri().endsWith("/AfterPosition")) {
            AfterPosition afterPosition = (AfterPosition) position;
            return afterPosition.getPosition();
        } else {
            throw new Exception("Type not known yet " + position.getClassTypeIri());
        }
    }
}
//
////                    Iterable<ResultLine> features;
////
////                    features = rdfSimpleCon.runQuery("getFeatures.txt", false, iri);
////
////                    for (ResultLine feat : features) {
////
////                        String feature = feat.getIRI("ftype");
////                        feature = feature.replaceAll("http://gbol.life/0.1/", "").toLowerCase();
//
//                        if (!feature.getClassTypeIri().toLowerCase().matches(".*gene")) {
//                            logger.debug("Skipping " + feature);
//                            continue;
//                        }
//                        // String locus = rdfSimpleCon.runQuery("getLocusTag.txt", false, feat.getIRI("feature")).iterator().next().getLitString("locus");
//                        Iterable<ResultLine> genelocs = null; // = rdfSimpleCon.runQuery("getGenePositions.txt", false, feat.getIRI("feature"));
//
//                        for (ResultLine geneloc : genelocs) {
//                            int begin = geneloc.getLitInt("begin");
//                            int end = geneloc.getLitInt("end");
//                            String strand = geneloc.getIRI("strand");
//                            if (strand.contains("ForwardStrandPosition"))
//                                strand = "+";
//                            if (strand.contains("ReverseStrandPosition"))
//                                strand = "-";
//
//                            String score = ".";
//                            String frame = ".";
//
//                            String suffix = begin + "_" + end;
//                            String attribute = "";
//
//                            if (feature.getClassTypeIri().toLowerCase().contains("gene")) {
//                                attribute = "gene_id \"gene_" + suffix + "\"; transcript_id \"transcript_" + suffix + "\";";
////                                if (locus != null)
////                                    attribute = attribute + "locus_tag \"" + locus + "\";";
//                            }
//
//                            gtfWriter.print(accession + "\tGBOL\t" + feature.getClassTypeIri().toLowerCase() + "\t" + begin + "\t" + end + "\t"
//                                    + score + "\t" + strand + "\t" + frame + "\t" + attribute + "\n");
//
//
//                            // Currently the transcript is identical to the gene positions
//                            gtfWriter.print(accession + "\tGBOL\t" + "transcript" + "\t" + begin + "\t" + end + "\t"
//                                    + score + "\t" + strand + "\t" + frame + "\n");
//
//                        }
//
//                        Iterable<ResultLine> transcripts = null; //rdfSimpleCon.runQuery("getTranscript.txt", false, feat.getIRI("feature"));
//
//                        for (ResultLine transcript : transcripts) {
//                            // For each transcript get the exonlist
//                            Iterable<ResultLine> exons;
////                            if (arguments.hdt != null)
////                                exons = rdfSimpleCon.runQuery(arguments.hdt, "getExonLocations2.txt", transcript.getIRI("transcript"));
////                            else
//                                exons = rdfSimpleCon.runQuery("getExonLocations2.txt", false, transcript.getIRI("transcript"));
//
//                            for (ResultLine exon : exons) {
//                                int begin = exon.getLitInt("begin");
//                                int end = exon.getLitInt("end");
//                                String strand = exon.getIRI("strand");
//                                if (strand.contains("ForwardStrandPosition"))
//                                    strand = "+";
//                                if (strand.contains("ReverseStrandPosition"))
//                                    strand = "-";
//
//                                String score = ".";
//                                String frame = ".";
//                                String suffix = begin + "_" + end;
//                                String attribute = "gene_id \"gene_" + suffix + "\"; transcript_id \"transcript_" + suffix + "\";";
//
//                                gtfWriter.print(accession + "\tGBOL\t" + "exon" + "\t" + begin + "\t" + end + "\t"
//                                        + score + "\t" + strand + "\t" + frame + "\t" + attribute + "\n");
//                            }
//                        }
//                    }
//                }
//                fastaWriter.close();
//                gtfWriter.close();
//                } catch (Exception e) {
//                    // Cleans failed files when something happend during querying
//                    new File(arguments.fasta + "_" + sampleName).delete();
//                    new File(arguments.gtf + "_" + sampleName).delete();
//                    throw new Exception(e);
//                }
//            }
//        }
// seqname - name of the chromosome or scaffold; chromosome names can be given with or without
// the 'chr' prefix. Important note: the seqname must be one used within Ensembl, i.e. a
// standard chromosome name or an Ensembl identifier such as a scaffold ID, without any
// additional content such as species or assembly. See the example GFF output below.

// source - name of the program that generated this feature, or the data source (database or
// project name)

// feature - feature type name, e.g. Gene, Variation, Similarity

// start - Start position of the feature, with sequence numbering starting at 1.

// end - End position of the feature, with sequence numbering starting at 1.

// score - A floating point value.

// strand - defined as + (forward) or - (reverse).

// frame - One of '0', '1' or '2'. '0' indicates that the first base of the feature is the
// first base of a codon, '1' that the second base is the first base of a codon, and so on..

// attribute - A semicolon-separated list of tag-value pairs, providing additional information
// about each feature.
//    }
//}
