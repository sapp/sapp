package nl.wur.ssb.conversion;


import nl.wur.ssb.conversion.fasta2rdf.Fasta;
import nl.wur.ssb.conversion.flatfile.Flatfile;
import nl.wur.ssb.conversion.gff3.GFF3Mapper;
import nl.wur.ssb.conversion.options.*;
import nl.wur.ssb.conversion.proteinAnnotation.CommandOptionsAnnotate;
import nl.wur.ssb.conversion.proteinAnnotation.ProteinAnnotation;
import nl.wur.ssb.conversion.rdf2fasta.RDF2Fasta;
import nl.wur.ssb.conversion.rdfconversion.Merge;
import nl.wur.ssb.conversion.reduce.Reduce;
import nl.wur.ssb.conversion.toembl.ToEmbl;
import nl.wur.ssb.conversion.togtf.ToGTF;
import org.apache.jena.ext.com.google.common.io.Files;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;

/**
 * SAPP conversion
 */

public class Conversion {

    private static final Logger logger = LogManager.getLogger(Conversion.class);
    public static HashSet<String> messages = new HashSet<>();

    public static void main(String[] args) throws Exception {
        List<String> argsArray = Arrays.asList(args);
        if (argsArray.contains("-debug")) {
            logger.atLevel(Level.DEBUG);
        }
        if (argsArray.contains("-genbank2rdf") || argsArray.contains("-embl2rdf")) {
            Flatfile.main(args);
        } else if (argsArray.contains("-fasta2rdf")) {
            Fasta.app(args);
        } else if (argsArray.contains("-gff2rdf") || argsArray.contains("-gff2embl")) {
            new GFF3Mapper(args);
        } else if (argsArray.contains("-rdf2gtf")) {
            logger.info("Starting RDF to GTF conversion");
            CommandOptionsToGTF gtfArguments = new CommandOptionsToGTF(args);
            ToGTF.app(gtfArguments);
        } else if (argsArray.contains("-merge")) {
            CommandOptionsMerge arguments = new CommandOptionsMerge(args);
            logger.info("Now performing RDF merge");
            Merge.app(arguments);
        } else if (argsArray.contains("-convert")) {
            CommandOptionsFormatConversion arguments = new CommandOptionsFormatConversion(args);
            logger.info("Now performing RDF conversion");
            nl.wur.ssb.conversion.rdfconversion.Conversion.app(arguments);
        } else if (argsArray.contains("-rdf2fasta")) {
            CommandOptionsRDF2FASTA arguments = new CommandOptionsRDF2FASTA(args);
            logger.info("Now performing FASTA analysis");
            File diskstore = Files.createTempDir();
            // Domain domain = new Domain("file://" + diskstore.getAbsolutePath());
            RDF2Fasta.app(arguments);
            Flatfile.deleteFolder(diskstore);
        } else if (argsArray.contains("-rdf2embl")) {
            ToEmbl.app(args);
        } else if (argsArray.contains("-annotate")) {
            CommandOptionsAnnotate commandOptionsAnnotate = new CommandOptionsAnnotate(args);
            ProteinAnnotation.main(commandOptionsAnnotate, commandOptionsAnnotate.domain);
        } else if (argsArray.contains("-reduce")) {
            CommandOptionsReduce commandOptionsReduce = new CommandOptionsReduce(args);
            Reduce.main(commandOptionsReduce);
        } else {
            String[] help = {"--help", "--error"};
            new CommandOptionsBase(help);
        }
    }

    public static BufferedReader getStreamFromZip(File file) throws Exception {
        BufferedReader reader;
        try {
            InputStream fileStream = new FileInputStream(file);
            InputStream gzipStream = new GZIPInputStream(fileStream);
            Reader decoder = new InputStreamReader(gzipStream, StandardCharsets.UTF_8);
            reader = new BufferedReader(decoder);
        } catch (ZipException e) {
            InputStream fileStream = new FileInputStream(file);
            Reader decoder = new InputStreamReader(fileStream, StandardCharsets.UTF_8);
            reader = new BufferedReader(decoder);
        }
        return reader;
    }

    /**
     * Prevents printing a message more than once, helps with small warning message that occur for many elements
     *
     * @param logger
     * @param message
     */
    public static void message(org.apache.logging.log4j.Logger logger, String message) {
        if (!messages.contains(message)) {
            logger.warn(message);
            messages.add(message);
        }
    }
}