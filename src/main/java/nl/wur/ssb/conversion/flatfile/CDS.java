package nl.wur.ssb.conversion.flatfile;

import uk.ac.ebi.embl.api.entry.feature.Feature;

public class CDS extends GeneElement {
    public life.gbol.domain.CDS gbolFeature;
    public Gene gene;
    public Transcript transcript;

    public CDS(Feature gbFeature, life.gbol.domain.CDS gbolFeature) {
        super(gbFeature);
        this.gbolFeature = gbolFeature;
    }

}
