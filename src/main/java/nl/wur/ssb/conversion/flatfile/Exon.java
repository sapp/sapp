package nl.wur.ssb.conversion.flatfile;

import uk.ac.ebi.embl.api.entry.feature.Feature;

public class Exon extends GeneElement {
    public life.gbol.domain.Exon gbolFeature;

    public Exon(Feature gbFeature, life.gbol.domain.Exon gbolFeature) {
        super(gbFeature);
        this.gbolFeature = gbolFeature;
    }

}
