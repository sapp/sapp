package nl.wur.ssb.conversion.flatfile;

import uk.ac.ebi.embl.api.entry.feature.Feature;

import java.util.LinkedList;

public class Transcript extends GeneElement {
    private final LinkedList<Exon> exonList = new LinkedList<>();
    public life.gbol.domain.Transcript gbolSequence;
    public Gene gene;
    boolean isReverse;

    public Transcript(Feature gbFeature, life.gbol.domain.Transcript gbolFeature) {
        super(gbFeature);
        this.gbolSequence = gbolFeature;
    }

    public void addExon(Exon toAdd) {
        this.exonList.add(toAdd);
    }

    public void setGene(Gene gene) {
        this.gene = gene;
    }

}
