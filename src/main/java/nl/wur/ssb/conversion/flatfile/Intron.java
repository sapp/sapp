package nl.wur.ssb.conversion.flatfile;

import uk.ac.ebi.embl.api.entry.feature.Feature;

public class Intron extends GeneElement {
    public life.gbol.domain.Intron gbolFeature;

    public Intron(Feature gbFeature, life.gbol.domain.Intron gbolFeature) {
        super(gbFeature);
        this.gbolFeature = gbolFeature;
    }

}
