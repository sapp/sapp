package nl.wur.ssb.conversion.flatfile;

import uk.ac.ebi.embl.api.entry.feature.Feature;

public class ProteinFeature extends GeneElement {
    public life.gbol.domain.CDS gbolFeature;

    public ProteinFeature(Feature gbFeature, life.gbol.domain.CDS gbolFeature) {
        super(gbFeature);
        this.gbolFeature = gbolFeature;
    }

}
