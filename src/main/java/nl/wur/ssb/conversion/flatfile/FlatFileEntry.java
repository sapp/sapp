package nl.wur.ssb.conversion.flatfile;

import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.SappGeneric.Xrefs;
import nl.wur.ssb.conversion.gbolclasses.Thing;
import nl.wur.ssb.conversion.gff3.GFF3Mapper;
import nl.wur.ssb.conversion.options.CommandOptionsFlatfile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.purl.ontology.bibo.domain.Issue;
import org.purl.ontology.bibo.domain.Journal;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.entry.Text;
import uk.ac.ebi.embl.api.entry.XRef;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.location.CompoundLocation;
import uk.ac.ebi.embl.api.entry.location.Location;
import uk.ac.ebi.embl.api.entry.qualifier.Qualifier;
import uk.ac.ebi.embl.api.entry.reference.*;
import uk.ac.ebi.embl.api.entry.sequence.Sequence;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static nl.wur.ssb.conversion.flatfile.Flatfile.args;


public class FlatFileEntry extends SequenceBuilder {
    private static final Logger logger = LogManager.getLogger(FlatFileEntry.class);
    public static AnnotationResult annotResult;
    LocalDateTime time;
    private Sample sample;
    private Entry entry;
    private GBOLDataSet dataset;
    private AnnotationLinkSet annotationLinkSet;
    private FeatureProvenance featureProv;
    private XRefProvenance xrefProv;
    private boolean GFF = false;
    private int locusTag;
    private boolean assemblyGap;

    // To supply that this module is used from the GFF parser using the parental option
    public FlatFileEntry(Domain domain, String identifier, Entry entry, ImportProv importProv) throws Exception {
        super(domain, "http://gbol.life/0.1/" + identifier + "/");
        new FlatFileEntry(domain, identifier, entry, importProv, false);
    }

    public FlatFileEntry(Domain domain, String identifier, Entry entry, ImportProv importProv, boolean GFFrun) throws Exception {
        super(domain, "http://gbol.life/0.1/" + identifier + "/");

        /**
         * This is also the GFF entry point
         */

        if (args != null) {
            annotResult = domain.make(AnnotationResult.class, args.annotURI);
            // annotResult = args.annotResult;
            locusTag = args.locusTag;
        } else {
            annotResult = GFF3Mapper.arguments.annotResult;
            locusTag = GFF3Mapper.arguments.locusTag;
        }

        // TODO check this line
        importProv.linkEntity(annotResult);

        this.entry = entry;
        rootIRI = "http://gbol.life/0.1/" + identifier + "/";
        GFF = GFFrun;

        // Create published data set if first public date is given
        if (entry.getFirstPublic() != null) {
            PublishedGBOLDataSet pubDataset = domain.make(PublishedGBOLDataSet.class, rootIRI + "gboldataset");
            if (entry.getLastUpdated() != null)
                pubDataset.setLastPublishedDate(entry.getLastUpdated().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            pubDataset.setFirstPublic(entry.getFirstPublic().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            pubDataset.setFirstPublicRelease(entry.getFirstPublicRelease());
            pubDataset.setLastPublishedRelease(entry.getLastUpdatedRelease());

            if (entry.getHoldDate() != null)
                pubDataset.setHolddate(entry.getHoldDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            if (entry.getStatus() != null)
                pubDataset.setReleaseStatus(entry.getStatus().toString());
            this.dataset = pubDataset;
        } else {
            dataset = domain.make(GBOLDataSet.class, rootIRI + "gboldataset");
        }

        if (this.entry.getDataClass() != null) {
            if (this.entry.getDataClass().matches("^CON$")) {
                this.dataset.setEntryType(EntryType.HighLevelAssemblyInformation);
            } else {
                this.dataset.setEntryType((EntryType) GBOLUtil.getEnum(EntryType.class, this.entry.getDataClass()));
            }
        } else {
            dataset.setEntryType(EntryType.StandardEntry);
        }

        if (this.entry.getVersion() != null) {
            dataset.setDataSetVersion(this.entry.getVersion());
        } else {
            dataset.setDataSetVersion(-1);
        }

        importProv.linkEntity(dataset);
        annotationLinkSet = this.domain.make(AnnotationLinkSet.class, rootIRI + "annotationresult");
        importProv.linkEntity(annotationLinkSet);
        featureProv = this.domain.make(FeatureProvenance.class, rootIRI + "featureprov");
        featureProv.setOrigin(annotationLinkSet);
        xrefProv = this.domain.make(XRefProvenance.class, rootIRI + "xrefprov");
        xrefProv.setOrigin(annotationLinkSet);
        // Create sequence object
        this.dNASequence = buildSequence();
        // Create sample obejct
        this.sample = buildSample();
        this.sample.setName(args.identifier);

        // Linking it to the corresponding sample
        this.dNASequence.setSample(sample);

        for (XRef xref : entry.getXRefs()) {
            // As of April 2013, the supported DBLINK cross-reference types are "Project"
            // (predecessor of BioProject), "BioProject", "BioSample", "Trace Assembly Archive",
            // "Sequence Read Archive", and "Assembly".
            life.gbol.domain.XRef gbolXref = this.createXRef(xref);
            if (xref.getDatabase().equals("BioSample")) {
                sample.addXref(gbolXref);
            } else if (xref.getDatabase().equals("Trace Assembly Archive") || xref.getDatabase().equals("Sequence Read Archive") || xref.getDatabase().equals("Assembly")) {
                this.dNASequence.addXref(gbolXref);
                dataset.addXref(gbolXref);
            } else {
                dataset.addXref(gbolXref);
            }
        }
        // Add the keywords
        if (entry.getKeywords() != null) {
            for (Text key : entry.getKeywords()) {
                dataset.addKeywords(key.getText());
            }
        }
        if (entry.getDivision() != null) {
            //  Division is ignored as information is already captured in the taxonomy
        }

        if (entry.getDataClass() != null) {
            if (entry.getDataClass().matches("^CON$")) {
                dataset.setEntryType(EntryType.HighLevelAssemblyInformation);
            } else {
                dataset.setEntryType((EntryType) GBOLUtil.getEnum(EntryType.class, entry.getDataClass()));
            }
        }

        if (entry.getVersion() != null)
            dataset.setDataSetVersion(entry.getVersion());
        if (entry.getDescription() != null && entry.getDescription().getText() != null)
            if (!entry.getDescription().getText().trim().equals(""))
                dataset.setDescription(entry.getDescription().getText());
        if (entry.getComment().getText() != null && !entry.getComment().getText().trim().equals(""))
            dataset.setComment(entry.getComment().getText());

        dataset.addAnnotationResults(annotationLinkSet);
        dataset.addSamples(sample);
        dataset.addSequences(this.dNASequence);

        // Building the features instance on top of the dNASequence of
        // whatever type...
        time = LocalDateTime.now();
        // Build all the features
        buildFeatures();
        if (Duration.between(LocalDateTime.now(), time).getSeconds() > 1)
            logger.debug("Flatfile Entry: " + Duration.between(LocalDateTime.now(), time));
        time = LocalDateTime.now();

        // Setting the locus tag counts back to the arguments
        if (args != null) {
            args.locusTag = locusTag;
        } else {
            GFF3Mapper.arguments.locusTag = locusTag;
        }
    }

    public Sample getSample() {
        return sample;
    }

    public NASequence getdNASequence() {
        return dNASequence;
    }

    public CommandOptionsFlatfile getArgs() {
        return args;
    }

    public Entry getEntry() {
        return entry;
    }

    public life.gbol.domain.XRef createXRef(XRef xref) {
        return this.createXRef(xref.getDatabase(), xref.getPrimaryAccession(), xref.getSecondaryAccession());
    }

    public life.gbol.domain.XRef createXRef(String dbIdentifier, String primary) {
        return this.createXRef(dbIdentifier, primary, null);
    }

    public life.gbol.domain.XRef createXRef(String dbIdentifier, String primary, String secondary) {
        life.gbol.domain.XRef toRet = null;
        try {
            toRet = Xrefs.create(this.domain, this.xrefProv, dbIdentifier, primary, secondary);
            this.annotationLinkSet.addTarget(toRet.getDb());
            dataset.addLinkedDataBases(toRet.getDb());
        } catch (Exception e) {
            logger.warn(dbIdentifier + "\t" + primary + "\t" + secondary);
            e.printStackTrace();
        }
        return toRet;
    }

    public void finishFeature(Feature gbFeature, life.gbol.domain.Feature naFeature) {
        if (naFeature.getClassTypeIri().toLowerCase().endsWith("/gene")) {
            life.gbol.domain.Gene gene = (life.gbol.domain.Gene) naFeature;
            try {
                gene.getLocusTag();
            } catch (RuntimeException e) {
                gene.setLocusTag("XXX");
            }
        }

        naFeature.setLocation(makeLocation(gbFeature, naFeature));
        // TODO testing if this adds the featureProv to all the features
        naFeature.addProvenance(featureProv);

        addXrefs(gbFeature, naFeature);
        // System.out.println(naFeature.getResource().getURI());
        dNASequence.addFeature(naFeature);
    }

    private void addXrefs(Feature gbFeature, life.gbol.domain.Feature naFeature) {
        if (gbFeature.getXRefs() != null) {
            for (XRef xref : gbFeature.getXRefs()) {
                naFeature.addXref(this.createXRef(xref));
            }
        }
    }

    public Sample buildSample() throws Exception {

        Sample sample = domain.make(Sample.class, this.rootIRI + "sample");

        this.sample = sample;

        nl.wur.ssb.conversion.gbolclasses.Sample pSample = new nl.wur.ssb.conversion.gbolclasses.Sample(this);
        pSample.parseQualifiers(entry.getPrimarySourceFeature());

        //Set the main taxonomy reference
        if (entry.getPrimarySourceFeature() != null) {
            uk.ac.ebi.ena.taxonomy.taxon.Taxon taxon = entry.getPrimarySourceFeature().getTaxon();
            if (taxon != null) {
                Organism organism = this.domain.make(Organism.class, this.rootIRI + "organism");
                if (taxon.getTaxId() != null) {
                    TaxonomyRef taxonRef = (TaxonomyRef) Xrefs.create(TaxonomyRef.class, this.domain, this.xrefProv, "taxonomy", "" + taxon.getTaxId(), null);
                    this.annotationLinkSet.addTarget(taxonRef.getDb());
                    dataset.addLinkedDataBases(taxonRef.getDb());
                    organism.setTaxonomy(taxonRef);
                }
                this.dataset.addOrganisms(organism);
                this.dNASequence.setOrganism(organism);
                if (taxon.getScientificName() != null && !taxon.getScientificName().equals(""))
                    organism.setScientificName(taxon.getScientificName());
                if (taxon.getLineage() != null) {
                    String[] lineageParts = taxon.getLineage().split(";");
                    for (int i = 0; i < lineageParts.length; i++) {
                        String part = lineageParts[i].trim();
                        if (part.equals(""))
                            continue;
                        Taxon gbolTaxon = this.domain.make(Taxon.class, this.rootIRI + "organism/taxon/" + i);
                        gbolTaxon.setTaxonName(part);
                        gbolTaxon.setTaxonRank(Rank.make("http://gbol.life/0.1/RankLevel" + (i + 1)));
                        gbolTaxon.setProvenance(featureProv);
                        organism.addTaxonomyLineage(gbolTaxon);
                    }
                }
            }

            for (XRef crossRef : entry.getPrimarySourceFeature().getXRefs()) {
                sample.addXref(this.createXRef(crossRef));
            }
        }
        return sample;
    }

    private void doSingleFeature(Feature feature, String name, Consumer<String> lambda) {
        List<Qualifier> quals = feature.getQualifiers(name);
        if (quals.size() > 1)
            logger.error("Qualifier: " + name + " only expected once but encountered it " + quals.size() + " times");
        if (quals.size() >= 1) {
            lambda.accept(quals.get(0).getValue());
        }
    }

    public String buildSubSequence(CompoundLocation<Location> locations, boolean complement) {

        String subseq = "";
        // FROM GFF parser CDS were not always sorted
        for (Location loc : locations.getSortedLocations().getLocations()) {
            //    for (Location loc : locations.getLocations()) {
            long begin = loc.getBeginPosition();
            long end = loc.getEndPosition();
            if (!complement) {
                subseq = subseq + new String(entry.getSequence().getSequenceByte(begin, end));
            } else {
                //        System.err.println("DEBUG: "+begin+"\t"+end+"\t"+new String(entry.getSequence().getReverseComplementSequenceByte(begin, end)));
                subseq = new String(entry.getSequence().getReverseComplementSequenceByte(begin, end)) + subseq;
            }
        }
        //    if (complement)
        //     System.err.println("DEBUGGING: "+ subseq);
        return subseq;
    }

    public NASequence buildSequence() throws Exception {
        /*
         * taxonomy @:TaxonomyRef?; strandType type::StrandType; translTable xsd:PositiveInteger?;
         * integratedInto @:NASequence?; chromosome xsd:String?; plasmidName xsd:String?;
         */
        /*
         * Creation of the DNASequence...
         */


        // Couple NASequence to Sample
        String primAccession = entry.getPrimaryAccession();
        // MD5 for the URI (to keep it small, but as it is linked to the unique
        // genome ID it should be unique enough... Proteins will be sha384 by URI
        if (entry.getPrimaryAccession() == null) {
            primAccession = Generic.checksum(entry.getSequence().toString(), "md5");
            logger.warn("No primary accession available using: " + primAccession);
        }

        NASequence naSequence = null;
        if (entry.getPrimarySourceFeature() != null) {
            if (!entry.getPrimarySourceFeature().getQualifiers("plasmid").isEmpty()) {
                Plasmid plasmid = domain.make(Plasmid.class, this.rootIRI + GBOLUtil.cleanURI(primAccession));
                // TODO set Strandtype based on entry header
                plasmid.setStrandType(StrandType.DoubleStrandedDNA);
                plasmid.setTranslTable(args.codon);
                naSequence = plasmid;
            } else if (!entry.getPrimarySourceFeature().getQualifiers("chromosome").isEmpty()) {
                Chromosome chrom = domain.make(Chromosome.class, this.rootIRI + GBOLUtil.cleanURI(primAccession));
                // TODO set Strandtype based on entry header
                chrom.setStrandType(StrandType.DoubleStrandedDNA);
                chrom.setTranslTable(args.codon);
                naSequence = chrom;
            } // TODO > scaffold (need example)
        }

        if (naSequence == null) {
            logger.info("Creating contig " + primAccession);
            Contig contig = domain.make(Contig.class, this.rootIRI + GBOLUtil.cleanURI(primAccession));
            // TODO set Strandtype based on entry header
            contig.setStrandType(StrandType.DoubleStrandedDNA);
            contig.setTranslTable(args.codon);
            naSequence = contig;
        }

        // Updating the contigURI
        this.featureURI = naSequence.getResource().getURI();

        naSequence.addAccession(primAccession);

        // Setting sequence
        if (entry.getSequence().getSequenceByte() != null) {
            if (new String(entry.getSequence().getSequenceByte()).toUpperCase() != null) {
                String seq = new String(entry.getSequence().getSequenceByte()).toUpperCase();
                naSequence.setSequence(seq);
                naSequence.setSha384(Generic.checksum(seq, "SHA-384"));
                naSequence.setLength((long) seq.length());
            }
        } else {
            throw new Exception("Warning... no sequence found cannot continue...\n Use -fasta to provide a FASTA file");
        }

        if (entry.getSequence().getAccession() != null) {
            naSequence.addAccession(entry.getSequence().getAccession());
        }

        if (entry.getSequence().getGIAccession() != null) {
            naSequence.addXref(this.createXRef("gi", entry.getSequence().getGIAccession()));
        }

        // TODO ? Is this still needed? This is allowed: "genomic DNA", "genomic RNA", "mRNA", "tRNA",
        // "rRNA", "other
        // RNA", "other DNA", "transcribed RNA", "viral cRNA", "unassigned
        // DNA", "unassigned RNA"
        if (entry.getSequence().getMoleculeType() != null) {
            // NASequence.setMolType(entry.getSequence().getMoleculeType());
        } else {
            // NASequence.setMolType("unassigned DNA");
        }

        if (entry.getSequence().getVersion() != null) {
            naSequence.setSequenceVersion(entry.getSequence().getVersion());
        }
        if (entry.getSequence().getTopology() != null) {
            if (entry.getSequence().getTopology() == Sequence.Topology.LINEAR) {
                naSequence.setTopology(Topology.Linear);
            } else if (entry.getSequence().getTopology() == Sequence.Topology.CIRCULAR) {
                naSequence.setTopology(Topology.Circular);
            } else {
                naSequence.setTopology(Topology.Linear);
                logger.warn("Topology is set to " + naSequence.getTopology());
            }
        } else {
            // Default it is linear...
            naSequence.setTopology(Topology.Linear);
        }

        if (entry.getDescription() != null && entry.getDescription().getText() != null) {
            naSequence.setDescription(entry.getDescription().getText());
        }

        return naSequence;
    }

    public Citation buildCitation(Reference ref) throws Exception {
        org.purl.ontology.bibo.domain.Document gbolRef = this.buildRef(ref);
        Citation toRet = domain.make(Citation.class, this.rootIRI + "citation/" + ref.getReferenceNumber());
        toRet.setProvenance(this.featureProv);
        toRet.setReference(gbolRef);
        return toRet;
    }

    public org.purl.ontology.bibo.domain.Document buildRef(Reference ref) throws Exception {
        Publication publication = ref.getPublication();
        org.purl.ontology.bibo.domain.Document toRet = null;
        if (publication instanceof Article article) {
            toRet = domain.make(org.purl.ontology.bibo.domain.AcademicArticle.class, this.rootIRI + "academic/article/" + ref.getReferenceNumber());
            if (article.getFirstPage() != null && !article.getFirstPage().trim().equals(""))
                toRet.setPageStart(article.getFirstPage());
            if (article.getLastPage() != null && !article.getLastPage().trim().equals(""))
                toRet.setPageEnd(article.getLastPage());
            if (article.getVolume() != null && !article.getVolume().trim().equals(""))
                toRet.setVolume(article.getVolume());
            String journalName = article.getJournal();
            Journal journal = null;
            if (journalName != null && !journalName.trim().equals("")) {
                // If we see more then 3 numbers then it can not be a valid journal name
                if (journalName.matches("\\d\\d\\d"))
                    logger.error("Invalid journal name:" + journalName);
                journal = domain.make(Journal.class, this.rootIRI + "journal/" + GBOLUtil.cleanURI(article.getJournal()));
                journal.setTitle(article.getJournal());
            }

            String issueName = article.getIssue();
            if ((issueName == null || issueName.trim().equals("")) && journal != null)
                issueName = "unknown issue in journal " + article.getJournal();
            Issue issue = null;
            if (issueName != null && !issueName.trim().equals("")) {
                issue = domain.make(Issue.class, this.rootIRI + "issue/" + GBOLUtil.cleanURI(article.getJournal()));
                issue.setTitle(issueName);
                if (article.getYear() != null)
                    issue.setIssued(LocalDateTime.from(article.getYear().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
                issue.addHasPart(toRet);
                if (journal != null)
                    journal.addHasPart(issue);
            }
        } else if (publication instanceof Book book) {
            toRet = domain.make(org.purl.ontology.bibo.domain.Book.class, this.rootIRI + "ref/" + ref.getReferenceNumber());
            if (book.getFirstPage() != null && !book.getFirstPage().trim().equals(""))
                toRet.setPageStart(book.getFirstPage());
            if (book.getLastPage() != null && !book.getLastPage().trim().equals(""))
                toRet.setPageEnd(book.getLastPage());
            if (book.getPublisher() != null && !book.getPublisher().trim().equals("")) {
                toRet.setPublisher(this.makeOrganization(book.getPublisher()));
            }
            if (book.getYear() != null)
                toRet.setIssued(LocalDateTime.from(book.getYear().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
            if (book.getBookTitle() != null && !book.getBookTitle().trim().equals(""))
                toRet.setTitle(book.getBookTitle());
        } else if (publication instanceof ElectronicReference elecRef) {
            toRet = domain.make(org.purl.ontology.bibo.domain.Webpage.class, this.rootIRI + "ref/" + ref.getReferenceNumber());
            if (elecRef.getText() == null || elecRef.getText().trim().equals("")) ;
            logger.error("Electronic reference with no IRI");
            toRet.setUri(elecRef.getText());
        } else if (publication instanceof Patent patent) {
            toRet = domain.make(org.purl.ontology.bibo.domain.Patent.class, this.rootIRI + "ref/" + ref.getReferenceNumber());

            // patent sequence number & patentType are not stored

            if (patent.getPatentOffice() != null && !patent.getPatentOffice().trim().equals(""))
                toRet.setIssuer(this.makeOrganization(patent.getPatentOffice()));

            if (patent.getPatentNumber() != null && !patent.getPatentNumber().trim().equals(""))
                toRet.setNumber(patent.getPatentNumber());

            if (patent.getDay() != null)
                toRet.setIssued(LocalDateTime.from(patent.getDay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));

            for (String author : patent.getApplicants()) {
                toRet.addAuthorList(makeAuthor(author));
            }
        } else if (publication instanceof Submission submission) {
            toRet = domain.make(org.purl.ontology.bibo.domain.Manuscript.class, this.rootIRI + "ref/" + ref.getReferenceNumber());

            if (submission.getDay() != null)
                toRet.setDateSubmitted(LocalDateTime.from(submission.getDay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));

            // submitterAddress not stored no support in bibo for this :(
        } else if (publication instanceof Thesis thesis) {
            toRet = domain.make(org.purl.ontology.bibo.domain.Thesis.class, this.rootIRI + "ref/" + ref.getReferenceNumber());

            if (thesis.getYear() != null)
                toRet.setIssued(LocalDateTime.from(thesis.getYear().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));

            if (thesis.getInstitute() != null && !thesis.getInstitute().trim().equals(""))
                toRet.setPublisher(this.makeOrganization(thesis.getInstitute()));

        } else if (publication instanceof Unpublished article) {
            toRet = domain.make(org.purl.ontology.bibo.domain.Manuscript.class, this.rootIRI + "ref/" + ref.getReferenceNumber());
        }

        if (publication.getId() != null && !publication.getId().trim().equals(""))
            toRet.setIdentifier(publication.getId());
        if (publication.getTitle() != null && !publication.getTitle().trim().equals(""))
            toRet.setTitle(publication.getTitle());
        if (publication.getConsortium() != null && !publication.getConsortium().trim().equals(""))
            toRet.addContributor(this.makeOrganization(publication.getConsortium()));

        for (Person author : publication.getAuthors()) {
            toRet.addAuthorList(makeAuthor(author));
        }

        for (XRef crossRef : publication.getXRefs()) {
            if (crossRef.getDatabase().equals("PUBMED")) {
                toRet.setPmid(crossRef.getPrimaryAccession());
            } else if (crossRef.getDatabase().equals("DOI")) {
                toRet.setDoi(crossRef.getPrimaryAccession());
            } else {
                logger.debug("Detected xref other then PUBMED on reference: " + crossRef.getDatabase());
                throw new Exception("Xref accession missing");
            }
        }
        return toRet;
    }

    public com.xmlns.foaf.domain.Person makeAuthor(Person author) {
        String personName = "";
        if (author.getFirstName() != null)
            personName += author.getFirstName() + " ";
        if (author.getSurname() != null)
            personName += author.getSurname();

        personName = personName.trim();

        if (personName.equals(""))
            return null;

        com.xmlns.foaf.domain.Person toRet = domain.make(com.xmlns.foaf.domain.Person.class, this.rootIRI + "person/" + GBOLUtil.cleanURI(personName));
        if (author.getFirstName() != null)
            toRet.setFirstName(author.getFirstName());
        if (author.getSurname() != null)
            toRet.setSurName(author.getSurname());
        toRet.setName(personName);
        return toRet;
    }

    public com.xmlns.foaf.domain.Organization makeOrganization(String name) {
        com.xmlns.foaf.domain.Organization toRet = domain.make(com.xmlns.foaf.domain.Organization.class, this.rootIRI + "org/" + GBOLUtil.cleanURI(name));
        toRet.setName(name);
        toRet.setLegalName(name);
        return toRet;
    }

    public com.xmlns.foaf.domain.Agent makeAgent(String name) {
        com.xmlns.foaf.domain.Agent toRet = domain.make(com.xmlns.foaf.domain.Agent.class, this.rootIRI + "org/" + GBOLUtil.cleanURI(name));
        toRet.setName(name);
        return toRet;
    }

    public com.xmlns.foaf.domain.Person makeAuthor(String name) {
        com.xmlns.foaf.domain.Person toRet = domain.make(com.xmlns.foaf.domain.Person.class, this.rootIRI + "org/" + GBOLUtil.cleanURI(name));
        toRet.setName(name);
        return toRet;
    }

    // TODO ALL EXONS SHOULD FIT IN? AS DIFFERENT CDS CAN OTHERWISE FIT IN THE SAME MRNA
    public boolean isAssociatedElem(Feature feature, GeneElement fitsInto) {
        // If locus tags do not match...
        if (feature.getSingleQualifier("locus_tag") != null && fitsInto.gbFeature.getSingleQualifier("locus_tag") != null) {
            if (!feature.getSingleQualifier("locus_tag").getValue().matches(fitsInto.gbFeature.getSingleQualifier("locus_tag").getValue())) {
                return false;
            }
        }
        long left = feature.getLocations().getMinPosition();
        long right = feature.getLocations().getMaxPosition();
        return left >= fitsInto.gbFeature.getLocations().getMinPosition() && right <= fitsInto.gbFeature.getLocations().getMaxPosition();
    }

    public GeneElement getAssociatedElem(LinkedList<? extends GeneElement> searchIn, Feature feature) {
        // If locus tags are present use those for matching... otherwise use positioning?
        try {
            for (GeneElement element : searchIn) {
                if (element.gbFeature.getSingleQualifier("locus_tag").getValue().matches(feature.getSingleQualifier("locus_tag").getValue())) {
                    return element;
                }

            }
            // When everything has locus-tags and thus no null pointer... we can skip this further matching... even when no hit is found
            return null;
        } catch (NullPointerException e) {

        }

        // Strand of the element?
        long left = feature.getLocations().getMinPosition();
        long right = feature.getLocations().getMaxPosition();
        boolean complement = feature.getLocations().getLocations().get(0).isComplement();

        // First check for identical position, next check for within
        for (GeneElement element : searchIn) {
            // Genes such as complement(join(726546..727176,1..245)) will cause everything to fall within...
            if (element.gbFeature.getLength() != entry.getSequence().getLength()) {
                if (complement == element.gbFeature.getLocations().getLocations().get(0).isComplement() && left == element.gbFeature.getLocations().getMinPosition() && right == element.gbFeature.getLocations().getMaxPosition()) {
//                    System.err.println("perfect match");
                    return element;
                }
            }
        }

        if (args.overlapDetection) {
            for (GeneElement element : searchIn) {
//                System.err.println(left + " " + element.gbFeature.getLocations().getMinPosition());
//                System.err.println(right + " " + element.gbFeature.getLocations().getMaxPosition());

                // Genes such as complement(join(726546..727176,1..245)) will cause everything to fall within...
                if (element.gbFeature.getLength() != entry.getSequence().getLength()) {
                    if (complement == element.gbFeature.getLocations().getLocations().get(0).isComplement() && left >= element.gbFeature.getLocations().getMinPosition() && right <= element.gbFeature.getLocations().getMaxPosition()) {
                        // Now perform check of locus tag when available
                        try {
                            String elementLocusTag = element.gbFeature.getSingleQualifier("locus_tag").getValue();
                            String featureLocusTag = feature.getSingleQualifier("locus_tag").getValue();
                            if (elementLocusTag.matches(featureLocusTag)) {
                                return element;
                            } else {
                                return null;
                            }
                        } catch (NullPointerException e) {
                            return element;
                        }
                    }
                }
            }
        }
        return null;
    }

//    static Map<String, Exon> exonLookup = new HashMap<>();

    public void buildFeatures() throws Exception {
        /*
         * For each feature... Creating a GENEOBJECT with (dNASequenceURI / MIN - MAX position)
         */
        LinkedList<nl.wur.ssb.conversion.flatfile.Exon> exons = new LinkedList<>();
        LinkedList<nl.wur.ssb.conversion.flatfile.Intron> introns = new LinkedList<>();
        LinkedList<Gene> genes = new LinkedList<>();
        LinkedList<nl.wur.ssb.conversion.flatfile.Transcript> transcripts = new LinkedList<>();
        LinkedList<CDS> cds = new LinkedList<>();
        LinkedList<Thing> withDelayedProps = new LinkedList<>();

        for (Feature feature : entry.getFeatures()) {
            // (mrna|ncrna|rrna|tmrna|trna|misc_rna|precursor_rna|prim_transcript)
            String name = feature.getName();
            String className = featureMap.get(name);

            // Issues with C_region...
            if (className != null) {
                if (className.contains("ConstantRegion") || className.contains("VariableSegment")) {
                    logger.warn(className + " is currently not parsed");
                } else {
                    Object gbolInstance;
                    if (name.matches("(enhancer|promoter|CAAT_signal|TATA_signal|-35_signal|-10_signal|RBS|GC_signal|polyA_signal|attenuator|terminator|misc_signal)")) {
                        gbolInstance = domain.make(RegulationSite.class, this.makeIRIFeature(feature));
                        ((RegulationSite) gbolInstance).setRegulatoryClass((RegulatoryClass) GBOLUtil.getEnum(RegulatoryClass.class, className));
                        className = "RegulationSite";
                    } else if (className.matches("UnknownAssemblyGap")) {
                        className = "UnknownLengthAssemblyGap";
                        gbolInstance = domain.make(Class.forName("life.gbol.domain." + className), this.makeIRIFeature(feature));
                        UnknownLengthAssemblyGap unknownLengthAssemblyGap = (UnknownLengthAssemblyGap) gbolInstance;
                        unknownLengthAssemblyGap.setGapType(GapType.UnknownGap);
                        assemblyGap = true;
                    } else if (className.matches("AssemblyGap")) {
                        gbolInstance = domain.make(Class.forName("life.gbol.domain." + className), this.makeIRIFeature(feature));
                        AssemblyGap assemblyGapX = (AssemblyGap) gbolInstance;
                        assemblyGapX.setGapType(GapType.UnknownGap);
                        assemblyGap = true;
                    } else {
                        feature.setName(feature.getName().toLowerCase());
                        gbolInstance = domain.make(Class.forName("life.gbol.domain." + className), this.makeIRIFeature(feature));
                    }

                    Class clazz;
                    try {
                        clazz = Class.forName("nl.wur.ssb.conversion.gbolclasses.features." + className);
                    } catch (ClassNotFoundException e) {
                        clazz = Class.forName("nl.wur.ssb.conversion.gbolclasses.sequences." + className);
                    }

                    Thing visitor = (Thing) clazz.getConstructor(SequenceBuilder.class, gbolInstance.getClass().getInterfaces()[0]).newInstance(this, gbolInstance);

                    if (!name.equalsIgnoreCase("source"))
                        visitor.parseQualifiers(feature);

                    if (visitor.hasDelayedProps())
                        withDelayedProps.add(visitor);

                    if (className.equalsIgnoreCase("cds")) {
                        CDS cdsElem = new CDS(feature, (life.gbol.domain.CDS) gbolInstance);

                        cds.add(cdsElem);

                        // GFF TO EMBL auto sorts the features causing problems with attaching the right types
                        // Parent could be a good solution MRNA <> MRNA <> CDS <> CDS wrongly assigns
                        if (GFF && !transcripts.isEmpty()) {
                            // Perform association based on parental values
                            String parentCDS = cdsElem.gbFeature.getSingleQualifierValue("Parent");
                            String transcriptID = transcripts.getLast().gbFeature.getSingleQualifierValue("ID");
                            if (parentCDS == null) {
                                // When CDS has no parent...?
                            } else if (transcriptID != null && parentCDS.matches(transcriptID)) {
                                cdsElem.transcript = transcripts.getLast();
                            } else {
                                Iterator<nl.wur.ssb.conversion.flatfile.Transcript> transcriptsIter = transcripts.iterator();
                                while (transcriptsIter.hasNext()) {
                                    nl.wur.ssb.conversion.flatfile.Transcript transcript = transcriptsIter.next();
                                    transcriptID = transcript.gbFeature.getSingleQualifierValue("ID");
                                    if (transcriptID != null && parentCDS.matches(transcriptID)) {
                                        cdsElem.transcript = transcript;
                                        break;
                                    }
                                }
                            }
                            // END OF GFF SOLUTION
                        } else if (!transcripts.isEmpty() && isAssociatedElem(feature, transcripts.getLast())) {
                            cdsElem.transcript = transcripts.getLast();
                        }

                        // Adding genes to CDS based on positioning... TODO check for strand and... use locus tag first?
                        if (!genes.isEmpty() && isAssociatedElem(feature, genes.getLast())) {
                            cdsElem.gene = genes.getLast();
                        }
                        addXrefs(feature, cdsElem.gbolFeature);

                    } else if (name.toLowerCase().matches("(misc_rna|miscrna|mrna|ncrna|precursorrna|rrna|tmrna|trna)")) {
//                        System.err.println(name);
                        nl.wur.ssb.conversion.flatfile.Transcript transcript = new nl.wur.ssb.conversion.flatfile.Transcript(feature, (life.gbol.domain.Transcript) gbolInstance);
                        transcripts.add(transcript);
                        if (!genes.isEmpty() && isAssociatedElem(feature, genes.getLast())) {
                            transcript.setGene(genes.getLast());
                        }
                    } else if (feature.getName().equalsIgnoreCase("exon")) {
                        exons.add(new nl.wur.ssb.conversion.flatfile.Exon(feature, (life.gbol.domain.Exon) gbolInstance));
                        finishFeature(feature, (NAFeature) gbolInstance);
                    } else if (feature.getName().equalsIgnoreCase("intron")) {
                        introns.add(new nl.wur.ssb.conversion.flatfile.Intron(feature, (life.gbol.domain.Intron) gbolInstance));
                        finishFeature(feature, (NAFeature) gbolInstance);
                    } else if (feature.getName().equalsIgnoreCase("gene")) {
                        genes.add(new Gene(feature, (life.gbol.domain.Gene) gbolInstance));
                        finishFeature(feature, (NAFeature) gbolInstance);
                    } else if (name.toLowerCase().matches("(variableregion|variablesegment|constantregion|diversitysegment|switchregion|joiningsegment|gapregion)")) {
                        logger.info("Conversion of protein properties not implemented yet");
                    } else if (name.equals("source")) {
                        // TODO ? something with source
                    } else {
                        finishFeature(feature, (life.gbol.domain.Feature) gbolInstance);
                    }
                }
            }
        }
        // Validating for gaps... if there were no gaps set
        if (!assemblyGap && Pattern.compile(Pattern.quote("n"), Pattern.CASE_INSENSITIVE).matcher(this.dNASequence.getSequence()).find()) {
            Pattern gapPattern = Pattern.compile("n+", Pattern.CASE_INSENSITIVE);
            Matcher matcher = gapPattern.matcher(this.dNASequence.getSequence());
            while (matcher.find()) {
                KnownLengthAssemblyGap gap = domain.make(KnownLengthAssemblyGap.class, this.dNASequence.getResource().getURI() + "/assemblygap/" + matcher.start() + "-" + matcher.end());
                gap.setGapType(GapType.UnknownGap);
                Region region = domain.make(Region.class, this.dNASequence.getResource().getURI() + "/" + matcher.start() + "-" + matcher.end());
                ExactPosition begin = domain.make(ExactPosition.class, this.dNASequence.getResource().getURI() + "/" + matcher.start());
                begin.setPosition((long) matcher.start() + 1);
                ExactPosition end = domain.make(ExactPosition.class, this.dNASequence.getResource().getURI() + "/" + matcher.end());
                end.setPosition((long) matcher.end());
                region.setBegin(begin);
                region.setEnd(end);
                region.setStrand(StrandPosition.ForwardStrandPosition);
                gap.setLocation(region);
                gap.addProvenance(featureProv);

                dNASequence.addFeature(gap);
        /*
        FT   assembly_gap    641268..641329
FT                   /estimated_length=62
FT                   /gap_type="unknown"
         */
            }
        }
        // Assigning transcripts to CDS
        for (CDS cdsElem : cds) {
            if (cdsElem.transcript == null) {
                // Trying to identify corresponding transcript
                nl.wur.ssb.conversion.flatfile.Transcript transcript = (nl.wur.ssb.conversion.flatfile.Transcript) this.getAssociatedElem(transcripts, cdsElem.gbFeature);
                if (transcript == null) {
                    // Create new one and give the mRNA the same length as the CDS...
                    mRNA gbolTranscript = domain.make(mRNA.class, this.makeIRIFeature(cdsElem.gbFeature, "mrna"));

                    // TODO Obtain sequence of CDS...
                    transcript = new nl.wur.ssb.conversion.flatfile.Transcript(cdsElem.gbFeature, gbolTranscript);
                    transcripts.add(transcript);
                    if (cdsElem.gene != null) {
                        transcript.gene = cdsElem.gene;
                        makeExonList(transcript.gbFeature, transcript.gene, transcript);
                        transcript.gene.gbolFeature.addTranscript(transcript.gbolSequence);
                    }
                }
                // link the transcript to the CDS
                cdsElem.transcript = transcript;
            }
        }

        // Checks first if transcript has a gene
        for (nl.wur.ssb.conversion.flatfile.Transcript transcript : transcripts) {
            if (transcript.gene == null) {
                // first try to find it
                Gene gene = (Gene) this.getAssociatedElem(genes, transcript.gbFeature);
                if (gene == null) {
                    // create new one but check if it already exists for the rare case of having a CDS and a misc_RNA on the same position GCA_900110325.1/FODI01.dat
                    String iri = this.makeIRIFeature(transcript.gbFeature, "gene");
                    OWLThingImpl obj = domain.getObject(iri, Gene.class);
                    // Keeps checking if the newer object does not exist yet...
                    int index = 2;
                    while (obj != null) {
                        logger.warn("Warning identical transcript overlap detected in " + iri);
                        iri = this.makeIRIFeature(transcript.gbFeature, "gene" + index);
                        logger.warn("Creating " + iri);
                        obj = domain.getObject(iri, Gene.class);
                        index = index + 1;
                    }

                    life.gbol.domain.Gene gbolGene = domain.make(life.gbol.domain.Gene.class, iri);

//                    System.err.println(gbolGene.getResource().getURI());

                    gene = new Gene(transcript.gbFeature, gbolGene);
                    genes.add(gene);
                    gbolGene.setLocation(makeLocation(transcript.gbFeature, gbolGene));
                    FeatureProvenance prov = domain.make(FeatureProvenance.class, gbolGene.getResource().getURI() + "/prov");
                    prov.setOrigin(annotResult);
                    gbolGene.addProvenance(prov);
                    gbolGene.setLocusTag("XXX");
//                    checkLocusTag(gene);
                    // System.err.println(gbolGene.getResource().getURI());
                    this.dNASequence.addFeature(gbolGene);
                    // TODO Missing sequence information based on Exons?
                }
                // link the transcript the CDS
                transcript.gene = gene;
            }
            // Attach the gene? exon list to the transcript
            this.makeExonList(transcript.gbFeature, transcript.gene, transcript);

            // and the link the transcript to the gene
            transcript.gene.gbolFeature.addTranscript(transcript.gbolSequence);

            // transcript.createMissingExons(exons.size() > 0);
        }
        // Assigning exons to genes
        for (nl.wur.ssb.conversion.flatfile.Exon exon : exons) {
            Gene gene = (Gene) getAssociatedElem(genes, exon.gbFeature);
            if (gene == null)
                logger.error("EXON could not be matched to gene: " + exon.gbFeature.getLocations());
            else
                gene.addExon(exon);
        }

        // Assigning introns to genes
        for (nl.wur.ssb.conversion.flatfile.Intron intron : introns) {
            Gene gene = (Gene) getAssociatedElem(genes, intron.gbFeature);
            if (gene == null)
                logger.error("Intron could not be matched to gene: " + intron.gbFeature.getLocations());
            else
                gene.addIntron(intron);
        }


        // Introns and Exons added to the Gene GBOL objects
        for (Gene gene : genes) {
            for (nl.wur.ssb.conversion.flatfile.Intron intron : gene.getIntronList()) {
                gene.gbolFeature.addIntron(intron.gbolFeature);
            }
            for (nl.wur.ssb.conversion.flatfile.Exon exon : gene.getExonList()) {
                gene.gbolFeature.addExon(exon.gbolFeature);
            }
            // TODO ! create missing introns based on the exon list
        }
        for (CDS cdsElem : cds) {
//            System.err.println(StringUtils.join(cdsElem.gbFeature.getLocations().getLocations()," "));
            Region region = null;
            long begin;
            long end;
            long transcriptEnd = cdsElem.transcript.gbFeature.getLocations().getMaxPosition();
            long transcriptBegin = cdsElem.transcript.gbFeature.getLocations().getMinPosition();
            long dnaLength = transcriptEnd - transcriptBegin;
            long mrnaLength = -1;
            // mRNA is not always present
            try {
                mrnaLength = cdsElem.transcript.gbolSequence.getLength();
            } catch (NullPointerException e) {
                mrnaLength = dnaLength;
            }
            long intronLength = dnaLength - mrnaLength;
            long correction = 1;
            long cdsBegin = cdsElem.gbFeature.getLocations().getMinPosition();
            long cdsEnd = cdsElem.gbFeature.getLocations().getMaxPosition();

            if (cdsElem.transcript.isReverse) {
                long cdsOnTranscriptEnd = (transcriptEnd - (transcriptEnd - cdsEnd) - intronLength) - transcriptBegin;
                long cdsOnTranscriptBegin = transcriptBegin - cdsBegin + correction;

                begin = cdsOnTranscriptBegin;
                end = cdsOnTranscriptEnd;
            } else {
                begin = cdsBegin - transcriptBegin + 1;
                end = cdsEnd - transcriptBegin + 1;
            }

            cdsElem.gbolFeature.setLocation(this.makeRegion(begin, end, "cds", cdsElem.gbolFeature));
            if (begin > 1) {
                FivePrimeUTR fiveUTR = domain.make(FivePrimeUTR.class, cdsElem.gbolFeature.getResource().getURI() + "/fiveUTR");
                fiveUTR.setLocation(this.makeRegion(1, begin - 1, "fiveUTR/Loc", cdsElem.gbolFeature));
                FeatureProvenance prov = domain.make(FeatureProvenance.class, fiveUTR.getResource().getURI() + "/prov");
                prov.setOrigin(annotResult);
                fiveUTR.addProvenance(prov);

                cdsElem.transcript.gbolSequence.addFeature(fiveUTR);
            }

            if (transcriptEnd != cdsEnd) {
                long length = cdsElem.transcript.gbolSequence.getSequence().length();
                ThreePrimeUTR threeUTR = domain.make(ThreePrimeUTR.class, cdsElem.gbolFeature.getResource().getURI() + "/threeUTR");
                threeUTR.setLocation(this.makeRegion(end + 1, length, "threeUTR/Loc", cdsElem.gbolFeature));
                FeatureProvenance prov = domain.make(FeatureProvenance.class, threeUTR.getResource().getURI() + "/prov");
                prov.setOrigin(annotResult);
                threeUTR.addProvenance(prov);
                cdsElem.transcript.gbolSequence.addFeature(threeUTR);
            }

            CompoundLocation<Location> locs = cdsElem.gbFeature.getLocations();

            //TODO use codon start if present

            // This was a fix required for complemtation
            boolean complement = cdsElem.gbFeature.getLocations().isComplement() || cdsElem.gbFeature.getLocations().getLocations().get(0).isComplement();

            String subseq = buildSubSequence(locs, complement);

            if (subseq.length() < 3)
                subseq = "NNN";


            // Found r character in dna sequence
            try {
                this.makeProtein(cdsElem.gbolFeature, cdsElem.gbFeature, subseq);
            } catch (CompoundNotFoundException e) {
                subseq = subseq.replaceAll("[^atgcATGC]", "n");
                this.makeProtein(cdsElem.gbolFeature, cdsElem.gbFeature, subseq);
            }

            FeatureProvenance prov = domain.make(FeatureProvenance.class, cdsElem.gbolFeature.getResource().getURI() + "/prov");
            prov.setOrigin(annotResult);
            cdsElem.gbolFeature.addProvenance(prov);

            cdsElem.transcript.gbolSequence.addFeature(cdsElem.gbolFeature);
            // TODO > Protein features should not be set on the proteins as the proteins are generic
            // identifiers
        }


        //All elements are connected to deal with props that need this information
        for (Thing elem : withDelayedProps) {
            elem.handleDelayedProps();
        }
        // Fixing locus tags...
        for (Gene gene : genes) {
            if (gene.gbolFeature.getLocusTag() == null || gene.gbolFeature.getLocusTag().matches("XXX")) {
                if (locusTag == 0) {
                    locusTag = 1;
                } else if (locusTag == 1) {
                    locusTag = locusTag + 9;
                } else {
                    locusTag = locusTag + 10;
                }
//                System.err.println(args.identifier + "_" + locusTag);
                gene.gbolFeature.setLocusTag(args.identifier + "_" + locusTag);
            }
        }
    }

    public void makeExonList(Feature gbTranscript, Gene gene, nl.wur.ssb.conversion.flatfile.Transcript transcript) throws Exception {

        CompoundLocation<Location> locTransObj = gbTranscript.getLocations();

        boolean isComplement = locTransObj.isComplement();
        List<Location> transcriptLocs = locTransObj.getLocations();

        boolean isFirst = true;
        boolean complementVal = false;
        LinkedList<nl.wur.ssb.conversion.flatfile.Exon> exons = new LinkedList<>();
        Iterator<Location> locIter = transcriptLocs.iterator();
        for (int i = 0; i < transcriptLocs.size(); i++) {
            Location transcriptLoc = transcriptLocs.get(i);
            if (isFirst) {
                complementVal = transcriptLoc.isComplement();
                isFirst = false;
            } else if (complementVal != transcriptLoc.isComplement()) {
                logger.error("Exon list with varying complementation: " + transcript.gbolSequence.getResource().getURI());
            }
            // Using the LOC method getting Exons with the right locations
            nl.wur.ssb.conversion.flatfile.Exon exon = gene.getExon(transcriptLoc);
            if (exon == null) {
                life.gbol.domain.Exon gbolExon = domain.make(life.gbol.domain.Exon.class, this.dNASequence.getResource().getURI() + "/exon/" + (transcriptLoc.isComplement() ^ isComplement ? "c" : "") + transcriptLoc.getBeginPosition() + "-" + transcriptLoc.getEndPosition());

                gbolExon.setLocation(makeRegion(transcriptLocs.get(i), isComplement, i == 0 && locTransObj.isLeftPartial(), i == transcriptLocs.size() - 1 && locTransObj.isRightPartial(), transcript.gbolSequence));
                FeatureProvenance prov = domain.make(FeatureProvenance.class, gbolExon.getResource().getURI() + "/prov");

                prov.setOrigin(annotResult);
                gbolExon.addProvenance(prov);

                // Disabled exons directly to DNA sequence, trying to reduce duplication of triples
                // this.dNASequence.addFeature(gbolExon);
                exon = new nl.wur.ssb.conversion.flatfile.Exon(null, gbolExon);
                exon.begin = transcriptLocs.get(i).getBeginPosition();
                exon.end = transcriptLocs.get(i).getEndPosition();
                gene.gbolFeature.addExon(exon.gbolFeature);
            }
            exons.add(exon);
        }

        transcript.isReverse = isComplement ^ complementVal;
        if (transcript.isReverse)
            Collections.reverse(exons);

        // There is only one exon list per transcript
        ExonList exonList = domain.make(ExonList.class, transcript.gbolSequence.getResource().getURI() + "/exonlist");
        String sequence = "";

        for (nl.wur.ssb.conversion.flatfile.Exon exon : exons) {
            // gbFeature == null
            long begin = exon.begin;
            long end = exon.end;
            try {
                if (transcript.isReverse)
                    sequence += new String(entry.getSequence().getReverseComplementSequenceByte(begin, end));
                else

                    sequence += new String(entry.getSequence().getSequenceByte(begin, end));

                // Only unique entries due to mis annotations in files duplicates can occur
                try {
                    // Check for duplicates
                    boolean duplicate = false;
                    for (life.gbol.domain.Exon element : exonList.getAllExon()) {
                        if (element.getResource().getURI().equalsIgnoreCase(exon.gbolFeature.getResource().getURI())) {
                            duplicate = true;
                        }
                    }
                    if (!duplicate)
                        exonList.addExon(exon.gbolFeature);
                } catch (RuntimeException e) {
                    // When empty a runtime exception is thrown due to cardinality
                    exonList.addExon(exon.gbolFeature);
                }
            } catch (NullPointerException ex) {
                ex.printStackTrace();
                logger.error("--------------ERROR-----------\nNo sequence found. Please check input genbank file for ORIGIN tag and check for wrong placed new lines/tabs");
                System.exit(1);
            }
        }
        setSequence(transcript.gbolSequence, sequence);
        transcript.gbolSequence.setExonList(exonList);
    }

    private String getOtherLocus(Domain domain, String locus, int begin, int end) throws Exception {
        RDFSimpleCon simplecon = domain.getRDFSimpleCon();
        if (locus.contains("TEST")) {
            for (ResultLine result : simplecon.runQuery("getLocusTagBeginEnds.txt", true, locus)) {
                int beginpos = result.getLitInt("beginpos");
                int endpos = result.getLitInt("endpos");
                if (beginpos != begin && beginpos != end) {
                    if (endpos != begin && endpos != end) {
                        // These are completely different but same locus tag?...
                        logger.error("Locus tag already in use by other gene... with as value: " + locus);
                        // System.err.println(beginpos + "\t" + begin);
                        // System.err.println(endpos + "\t" + end);
                        locus = locus + "_" + locusTag;
                        logger.error("Now setting to: " + locus);
                        locusTag = locusTag + 1;
                        return locus;
                    }
                }
            }
        }
        return locus;
    }
}
