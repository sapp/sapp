package nl.wur.ssb.conversion.flatfile;

import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.location.Location;

import java.util.HashMap;
import java.util.LinkedList;

public class Gene extends GeneElement {
    private final LinkedList<nl.wur.ssb.conversion.flatfile.Exon> exonList = new LinkedList<>();
    private final LinkedList<nl.wur.ssb.conversion.flatfile.Intron> intronList = new LinkedList<>();
    private final HashMap<String, nl.wur.ssb.conversion.flatfile.Exon> exonMap = new HashMap<>();
    public life.gbol.domain.Gene gbolFeature;

    public Gene(Feature gbFeature, life.gbol.domain.Gene gbolFeature) {
        super(gbFeature);
        this.gbolFeature = gbolFeature;
    }

    public void addExon(Exon toAdd) {
        this.exonMap.put(toAdd.gbFeature.getLocations().getMinPosition() + "-" + toAdd.gbFeature.getLocations().getMaxPosition(), toAdd);
        this.exonList.add(toAdd);
    }

    public void addIntron(Intron toAdd) {
        this.intronList.add(toAdd);
    }

    public Exon getExon(Location loc) {
        return this.exonMap.get(loc.getBeginPosition() + "-" + loc.getEndPosition());
    }

    public LinkedList<Exon> getExonList() {
        return exonList;
    }

    public LinkedList<Intron> getIntronList() {
        return intronList;
    }
}
