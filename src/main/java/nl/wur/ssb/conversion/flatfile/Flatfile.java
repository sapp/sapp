package nl.wur.ssb.conversion.flatfile;

import com.beust.jcommander.ParameterException;
import nl.wur.ssb.App;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.conversion.Conversion;
import nl.wur.ssb.conversion.options.CommandOptionsFlatfile;
import org.apache.commons.io.FileUtils;
import org.apache.jena.ext.com.google.common.hash.Hashing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.validation.Origin;
import uk.ac.ebi.embl.api.validation.ValidationMessage;
import uk.ac.ebi.embl.api.validation.ValidationResult;
import uk.ac.ebi.embl.fasta.reader.FastaFileReader;
import uk.ac.ebi.embl.fasta.reader.FastaLineReader;
import uk.ac.ebi.embl.flatfile.reader.EntryReader;
import uk.ac.ebi.embl.flatfile.reader.embl.EmblEntryReader;
import uk.ac.ebi.embl.flatfile.reader.genbank.GenbankEntryReader;
import uk.ac.ebi.embl.flatfile.writer.embl.EmblEntryWriter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.*;

public class Flatfile {

    private static final Logger logger = LogManager.getLogger(Flatfile.class);
    public static CommandOptionsFlatfile args;
    private static List<String> md5s;
    private static HashMap<String, Entry> fasta;

    public static void main(String[] args) throws Exception {

        Flatfile.args = new CommandOptionsFlatfile(args);

        Path dirPath = new File(App.storageDir + "/" + Flatfile.args.output.getName() + ".temp").toPath();
        logger.info("Temporary directory: " + dirPath.toFile().getAbsolutePath());
        while (dirPath.toFile().exists()) {
            FileUtils.deleteDirectory(dirPath.toFile());
        }
        File localTempDir = Files.createDirectory(dirPath).toFile();

        if (Flatfile.args.codon == -1) {
            // Quick read for /transl_table=
            boolean codonFound = false;
            BufferedReader br = Conversion.getStreamFromZip(Flatfile.args.input);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.contains("/transl_table=")) {
                    Flatfile.args.codon = Integer.parseInt(line.split("=")[1]);
                    logger.info("Codon table set to " + Flatfile.args.codon);
                    codonFound = true;
                    break;
                }
            }
            br.close();


            if (!codonFound && Flatfile.args.codon <= 0) {
                if (localTempDir.exists()) {
                    logger.info("Removing temp directory " + localTempDir);
                    FileUtils.deleteDirectory(localTempDir);
                }
                throw new ParameterException("No translation information available in the input file, use -codon to provide translation information");
            }
        }

        // Reading input file from zip or normally
        BufferedReader reader = Conversion.getStreamFromZip(Flatfile.args.input);

        // Start embl reader
        EntryReader emblReader = checkReader(reader);

        // If validation is empty..
        emblReader = validateEntry(emblReader);

        // Genome FASTA reader stored in a hashmap like entry... when needed
        fasta = new HashMap<>();
        if (Flatfile.args.fasta != null) {
            BufferedReader bufreader = new BufferedReader(new FileReader(Flatfile.args.fasta));
            FastaFileReader fastaReader = new FastaFileReader(new FastaLineReader(bufreader));
            ValidationResult result = fastaReader.read();
            Collection<ValidationMessage<Origin>> messages = result.getMessages();
            for (ValidationMessage<Origin> message : messages) {
                logger.info(message.getMessage());
            }

            while (fastaReader.isEntry()) {
                Entry entry = fastaReader.getEntry();
                String accession = entry.getComment().getText().split(" ")[0];
                fasta.put(accession, entry);
                fastaReader.read();
            }
        }

        // Preparing MD5 list
        md5s = new ArrayList<>();
        md5s.add(Generic.checksum(Flatfile.args.input, Hashing.md5()));

        logger.info("Starting parser");

        // We can now have SPARK implementation here...
        // int numberOfFiles = 1;

        // Split entries into individual files
        Set<File> entryFiles = new LinkedHashSet<>();

        File tmpFile = new File(localTempDir + "/" + Flatfile.args.identifier + "_" + entryFiles.size() + ".dat");
        entryFiles.add(tmpFile);

        // TODO implement FASTA parser here... so it updates the entry and write to disk

        /*
        Splits the file function
         */

        while (emblReader.isEntry()) {
            Entry entry = emblReader.getEntry();
            StringWriter writer = new StringWriter();
            new EmblEntryWriter(entry).write(writer);

            if (tmpFile.length() / (1024L * 1024L) >= 10) {
                if (entryFiles.size() > 10 && Flatfile.args.debug) {
                    logger.warn("Debug mode, parsing maximum of 10 entries");
                    break;
                }
                // Creates a file and the dir for the triple store
                tmpFile = new File(localTempDir + "/" + Flatfile.args.identifier + "_" + entryFiles.size() + ".dat");
                entryFiles.add(tmpFile);
                System.out.print("Split into: " + entryFiles.size() + "\r");
            }

            if (tmpFile.exists()) {
                Files.write(Paths.get(tmpFile.getAbsolutePath()), writer.toString().getBytes(), StandardOpenOption.APPEND);
            } else {
                Files.write(Paths.get(tmpFile.getAbsolutePath()), writer.toString().getBytes());
            }

            emblReader.read();
        }

        // Performs one file after the other conversion but should write to the output as compressed nt
        logger.info("Split finished into " + entryFiles.size() + " files");

        int index = 0;
        ArrayList<File> turtleFiles = new ArrayList<>();
        // TODO implement parallel processing here
        for (File entryFile : entryFiles) {
             if (new File(entryFile + ".ttl").exists()) {
                 logger.info("Skipping single conversion: " + index + " " + entryFiles.size() + " " + entryFile);
                 turtleFiles.add(new File(entryFile + ".ttl"));
             } else {
                 index++;
                 logger.info("Starting single conversion: " + index + " " + entryFiles.size() + " " + entryFile);
                 // Create temp rdf directory?
                 File localDir = Files.createDirectory(new File(entryFile + ".dir").toPath()).toFile();
                 logger.info("Creating temporary local repository at " + localDir);
                 Flatfile.args.domain = new Domain("file://" + localDir);
                 entryConversion(entryFile);
                 // Save the domain to a turtle file
                 nl.wur.ssb.SappGeneric.InputOutput.Output.save(Flatfile.args.domain, new File(entryFile + ".ttl"));
                 turtleFiles.add(new File(entryFile + ".ttl"));
                 Flatfile.args.domain.close();
                 // Remove domain directory
                 logger.info("Removing local directory " + localDir);
                 FileUtils.deleteDirectory(localDir);
             }
        }

        // Merge all files and save to output
        logger.info("Saving all stores to " + Flatfile.args.output);
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(turtleFiles, Flatfile.args.output);
//
//        // Merge the folders?
//        File finalDir = Files.createDirectories(new File(localTempDir + "/final.dir").toPath()).toFile();
//        Domain finalDirDomain = new Domain("file://" + finalDir);
//
//        for (File entryFile : entryFiles) {
//            logger.info("Merging " + entryFile);
//            File localDir = new File(entryFile + ".dir");
//            Flatfile.args.domain = new Domain("file://" + localDir);
//            finalDirDomain.getRDFSimpleCon().getModel().add(Flatfile.args.domain.getRDFSimpleCon().getModel());
//        }
//
//        nl.wur.ssb.SappGeneric.InputOutput.Output.save(finalDirDomain, Flatfile.args.output);
//
//        File domainDir = Flatfile.args.domain.getRDFSimpleCon().directory;
//
//        Flatfile.args.domain.close();
//        finalDirDomain.close();
//        if (domainDir != null) {
//            logger.info("Removing domain directory " + domainDir);
//            FileUtils.deleteDirectory(domainDir);
//        }
//        if (finalDir != null) {
//            logger.info("Removing  final directory " + finalDir);
//            FileUtils.deleteDirectory(finalDir);
//        }
        if (localTempDir.exists()) {
            logger.info("Removing  temp directory " + localTempDir);
            FileUtils.deleteDirectory(localTempDir);
        }
    }

    private static EntryReader checkReader(BufferedReader reader) throws Exception {
        if (Flatfile.args.genbank) {
            return new GenbankEntryReader(reader);
        } else if (Flatfile.args.embl) {
            return new EmblEntryReader(reader);
        } else {
            throw new Exception("define either genbank or embl");
        }
    }

    /**
     * TODO SPARK conversion module...
     *
     * @param entryFile
     */
    private static void entryConversion(File entryFile) throws Exception {
        // reader for the Entry File
        BufferedReader reader = Conversion.getStreamFromZip(entryFile);

        // Parsing file into Reader

        // Due to split it is converted to embl format...
        Flatfile.args.genbank = false;
        Flatfile.args.embl = true;

        EntryReader emblReader = checkReader(reader);

        ImportProv importProv = new ImportProv(Flatfile.args.domain, List.of(Flatfile.args.input.getAbsolutePath()), Flatfile.args.output, Flatfile.args.commandLine, Flatfile.args.toolName, Flatfile.args.toolVersion,
                Flatfile.args.repository, Flatfile.args.userAgentIri, Flatfile.args.starttime, LocalDateTime.now(), md5s);

        logger.debug("Validating new entry");

        emblReader = validateEntry(emblReader);

        while (emblReader.isEntry()) {
            logger.debug("Parsing new entry");
            Entry entry = emblReader.getEntry();
            new FlatFileEntry(Flatfile.args.domain, Flatfile.args.identifier, entry, importProv);
            emblReader.read();
        }

        // Closes the input flatfile reader
        logger.debug("Finished new entry");
        reader.close();
        importProv.finished();
    }

    private static EntryReader validateEntry(EntryReader emblReader) throws IOException {
        ValidationResult validations = emblReader.read();
        HashSet<String> uniqueMessages = new HashSet<>();
        if (!validations.getMessages().isEmpty()) {
            Iterator<ValidationMessage<Origin>> valiter = validations.getMessages().iterator();
            int count = 0;
            while (valiter.hasNext()) {
                count++;
                ValidationMessage<Origin> validation = valiter.next();
                if (uniqueMessages.contains(validation.getMessage())) {
                    continue;
                } else {
                    uniqueMessages.add(validation.getMessage());
                    logger.warn(validation.getMessage());
                }
                if (count > 20) {
                    logger.warn("And the list continues...");
                    break;
                }
            }
        }
        return emblReader;
    }

    public static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File f : files) {
                if (f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    public CommandOptionsFlatfile getArgs() {
        return args;
    }
}