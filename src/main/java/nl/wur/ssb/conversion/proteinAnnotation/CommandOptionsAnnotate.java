package nl.wur.ssb.conversion.proteinAnnotation;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsAnnotate extends CommandOptionsGeneric {

    @Parameter(names = {"-annotate"}, description = "Reannotate the CDS based on the data available")
    public boolean annotate;

    @Parameter(names = {"-l", "-locus"}, description = "Regenerates locus tags using a given identifier")
    public String locus;

    public CommandOptionsAnnotate(String[] args) throws Exception {
        try {
            JCommander jc = new JCommander(this, args);

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}


