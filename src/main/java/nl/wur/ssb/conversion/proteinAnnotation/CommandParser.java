package nl.wur.ssb.conversion.proteinAnnotation;

import org.apache.commons.cli.*;

public class CommandParser {

    public static CommandLine argsValidator(String[] args) {
        Options options = new Options();

        Option option = Option.builder("i").argName("file").desc("Genome RDF file").hasArg(true)
                .required(true).longOpt("input").build();
        options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        try {
            return parser.parse(options, args);
        } catch (Exception e) {
            System.err.println(e);
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("Missing arguments, possible options see below", options);
        }
        System.exit(0);
        return null;

    }
}
