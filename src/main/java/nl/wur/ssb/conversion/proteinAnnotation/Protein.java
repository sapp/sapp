package nl.wur.ssb.conversion.proteinAnnotation;

import java.util.ArrayList;
import java.util.List;

public class Protein {

    private final List<ProteinFeature> proteinFeatures = new ArrayList<>();
    private String ProteinURI;
    private String ProteinName;
    private int ProteinScore;

    public void addProteinURI(String protein) {
        this.setProteinURI(protein);
    }

    public String getProteinURI() {
        return ProteinURI;
    }

    public void setProteinURI(String proteinURI) {
        this.ProteinURI = proteinURI;
    }

    public List<ProteinFeature> getProteinFeatures() {
        return proteinFeatures;
    }

    public void addProteinFeature(ProteinFeature domainURI) {
        this.proteinFeatures.add(domainURI);
    }

    public String getProteinName() {
        return ProteinName;
    }

    public void setProteinName(String proteinName) {
        ProteinName = proteinName;
    }

    public int getProteinScore() {
        return ProteinScore;
    }

    public void setProteinScore(int proteinScore) {
        ProteinScore = proteinScore;
    }
}
