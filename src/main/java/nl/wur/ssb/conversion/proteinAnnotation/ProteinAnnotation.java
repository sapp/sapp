package nl.wur.ssb.conversion.proteinAnnotation;

import life.gbol.domain.Protein;
import life.gbol.domain.ProteinFeature;
import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ProteinAnnotation {
    private static final Logger logger = LogManager.getLogger(ProteinAnnotation.class);

    public static void main(CommandOptionsAnnotate arguments, Domain domain) throws Exception {

        arguments.domain = domain;

        logger.info("Performing protein annotation...");

        if (arguments.input == null) {
            logger.error("No -input directory given for RDF database");
            new CommandOptionsAnnotate(new String[]{""});
        }


        // Rebuilding locus tags
        if (arguments.locus != null) {
            deletePreviousLocusTags(arguments);
            buildLocusTags(arguments);
        }

        deletePreviousProteinAnnotation(arguments);
        proteinAnnotation(arguments);

        // arguments.domain.close();
    }

    private static void buildLocusTags(CommandOptionsAnnotate arguments) throws Exception {
        logger.info("Rebuilding locus tags...");
        int locusCount = 10;
        Iterator<ResultLine> genesIterators = arguments.domain.getRDFSimpleCon().runQuery("getGenesOrdered.txt", true).iterator();
        while (genesIterators.hasNext()) {
            ResultLine result = genesIterators.next();
            Gene gene = arguments.domain.make(Gene.class, result.getIRI("gene"));
            try {
                gene.setLocusTag(arguments.locus + locusCount);
            } catch (org.apache.jena.atlas.lib.InternalErrorException e) {
                logger.error("Setting locus tag failed for " + gene.getResource().getURI());
            }
            locusCount = locusCount + 10;

        }
    }

    private static void deletePreviousLocusTags(CommandOptionsAnnotate arguments) {
        arguments.domain.getRDFSimpleCon().runUpdateQuery("removePreviousLocusTags.txt");
    }

    private static void deletePreviousProteinAnnotation(CommandOptionsAnnotate arguments) throws Exception {
        // Removes previous annotation performed by previous annotation runs at protein level.
        logger.info("Removing previous annotation fields");
        // arguments.domain.getRDFSimpleCon().runUpdateQuery("removePreviousannotation.txt");
        int counter = 0;
        for (ResultLine item : arguments.domain.getRDFSimpleCon().runQuery("getCDSonly.txt", true)) {
            CDS cds = arguments.domain.make(CDS.class, item.getIRI("cds"));
            cds.setProduct("hypothetical protein");
            counter = counter + 1;
            if (counter % 1000 == 0) {
                logger.info("Erasing annotation information from cds " + counter);
            }
        }
    }


    /**
     * Performs protein annotation on the domain object
     *
     * @param arguments
     * @throws Exception
     */
    private static void proteinAnnotation(CommandOptionsAnnotate arguments) throws Exception {
        logger.info("Performing protein function assignment");

        // Priority naming... based on local knowledge
        String[] no_priority = {"p-loop", "domain-like", "conserved", "binding", "like", "family",
                "enzyme", "containing", "unknown", "uncharacterized", "UPF", "(duf)", "terminal",
                "helix-turn-helix", "repeat", "coil", "domain", "consensus disorder prediction", "hypothetical"};
        String[] low_priority = {"predicted", "homolog", "putative", "related", "associated", "prediction"};
        String[] higher_priority_enzymes = {"synthase"};
        String[] higher_priority_spores = {"spore", "spoI", "sporulation", "germination", "capsule"};
        String[] higher_priority_virus = {"virus", "viral", "phage", "transposase", "transposon",
                "transposition", "integrase", "resolvase", "dde superfamily endonuclease"};
        String[] higher_priority_transporter = {"transport", "influx", "efflux", "permease", "intake",
                "uptake", "symport", "antiport", "import", "pump", "exchanger", "channel", "translocase"};
        String[] higher_priority_transcription_factors =
                {"regul", "repress", "transcription", "zinc-finger", "zinc finger"};
        String[] higher_priority_chaperones =
                {"chaperone", "chaperonin", "heat shock", "heat-shock", "cold-shock", "cold shock"};


        long counter = 0L;
        for (ResultLine item : arguments.domain.getRDFSimpleCon().runQuery("getCDSonly.txt", true)) {
            HashMap<String, Double> descriptionLookup = new HashMap<>();
            HashMap<String, Double> descriptionLookupBlast = new HashMap<>();

            String proteinName = "hypothetical protein";

            CDS cds = arguments.domain.make(CDS.class, item.getIRI("cds"));

            // Try to get product information if already present
            descriptionLookup.put(cds.getProduct(), 100.00);

            // Obtain protein object
            Protein protein = cds.getProtein();

            counter = counter + 1;
            if (counter % 1000 == 0) {
                logger.info("Analysing cds " + counter);
            }

            for (ProteinFeature proteinFeature : protein.getAllFeature()) {
                String description;
                if (proteinFeature.getClassTypeIri().endsWith("ProteinDomain")) {
                    ProteinDomain proteinDomain = (ProteinDomain) proteinFeature;
                    description = proteinDomain.getSignatureDesc();
                    descriptionLookup = addCoverage(proteinFeature, protein, description, descriptionLookup);
                } else if (proteinFeature.getClassTypeIri().endsWith("ProteinHomology")) {
                    ProteinHomology proteinHomology = (ProteinHomology) proteinFeature;
                    description = proteinHomology.getHomologousToDesc().replaceAll("(OS=.*|GN=.*|PE=[0-9]+|SV=[0-9]+)", "").trim();
                    for (FeatureProvenance featureProvenance : proteinHomology.getAllProvenance()) {
                        ProvenanceApplication annotation = featureProvenance.getAnnotation();
                        NodeIterator nodeIterator = arguments.domain.getRDFSimpleCon().getObjects(annotation.getResource(), "");
                        logger.error("Property unknown!");
                        if (nodeIterator.hasNext()) {
                            descriptionLookupBlast.put(description, nodeIterator.next().asLiteral().getDouble());
                        }
                    }
                } else {
                    logger.info("New annotation object found but not recognised " + proteinFeature.getClassTypeIri());
                }

                // Get coverage?
            }

            // For each description found.. check the best information // Order
            // of description based on coverage?

            // Sort description by value double..

            // Get best three hits of blast
            descriptionLookupBlast = sortByValues(descriptionLookupBlast);
            List<String> descriptionLookupBlastKeySet = new ArrayList<>(descriptionLookupBlast.keySet());
            Collections.reverse(descriptionLookupBlastKeySet);
            if (descriptionLookupBlastKeySet.size() > 3) {
                descriptionLookupBlastKeySet = descriptionLookupBlastKeySet.subList(0, 3);
                // Store top 3 of BLAST in the list
                for (String description : descriptionLookupBlastKeySet) {
                    descriptionLookup.put(description, descriptionLookupBlast.get(description));
                }
            }

            descriptionLookup = sortByValues(descriptionLookup);
            List<String> descriptionLookupKeySet = new ArrayList<>(descriptionLookup.keySet());
            Collections.reverse(descriptionLookupKeySet);

            int annotationScore = -10;

            for (String description : descriptionLookupKeySet) {
                if (description == null || description.contains("consensus disorder prediction") ||
                        description.contains("hypothetical protein") || description == "")
                    continue;

                int score = 0;

                description = description.toLowerCase().replace(",", " ");

                score = matching(description, no_priority, -2, score);
                score = matching(description, low_priority, -1, score);
                score = matching(description, higher_priority_spores, +1, score);
                score = matching(description, higher_priority_virus, +1, score);
                score = matching(description, higher_priority_transporter, +1, score);
                score = matching(description, higher_priority_transcription_factors, +1, score);
                score = matching(description, higher_priority_chaperones, +1, score);
                score = matching(description, higher_priority_enzymes, +1, score);

                // Setting the name...
                if (score > annotationScore) {
                    proteinName = description;
                    annotationScore = score;
                }
            }

            if (cds.getProduct() == null || !cds.getProduct().contains(proteinName)) {
                // Double space seen...
                // System.err.println(cds.getProduct());
                cds.setProduct(proteinName.replaceAll(" +", " "));
            }
        }
    }

    private static HashMap<String, Double> addCoverage(ProteinFeature proteinFeature, Protein protein, String description, HashMap<String, Double> descriptionLookup) {
        if (proteinFeature.getLocation().getClassTypeIri().endsWith("Region")) {
            Region region = (Region) proteinFeature.getLocation();
            ExactPosition regionBegin = (ExactPosition) region.getBegin();
            ExactPosition regionEnd = (ExactPosition) region.getEnd();

            // Something weird is happening with long / ints... try catch and parse
            Long end;
            Long begin;
            Long length;

            try {
                end = regionEnd.getPosition();
            } catch (RuntimeException e) {
                end = Long.valueOf(e.getMessage().replaceAll(".* ", ""));
            }
            try {
                begin = regionBegin.getPosition();
            } catch (RuntimeException e) {
                begin = Long.valueOf(e.getMessage().replaceAll(".* ", ""));
            }

            try {
                length = protein.getLength();
            } catch (RuntimeException e) {
                length = Long.valueOf(e.getMessage().replaceAll(".* ", ""));
            }

            double range = ((double) end - (double) begin) / (double) length * 100;

            if (descriptionLookup.containsKey(description)) {
                if (range < descriptionLookup.get(description)) {
                    range = -1;
                }
            }

            if (range > 0) {
                descriptionLookup.put(description, range);
            }

        } else {
            logger.error("Do not understand type " + proteinFeature.getLocation().getClassTypeIri());
        }
        return descriptionLookup;
    }

    private static int matching(String description, String[] elements, int value, int score) {
        for (String element : elements) {
            if (description.contains(element)) {
                score += value;
            }
        }
        return score;
    }

    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}
