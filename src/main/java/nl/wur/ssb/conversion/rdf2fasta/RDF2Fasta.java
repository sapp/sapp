package nl.wur.ssb.conversion.rdf2fasta;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.conversion.options.CommandOptionsRDF2FASTA;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.PrintWriter;
import java.util.HashMap;

public class RDF2Fasta {
    private static final Logger logger = LogManager.getLogger(RDF2Fasta.class);

    // TODO build in HDT / DIR support?
    public static void app(CommandOptionsRDF2FASTA arguments) throws Exception {

        if (arguments.gene != null) {
            MakeFasta("getGenes.txt", arguments.gene, arguments);
        }
        if (arguments.genome != null) {
            MakeFasta("getGenomeFasta.txt", arguments.genome, arguments);
        }
        if (arguments.protein != null) {
            MakeFasta("getProteins.txt", arguments.protein, arguments);
        }
        if (arguments.rrna != null) {
            MakeFasta("getrRNAs.txt", arguments.rrna, arguments);
        }
        if (arguments.trna != null) {
            MakeFasta("gettRNAs.txt", arguments.trna, arguments);
        }
    }

    private static void MakeFasta(String query, File fastafile, CommandOptionsRDF2FASTA arguments) throws Exception {
        PrintWriter writer = new PrintWriter(fastafile);
        int count = 0;
        HashMap<String, Integer> headerLookup = new HashMap<>();
        for (ResultLine item : arguments.domain.getRDFSimpleCon().runQuery(query, true)) { // , sampleIRI.getIRI("sample"))
            count++;
            String header = item.getLitString("locus");
            if (headerLookup.containsKey(header)) {
                Integer value = headerLookup.get(header);
                // Increment store
                int tempValue = value + 1;
                headerLookup.put(header, tempValue);
                header = header + ".t" + value;
            } else {
                headerLookup.put(header, 1);
            }
            String sequence = item.getLitString("sequence");
            writer.write(">" + header + "\n" + sequence + "\n");
        }
        logger.info("Created: " + arguments.input.getName() + " with " + count + " sequences of " + query);
        writer.close();
    }
}
