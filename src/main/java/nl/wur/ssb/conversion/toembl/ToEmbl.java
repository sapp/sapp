package nl.wur.ssb.conversion.toembl;

import com.beust.jcommander.ParameterException;
import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.conversion.Conversion;
import nl.wur.ssb.conversion.options.CommandOptionsToEmbl;
import nl.wur.ssb.conversion.proteinAnnotation.CommandOptionsAnnotate;
import nl.wur.ssb.conversion.proteinAnnotation.ProteinAnnotation;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.entry.EntryFactory;
import uk.ac.ebi.embl.api.entry.Text;
import uk.ac.ebi.embl.api.entry.XRef;
import uk.ac.ebi.embl.api.entry.feature.CdsFeature;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.feature.FeatureFactory;
import uk.ac.ebi.embl.api.entry.feature.SourceFeature;
import uk.ac.ebi.embl.api.entry.location.LocationFactory;
import uk.ac.ebi.embl.api.entry.reference.Reference;
import uk.ac.ebi.embl.api.entry.reference.ReferenceFactory;
import uk.ac.ebi.embl.api.entry.reference.Unpublished;
import uk.ac.ebi.embl.api.entry.sequence.SequenceFactory;
import uk.ac.ebi.embl.flatfile.writer.embl.EmblEntryWriter;
import uk.ac.ebi.ena.taxonomy.taxon.Taxon;
import uk.ac.ebi.ena.taxonomy.taxon.TaxonFactory;

import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class ToEmbl {

    // private static RDFSimpleCon RDFResource;
    // EMBL things
    private static final FeatureFactory featureFactory = new FeatureFactory();
    //    private static QualifierFactory qualifierFactory = new QualifierFactory();
    private static final LocationFactory locationFactory = new LocationFactory();
    private static final SequenceFactory sequenceFactory = new SequenceFactory();
    private static final Date FFDate = new Date();
    //    private static int locusTag = 1;
    private static final Logger logger = LogManager.getLogger(ToEmbl.class);
    public static CommandOptionsToEmbl arguments;

    public static void app(String[] args) throws Exception {

        arguments = new CommandOptionsToEmbl(args);

        if (arguments.debug)
            logger.atLevel(Level.DEBUG);

        logger.info("Now generating EMBL file");

        arguments.output.delete();

        // Annotate the proteins
        logger.info("All proteins are being annotated");

        // LOAD HDT file...
        logger.debug("Mounting embl file");

        ArrayList<String> annotateOptions = new ArrayList<>();
        annotateOptions.addAll(List.of(args));
        annotateOptions.remove("-rdf2embl");

        if (arguments.locus != null) {
            annotateOptions.addAll(List.of("-annotate", "-locus", arguments.locus));
        } else {
            annotateOptions.add("-annotate");
        }

        CommandOptionsAnnotate commandOptionsAnnotate = new CommandOptionsAnnotate(annotateOptions.toArray(new String[0]));

        logger.debug("Size is now: " + arguments.domain.getRDFSimpleCon().getModel().size());
        ProteinAnnotation.main(commandOptionsAnnotate, arguments.domain);
        logger.debug("Size is now: " + arguments.domain.getRDFSimpleCon().getModel().size());

        // Create for each sample an EMBL file
        logger.info("Generating entry files");
//        arguments.domain.close();

        boolean foundContigs = false;
        for (ResultLine item : arguments.domain.getRDFSimpleCon().runQuery("getContigs.txt", true)) {
            foundContigs = true;
            // Sample sample = domain.make(life.gbol.domain.Sample.class, item.getIRI("sample"));
            NASequence naSequence = arguments.domain.make(NASequence.class, item.getIRI("naobject"));

            logger.info("DNAObject: " + item.getIRI("naobject"));

//            String sequence = naSequence.getSequence();

            // Creating the EMBL entry
            Entry entry = (new EntryFactory()).createEntry();

            // ID XXX; SV XXX; linear; genomic DNA; XXX; FUN; 2261852 BP.

            // Default WGS DataClass
            entry.setDataClass(Entry.WGS_DATACLASS);


            // Setting the sequence version in the header *not working
            // entry.setVersion(naSequence.getSequenceVersion());
            // entry.setSubmitterWgsVersion(naSequence.getSequenceVersion());

            // Date lastUpdated = new Date();
            // entry.setLastUpdated(lastUpdated);

            // Setting the translation table, if available from genome use that and overwrite the transltable
            if (naSequence.getTranslTable() > 0) {
                arguments.translTable = naSequence.getTranslTable();
            } else if (arguments.translTable <= 0) {
                throw new ParameterException("No translation table information available, use -codon instead");
            }

            entry.setSequence(sequenceFactory.createSequenceByte(naSequence.getSequence().toLowerCase().getBytes()));

            // Setting accession
            logger.debug("Translation table: " + naSequence.getTranslTable());
            logger.debug("Xrefs: " + StringUtils.join(naSequence.getAllXref(), "\n"));

            String accession = naSequence.getAllAccession().get(0);
            if (accession == null) {
                accession = "UNKNOWN";
            }

            entry.getSequence().setAccession(accession);
            entry.setPrimaryAccession(accession);

      /*
      TODO Setting other stuff...
       */

            Text comment = new Text();
            comment.setText("COMMENT");
            entry.setComment(comment);
            entry.setDataClass("DCLASS");
            Text description = new Text();
            description.setText("DESCRIPTION");
            entry.setDescription(description);
            entry.setDivision("DIV");
            entry.setFFDate(FFDate);
            entry.setId("ID");
            entry.setVersion(1);
            Text keyword = new Text();
            keyword.setText("key, word");
            entry.addKeyword(keyword);
            Text projectAccession = new Text();
            projectAccession.setText("projectAccession");
            entry.addProjectAccession(projectAccession);

            // TODO improve references...
            Reference reference = (new ReferenceFactory()).createReference();
            reference.setReferenceNumber(1);
            reference.getLocations().addLocation(locationFactory.createLocalRange(1L, (long) naSequence.getSequence().length()));
            ReferenceFactory referenceFactory = new ReferenceFactory();
            Unpublished publication = referenceFactory.createUnpublished("Unpublished");
            publication.setTitle("Publication title");
            publication.addAuthor(new ReferenceFactory().createPerson("Doe", "Jane"));
            publication.addAuthor(new ReferenceFactory().createPerson("Doe", "John"));
            publication.setConsortium("Consortium");
            reference.setPublication(publication);
            entry.addReference(reference);

            // Source feature
            entry = Source(entry, naSequence);

            // Other features
            createFeatures(entry, naSequence);

            // Writing entry to file
            StringWriter writer = new StringWriter();
            new EmblEntryWriter(entry).write(writer);

            /*
             * Writing to file
             */

            if (new File(arguments.output.getAbsolutePath()).exists()) {
                Files.write(Paths.get(arguments.output.getAbsolutePath()), writer.toString().getBytes(),
                        StandardOpenOption.APPEND);
            } else {
                Files.write(Paths.get(arguments.output.getAbsolutePath()), writer.toString().getBytes());
            }
        }

        if (!foundContigs) {
            throw new Exception("No contigs found in the supplied HDT file");
        }

        arguments.domain.close();
    }

    private static void createFeatures(Entry entry, NASequence naSequence) {

        // Creating the features

        for (NAFeature naFeature : naSequence.getAllFeature()) {
            if (naFeature.getClassTypeIri().toLowerCase().endsWith("/gene")) {
                // Gene part
                logger.debug("Creating gene entry");
                Gene gene = (Gene) naFeature;
                GeneCreator(gene, entry);

                // Transcript part
                List<? extends Transcript> transcripts = gene.getAllTranscript();
                for (Transcript transcript : transcripts) {
                    logger.debug("Creating transcript entry");
                    TranscriptCreator(gene, transcript, entry);
                }

            } else if (naFeature.getClassTypeIri().toLowerCase().endsWith("/exon")) {
                logger.debug("Skipping exon entry");
            } else if (naFeature.getClassTypeIri().toLowerCase().endsWith("/intron")) {
                logger.debug("Skipping intron entry");
            } else if (naFeature.getClassTypeIri().toLowerCase().endsWith("/knownlengthassemblygap")) {
                KnownLengthAssemblyGap assemblyGap = (KnownLengthAssemblyGap) naFeature;
                GapCreator(entry, assemblyGap);
            } else if (naFeature.getClassTypeIri().toLowerCase().endsWith("/repeatregion")) {
                RepeatRegion repeatRegion = (RepeatRegion) naFeature;
                RepeatCreator(entry, repeatRegion);
            } else if (naFeature.getClassTypeIri().toLowerCase().endsWith("/replicationorigin")) {
                logger.info("TODO: replication origin");
            } else {
                logger.warn("NAFeature unknown: " + naFeature.getClassTypeIri());
            }
        }
    }


    /**
     * Not complete yet
     *
     * @param entry        embl entry
     * @param repeatRegion gbol region object
     */
    private static void RepeatCreator(Entry entry, RepeatRegion repeatRegion) {
        Feature repeatFeature = featureFactory.createFeature("repeat_region", true);
        repeatFeature = SetPositions(repeatFeature, repeatRegion.getLocation());
        entry.addFeature(repeatFeature);

    }

    private static void GapCreator(Entry entry, KnownLengthAssemblyGap assemblyGap) {
        Feature gapFeature = featureFactory.createFeature("assembly_gap", true);
        gapFeature = SetPositions(gapFeature, assemblyGap.getLocation());
        Region region = (Region) assemblyGap.getLocation();
        ExactPosition exact = (ExactPosition) region.getEnd();
        Long length = exact.getPosition();
        exact = (ExactPosition) region.getBegin();
        length = length - exact.getPosition() + 1;
        gapFeature.addQualifier("estimated_length", String.valueOf(length));
        if (assemblyGap.getGapType().getIRI().toLowerCase().endsWith("/unknownGap")) {
            gapFeature.addQualifier("gap_type", "unknown");
        } else {
            Conversion.message(logger, "Gap type not captured properly");
        }
        entry.addFeature(gapFeature);
    }

//
//
//    if (1==1) {
//      return;
//    }
//
//
//    for (ResultLine item : RDFResource.runQuery(hdt, "getContigs.txt")) {
//      Entry entry = (new EntryFactory()).createEntry();
//      // TODO no error, but also not adding to id
//      // TODO this is NOT the ID. (for id line)
//      if (item.getLitString("accession") != null)
//        entry.setId(item.getLitString("accession"));
//
//      // ADDING DIVISION NAME TO ID (for id line)
//      if (item.getLitString("division") != null)
//        entry.setDivision(item.getLitString("division"));
//
//      // ADDING DATACLASS NAME TO ID (for id line)
//      if (item.getLitString("dataclass") != null)
//        entry.setDataClass(item.getLitString("dataclass"));
//
//      // ADDING ACCESSION
//      if (item.getLitString("accession") != null)
//        entry.setPrimaryAccession(item.getLitString("accession"));
//
//      // ADDING DATES
//      SimpleDateFormat fromTtl = new SimpleDateFormat("yyyy-MM-dd");
//      SimpleDateFormat toEmbl = new SimpleDateFormat("dd-MMM-yyyy");
//
//      if (item.getLitString("fp") != null) {
//        String date = toEmbl.format(fromTtl.parse(item.getLitString("fp")));
//        entry.setFirstPublic(FlatFileUtils.getDay(date));
//        entry.setFirstPublicRelease(item.getLitInt("fpr"));
//      }
//
//      if (item.getLitString("lu") != null) {
//        String date = toEmbl.format(fromTtl.parse(item.getLitString("lu")));
//        entry.setLastUpdated(FlatFileUtils.getDay(date));
//        entry.setLastUpdatedRelease(item.getLitInt("lur"));
//      }
//      if (item.getLitString("version") != null)
//        entry.setVersion(item.getLitInt("version"));
//
//      // ADDING DESCRIPTION
//      if (item.getLitString("description") != null) {
//        Text description = new Text();
//        description.setText(item.getLitString("description"));
//        entry.setDescription(description);
//      } else {
//        Text description = new Text();
//        description.setText("No description available");
//        entry.setDescription(description);
//      }
//
//      // ADDING KEYWORDS
//      // TODO main collections is outside loop, will give errors??
//      Collection<Text> keywords = new ArrayList<Text>();
//      for (ResultLine key : RDFResource.runQuery(hdt, "getKeywords.txt", item.getIRI("naobject"))) {
//        Text keyword = new Text();
//        keyword.setText(key.getLitString("keyword"));
//        keywords.add(keyword);
//      }
//      entry.addKeywords(keywords);
//
//
//      // TODO ADDING CITATIONS (ARE PRESENT, NOT PARSABLE)
//      // only has authors and title (year is wrong)
//
//      // ADDING FEATURES
//      entry = Features.add(RDFResource, hdt, entry, item.getIRI("naobject"), featureFactory);
//
//      // ADDING SEQUENCE
//      entry.setSequence(sequenceFactory
//          .createSequenceByte(item.getLitString("sequence").toLowerCase().getBytes()));
//      // entry.setSequence(sequenceFactory.createSequenceByte("aaaccggtt".toLowerCase().getBytes()));
//
//      // Setting sequence version (for id line)
//      if (item.getLitString("version") != null)
//        entry.getSequence().setVersion(item.getLitInt("version"));
//
//      // Setting molecule type (for id line)
//      if (item.getLitString("moleculetype") != null)
//        entry.getSequence().setMoleculeType(item.getLitString("moleculetype"));
//
//      // Setting topology (for id line)
//      if (item.getIRI("topology") != null) {
//        if (item.getIRI("topology").contains("Linear")) {
//          entry.getSequence().setTopology(Topology.LINEAR);
//        } else if (item.getIRI("topology").contains("Circular")) {
//          entry.getSequence().setTopology(Topology.CIRCULAR);
//        } else {
//          throw new Exception("Topology is of..." + item.getIRI("topology"));
//        }
//      }
//      // CREATING THE SOURCE ENTRY...
//      entry = Source(RDFResource, hdt, item.getIRI("naobject"), entry, featureFactory,
//          locationFactory, qualifierFactory);
//
//      // TODO adding xrefs to gene?
//      entry = getGeneXrefs(entry, hdt, item.getIRI("naobject"));
//
//      StringWriter writer = new StringWriter();
//      new EmblEntryWriter(entry).write(writer);
//
//      /*
//       * Writing to file
//       */
//      if (new File(arguments.output.getAbsolutePath()).exists())
//        Files.write(Paths.get(arguments.output.getAbsolutePath()), writer.toString().getBytes(),
//            StandardOpenOption.APPEND);
//      else
//        Files.write(Paths.get(arguments.output.getAbsolutePath()), writer.toString().getBytes());
//    }
//
//    File filename = new File(arguments.output.getAbsolutePath());
//    String encoding = "UTF-8";
//    InputStream fileStream = new FileInputStream(filename);
//    Reader decoder = new InputStreamReader(fileStream, encoding);
//    BufferedReader reader = new BufferedReader(decoder);
//
//    logger.info("Validating embl file..");
//    EmblEntryReader emblReader = new EmblEntryReader(reader);
//    ValidationResult validations = emblReader.read();
//    if (!validations.getMessages().isEmpty()) {
//      Iterator<ValidationMessage<Origin>> valiter = validations.getMessages().iterator();
//      while (valiter.hasNext()) {
//        ValidationMessage<Origin> validation = valiter.next();
//        // TODO enable in the end...
//        logger.warn(validation.getMessage());
//      }
//    } else {
//      logger.warn("EMBL files seems to be good....");
//    }

    private static void TranscriptCreator(Gene gene, Transcript transcript, Entry entry) {

        Feature feature = null;
        CdsFeature cdsFeature = null;

        if (transcript.getClassTypeIri().toLowerCase().endsWith("mrna") && !transcript.getClassTypeIri().toLowerCase().endsWith("tmrna")) {
            mRNA mrna = (mRNA) transcript;
            feature = featureFactory.createFeature("mrna", true);
            if (mrna.getFunction() != null) {
                feature.addQualifier("function", mrna.getFunction());
            }
            if (gene.getLocusTag() != null) {
                feature.addQualifier("locus_tag", gene.getLocusTag());
            }

            for (TranscriptFeature transcriptFeature : mrna.getAllFeature()) {
                if (transcriptFeature.getClassTypeIri().toLowerCase().endsWith("/cds")) {
                    logger.debug("Creating CDS entry");
                    CDS cds = (CDS) transcriptFeature;
                    cdsFeature = featureFactory.createCdsFeature();
                    cdsFeature.setTranslationTable(arguments.translTable);
                    cdsFeature.setTranslation(cds.getProtein().getSequence().replaceAll("\\*", "X"));
                    ProteinAnnotation(cds, cdsFeature);
                }
            }

            // Setting tRNAs
        } else if (transcript.getClassTypeIri().toLowerCase().endsWith("tmrna")) {
            tmRNA tmRNA = (tmRNA) transcript;
            feature = featureFactory.createFeature("tmrna", true);
            if (tmRNA.getFunction() != null) {
                feature.addQualifier("function", tmRNA.getFunction());
            }
            if (tmRNA.getProduct() != null) {
                feature.addQualifier("product", tmRNA.getProduct());
            }

            if (gene.getLocusTag() != null) {
                feature.addQualifier("locus_tag", gene.getLocusTag());
            }
            // Setting rRNAs
        } else if (transcript.getClassTypeIri().toLowerCase().endsWith("trna")) {
            tRNA trna = (tRNA) transcript;
            feature = featureFactory.createFeature("trna", true);
            if (trna.getFunction() != null) {
                feature.addQualifier("function", trna.getFunction());
            }
            if (gene.getLocusTag() != null) {
                feature.addQualifier("locus_tag", gene.getLocusTag());
            }
            if (trna.getProduct() != null) {
                feature.addQualifier("product", trna.getProduct());
            }

            // Setting rRNAs
        } else if (transcript.getClassTypeIri().toLowerCase().endsWith("rrna")) {
            rRNA rrna = (rRNA) transcript;
            feature = featureFactory.createFeature("rrna", true);
            if (rrna.getFunction() != null) {
                feature.addQualifier("function", rrna.getFunction());
            }
            if (gene.getLocusTag() != null) {
                feature.addQualifier("locus_tag", gene.getLocusTag());
            }
            if (rrna.getProduct() != null) {
                feature.addQualifier("product", rrna.getProduct());
            }

        } else {
            logger.warn("Transcript type skipped: " + transcript.getClassTypeIri());
        }

        // Setting exons
        if (feature != null) {
            Iterator<? extends Exon> exonIter = transcript.getExonList().getAllExon().iterator();
            int count = 0;
            while (exonIter.hasNext()) {
                count = count + 1;
                Exon exon = exonIter.next();
                SetPositions(feature, exon.getLocation());
                if (cdsFeature != null) {
                    cdsFeature = (CdsFeature) SetPositions(cdsFeature, exon.getLocation());
                }
            }
            entry.addFeature(feature);

            if (cdsFeature != null) {
                entry.addFeature(cdsFeature);
            }
        }
//    String locType = transcript.getLocation().getClassTypeIri();

//    SetPositionsGene(feature, gene.getLocation());

//    entry.setPrimaryAccession("PRIMACC");
//    Provenance(feature, gene.getAllProvenance());

    }

    private static void ProteinAnnotation(CDS cds, CdsFeature cdsFeature) {
        Protein protein = cds.getProtein();

        // Detect EC level
        Iterator<? extends life.gbol.domain.XRef> xrefIter = protein.getAllXref().iterator();
        // Number of -.-.-.- in ecs
        int ecCount = 4;
        while (xrefIter.hasNext()) {
            life.gbol.domain.XRef proteinFeature = xrefIter.next();
            if (proteinFeature.getDb().getId().matches("ec")) {
                int matches = StringUtils.countMatches(proteinFeature.getAccession(), "-");
                if (matches < ecCount) {
                    ecCount = matches;
                }
            }
        }

        Iterator<? extends ProteinFeature> featIter = protein.getAllFeature().iterator();

        // Annotate the protein...
        cdsFeature.addQualifier("product", cds.getProduct());

        // Add sha key when wanted
        if (arguments.sha)
            cdsFeature.addQualifier("shakey", protein.getSha384());

        // Setting the provenance

        HashSet<String> xrefs = new HashSet<>();
        while (featIter.hasNext()) {
            ProteinFeature proteinFeature = featIter.next();
//            for (FeatureProvenance featureProvenance : proteinFeature.getAllProvenance()) {
//                AnnotationSoftware annotationSoftware = (AnnotationSoftware) featureProvenance.getOrigin().getWasAttributedTo();
//                String name = annotationSoftware.getName();
//                String version = annotationSoftware.getVersion();
//                String annotType = featureProvenance.getAnnotation().getClassTypeIri();
//                if (annotType.toLowerCase().endsWith("interproscan")) {
//                    InterProScan interProScan = (InterProScan) featureProvenance.getAnnotation();
//                    //          String note = "Evalue:"+ interProScan.getEvalue();
//                    //          cdsFeature.addQualifier("note","inference:"+name+":"+version + " " + note);
//                    //          cdsFeature.addQualifier("inference", "protein motif:" + name + ":" + version);
//                } else if (annotType.toLowerCase().endsWith("enzdp")) {
//                    ENZDP enzdp = (ENZDP) featureProvenance.getAnnotation();
//                } else {
//                    // logger.warn("FeatureProvenance of protein not known: " + annotType);
//                }
//            }

            // Setting the DB / Accessions
            for (life.gbol.domain.XRef xref : proteinFeature.getAllXref()) {
                Database db = xref.getDb();
                String acc = xref.getAccession();
                String dbid = db.getId();

                if (dbid.matches("gene3d")) {
                    acc = acc.replace("G3DSA", "");
                }

                if (dbid.matches("pfam|gene3d|superfamily|cdd")) {
                    cdsFeature.addQualifier("inference", "protein motif:" + dbid + ":" + acc);
                    // Skip xref as it is now added as inference
                    acc = null;
                }

                if (dbid.matches("go")) {
                    acc = acc.replaceAll("GO:", "");
                }

                if (dbid.matches("ec")) {
                    if (StringUtils.countMatches(acc, "-") != ecCount) {
                        acc = null;
                    }
                }

                if (dbid.matches("mobidblite")) {
                    acc = null;
                }

                if (acc != null && !xrefs.contains(dbid + ":" + acc)) {
                    XRef xRef = new XRef();
                    xRef.setDatabase(dbid);
                    xRef.setPrimaryAccession(acc);
                    cdsFeature.addXRef(xRef);
                }

                // Making sure no duplicates are present
                xrefs.add(dbid + ":" + acc);
            }
        }
    }

    private static void GeneCreator(Gene gene, Entry entry) {
        logger.debug(gene.getResource().getURI());
        String locType = gene.getLocation().getClassTypeIri();
        Feature feature = featureFactory.createFeature("gene", false);
        feature.addQualifier("locus_tag", gene.getLocusTag());
        SetPositions(feature, gene.getLocation());
        entry.addFeature(feature);
        Provenance(feature, gene.getAllProvenance());
    }

    private static void Provenance(Feature feature, List<? extends FeatureProvenance> allProvenance) {
        for (FeatureProvenance featureProvenance : allProvenance) {
            AnnotationSoftware annotationSoftware = (AnnotationSoftware) featureProvenance.getOrigin().getWasAttributedTo();
            String name = annotationSoftware.getName();
            String version = annotationSoftware.getVersion();
            String annotType = "unknown";
            if (featureProvenance.getAnnotation() != null)
                annotType = featureProvenance.getAnnotation().getClassTypeIri();

            // TODO fix qualifiers
            if (annotType.endsWith("Prodigal")) {
                // RDFList rdfList = featureProvenance.getResource().getModel().getList(featureProvenance.getAnnotation().getResource());
                // String note = "Conf:" + prodigal.getConf();
                // feature.addQualifier("note", "ab initio prediction:" + name + ":" + version + " " + note);
                feature.addQualifier("note", "ab initio prediction:" + name + ":" + version);
                feature.addQualifier("inference", "ab initio prediction:" + name + ":" + version);
            } else if (annotType.endsWith("Aragorn")) {
                // Aragorn aragorn = (Aragorn) featureProvenance.getAnnotation();
                feature.addQualifier("inference", "ab initio prediction:" + name + ":" + version);
            } else if (annotType.endsWith("RNAmmer")) {
                // RNAmmer rnammer = (RNAmmer) featureProvenance.getAnnotation();
                feature.addQualifier("inference", "ab initio prediction:" + name + ":" + version);
                // String note = "Score:" + rnammer.getScore();
                // feature.addQualifier("note", "ab initio prediction:" + name + ":" + version + " " + note);
                feature.addQualifier("note", "ab initio prediction:" + name + ":" + version);
            } else if (annotType.endsWith("unknown")) {

            } else {
                logger.warn("Annotation type unknown: " + annotType);
            }
        }
    }

    private static Feature SetPositions(Feature feature, Location location) {
        // Setting the region of an object
        Position begin = null;
        Position end = null;
        long beginpos = 0;
        long endpos = 0;
        boolean complement = Boolean.parseBoolean(null);
        if (location.getClassTypeIri().endsWith("Region")) {
            Region region = (Region) location;
            begin = region.getBegin();
            end = region.getEnd();
            complement = !region.getStrand().getIRI().endsWith("ForwardStrandPosition");
        } else {
            logger.warn("Location type not known: " + location.getClassTypeIri());
        }

        if (begin.getClassTypeIri().endsWith("ExactPosition")) {
            ExactPosition exactpos = (ExactPosition) begin;
            beginpos = exactpos.getPosition();
        } else if (begin.getClassTypeIri().endsWith("BeforePosition")) {
            // TODO create <...
            BeforePosition beforepos = (BeforePosition) begin;
            beginpos = beforepos.getPosition();
        } else {
            logger.warn("Different type detected: " + begin.getClassTypeIri());
        }

        if (end.getClassTypeIri().endsWith("ExactPosition")) {
            ExactPosition exactpos = (ExactPosition) end;
            endpos = exactpos.getPosition();
        } else if (end.getClassTypeIri().endsWith("AfterPosition")) {
            // TODO create >...
            AfterPosition afterpos = (AfterPosition) end;
            endpos = afterpos.getPosition();
        } else {
            logger.warn("Different type detected: " + end.getClassTypeIri());
        }

        feature.getLocations().addLocation(locationFactory.createLocalRange(beginpos, endpos, complement));
        return feature;

    }

    private static Entry Source(Entry entry, NASequence naSequence) {

        // Creating the source feature
        SourceFeature sourceFeature = featureFactory.createSourceFeature();
        // TODO ADDING TAXONOMY


        sourceFeature.getLocations().addLocation(locationFactory.createLocalRange(1L, naSequence.getLength(), false));

        // Setting scientific name
        try {
            sourceFeature.setScientificName(naSequence.getOrganism().getScientificName());
        } catch (RuntimeException e) {
            sourceFeature.setScientificName("<<<Scientific Name>>>");
        }

        // Setting lineage
        // primarySourceFeature.getTaxon().getLineage());
        TaxonFactory taxonFactory = new TaxonFactory();
        Taxon taxon = taxonFactory.createTaxon();
        try {
            if (naSequence.getOrganism() != null && naSequence.getOrganism().getAllTaxonomyLineage() != null) {
                String lineage = "";

                for (life.gbol.domain.Taxon taxonLineage : naSequence.getOrganism().getAllTaxonomyLineage()) {
                    lineage = lineage + ";" + taxonLineage.getTaxonName();
                }
                lineage = lineage.replaceFirst(";", "");
//            System.out.println(lineage);
//            System.out.println(taxon);
                sourceFeature.setTaxon(taxon);
                taxon.setLineage(lineage);
            }
        } catch (RuntimeException e) {
            sourceFeature.setTaxon(taxon);
            taxon.setLineage("<<<Unknown; Lineage>>>");
        }

        // Setting host
        try {
            if (naSequence.getSample().getHost() != null) {
                sourceFeature.setSingleQualifierValue("host", naSequence.getSample().getHost().getAccession());
            }
        } catch (RuntimeException e) {

        }


        // Setting organism
        try {
            sourceFeature.setSingleQualifierValue("organism", naSequence.getOrganism().getScientificName());
        } catch (RuntimeException e) {
            sourceFeature.setSingleQualifierValue("organism", "<<<Unknown Organism>>>");
        }

        // Setting country
        try {
            if (naSequence.getSample().getGeographicalLocation() != null) {
                sourceFeature.setSingleQualifierValue("country", naSequence.getSample().getGeographicalLocation().getRegion());
            }
        } catch (RuntimeException e) {

        }

        // Setting strain
        try {
            String strain = naSequence.getSample().getStrain();
            if (strain.length() > 0)
                sourceFeature.setSingleQualifierValue("strain", strain);
            else
                sourceFeature.setSingleQualifierValue("strain", "<<<Unknown Strain>>>");

        } catch (RuntimeException e) {
            sourceFeature.setSingleQualifierValue("strain", "<<<Unknown Strain>>>");
        }

        // Setting segment
        try {
            if (naSequence.getSample().getSegment() != null) {
                sourceFeature.setSingleQualifierValue("segment", naSequence.getSample().getSegment());
            }
        } catch (RuntimeException e) {

        }


        // Setting isolate
        try {
            if (naSequence.getSample().getIsolate() != null) {
                sourceFeature.setSingleQualifierValue("isolate", naSequence.getSample().getIsolate());
            }
        } catch (RuntimeException e) {

        }


        // TODO double check: Setting tax id
        try {
            XRef xRef = new XRef();
            xRef.setDatabase("taxon");
            xRef.setPrimaryAccession(naSequence.getOrganism().getTaxonomy().getAccession());
            sourceFeature.addXRef(xRef);
        } catch (RuntimeException e) {
            XRef xRef = new XRef();
            xRef.setDatabase("taxon");
            xRef.setPrimaryAccession("-1");
            sourceFeature.addXRef(xRef);
        }

        entry.addFeature(sourceFeature);

        return entry;
    }

//    private static Entry getGeneXrefs(Entry entry, HDT hdt, String featureIRI) throws Exception {
//        for (ResultLine xrefs : RDFResource.runQuery(hdt, "getGeneXrefs.txt", featureIRI)) {
//            String xrefIRI = xrefs.getIRI("xrefs");
//            for (ResultLine xref : RDFResource.runQuery(hdt, "getGeneXref.txt", xrefIRI)) {
//                XRef xRef = new XRef();
//                xRef.setDatabase(xref.getLitString("db"));
//                xRef.setPrimaryAccession(xref.getLitString("pa"));
//                if (xref.getLitString("sa") != null) {
//                    xRef.setSecondaryAccession(xref.getLitString("sa"));
//                }
//                entry.addXRef(xRef);
//            }
//        }
//        return entry;
//    }
}
