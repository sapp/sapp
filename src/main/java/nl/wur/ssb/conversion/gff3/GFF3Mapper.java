package nl.wur.ssb.conversion.gff3;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.InputOutput.Output;
import nl.wur.ssb.conversion.Conversion;
import nl.wur.ssb.conversion.options.CommandOptionsGFF3;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.entry.EntryFactory;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.feature.FeatureFactory;
import uk.ac.ebi.embl.api.entry.location.Join;
import uk.ac.ebi.embl.api.entry.location.Location;
import uk.ac.ebi.embl.api.entry.location.LocationFactory;
import uk.ac.ebi.embl.api.entry.qualifier.QualifierFactory;
import uk.ac.ebi.embl.api.entry.sequence.SequenceFactory;
import uk.ac.ebi.embl.api.gff3.GFF3Record;
import uk.ac.ebi.embl.api.gff3.GFF3RecordSet;
import uk.ac.ebi.embl.flatfile.writer.embl.EmblEntryWriter;
import uk.ac.ebi.embl.gff3.reader.GFF3FlatFileEntryReader;

import java.io.*;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.GZIPInputStream;

public class GFF3Mapper {

    private static final Logger logger = LogManager.getLogger(GFF3Mapper.class);
    public static CommandOptionsGFF3 arguments;
    private final String[] gbolClasses = {
            "nl.wur.ssb.conversion.gbolclasses.features.ArtificialRecognizedRegion", "nl.wur.ssb.conversion.gbolclasses.features.AssemblyAnnotation", "nl.wur.ssb.conversion.gbolclasses.features.AssemblyGap", "nl.wur.ssb.conversion.gbolclasses.features.BiologicalRecognizedRegion", "nl.wur.ssb.conversion.gbolclasses.features.CDS", "nl.wur.ssb.conversion.gbolclasses.features.Centromere",
            //      "nl.wur.ssb.conversion.gbolclasses.features.ConstantRegion",
            "nl.wur.ssb.conversion.gbolclasses.features.CRISPRCassette",
            //      "nl.wur.ssb.conversion.gbolclasses.features.DiversitySegment",
            "nl.wur.ssb.conversion.gbolclasses.features.DLoop", "nl.wur.ssb.conversion.gbolclasses.features.Exon", "nl.wur.ssb.conversion.gbolclasses.features.Feature", "nl.wur.ssb.conversion.gbolclasses.features.FivePrimeUTR",
            //      "nl.wur.ssb.conversion.gbolclasses.features.Gap",
            //      "nl.wur.ssb.conversion.gbolclasses.features.GapRegion",
            "nl.wur.ssb.conversion.gbolclasses.features.Gene", "nl.wur.ssb.conversion.gbolclasses.features.GeneralFeature", "nl.wur.ssb.conversion.gbolclasses.features.GenomicFeature", "nl.wur.ssb.conversion.gbolclasses.features.Homology", "nl.wur.ssb.conversion.gbolclasses.features.ImmunoglobulinFeature", "nl.wur.ssb.conversion.gbolclasses.features.IntegratedVirus", "nl.wur.ssb.conversion.gbolclasses.features.Intron",
            //      "nl.wur.ssb.conversion.gbolclasses.features.JoiningSegment",
            "nl.wur.ssb.conversion.gbolclasses.features.MaturePeptide", "nl.wur.ssb.conversion.gbolclasses.features.MiscBinding", "nl.wur.ssb.conversion.gbolclasses.features.MiscFeature", "nl.wur.ssb.conversion.gbolclasses.features.MiscRecomb", "nl.wur.ssb.conversion.gbolclasses.features.MiscStructure", "nl.wur.ssb.conversion.gbolclasses.features.MiscVariation", "nl.wur.ssb.conversion.gbolclasses.features.MobileElement", "nl.wur.ssb.conversion.gbolclasses.features.ModifiedBase", "nl.wur.ssb.conversion.gbolclasses.features.ModifiedResidue", "nl.wur.ssb.conversion.gbolclasses.features.NAFeature", "nl.wur.ssb.conversion.gbolclasses.features.NaturalVariation", "nl.wur.ssb.conversion.gbolclasses.features.Operon", "nl.wur.ssb.conversion.gbolclasses.features.PolyASite", "nl.wur.ssb.conversion.gbolclasses.features.PrimerBinding", "nl.wur.ssb.conversion.gbolclasses.features.ProteinBinding", "nl.wur.ssb.conversion.gbolclasses.features.ProteinFeature", "nl.wur.ssb.conversion.gbolclasses.features.ProteinHomology", "nl.wur.ssb.conversion.gbolclasses.features.ProteinRepeat", "nl.wur.ssb.conversion.gbolclasses.features.ProteinStructure", "nl.wur.ssb.conversion.gbolclasses.features.RecognizedRegion", "nl.wur.ssb.conversion.gbolclasses.features.RegulationSite", "nl.wur.ssb.conversion.gbolclasses.features.RepeatFeature", "nl.wur.ssb.conversion.gbolclasses.features.RepeatRegion", "nl.wur.ssb.conversion.gbolclasses.features.ReplicationOrigin", "nl.wur.ssb.conversion.gbolclasses.features.SequenceAnnotation", "nl.wur.ssb.conversion.gbolclasses.features.SequenceTaggedSite", "nl.wur.ssb.conversion.gbolclasses.features.SignalPeptide", "nl.wur.ssb.conversion.gbolclasses.features.Source", "nl.wur.ssb.conversion.gbolclasses.features.StemLoop", "nl.wur.ssb.conversion.gbolclasses.features.StructureFeature",
            //      "nl.wur.ssb.conversion.gbolclasses.features.SwitchRegion",
            "nl.wur.ssb.conversion.gbolclasses.features.Telomere", "nl.wur.ssb.conversion.gbolclasses.features.ThreePrimeUTR", "nl.wur.ssb.conversion.gbolclasses.features.TranscriptFeature", "nl.wur.ssb.conversion.gbolclasses.features.TranscriptionElement", "nl.wur.ssb.conversion.gbolclasses.features.TransferOrigin", "nl.wur.ssb.conversion.gbolclasses.features.TransMembraneRegion",
            //      "nl.wur.ssb.conversion.gbolclasses.features.UnknownAssemblyGap",
            "nl.wur.ssb.conversion.gbolclasses.features.UnsureBases", "nl.wur.ssb.conversion.gbolclasses.features.UpdatedSequence",
            //      "nl.wur.ssb.conversion.gbolclasses.features.VariableRegion",
            //      "nl.wur.ssb.conversion.gbolclasses.features.VariableSegment",
            "nl.wur.ssb.conversion.gbolclasses.features.VariationFeature", "nl.wur.ssb.conversion.gbolclasses.Sample", "nl.wur.ssb.conversion.gbolclasses.sequences.Chromosome", "nl.wur.ssb.conversion.gbolclasses.sequences.CompleteNASequence", "nl.wur.ssb.conversion.gbolclasses.sequences.Contig", "nl.wur.ssb.conversion.gbolclasses.sequences.MaturedRNA", "nl.wur.ssb.conversion.gbolclasses.sequences.MiscRna", "nl.wur.ssb.conversion.gbolclasses.sequences.mRNA", "nl.wur.ssb.conversion.gbolclasses.sequences.NASequence", "nl.wur.ssb.conversion.gbolclasses.sequences.ncRNA", "nl.wur.ssb.conversion.gbolclasses.sequences.Plasmid", "nl.wur.ssb.conversion.gbolclasses.sequences.PrecursorRNA", "nl.wur.ssb.conversion.gbolclasses.sequences.Read", "nl.wur.ssb.conversion.gbolclasses.sequences.rRNA", "nl.wur.ssb.conversion.gbolclasses.sequences.Scaffold", "nl.wur.ssb.conversion.gbolclasses.sequences.Sequence", "nl.wur.ssb.conversion.gbolclasses.sequences.tmRNA", "nl.wur.ssb.conversion.gbolclasses.sequences.Transcript", "nl.wur.ssb.conversion.gbolclasses.sequences.tRNA", "nl.wur.ssb.conversion.gbolclasses.sequences.UncompleteNASequence", "nl.wur.ssb.conversion.gbolclasses.Thing"};
    private final HashMap<String, String> allowed = new HashMap<>();
    private final Set<String> missed = new HashSet<>();
    private final EntryFactory entryFactory = new EntryFactory();
    private final FeatureFactory featureFactory = new FeatureFactory();
    private final LocationFactory locationFactory = new LocationFactory();
    /*
     * # GFF3 FEATURES AND QUALIFIERS MAPPING
     *
     * gene=gene_embl
     *
     * TF_binding_site=TF_binding_site_embl
     *
     * mRNA=mRNA_embl
     *
     * five_prime_UTR=five_prime_UTR_embl
     *
     * CDS=CDS_embl
     *
     * exon=exon_embl
     *
     * three_prime_UTR=three_prime_UTR_embl
     */
    private final Domain fastaDomain;
    QualifierFactory qualifierFactory = new QualifierFactory();
    String resourceBundle = "uk.ac.ebi.embl.gff3.mapping.gffMapper";
    private Entry entry;
    private Feature feature;
    // StringWriter writer;
    private uk.ac.ebi.embl.api.entry.qualifier.Qualifier qualifier;

    public GFF3Mapper(String[] args) throws Exception {
        arguments = new CommandOptionsGFF3(args);
        if (arguments.debug)
            logger.atLevel(Level.DEBUG);

        logger.debug("Debug option enabled");
        getHandles();

        logger.info("Parsing FASTA");

        ArrayList<String> argList = new ArrayList<>();
        argList.addAll(Arrays.asList("-id", arguments.identifier, "-genome", "-contig",
                "-i", arguments.fasta.getAbsolutePath(), "-o", arguments.output.getAbsolutePath(),
                "-codon", String.valueOf(arguments.codon)));

        String[] argsArr = new String[argList.size()];
        argsArr = argList.toArray(argsArr);
        nl.wur.ssb.conversion.fasta2rdf.Fasta.app(argsArr);

        // Load RDF file
        fastaDomain = new Domain("file://" + arguments.output);
        logger.info("Loaded " + fastaDomain.getRDFSimpleCon().getModel().size() + " triples ");

        logger.info("Parsing GFF");

        BufferedReader buffered;

        // Setting up the file reader (txt or gz)
        if (arguments.input.getName().endsWith(".gz")) {
            InputStream fileStream = new FileInputStream(arguments.input);
            InputStream gzipStream = new GZIPInputStream(fileStream);
            Reader decoder = new InputStreamReader(gzipStream, StandardCharsets.UTF_8);
            buffered = new BufferedReader(decoder);
        } else {
            InputStream fileStream = new FileInputStream(arguments.input);
            Reader decoder = new InputStreamReader(fileStream, StandardCharsets.UTF_8);
            buffered = new BufferedReader(decoder);
        }

        // Multiple writers
        Map<String, PrintWriter> gffSplitter = new HashMap<>();

        // Split the file into batches of at least 10mb and into complete contig parts
        String previousID = null;
        String currentID = null;
        File currentFile = null;
        PrintWriter currentFileWriter = null;
        String line = "";
        while (line != null) {
            line = buffered.readLine();

            if (line == null) {
                break;
            }

            if (!line.startsWith("#") && line.length() > 1) {
                // Sometimes fasta is embedded, skip that part for now
                if (line.startsWith(">")) {
                    break;
                }
                // TODO identify problem with start site
                if (line.contains("transcription_start_site")) continue;
                if (line.contains("transcription_end_site")) continue;

                currentID = line.split("\\s")[0];
                // First line ...
                if (previousID == null) {
                    currentFile = new File(line.split("\\s")[0] + ".tmp.gff");
                    currentFileWriter = new PrintWriter(currentFile);
                    previousID = currentID;
                    gffSplitter.put(currentFile.getName(), currentFileWriter);
                }

                // If they are the same the line needs to be written always
                if (previousID.matches(currentID)) {
                    currentFileWriter.println(line);
                } else {
                    // Check filesize to decide if we need to start a new file
                    if (currentFile.length() > (1024 * 1024)) {
                        currentFile = new File(line.split("\\s")[0] + ".tmp.gff");
                        System.out.println("Starting new file: " + currentFile);
                        currentFileWriter.close();
                        currentFileWriter = new PrintWriter(currentFile);
                        currentFileWriter.println(line);
                        gffSplitter.put(currentFile.getName(), currentFileWriter);
                    } else {
                        currentFileWriter.println(line);
                    }
                }
            }
            previousID = currentID;
        }
        currentFileWriter.close();

        logger.info("Splitting GFF completed");

        // Parsing each file, writing it to RDF and closing them if the RDF file does not exist
        HashSet<File> emblFiles = new HashSet<>();

        for (String key : gffSplitter.keySet()) {
            // For intermediate embl file
            StringWriter writer = new StringWriter();

            System.err.print("Parsing file: " + key + " of in total: " + gffSplitter.keySet().size() + "\r");
            // Making sure the file is closed
            gffSplitter.get(key).close();

            BufferedReader fileReader = new BufferedReader(new FileReader(key));
            GFF3FlatFileEntryReader reader = new GFF3FlatFileEntryReader(fileReader);
            // Skipping validation (for large files in the future...)
            reader.skip(); //.read();
            GFF3RecordSet records = reader.getEntry();

            logger.info("Parsing GFF " + records.getRecords().size() + " records");

            List<Entry> entryList = mapGFF3ToEntry(records);

            if (missed.size() > 0) {
                logger.warn("The following properties are not captured by the ontology: " + StringUtils.join(missed, " "));
            }

            /*
            Section to convert GFF to EMBL... to use as an intermediate stasis
             */

            File emblFile = new File(arguments.output + "." + key + ".embl");

            if (!arguments.debug && !arguments.gff2embl) {
                emblFile.deleteOnExit();
            }

            logger.info("Writing to intermediate EMBL file... " + emblFile.getAbsolutePath());

            for (Entry entry : entryList) {
                new EmblEntryWriter(entry).write(writer);
            }

            // Will not happen anymore...?... on code kill?
            if (emblFile.exists()) {
                throw new Exception("File should not exists, bug in code detected " + emblFile); //Files.write(Paths.get(emblFile.getAbsolutePath()), writer.toString().getBytes(), StandardOpenOption.APPEND);
            } else {
                Files.write(Paths.get(emblFile.getAbsolutePath()), writer.toString().getBytes());
            }

            writer.close();
            emblFiles.add(emblFile);
        }

        // Perform conversion of EMBL to RDF
        for (File emblFile : emblFiles) {
            logger.info("Converting " + emblFile);
            ArrayList<String> emblArray = new ArrayList<>();
            emblArray.addAll(Arrays.asList("-embl2rdf", "-input", emblFile.getAbsolutePath(), "-identifier", arguments.identifier, "-output", emblFile + ".ttl", "-codon", String.valueOf(arguments.codon)));
            String[] emblArgs = new String[emblArray.size()];
            emblArgs = emblArray.toArray(emblArgs);
            System.err.println(StringUtils.join(emblArgs, " "));
            Conversion.main(emblArgs);
        }

        // Collect all turtle files and load in disk store
        File tempFolder = new File(arguments.output + ".dir");
        tempFolder.mkdirs();
        Domain domain = new Domain("file://" + tempFolder);
        for (File emblFile : emblFiles) {
            logger.info("Parsing " + emblFile + ".ttl" + " with " + domain.getRDFSimpleCon().getModel().size() + " triples in the database");
            RDFDataMgr.read(domain.getRDFSimpleCon().getModel(), emblFile + ".ttl");
        }

        domain.getRDFSimpleCon().setNsPrefix("gbol", "http://gbol.life/0.1/");
        domain.getRDFSimpleCon().setNsPrefix("terms", "http://purl.org/dc/terms/");
        domain.getRDFSimpleCon().setNsPrefix("prov", "http://www.w3.org/ns/prov#");
        domain.getRDFSimpleCon().setNsPrefix("void", "http://rdfs.org/ns/void#");
        domain.getRDFSimpleCon().setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");

        Output.save(domain, arguments.output);

        // Cleanup!
        for (File emblFile : emblFiles) {
            while (emblFile.exists()) {
                emblFile.delete();
            }
            while (new File(emblFile + ".ttl").exists()) {
                new File(emblFile + ".ttl").delete();
            }
        }
        // Remove directory
        FileUtils.deleteDirectory(tempFolder);
    }

    private void getHandles() throws SecurityException, ClassNotFoundException {
        for (String clazzName : gbolClasses) {
            Class<?> clazz = Class.forName(clazzName);
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                String name = method.getName();
                if (name.contains("handle")) {
                    allowed.put(name, "");
                }
            }
        }
    }

    public List<Entry> mapGFF3ToEntry(GFF3RecordSet records) throws Exception {

        List<GFF3RecordElement> gff3RecordElements = new ArrayList<>();
        Set<String> ids = new TreeSet<>();
        HashMap<String, Entry> entries = new HashMap<>();

        // Translation when fasta and gff do not have matching contig identifiers
        HashMap<String, String> gff2fasta = new HashMap<>();

        for (GFF3Record record : records.getRecords()) {
            String id = null;
            String parent = null;
            Map<?, ?> attributes = record.getAttributes();
            Iterator<?> attributeIterator = attributes.entrySet().iterator();

            // Retrieval of parent and current identifier of line entry
            while (attributeIterator.hasNext()) {
                Map.Entry attributePairs = (Map.Entry) attributeIterator.next();
                String attributeKey = attributePairs.getKey().toString();
                String attributeValue = attributePairs.getValue().toString().toLowerCase();
                String attributeKeyLookup = WordUtils.capitalizeFully(attributeKey.replaceAll("_", " "));
                attributeKeyLookup = attributeKeyLookup.replaceAll(" +", "");

                if (attributeKey.toLowerCase().matches("parent")) {
                    parent = attributeValue;
                }

                if (attributeKey.toLowerCase().matches("id")) {
                    id = attributeValue;
                }
            }

            // ID null?
            if (id == null) {
                id = parent + "___" + record.getType();
            }

            // Only create new feature if ID does not exist yet... otherwise add the record to the id
            // Hereby CDS need identical names which could give issues in the feature
            // TODO Merge CDS based on parent ID not on element ID

            if (!ids.contains(id)) {
                ids.add(id);
                GFF3RecordElement gff3RecordElement = new GFF3RecordElement();
                gff3RecordElement.setId(id);
                gff3RecordElement.setParent(parent);

                // If exon or cds for mRNA positioning
                if (record.getType().toLowerCase().matches("exon")) {
                    gff3RecordElement.addExon(record);
                } else if (record.getType().toLowerCase().matches("cds")) {
                    gff3RecordElement.addCds(record);
                }

                // Redundant code but CDS / Exon are used for positioning of parent temporarily
                gff3RecordElement.addGFFRecord(record);

                gff3RecordElement.setSequenceID(record.getSequenceID());
                if (record.getType().toLowerCase().matches("(region|chromosome)")) {
                    gff3RecordElement.setFeature(featureFactory.createFeature("source"));
                } else if (record.getType().toLowerCase().endsWith("gene")) {
                    gff3RecordElement.setFeature(featureFactory.createFeature("gene"));
                } else if (record.getType().toLowerCase().endsWith("trna")) {
                    gff3RecordElement.setFeature(featureFactory.createFeature("trna"));
                } else if (record.getType().toLowerCase().endsWith("ncrna")) {
                    gff3RecordElement.setFeature(featureFactory.createFeature("ncrna"));
                } else if (record.getType().toLowerCase().endsWith("rrna")) {
                    gff3RecordElement.setFeature(featureFactory.createFeature("rrna"));
                } else if (record.getType().toLowerCase().endsWith("mrna")) {
                    gff3RecordElement.setFeature(featureFactory.createFeature("mrna"));
                    // All other RNA's...
                } else if (record.getType().toLowerCase().matches("(transcript|.*rna)")) {
                    // System.out.println(record.getType().toLowerCase());
                    gff3RecordElement.setFeature(featureFactory.createFeature("mrna"));
                } else {
                    // Interestingly... When creating a CDS object it automatically creates an empty locations
                    // object but not for the other types...
                    gff3RecordElement.setFeature(featureFactory.createFeature(record.getType()));
                }

                gff3RecordElements.add(gff3RecordElement);

                /*
                 * Entry store with fasta sequence...
                 */

                // Translation when fasta and gff do not have matching contig identifiers
                String sequenceID = gff3RecordElement.getSequenceID();
                if (gff2fasta.containsKey(sequenceID)) {
                    sequenceID = gff2fasta.get(sequenceID);
                }

                entry = entries.get(sequenceID);

                if (entry == null) {
                    entry = entryFactory.createEntry();
                    entry.setPrimaryAccession(sequenceID);
                    entry.setId(sequenceID);

                    logger.info("Parsing sequence id: " + sequenceID);
                    String sequence = fastaDomain.getRDFSimpleCon().runQuerySingleRes("getSequenceFromAccession.txt", true, sequenceID).getLitString("sequence");

                    SequenceFactory sequenceFactory = new SequenceFactory();
                    entry.setSequence(sequenceFactory.createSequenceByte(sequence.toLowerCase().getBytes()));
                    entry.getSequence().setAccession(sequenceID);

                    logger.info("Entry added with sequence");

                    entries.put(sequenceID, entry);
                }

                entry.addFeature(gff3RecordElement.getFeature());

            } else {
                // If the identifier already exists
                boolean check = false;
                for (GFF3RecordElement element : gff3RecordElements) {
                    if (element.getId().equals(id)) {
                        element.addGFFRecord(record);
                        if (record.getType().toLowerCase().matches("exon")) {
                            element.addExon(record);
                        } else if (record.getType().toLowerCase().matches("cds")) {
                            element.addCds(record);
                        } else if (record.getType().toLowerCase().matches("intron")) {
                            // TODO intron support?
                        }
                        check = true;
                        break;
                    }
                }

                if (!check) {
                    throw new Exception("Not found??...");
                }
            }
        }

        /*
         * Creation of combined records completed...
         */

        logger.info("Creation of combined records completed...");

        /*
         * For each child in the elementList (which is a GFF3RecordElement with parentid, id, genbank
         * feature object, gff record(s)
         */

        logger.info("Parental positioning...");

        // ElementList contains exons separately but CDS joined when same parent is detected
        int count = 0;
        for (GFF3RecordElement childRecord : gff3RecordElements) {
            count = count + 1;
            System.out.print(count + "\t" + gff3RecordElements.size() + "\r");
            // When a child has a parent class like exon? > mrna > gene
            if (childRecord.getParent() != null) {
                // System.out.println("childRecord ID : " + childRecord.getId());
                GFF3RecordElement parentRecord = null;
                // Getting the parental record
                for (GFF3RecordElement possibleParent : gff3RecordElements) {
                    if (possibleParent.getId().equals(childRecord.getParent())) {
                        // System.out.println(
                        // "possibleParent: " + possibleParent.getId() + " " + childRecord.getParent());
                        parentRecord = possibleParent;
                        break;
                    }
                }
                // When a parent record is found
                if (parentRecord != null) {
                    // Gene is ignored as the positioning is going to be fine...
                    if (parentRecord.getRecords().get(0).getType().toLowerCase().contains("gene")) {
                    }
                    // If parent is of type "RNA"
                    else if (parentRecord.getRecords().get(0).getType().toLowerCase().matches("(transcript|.*rna)")) {
                        String childType = childRecord.getRecords().get(0).getType().toLowerCase();
                        // Only use CDS or Exons for parent position fixing
                        if (childType.matches("cds") || childType.matches("exon")) {
                            //   System.err.println("Child type : " + childRecord.getRecords().get(0).getType());
                            // This is for CDS as well as for EXONS..!!!!

                            // Position needs to change based on the childs... when exons available

                            // Check if exons are available
                            if (childRecord.getExons() != null && childRecord.getExons().getRecords().size() > 0) {
                                records = childRecord.getExons();
                                // Check if otherwise CDS is available
                            } else if (childRecord.getCds() != null && childRecord.getCds().getRecords().size() > 0) {
                                records = childRecord.getCds();
                            }
                            //   System.out.println("Record size: " + records.getRecords().size());
                            //   System.out.println(
                            //   "testing..." + childRecord.getExons().getRecords().size() + "\t" + childRecord
                            //   .getCds().getRecords().size());

                            // There is an issue with for example exons they do not get an identifier and there for
                            // are all unique but the parent needs the positions thus the positions should be
                            // updated not overwritten

                            if (parentRecord.getFeature().getLocations() != null) {
                                // We already have set the location object now just add to it
                                for (GFF3Record gff3Record : records.getRecords()) {
                                    parentRecord.getFeature().getLocations().addLocation(createLocation(gff3Record));
                                }
                                // System.out.println(parentRecord.getFeature().getLocations().getLocations().size());

                            } else {
                                // If there is no location object yet...
                                Join<Location> compoundJoin = new Join<>();
                                for (GFF3Record gff3Record : records.getRecords()) {
                                    Location location = createLocation(gff3Record);
                                    compoundJoin.addLocation(location);
                                }
                                parentRecord.getFeature().setLocations(compoundJoin);
                                // System.out.println("parentRecord.getFeature().getLocations(): "+ parentRecord.getFeature().getLocations().getLocations().size());
                            }
                            // Parent is mRNA if it is CDS... get positions?
                        }
                    }
                }
            }
        }

        logger.info("Finalizing locations and qualifiers...");
        // Finalizing each (merged) entry
        for (GFF3RecordElement element : gff3RecordElements) {
            //System.out.println(element.getId()+"\t"+element.getRecords().size());
            // If the position is not set by the childs use its own position to set...
            if (element.getFeature().getLocations() == null || element.getFeature().getLocations().getMinPosition() == null) {
                //System.out.println("THIS: "+element.getId()+"\t"+element.getRecords().size());
                //System.out.println("No pos: "+element.getId());
                Join<Location> compoundJoin = new Join<>();
                // Using each record with the same identifier (e.g. multiple CDS belonging to one single CDS)
                for (GFF3Record gff3Record : element.getRecords()) {
                    Location location = createLocation(gff3Record);
                    compoundJoin.addLocation(location);
                }
                //        compoundJoin.setComplement(compoundJoin.getLocations().get(0).isComplement());
                element.getFeature().setLocations(compoundJoin);
            }

            // Setting the qualifiers
            Collection<uk.ac.ebi.embl.api.entry.qualifier.Qualifier> qualifiers = new ArrayList<>();
            for (GFF3Record gff3Record : element.getRecords()) {
                qualifiers.addAll(setQualifiers(gff3Record));
            }
            element.getFeature().addQualifiers(qualifiers);
        }
        List<Entry> tmpList = new ArrayList<>();
        tmpList.addAll(entries.values());
        return tmpList;
    }

    private Location createLocation(GFF3Record record) {
        long start = record.getStart();
        long end = record.getEnd();
        Location location = locationFactory.createLocalRange(start, end);
        location.setComplement(setStrand(record.getStrand()));
        return location;
    }

    private boolean setStrand(int strand) {
        return strand == -1;
    }

    private Collection<uk.ac.ebi.embl.api.entry.qualifier.Qualifier> setQualifiers(GFF3Record record) {

        Iterator<?> attributeIterator = record.getAttributes().entrySet().iterator();
        Collection<uk.ac.ebi.embl.api.entry.qualifier.Qualifier> qualifierList = new ArrayList<>();
        while (attributeIterator.hasNext()) {
            Map.Entry attributePairs = (Map.Entry) attributeIterator.next();
            String attributeKey = attributePairs.getKey().toString();
            String attributeValue = attributePairs.getValue().toString().toLowerCase();
            String attributeKeyLookup = WordUtils.capitalizeFully(attributeKey.replaceAll("_", " "));
            attributeKeyLookup = attributeKeyLookup.replaceAll(" +", "");

            // System.out.println(attributeKeyLookup);

            if (attributeKeyLookup.matches("^Id$") && attributeValue.contains(":")) {
                attributeValue = attributeValue.split(":")[1];
            }

            if (allowed.get("handle" + attributeKeyLookup) != null) {
                qualifier = qualifierFactory.createQualifier(attributeKey, attributeValue);
                qualifierList.add(qualifier);
            } else if (attributeKey.toLowerCase().matches("dbxref")) {
                qualifier = qualifierFactory.createQualifier("db_xref", attributeValue);
            } else {
                missed.add(attributeKeyLookup);
            }
        }
        return qualifierList;
    }
}