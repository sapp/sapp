package nl.wur.ssb.conversion.fasta2rdf;

import life.gbol.domain.Note;
import life.gbol.domain.Provenance;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.InputOutput.Output;
import nl.wur.ssb.SappGeneric.Store;
import nl.wur.ssb.conversion.options.CommandOptionsFasta2RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.validation.Origin;
import uk.ac.ebi.embl.api.validation.ValidationMessage;
import uk.ac.ebi.embl.api.validation.ValidationResult;
import uk.ac.ebi.embl.fasta.reader.FastaFileReader;
import uk.ac.ebi.embl.fasta.reader.FastaLineReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Fasta {

    private static final Logger logger = LogManager.getLogger(Fasta.class);
    public static CommandOptionsFasta2RDF arguments;
    public static void app(String[] args) throws Exception {

        arguments = new CommandOptionsFasta2RDF(args);
        if (!arguments.genome && !arguments.gene && !arguments.protein) {
            throw new Exception("Missing parameter, -gene, -protein or -genome");
        }

        // Step 1: Break it down into pieces of 10.000 sequences per file
        BufferedReader bf = new nl.wur.ssb.SappGeneric.Reader().reader(arguments.input);
        String seqLine;
        tmpDir = Files.createTempDirectory("FASTA2RDF");
        // This makes array list obsolete but can be handy to show number of jobs..
        File tmpFile = File.createTempFile("FASTA2RDF", ".fasta", tmpDir.toFile());
        List<File> tmpFiles = new ArrayList<>();
        tmpFiles.add(new File(tmpFile.getAbsolutePath()));
        PrintWriter writer = new PrintWriter(tmpFile);

        logger.info("Number of files to process: " + tmpFiles.size());

        while ((seqLine = bf.readLine()) != null) {
            if (seqLine.contains(">")) {
                // New file... and reset counter when reaching beyond 10mb (complete fasta files)
                if (tmpFile.length() > 10000000) {
                    // Closing previous and creation of new tmp file
                    writer.close();
                    tmpFile = File.createTempFile("FASTA2RDF", ".fasta", tmpDir.toFile());
                    logger.error("\r");
                    logger.error("Number of files to process: " + tmpFiles.size());
                    tmpFiles.add(new File(tmpFile.getAbsolutePath()));
                    writer = new PrintWriter(tmpFile);
                }
                // Write to file...
                writer.println(seqLine);
            } else {
                writer.println(seqLine);
            }
        }

        // Finally close all
        writer.close();
        bf.close();

        // This is now calling the function
        ArrayList<File> ntFiles = new ArrayList<>();
        for (File file : tmpFiles) {
            ntFiles.add(performConversion(file));
        }

        // Merge all files and save to output
        logger.info("Saving all stores to " + arguments.output);
        Output.save(ntFiles, arguments.output);
    }

    private static Path tmpDir;

    /**
     * Implementation for FASTA conversion
     */
    private static File performConversion(File input) throws Exception {
        Domain domain = Store.createMemoryStore();
        if (arguments.protein) {
            // Treat me as a protein entry, cannot use embl parser for that
            logger.info("Parsing protein fasta file");
            nl.wur.ssb.conversion.fasta2rdf.Protein.build(domain, input);
        } else {
            life.gbol.domain.Sample sample = domain.make(life.gbol.domain.Sample.class, "http://gbol.life/0.1/" + arguments.identifier);
            sample.setName(arguments.identifier);
            // Entries...
            BufferedReader bufreader = new nl.wur.ssb.SappGeneric.Reader().reader(input);
            FastaFileReader fastaReader = new FastaFileReader(new FastaLineReader(bufreader));
            ValidationResult result = fastaReader.read();
            Collection<ValidationMessage<Origin>> messages = result.getMessages();
            for (ValidationMessage<Origin> message : messages) {
                logger.info(message.getMessage());
            }
            int fastaCount = 0;

            while (fastaReader.isEntry()) {
                fastaCount += 1;
                if (fastaCount % 100 == 0)
                    System.out.print("# Number of sequences parsed: " + fastaCount + " number of triples " + domain.getRDFSimpleCon().getModel().size() + "\r");

                Entry entry = fastaReader.getEntry();
                logger.debug("Comment: " + entry.getComment().getText());
                if (arguments.gene) {
                    // Treat me as a gene entry
                    nl.wur.ssb.conversion.fasta2rdf.Mrna.build(domain, sample, arguments, entry);
                } else if (arguments.genome) {
                    String dnatype;
                    // Treat me as a genome
                    if (arguments.chromosome) {
                        dnatype = "chromosome";
                    } else if (arguments.scaffold) {
                        dnatype = "scaffold";
                    } else {
                        dnatype = "contig";
                    }
                    nl.wur.ssb.conversion.fasta2rdf.Genome.build(arguments, domain, sample, dnatype, entry);
                } else {
                    throw new Exception("Missing parameter, -gene, -protein or -genome");
                }
                fastaReader.read();
            }
        }

        File tmpFile = File.createTempFile("domain", ".nt");
        domain.save(tmpFile.getAbsolutePath(), RDFFormat.N_TRIPLE);
        domain.closeAndDelete();
        return tmpFile;
    }

    public static Note setHeaderNote(Domain domain, String sequenceIRI, String content, CommandOptionsFasta2RDF arguments) {
        Note note = domain.make(Note.class, sequenceIRI + "/note");
        note.setText(content);
        Provenance noteProv = domain.make(Provenance.class, note.getResource().getURI() + "/noteprov");
        //    AnnotationResult annotResult = domain.make(AnnotationLinkSet.class,
        //        note.getResource().getURI() + "/noteprov/" + "AnnotationResult");
        noteProv.setOrigin(arguments.annotResult);
        note.setText(content);
        note.setProvenance(noteProv);
        return note;

    }
}
