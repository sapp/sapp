package nl.wur.ssb.conversion.fasta2rdf;

import life.gbol.domain.NASequence;
import life.gbol.domain.Region;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

public class Position {

    public static Region Region(Domain domain, NASequence naobject, long beginpos,
                                long endpos, String type) {

        // Creating the gene object locations of begin and end...

        /*
         * Should I create a region with or without the type?
         */

        Region region = domain.make(Region.class,
                "http://gbol.life/0.1/region/" + beginpos + "-" + endpos);

        life.gbol.domain.ExactPosition setbegin = domain.make(life.gbol.domain.ExactPosition.class,
                "http://gbol.life/0.1/exactbegin/" + beginpos);

        life.gbol.domain.ExactPosition setend = domain.make(life.gbol.domain.ExactPosition.class,
                "http://gbol.life/0.1/exactend/" + endpos);

        setbegin.setPosition(beginpos);

        region.setBegin(setbegin);

        setend.setPosition(endpos);
        region.setEnd(setend);

        return region;
    }

    public static Region RegionLeftPartial(Domain domain, NASequence naobject, long beginpos,
                                           long endpos, String type, NASequence dnaobject) {
        /*
         * This module creates the Left partial entry complement(join(<85..350...
         *
         * But currently you cannot set a location to a fuzzy position...
         */

        Region region = domain.make(Region.class,
                "http://gbol.life/0.1/region/leftpartial/" + beginpos + "-" + endpos);

        life.gbol.domain.BeforePosition setbegin = domain.make(life.gbol.domain.BeforePosition.class,
                "http://gbol.life/0.1/beforebegin/" + beginpos);

        life.gbol.domain.ExactPosition setend = domain.make(life.gbol.domain.ExactPosition.class,
                "http://gbol.life/0.1/exactend/" + endpos);

        setbegin.setPosition(beginpos);

        region.setBegin(setbegin);

        setend.setPosition(endpos);
        region.setEnd(setend);

        return region;
    }

    public static Region RegionRightPartial(Domain domain, NASequence naobject, long beginpos,
                                            long endpos, String type, NASequence dnaobject) {
        /*
         * This module creates the right partial entry ... FT 1160..>1261))
         *
         * But currently you cannot set a location to a fuzzy position...
         */


        Region region = domain.make(Region.class,
                "http://gbol.life/0.1/region/rightpartial/" + beginpos + "-" + endpos);

        life.gbol.domain.ExactPosition setbegin = domain.make(life.gbol.domain.ExactPosition.class,
                "http://gbol.life/0.1/exactbegin/" + beginpos);

        life.gbol.domain.AfterPosition setend = domain.make(life.gbol.domain.AfterPosition.class,
                "http://gbol.life/0.1/afterend/" + endpos);

        setbegin.setPosition(beginpos);

        region.setBegin(setbegin);

        setend.setPosition(endpos);

        region.setEnd(setend);

        return region;
    }
}
