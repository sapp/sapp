package nl.wur.ssb.conversion.fasta2rdf;

import life.gbol.domain.Note;
import life.gbol.domain.Provenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.ebi.embl.api.validation.Origin;
import uk.ac.ebi.embl.api.validation.ValidationMessage;
import uk.ac.ebi.embl.api.validation.ValidationResult;
import uk.ac.ebi.embl.fasta.reader.FastaFileReader;
import uk.ac.ebi.embl.fasta.reader.FastaLineReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Protein {
    private static final Logger logger = LogManager.getLogger(Protein.class);

    public static void build(Domain domain, File input) throws Exception {

        life.gbol.domain.Sample sample = domain.make(life.gbol.domain.Sample.class, "http://gbol.life/0.1/" + Fasta.arguments.identifier);
        sample.setName(Fasta.arguments.identifier);

        BufferedReader bufreader = new BufferedReader(new FileReader(input));
        FastaFileReader fastaReader = new FastaFileReader(new FastaLineReader(bufreader));


        ValidationResult result = fastaReader.read();
        Collection<ValidationMessage<Origin>> messages = result.getMessages();
        for (ValidationMessage<Origin> message : messages) {
            if (!message.getMessage().startsWith("Invalid base:"))
                logger.info(message.getMessage());
        }

        Scanner scanner = new Scanner(input);

        Map<String, String> fasta = new HashMap<>();
        String header = null;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.startsWith(">")) {
                header = line;
                fasta.put(header, "");
            } else {
                fasta.put(header, fasta.get(header) + line);
            }
        }

        scanner.close();

        for (String key : fasta.keySet()) {
            String sequence = fasta.get(key).replaceAll("\\*$", "");
            if (sequence.contains("*"))
                logger.warn("* detected...");

            String sha384 = Generic.checksum(sequence, "SHA-384");

            life.gbol.domain.Protein protein = domain.make(life.gbol.domain.Protein.class, "http://gbol.life/0.1/protein/" + sha384);

            // When a fasta file has multiple protein entries that are the same the headerNote needs to increment
            int noteSize = 0;
            Note note = null;
            try {
                protein.getSequence();
                noteSize = protein.getAllNote().size() + 1;
            } catch (RuntimeException e) {
                // Protein does not exist yet...
                protein.setSequence(sequence);
                protein.setSample(sample);
                protein.setSha384(sha384);
                note = domain.make(Note.class, protein.getResource().getURI() + "/headernote");
            }
            if (note == null)
                note = domain.make(Note.class, protein.getResource().getURI() + "/headernote/" + noteSize);

            note.setText(key.replaceFirst(">", ""));
            Provenance prov = domain.make(Provenance.class, note.getResource().getURI() + "/prov");

            prov.setOrigin(Fasta.arguments.annotResult);
            note.setProvenance(prov);
            protein.addNote(note);
        }
    }
}

