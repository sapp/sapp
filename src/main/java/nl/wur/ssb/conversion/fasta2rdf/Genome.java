package nl.wur.ssb.conversion.fasta2rdf;

import life.gbol.domain.Note;
import life.gbol.domain.Sample;
import life.gbol.domain.StrandType;
import life.gbol.domain.Topology;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.conversion.options.CommandOptionsFasta2RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.ebi.embl.api.entry.Entry;

public class Genome {
    private static final Logger logger = LogManager.getLogger(Genome.class);

    public static void build(CommandOptionsFasta2RDF arguments, Domain domain, Sample sample, String dnatype,
                             Entry entry, String sequenceIRI) throws Exception {

        String header = entry.getComment().getText();

        String sequence = new String(entry.getSequence().getSequenceByte()).toUpperCase();

        String sac = header.split(" ")[0];

        Topology topology;

        if (arguments.topology.equals("linear"))
            topology = Topology.Linear;
        else if (arguments.topology.equals("circular"))
            topology = Topology.Circular;
        else
            throw new Exception(
                    "Topology not given please indicate whether topology is linear or circular");

        Note note = Fasta.setHeaderNote(domain, sequenceIRI, header, arguments);

        // TODO reduce code base by casting
        if (dnatype.contains("scaffold")) {
            life.gbol.domain.Scaffold CM = domain.make(life.gbol.domain.Scaffold.class, sequenceIRI);
            CM.setSequence(sequence);
            CM.setSample(sample);
            CM.addNote(note);
            CM.setLength(entry.getSequence().getLength());
            CM.setTopology(topology);
            CM.addAccession(sac);
            StrandType type = StrandType.DoubleStrandedDNA;
            CM.setStrandType(type);
            CM.setSha384(Generic.checksum(sequence, "SHA-384"));
            CM.setTranslTable(arguments.codon);
        } else if (dnatype.contains("chromosome")) {
            life.gbol.domain.Chromosome CM = domain.make(life.gbol.domain.Chromosome.class, sequenceIRI);
            CM.setSequence(sequence);
            CM.setSample(sample);
            CM.addNote(note);
            CM.setLength(entry.getSequence().getLength());
            CM.addAccession(sac);
            CM.setTopology(Topology.Linear);
            StrandType type = StrandType.DoubleStrandedDNA;
            CM.setStrandType(type);
            CM.setSha384(Generic.checksum(sequence, "SHA-384"));
            CM.setTranslTable(arguments.codon);
        } else if (dnatype.matches("contig")) {
            life.gbol.domain.Contig CM = domain.make(life.gbol.domain.Contig.class, sequenceIRI);
            CM.setSequence(sequence.trim());
            CM.setSample(sample);
            CM.addNote(note);
            CM.addAccession(sac);
            CM.setLength(entry.getSequence().getLength());
            CM.setTopology(Topology.Linear);
            CM.setTranslTable(arguments.codon);
            StrandType type = StrandType.DoubleStrandedDNA;
            CM.setStrandType(type);
            CM.setSha384(Generic.checksum(CM.getSequence(), "SHA-384"));
        } else {
            logger.warn("contig, scaffold or chromosome option not given... using contig as default");
        }
    }

    public static void build(CommandOptionsFasta2RDF arguments, Domain domain, Sample sample, String dnatype, Entry entry) throws Exception {
        // Using minimal sequence length filtering
        if (arguments.length <= entry.getSequence().getLength()) {
            String sac = entry.getComment().getText().split(" ")[0].replaceAll("\\|", "/");
            logger.debug("Submitter Accession: " + sac);
            String sequenceIRI = sample.getResource().getURI() + "/" + sac;
            build(arguments, domain, sample, dnatype, entry, sequenceIRI);
        }
    }
}
