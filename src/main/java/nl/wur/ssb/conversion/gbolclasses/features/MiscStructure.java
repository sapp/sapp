package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MiscStructure<T extends life.gbol.domain.MiscStructure> extends StructureFeature<T> {

    public MiscStructure(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}
