package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class TransferOrigin<T extends life.gbol.domain.TransferOrigin>
        extends BiologicalRecognizedRegion<T> {

    public TransferOrigin(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleTransferDirection(String value) {
        //TODO ? handleTransferDirection
        throw new RuntimeException("handleTransferDirection for Feature not done");
    }
}
