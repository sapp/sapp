package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.AnnotationResult;
import life.gbol.domain.Note;
import life.gbol.domain.Provenance;
import life.gbol.domain.ReasonArtificialLocation;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.flatfile.Flatfile;
import nl.wur.ssb.conversion.gbolclasses.Thing;
import nl.wur.ssb.conversion.gff3.GFF3Mapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Feature<T extends life.gbol.domain.Feature> extends Thing {
    private static final Logger logger = LogManager.getLogger(Feature.class);
    protected T feature;

    public Feature(SequenceBuilder domain, T feature) {
        super(domain);
        this.feature = feature;
    }

    /*
     * Feature specific things location @:Location;
     */

    /*
     * Any publication associated to feature anotation (numbered in Genbank)
     *
     * citation @bibo:Document*;
     */

    /*
     * Short description of the function
     *
     * function xsd:String?;
     */

    public void handleFunction(String value) {
        feature.setFunction(value);
    }

    /*
     *
     * Provenance associated the feature
     *
     * provenance @:FeatureProvenance*;
     *
     * Descriptive functionally link of the feature to external resource
     *
     * xref @:XRef*;
     *
     * Notes on the feature
     *
     * note @:Note*;
     */

    public void handleNote(String value) {
        // TODO AnnotationResult rootURI not present... Same as Sequence.java Note
        Note note = this.parent.getDomain().make(Note.class, feature.getResource().getURI() + "/note");
        note.setText(value);
        Provenance noteProv = this.parent.getDomain().make(Provenance.class, note.getResource().getURI() + "/noteprov");

        if (Flatfile.args != null) {
            // Due to merge in multiple embl entries this should be resolved afterwards during merge...
            this.parent.getDomain().disableCheck();
            AnnotationResult annotResult = this.parent.getDomain().make(AnnotationResult.class, Flatfile.args.annotURI);
            noteProv.setOrigin(annotResult);
            this.parent.getDomain().enableCheck();
        } else {
            noteProv.setOrigin(GFF3Mapper.arguments.annotResult);
        }

        note.setProvenance(noteProv);
        feature.addNote(note);
    }

    public void handleDescription(String value) {
        handleNote(value);
    }

    /*
     *
     * Indicate that the location is artificial because of a heterogeous population for low quality*
     * sequence region**
     *
     * artificialLocation type::ReasonArtificialLocation?;
     */
    public void handleArtificialLocation(String value) throws Exception {
        ReasonArtificialLocation art = (ReasonArtificialLocation) GBOLUtil.getEnum(ReasonArtificialLocation.class, value);
        feature.setArtificialLocation(art);
    }

    /*
     * Associated phenotipic property
     *
     * phenotype xsd:String?;
     */
    public void handlePhenotype(String value) {
        feature.setPhenotype(value);
    }

    //--------------

    /*
     * Captured exceptions...
     */
    public void handleTransSplicing(String value) {
        /*
         * Ignored
         * Removed in GBOL, transcript are consistently stored as a list of exons
         */
    }
    //-

    public void handleDelayedCodonStart(String value) {
        //TODO
    }

    public void handleLabel(String value) {
        this.feature.setStandardName(value);
    }

    public void handleCitation(String value) {
        int num = Integer.parseInt(value.replaceAll("\\[|\\]", ""));
        life.gbol.domain.Citation ref = this.parent.getRef(num);
        if (ref != null)
            this.feature.addCitation(ref);
        else
            logger.warn("ref with reference number " + num + " not found");
    }

    public void handleName(String value) {
        feature.addAccession(value);
    }

    public void handleId(String value) {
        feature.addAccession(value.replaceAll("^[gene|transcript]:", ""));
    }

    public void handleTranslation(String value) {
        logger.error("Translation is not part of a Feature object");
    }

    public void handleNomenclature(String value) {
        // TODO handle nomenclature example in NC_002333
        handleNote(value);

    }
}


