package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class TranscriptionElement<T extends life.gbol.domain.TranscriptionElement>
        extends GenomicFeature<T> {

    public TranscriptionElement(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleNumber(String value) {
        //TODO ? handle number
        //this.delayedProps.put("handleNumber", value);
    }
}
