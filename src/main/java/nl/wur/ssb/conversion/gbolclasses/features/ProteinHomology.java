package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ProteinHomology<T extends life.gbol.domain.ProteinHomology> extends ProteinFeature<T> {

    public ProteinHomology(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * The entity to which it is homologous to, being either another protein or cluster of proteins.
     * If identifier is a non public identifier use http://gbol.life/0.1/<id>
     * <p>
     * homologousTo IRI;
     */

    public void handleHomologousTo(String value) {
        //TODO >low handleHomologousTo
        throw new RuntimeException("handleHomologousTo for Feature not done");
    }

    /**
     * An optional describtion of the element to which it is homologous to
     * <p>
     * homologousToDesc xsd:String?;
     */
    public void handleHomologousToDesc(String value) {
        //TODO >low handleHomologousToDesc
        throw new RuntimeException("handleHomologousToDesc for Feature not done");
    }

    /**
     * An optional region to indicate to which part it is homologous to
     * <p>
     * targetRegion @:Region?"@en
     */
    public void handleTargetRegion(String value) {
        //TODO >low handleTargetRegion
    }

}

