package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ArtificialRecognizedRegion<T extends life.gbol.domain.ArtificialRecognizedRegion>
        extends RecognizedRegion<T> {

    public ArtificialRecognizedRegion(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}
