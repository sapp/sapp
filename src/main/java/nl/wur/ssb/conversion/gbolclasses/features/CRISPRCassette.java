package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class CRISPRCassette<T extends life.gbol.domain.CRISPRCassette> extends RepeatFeature<T> {

    public CRISPRCassette(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * The regions that contain a spacer
     * <p>
     * spacer @:Region+
     */
    public void handleSpacer(String value) {
        // TODO ! feature.addSpacer(region);
    }
}
