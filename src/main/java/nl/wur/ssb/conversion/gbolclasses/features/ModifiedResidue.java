package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ModifiedResidue<T extends life.gbol.domain.ModifiedResidue> extends ProteinFeature<T> {

    public ModifiedResidue(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * The residue type as defined within the 'Protein Modification Ontology'
     * (http://www.psidev.info/MOD)
     * <p>
     * residue IRI;
     */
    public void handleResidue(String value) {
        throw new RuntimeException(
                "Not done as class was not found in eclipse");

    }
    /*
     * If applicable, the biologal process in which this modication is involved. Use a Biological
     * Process from the GO ontology.
     *
     * modificationFunction IRI?;
     *
     * Short describtion denoted the state of the protein if the residue in the modified state. For
     * example 'protein in active state'
     *
     * modifiedState xsd:String?;
     *
     * Short describtion denoted the state of the protein if the residue in the unmodified state. For
     * example 'protein in inactive state'
     *
     * unmodifiedState xsd:String?;
     */
}
