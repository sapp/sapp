package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.RecombinationType;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MiscRecomb<T extends life.gbol.domain.MiscRecomb> extends BiologicalRecognizedRegion<T> {
    public MiscRecomb(SequenceBuilder domain, T feature) {
        super(domain, feature);
        this.feature.setRecombinationType(RecombinationType.RecombinationTypeOther);
    }

    public void handleRecombinationClass(String value) {
        feature.setRecombinationType(RecombinationType.RecombinationTypeOther);
    }
}
