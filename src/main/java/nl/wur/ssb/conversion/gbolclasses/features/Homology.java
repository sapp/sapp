package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Homology<T extends life.gbol.domain.Homology> extends GenomicFeature<T> {
    private final Logger logger = LogManager.getLogger(Homology.class);

    public Homology(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleHomology(String value) {
        //TODO ! homology
        throw new RuntimeException(
                "handleHomology for Feature not done, jesse's fancy lookup function here");
    }

    public void handleGene(String value) {
        logger.warn("invalid gene tag on " + this.getClass().getName() + ", converting it to accession string");
        feature.addAccession(value);
    }
}
