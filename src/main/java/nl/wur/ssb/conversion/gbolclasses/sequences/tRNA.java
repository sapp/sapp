package nl.wur.ssb.conversion.gbolclasses.sequences;

import life.gbol.domain.AminoAcid;
import life.gbol.domain.AntiCodon;
import life.gbol.domain.Region;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class tRNA<T extends life.gbol.domain.tRNA> extends MaturedRNA<T> {
    public tRNA(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleProduct(String value) {
        feature.setProduct(value);
    }

    /*
     * propertyDefinitions "
     *
     * #* The anti codon of the TRNA anticodon @:AntiCodon?;
     *
     * #* The functional name product xsd:String?"@en
     */


    /**
     * @param value
     * @throws Exception
     */

    public void handleAnticodon(String value) throws Exception {

        // Properties
        String aa = "AminoAcidOther";
        String codon = "NNN";
        int begin = -1;
        int end = -1;

        value = value.replaceAll("complement", "");
        value = value.replaceAll("\\(", "");
        value = value.replaceAll("\\)", "");

        // Getting AA
        Pattern pattern = Pattern.compile(".*aa:([A-z]+).*");
        Matcher matcher = pattern.matcher(value);

        if (matcher.matches()) {
            aa = matcher.group(1);
        }

        // Getting codon
        pattern = Pattern.compile(".*seq:([A-z]+).*");
        matcher = pattern.matcher(value);
        if (matcher.matches()) {
            codon = matcher.group(1);
        }

        // Getting position
        pattern = Pattern.compile(".*pos:([0-9]+..[0-9]+).*");
        matcher = pattern.matcher(value);
        if (matcher.matches()) {
            matcher.group(0);
            String pos = value.replaceAll(".*pos:([0-9]+..[0-9]+).*", "$1");
            begin = Integer.parseInt(pos.split("\\.\\.")[0]);
            end = Integer.parseInt(pos.split("\\.\\.")[1]);
        }

        String antiUri = "/" + begin + "-" + end + "/" + codon + "/" + aa;

        AntiCodon anticodon = parent.getDomain().make(AntiCodon.class,
                this.feature.getResource().getURI() + antiUri);

        Region region = this.parent.makeRegion(begin, end, "anticodon", this.feature);
        anticodon.setAminoAcid((AminoAcid) GBOLUtil.getEnum(AminoAcid.class, aa));

        if (codon.length() > 0)
            anticodon.setAntiCodonSequence(codon);

        anticodon.setAntiCodingRegion(region);

        this.feature.setAnticodon(anticodon);

    }

    /**
     * Many properties unknown except for the codon recognized
     *
     * @param value
     */
    public void handleCodonRecognized(String value) {
        String antiUri = "/" + null + "-" + null + "/" + value + "/" + null;
        // TODO handle codonrecognized example in NC_002333
        AntiCodon anticodon = parent.getDomain().make(AntiCodon.class,
                this.feature.getResource().getURI() + antiUri);
        anticodon.setAntiCodonSequence(value);
        this.parent.getDomain().disableCheck();
        this.feature.setAnticodon(anticodon);
        this.parent.getDomain().enableCheck();

    }

}
