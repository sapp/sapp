package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class TransMembraneRegion<T extends life.gbol.domain.Source>
        extends Feature<T> {
    public TransMembraneRegion(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}
