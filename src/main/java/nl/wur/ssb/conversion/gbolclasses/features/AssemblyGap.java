package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.GapType;
import life.gbol.domain.LinkageEvidence;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class AssemblyGap<T extends life.gbol.domain.AssemblyGap> extends AssemblyAnnotation<T> {
    public AssemblyGap(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleEstimatedLength(String value) {
        //Ignored...
        //feature.setEstimatedLength(Integer.parseInt(value));
    }

    /**
     * propertyDefinitions "#* The linkage evidence use to link the sequences on both sides
     * <p>
     * linkageEvidence type::LinkageEvidence?;
     *
     * @throws Exception
     */
    public void handleLinkageEvidence(String value) throws Exception {
        // TODO remove exception
        try {
            value = value.replace(" ", "_");
            LinkageEvidence linkev = (LinkageEvidence) GBOLUtil.getEnum(LinkageEvidence.class, value);
            feature.setLinkageEvidence(linkev);
        } catch (Exception e) {
        }
    }


    public void handleGapType(String value) throws Exception {
        GapType gaptype = (GapType) GBOLUtil.getEnum(GapType.class, value);
        feature.setGapType(gaptype);
    }
}
