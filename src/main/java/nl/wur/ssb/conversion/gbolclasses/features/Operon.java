package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Operon<T extends life.gbol.domain.Operon> extends TranscriptionElement<T> {

    public Operon(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleOperonId(String value) {
        // TODO ! feature.setOperinId(value);
    }

}
