package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class NaturalVariation<T extends life.gbol.domain.NaturalVariation>
        extends VariationFeature<T> {

    public NaturalVariation(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * The frequency of this variation within the population
     * <p>
     * frequency xsd:Double?
     */

    public void handleFrequency(String value) {
        feature.setFrequency(Double.parseDouble(value));
    }

    public void handleCompare(String value) {

    }
}

