package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;


public class VariationFeature<T extends life.gbol.domain.VariationFeature>
        extends SequenceAnnotation<T> {
    public VariationFeature(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * The sequence replacing the original sequence
     * <p>
     * replace xsd:String?;"
     */

    public void handleReplace(String value) {
        feature.setReplace(value);
    }
}
