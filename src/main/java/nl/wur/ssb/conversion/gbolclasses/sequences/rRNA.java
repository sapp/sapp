package nl.wur.ssb.conversion.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class rRNA<T extends life.gbol.domain.rRNA> extends MaturedRNA<T> {
    public rRNA(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

}
