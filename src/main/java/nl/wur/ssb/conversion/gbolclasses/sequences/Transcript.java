package nl.wur.ssb.conversion.gbolclasses.sequences;

import life.gbol.domain.Gene;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class Transcript<T extends life.gbol.domain.Transcript> extends NASequence<T> {
    private final Logger logger = LogManager.getLogger(Transcript.class);

    public Transcript(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleTranscriptId(String value) {
        feature.addAccession(value);
    }

    public Gene getGene() throws Exception {
        Iterable<ResultLine> results = this.parent.getDomain().getRDFSimpleCon()
                .runQuery("getGeneForTranscript.sparql", false, this.feature.getResource().getURI());

        Iterator<ResultLine> itt = results.iterator();

        ResultLine result = itt.next();
        Gene gene = this.parent.getDomain().make(Gene.class, result.getIRI("gene"));

        if (itt.hasNext()) {
            logger.warn("More than one gene detected returning the first one...\nMost likely an mRNA has been assigned to two different genes");
        }
        return gene;

    }

    public List<life.gbol.domain.CDS> getAllCDS() {
        LinkedList<life.gbol.domain.CDS> toRet = new LinkedList<life.gbol.domain.CDS>();
        for (life.gbol.domain.NAFeature feature : this.feature.getAllFeature()) {
            if (feature instanceof life.gbol.domain.CDS)
                toRet.add((life.gbol.domain.CDS) feature);
        }
        return toRet;
    }

}
