package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Gene<T extends life.gbol.domain.Gene> extends TranscriptionElement<T> {

    public Gene(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * Symbol of the gene (not the symbol of the resulting protein)
     * <p>
     * geneSymbol xsd:String?;
     */
    public void handleGeneSymbol(String value) {
        feature.setGeneSymbol(value);
    }

    /**
     * Alternative/old gene symbols
     * <p>
     * geneSymbolSynonym xsd:String?;
     */
    public void handleGeneSymbolSynonym(String value) {
        feature.addGeneSymbolSynonym(value);
    }

    public void handleID(String value) {
        feature.addAccession(value.replaceAll("^gene:", ""));
    }

    public void handleGeneId(String value) {
        feature.addAccession(value);
    }

    public life.gbol.domain.Gene getGene() {
        return this.feature;
    }
}
