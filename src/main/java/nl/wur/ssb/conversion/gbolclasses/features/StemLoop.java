package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class StemLoop<T extends life.gbol.domain.StemLoop> extends StructureFeature<T> {

    public StemLoop(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

}
