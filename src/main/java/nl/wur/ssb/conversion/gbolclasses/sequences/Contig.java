package nl.wur.ssb.conversion.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Contig<T extends life.gbol.domain.Contig> extends UncompleteNASequence<T> {
    public Contig(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}
