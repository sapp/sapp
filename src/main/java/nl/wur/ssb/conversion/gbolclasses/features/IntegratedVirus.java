package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class IntegratedVirus<T extends life.gbol.domain.IntegratedVirus>
        extends TranscriptionElement<T> {

    public IntegratedVirus(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    // Reference to the sequence of the virus, genes of the integrated virus
    // can be annotated directly on genome as well as on the virus genome

    // virusGenome @:NASequence?;

    // The region of the virus genome that is integrated

    // integratedRegion @:Location;
}
