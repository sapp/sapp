package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.MobileElementType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MobileElement<T extends life.gbol.domain.MobileElement> extends RepeatFeature<T> {

    public MobileElement(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * Mobile element type
     * <p>
     * mobileElementType type::MobileElementType;
     *
     * @throws Exception
     */
    public void handleMobileElementType(String value) throws Exception {
        String type = value;
        String name = "unknown";
        if (value.contains(":")) {
            type = value.split(":")[0];
            name = value.split(":")[1];
        }

        // Check if fix works.. Also need to test multiple ddbj labels in ontology
        if (type.contains("retrotransposon"))
            type = "retro transposon";

        feature.setMobileElementType((MobileElementType) GBOLUtil.getEnum(MobileElementType.class, type));
        feature.setMobileElementName(name);
    }

    /**
     * The name of the mobile element
     * <p>
     * mobileElementName xsd:String;
     */

    public void handleMobileElementName(String value) {
        feature.setMobileElementName(value);
    }
}
