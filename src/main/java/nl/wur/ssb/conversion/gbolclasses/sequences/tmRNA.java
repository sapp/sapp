package nl.wur.ssb.conversion.gbolclasses.sequences;

import life.gbol.domain.Region;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class tmRNA<T extends life.gbol.domain.tmRNA> extends MaturedRNA<T> {
    public tmRNA(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }


    /**
     * Base location encoding the polypeptide for proteolysis tag of tmRNA and its termination
     * codon; tagPeptide @:Region?;
     */

    public void handleTagPeptide(String value) {
        String pos = value.replaceAll("([0-9]+..[0-9]+).*", "$1");
        int begin = Integer.parseInt(pos.split("\\.\\.")[0]);
        int end = Integer.parseInt(pos.split("\\.\\.")[1]);
        Region region = this.parent.makeRegion(begin, end, "anticodon", this.feature);
        feature.setTagPeptide(region);
    }
}
