package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ProteinStructure<T extends life.gbol.domain.ProteinStructure> extends ProteinFeature<T> {

    public ProteinStructure(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}
