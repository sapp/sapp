package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CDS<T extends life.gbol.domain.CDS> extends TranscriptFeature<T> {

    private final Logger logger = LogManager.getLogger(CDS.class);

    public CDS(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public life.gbol.domain.NASequence getRootSequence() throws Exception {
        ResultLine res = this.parent.getDomain().getRDFSimpleCon().runQuerySingleRes("getParentSeqForCds.sparql", false, this.feature.getResource().getURI());
        return this.parent.getDomain().make(life.gbol.domain.NASequence.class, res.getIRI("parentSeq"));
    }

    /**
     * Indicate the presence of ribomalsomal slippage, the slippage regioin are excluded from the CDS
     * using a CollectionOfRegions location for the CDS on the transcript.
     * <p>
     * ribosomalSlippage xsd:Boolean?;
     */
    public void handleRibosomalSlippage(String value) {
        feature.setRibosomalSlippage(true);
    }

    /**
     * Indicate all codons, which have translation other then the default translation of the
     * translation table
     * <p>
     * translExcept @:TranslExcept*;
     */
    public void handleTranslExcept(String value) {
        this.delayProp("TranslExcept", value);
    }

    public void handleDelayedTranslExcept(String value) {
        //TODO ! complete handle translExcept
        // TranslExcept translexcept = parent.getDomain().make(life.gbol.domain.TranslExcept.class,
        // feature.getResource().getURI() + "/" +GBOLUtil.cleanURI(value));
        // String pos = value.replaceAll("\\(pos:([0-9]+..[0-9]+).*", "$1");
        // int begin = Integer.parseInt(pos.split("\\.\\.")[0]);
        // int end = Integer.parseInt(pos.split("\\.\\.")[1]);
        // String aa = value.replaceAll(".*aa:(.*)\\)", "$1");
        // AminoAcid aminoacid = (AminoAcid ) GBOLUtil.getEnum(life.gbol.domain.AminoAcid.class, aa);
        // translexcept.setAminoAcid(aminoacid);
        // Region region = this.parent.makeRegion(begin, end, "translexcept", translexcept);
        // translexcept.setExceptRegion(region);
        // feature.addTranslExcept(translexcept);
    }

    public void handleTranslTable(String value) {
        this.delayProp("TranslTable", value);
    }

    public void handleDelayedTranslTable(String value) throws Exception {
//        System.err.println("handle delayed transl table: " + value);
        life.gbol.domain.NASequence seq = this.getRootSequence();
        if (seq != null) {
            int val = Integer.parseInt(value);
            if (seq.getTranslTable() != null && seq.getTranslTable() != 0 && !seq.getTranslTable().equals(val)) {
                logger.warn("translation table not same on parent sequence: " + seq.getTranslTable() + " <-> " + val);
            }
            seq.setTranslTable(val);
        }
    }

    /**
     * Deprecated feature, which is imported from GenBank, mark a unconventional translation of the
     * transcript
     * <p>
     * exception xsd:String?;
     */
    public void handleException(String value) {
        //TODO ! fix in GBOL ontology,
        feature.setException(value);
    }

    /*
     * The ID of the protein if translated from this CDS (format
     * ^\\\\s*([A-Z]{3}\\\\d{5})(\\\\.)(\\\\d+)\\\\s*$)
     *
     * proteinId xsd:String?;
     */
    public void handleProteinId(String value) {
        feature.setProteinId(value);
    }

    /*
     * Short description of the product resulting from the feature
     *
     * product xsd:String?;
     */
    public void handleProduct(String value) {
        feature.setProduct(value);
    }

    /**
     * If the start position is unknown, indicate with this property, where the first complete codon can be found
     * <p>
     * codonStart xsd:PositiveInteger?;
     */
    public void handleCodonStart(String value) {
        this.feature.setCodonStart(Integer.parseInt(value));
    }

    public void handleEcNumber(String value) {
        this.delayProp("EcNumber", value);
    }

    public void handleDelayedEcNumber(String value) throws Exception {
        life.gbol.domain.Protein protein = this.feature.getProtein();
        if (protein != null)
            protein.addXref(this.parent.createXRef("EC", value));
        else
            logger.warn("EC number on CDS without protein: " + value);
    }

    public void handleTranslation(String value) {
        //TODO >low add a double check
    }
}


