package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

/**
 * Created by jasperk on 05/04/2017.
 */
public class GenomicFeature<T extends life.gbol.domain.GenomicFeature> extends NAFeature<T> {

    public GenomicFeature(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleOperon(String value) {
        feature.setFunction(value);
    }
}