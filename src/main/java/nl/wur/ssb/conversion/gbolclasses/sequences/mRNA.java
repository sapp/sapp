package nl.wur.ssb.conversion.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;


public class mRNA<T extends life.gbol.domain.mRNA> extends MaturedRNA<T> {
    public mRNA(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleTransSplicing(String value) {
        // Transplicing boolean skipped
    }

    public void handleArtificialLocation(String value) throws Exception {
//    ReasonArtificialLocation art = (ReasonArtificialLocation ) GBOLUtil.getEnum(life.gbol.domain.ReasonArtificialLocation.class, value);
//    feature.setArtificialLocation(art);
    }


}
