package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class UpdatedSequence<T extends life.gbol.domain.Source> extends Feature<T> {
    public UpdatedSequence(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * The functional name
     * <p>
     * compare IRI;
     */

    public void handleCompare(String value) {
        //TODO ? handleCompare
        throw new RuntimeException("handleCompare for Feature not done");
    }
}
