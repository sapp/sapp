package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.Sample;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

//TODO ? additional source, this class not used at the moment
public class Source<T extends life.gbol.domain.Source> extends Feature<T> {
    public Source(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * Sample from which this piece of sequence originates from
     * <p>
     * sample @:Sample;
     */
    public void handleSample(String value) {
        Sample sample = parent.getDomain().make(Sample.class, value);
        feature.setSample(sample);
        throw new RuntimeException("URL?");
    }


    /**
     * The taxonomy URL of the organism from which this piece of sequence orginates. If not specified
     * it equal to the taxonomy given in the sequence object.
     * <p>
     * taxonomy @:TaxonomyRef?;
     */

    public void handleTaxonomy(String value) {
        life.gbol.domain.TaxonomyRef taxon = parent.getDomain().make(life.gbol.domain.TaxonomyRef.class, value);
        throw new RuntimeException("handleTaxonomy for Feature not done");
    }

    public void handleSegment(String value) {
        // TODO how to set type to a sample from within source?
    }

}
