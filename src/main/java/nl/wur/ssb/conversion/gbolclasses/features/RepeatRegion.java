package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.Satellite;
import life.gbol.domain.SatelliteType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class RepeatRegion<T extends life.gbol.domain.RepeatRegion> extends RepeatFeature<T> {

    public RepeatRegion(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }


    public void handleMobileElement(String value) {
        //TODO ? what to do with mobile element
        //this.delayedProps.put("mobile_element", value);
    }

    public void handleSatellite(String value) throws Exception {
        SatelliteType satelliteType = (SatelliteType) GBOLUtil.getEnum(SatelliteType.class, value);
        life.gbol.domain.RepeatRegion repeatRegion = feature;
        Satellite satellite = this.parent.getDomain().make(Satellite.class, feature.getResource().getURI() + "/sattelite");
        satellite.setSatelliteType(satelliteType);
        // TODO look into the satellite name
        satellite.setSatelliteName("");
        repeatRegion.addSatellite(satellite);
    }
}
