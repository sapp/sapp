package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ThreePrimeUTR<T extends life.gbol.domain.ThreePrimeUTR> extends TranscriptFeature<T> {

    public ThreePrimeUTR(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}
