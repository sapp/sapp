package nl.wur.ssb.conversion.gbolclasses.sequences;

import life.gbol.domain.Note;
import life.gbol.domain.Provenance;
import life.gbol.domain.Sample;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.flatfile.FlatFileEntry;
import nl.wur.ssb.conversion.gbolclasses.Thing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Sequence<T extends life.gbol.domain.Sequence> extends Thing {
    private static final Logger logger = LogManager.getLogger(Sequence.class);
    protected T feature;

    public Sequence(SequenceBuilder domain, T feature) {
        super(domain);
        this.feature = feature;
    }

    /**
     * propertyDefinitions "
     * <p>
     * The hash key of the sequence sha384 xsd:String;
     */

    public void handleSha384(String value) {
        feature.setSha384(value);
    }

    /**
     * The sequence sequence xsd:String?;
     */
    public void handleSequence(String value) {
        feature.setSequence(value);
    }

    /**
     * Length of the sequence length xsd:Integer?;
     */
    public void handleLength(String value) {
        feature.setLength(Long.parseLong(value));
    }

    /*
     * The set of features annotating the sequence feature @:Feature*;
     */

    /**
     * Sample from which the sequence is originating sample @:Sample?;
     */
    public void handleSample(String value) {
        Sample sample = parent.getDomain().make(Sample.class, value);
        feature.setSample(sample);
    }

    /**
     * GI accession number of the sequence giaccession xsd:String?;
     */
    public void handleGiAccession(String value) {
        //TODO add gi xref
    }

    /**
     * Accession number (any type) accession xsd:String;
     */
    public void handleAccession(String value) {
        feature.addAccession(value);
    }

    /**
     * Secondary accession number (any type) secondaryAccession xsd:String?;
     */
    public void handleSecondaryAccession(String value) {
        // feature.setSecondaryAccession(value);
        throw new RuntimeException("This is not going to work");
    }

    /**
     * The scientific name of the sequence scientificname xsd:String?;
     */
    public void handleScientificname(String value) {
        //Already captured by the sample creation function
        //TODO check if this is indeed the organism scientific name
        if (feature.getOrganism() != null)
            feature.getOrganism().setScientificName(value);
    }

    /**
     * A short description of the sequence description xsd:String?;
     */
    public void handleDescription(String value) {
        feature.setDescription(value);
    }

    /**
     * The common name of the sequence commonname xsd:String?;
     */
    public void handleCommonName(String value) {
        feature.setCommonName(value);
    }

    /*
     * A link to a specific citation citation @bibo:Document;
     */

    /**
     * The version of the sequence sequenceVersion xsd:PositiveInteger?;
     */
    public void handleSequenceVersion(String value) {
        feature.setSequenceVersion(Integer.parseInt(value));
    }

    /*
     * Reference to entries in other databases which also represent the same
     * sequence. Note that the GBOLDocment xref and sequence xref can point to
     * same GenBank/EBML entry. xref @:XRef*;
     */

    /**
     * A extra note on the sequence itself, do not add notes related to the
     * GBOLDocument or sample. note @:Note*;
     */

    public void handleNote(String value) {
        // TODO AnnotationResult rootURI not present...
        Note note = this.parent.getDomain().make(Note.class, feature.getResource().getURI() + "/note");
        note.setText(value);
        Provenance noteProv = this.parent.getDomain().make(Provenance.class, note.getResource().getURI() + "/noteprov");
//	  AnnotationResult annotResult = this.parent.getDomain().make(AnnotationLinkSet.class, note.getResource().getURI() +"/noteprov/" + "AnnotationResult");
        noteProv.setOrigin(FlatFileEntry.annotResult);

        note.setProvenance(noteProv);
        feature.addNote(note);
    }

    /**
     * The taxonomy of the organism from which the sequence orginates
     * taxonomy @:TaxonomyRef?;"@en
     */
    public void handleTaxonomy(String value) {
        throw new RuntimeException("This is not going to work");
    }

    public void handleMap(String value) {
        this.delayProp("Map", value);
    }

    public void handleDelayedMap(String value) throws Exception {
        life.gbol.domain.Gene gene = this.getGene();
        if (gene != null) {
            if (gene.getMapLocation() != null && !gene.getMapLocation().equals(value))
                logger.warn("map feature not same on Gene: " + gene.getMapLocation() + " <-> " + value);
            gene.setMapLocation(value);
        }
    }

    public void handleFunction(String value) {
        feature.setFunction(value);
    }

    public void handleOperon(String value) {
        this.delayProp("Operon", value);
    }

    public void handleCitation(String value) {
        int num = Integer.parseInt(value.replaceAll("\\[|\\]", ""));
        life.gbol.domain.Citation ref = this.parent.getRef(num);
        if (ref != null)
            this.feature.addCitation(ref);
        else
            logger.warn("ref with reference number " + num + " not found");
    }

    public void handleDelayedOperon(String value) {
        //TODO
    }

    public void handleId(String value) {
        feature.addAccession(value.replaceAll("^[gene|transcript]:", ""));
    }

    public void handleName(String value) {
        feature.addAccession(value);
    }
}

