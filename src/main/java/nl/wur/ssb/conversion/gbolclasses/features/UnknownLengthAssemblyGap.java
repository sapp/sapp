package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.GapType;
import life.gbol.domain.LinkageEvidence;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class UnknownLengthAssemblyGap<T extends life.gbol.domain.UnknownLengthAssemblyGap> extends AssemblyGap<T> {

    public UnknownLengthAssemblyGap(SequenceBuilder domain, T feature) {
        super(domain, feature);

    }

    /**
     * propertyDefinitions "#* The linkage evidence use to link the sequences on both sides
     * <p>
     * linkageEvidence type::LinkageEvidence?;
     *
     * @throws Exception
     */
    public void handleLinkageEvidence(String value) throws Exception {
        LinkageEvidence linkev = (LinkageEvidence) GBOLUtil.getEnum(LinkageEvidence.class, value);
        feature.setLinkageEvidence(linkev);
    }

    /**
     * The type of the gap gapType
     * <p>
     * type::GapType;"
     *
     * @throws Exception
     */

    public void handleGapType(String value) throws Exception {
        GapType gaptype = (GapType) GBOLUtil.getEnum(GapType.class, value);
        feature.setGapType(gaptype);

    }


    public void handleEstimatedLength(String value) {
        if (value.matches("unknown")) {
            value = "-1";
        }
        feature.setEstimatedLength(Integer.parseInt(value));
    }

    public void handleDelayedEstimatedLength(String value) {
    }
}
