package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.BaseType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ModifiedBase<T extends life.gbol.domain.ModifiedBase> extends SequenceAnnotation<T> {

    public ModifiedBase(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * modBase
     * <p>
     * type::BaseType;
     *
     * @throws Exception
     */

    public void handleModBase(String value) throws Exception {
        BaseType baseType = (BaseType) GBOLUtil.getEnum(BaseType.class, value);
        feature.setModBase(baseType);
    }
}
