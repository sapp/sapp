package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.Region;
import life.gbol.domain.RepeatType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Telomere<T extends life.gbol.domain.Telomere> extends Feature<T> {
    public Telomere(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    public void handleRptType(String value) throws Exception {
        RepeatType rtype = (RepeatType) GBOLUtil.getEnum(RepeatType.class, value);
        this.feature.setRptType(rtype);
    }

    public void handleRptUnitRange(String value) throws Exception {
        String[] pos = value.split("\\.\\.");

        Region region = this.parent.makeRegion(Long.parseLong(pos[0]), Long.parseLong(pos[1]), "rptunitrange", this.feature);
        this.feature.setRptUnitRange(region);
//    throw new Exception(value);

    }

    public void handleRptUnitSeq(String value) {
        this.feature.setRptUnitSeq(value);
    }


}
