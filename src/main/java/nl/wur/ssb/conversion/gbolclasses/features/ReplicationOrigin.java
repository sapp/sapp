package nl.wur.ssb.conversion.gbolclasses.features;

import life.gbol.domain.StrandPosition;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ReplicationOrigin<T extends life.gbol.domain.ReplicationOrigin>
        extends BiologicalRecognizedRegion<T> {

    public ReplicationOrigin(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * The direction of the replication can be forward, reverse or both
     * <p>
     * replicationDirection type::Direction?;
     *
     * @throws Exception
     */

    public void handleReplicationDirection(String value) throws Exception {
        feature.setReplicationDirection((StrandPosition) GBOLUtil.getEnum(StrandPosition.class, value));
    }

    public void handleDirection(String value) throws Exception {
        handleReplicationDirection(value);
    }
}
