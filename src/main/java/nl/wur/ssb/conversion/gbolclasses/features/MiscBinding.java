package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MiscBinding<T extends life.gbol.domain.MiscBinding>
        extends BiologicalRecognizedRegion<T> {

    public MiscBinding(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}
