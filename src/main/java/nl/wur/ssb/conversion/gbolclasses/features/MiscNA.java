package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.gbolclasses.sequences.CompleteNASequence;

public class MiscNA<T extends life.gbol.domain.CompleteNASequence>
        extends CompleteNASequence<T> {

    public MiscNA(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}

