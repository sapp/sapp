package nl.wur.ssb.conversion.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Plasmid<T extends life.gbol.domain.Plasmid> extends CompleteNASequence<T> {
    public Plasmid(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * propertyDefinitions "#*The name of the plasmid
     * <p>
     * plasmidName xsd:String?;"
     */

    public void handlePlasmidName(String value) {
        feature.setPlasmidName(value);
    }
}
