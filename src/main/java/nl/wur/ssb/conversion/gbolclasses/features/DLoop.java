package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class DLoop<T extends life.gbol.domain.DLoop> extends BiologicalRecognizedRegion<T> {

    public DLoop(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

}
