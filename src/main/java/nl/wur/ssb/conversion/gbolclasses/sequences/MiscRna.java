package nl.wur.ssb.conversion.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MiscRna<T extends life.gbol.domain.MiscRna> extends Transcript<T> {
    public MiscRna(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }

    /**
     * The functional name
     * <p>
     * product xsd:String?
     */

    public void handleProduct(String value) {
        feature.setProduct(value);
    }
}
