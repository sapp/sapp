package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class SequenceTaggedSite<T extends life.gbol.domain.SequenceTaggedSite>
        extends ArtificialRecognizedRegion<T> {

    public SequenceTaggedSite(SequenceBuilder domain, T feature) {
        super(domain, feature);
    }
}
