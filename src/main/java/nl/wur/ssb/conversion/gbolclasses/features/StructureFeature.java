package nl.wur.ssb.conversion.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StructureFeature<T extends life.gbol.domain.StructureFeature>
        extends GenomicFeature<T> {
    private static final Logger logger = LogManager.getLogger(StructureFeature.class);

    public StructureFeature(SequenceBuilder domain, T feature) {
        super(domain, feature);

    }

    public void handleGene(String value) {
        logger.warn("invalid gene tag on " + this.getClass().getName() + ", converting it to accession string");
        feature.addAccession(value);
    }

}
