package nl.wur.ssb.conversion.gbolclasses;

import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.conversion.flatfile.FlatFileEntry;
import nl.wur.ssb.conversion.flatfile.Flatfile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.ac.ebi.embl.api.validation.helper.FileUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*

//TODO
An optional name for the sample
name xsd:String?;
An optional short description of the sample
description xsd:String?;

A link to a specific citation related to the sample itself
citation @bibo:Document*;
A note on the sample
note @:Note*;

 */

public class Sample extends Thing {
    private static final Logger logger = LogManager.getLogger(Sample.class);
    private static final HashMap<String, String> compartments = new HashMap<String, String>();

    static {
        try {
            for (String item : FileUtils.readFile(Util.getResourceFile("CompartmentGo.tsv")).split("\n")) {
                if (item.trim().startsWith("#") || item.trim().equals(""))
                    continue;
                String[] elems = item.split("\t");
                String go = elems[0];
                String description = elems[1];
                compartments.put(description, "http://identifiers.org/go/" + go);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final life.gbol.domain.Sample sample;
    private final NASequence seq;

    public Sample(FlatFileEntry parent) {
        super(parent);
        this.sample = parent.getSample();
        this.seq = parent.getdNASequence();
    }

    /**
     * #organism --> replaced with taxonomy in organism object linked to sequence object
     * The taxonomy of the organism from which the sequence orginates
     * taxonomy @:TaxonomyRef?;
     */
    public void handleOrganism(String value) {
        //Already handle by the createSample function
    }

    // Salmon example
    public void handleBreed(String value) {
        Note note = this.parent.getDomain().make(Note.class, sample.getResource().getURI() + "/note");
        note.setText("Breed: " + value);
        Provenance noteProv = this.parent.getDomain().make(Provenance.class, note.getResource().getURI() + "/noteprov");
        noteProv.setOrigin(FlatFileEntry.annotResult);
        note.setProvenance(noteProv);
        sample.addNote(note);
    }

    /**
     * #type_material --> replaced with the nomenclaturalType in the TaxonomyRef under the Sequence object
     */
    public void handleTypeMaterial(String value) throws Exception {
        try {
            NomenclaturalType nomenclaturalType = (NomenclaturalType) GBOLUtil.getEnum(NomenclaturalType.class, value);
            // TODO Most likely a JENA bug... GCA_000012945 will not succeed without getIRI option...
            nomenclaturalType.getIRI();
            this.seq.getOrganism().getTaxonomy().setNomenclaturalType((NomenclaturalType) GBOLUtil.getEnum(NomenclaturalType.class, value));
        } catch (NullPointerException e) {
            logger.error("NomenclaturalType cannot be matched: " + value);

        }
    }

    /**
     * Replaced with sourceMaterial
     */
    public void handleCultureCollection(String value) {
        value = value.replace(">", "");
        value = value.replace("<", "");
        this.handleSourceMaterial(value, CollectionSampleType.CultureCollection);
    }

    /**
     * Replaced with sourceMaterial
     */
    public void handleBioMaterial(String value) {
        this.handleSourceMaterial(value, CollectionSampleType.OtherCollection);
    }

    /**
     * Replaced with sourceMaterial
     */
    public void handleSpecimenVoucher(String value) {
        this.handleSourceMaterial(value, CollectionSampleType.SpecimenVoucher);
    }

    /**
     * Identification of the material in a collection from which the sample is derived. Entries with more than one sampleSource indicates that the sequence was obtained from a sample that was deposited (by the submitter or a collaborator) in more than one collection.
     * sourceMaterial @:MaterialSource*;
     */
    private void handleSourceMaterial(String value, CollectionSampleType type) {
        String[] fields = value.split(":");
        MaterialSource matSource = parent.getDomain().make(MaterialSource.class, sample.getResource().getURI() + "/matSource/ " + GBOLUtil.cleanURI(value.replace(":", "_")));
        matSource.setCollectionCode(fields[0]);
        if (fields.length > 1)
            matSource.setInstitutionCode(fields[1]);
        else
            matSource.setInstitutionCode("Unknown");
        if (fields.length > 2)
            matSource.setSampleId(fields[2]);
        matSource.setCollectionSampleType(type);
        sample.addSourceMaterial(matSource);
    }

    /**
     * merged with serotype
     */
    public void handleSerovar(String value) {
        this.handleSerotype(value);
    }

    /**
     * germline --> moved to sequence & removed replace with rearranged = false
     */
    public void handleGermline(String value) {
        this.seq.setRearranged(false);
    }

    /**
     * #rearranged --> moved to sequence
     * Indicates if the sequence has undergone somatic rearrangement as part of an adaptive immune response.
     * If equal to false it is the unrearranged sequence that was inherited from the parenta germ line.
     * If equal to true the sequence has undergone somatic rearrangement as part of an adaptive immune response.
     * If not defined it is not applicable to the sequence or unknown.
     * rearranged xsd:boolean?;
     */
    public void handleRearranged(String value) {
        this.seq.setRearranged(true);
    }

    /**
     * #clone --> move to sequence, is a bit outdated now a days
     * If a clone library was used for sequencing, the sequence can orginate from one or more clones
     * clone xsd:String*;
     */
    public void handleClone(String value) {
        this.seq.addClone(value);
    }

    /**
     * #cloneLib xsd:String?; --> move to sequence, is a bit outdated now a days
     * If a clone library was used for sequencing, specify the clone library
     * cloneLib xsd:String?;
     */
    public void handleCloneLib(String value) {
        this.seq.setCloneLib(value);
    }

    /**
     * division --> removed information already captures by the taxonomy property
     */
    public void handleDivision(String value) {
    }

    /**
     * Replaced with the integratedInto property in the sequence object
     */
    public void handleTransgenic(String value) {
        logger.error("Transgenic property ignored: not implemented yet in conversion module");
    }

    /**
     * Replaced with the integratedInto property in the sequence object
     */
    public void handleFocus(String value) {
        logger.error("Focus property ignored: not implemented yet in conversion module");
    }

    /**
     * Indicates that this sequence from the macronuclear DNA from an organism which undergoes chromosomal
     * differentiation between macronuclear and micronuclear stages.
     * macronuclear xsd:boolean?;
     */
    public void handleMacronuclear(String value) {
        this.seq.setMacronuclear(true);
    }

    /**
     * #The primers used to get to get the sample from the collected material. The sample is the PCR product if the property is defined.
     * PCRPrimers @:PCRPrimerSet?;
     */
    public void handlePCR_primers(String value) throws Exception {
    /*
      Definition      PCR primers that were used to amplify the sequence.
                      A single /PCR_primers qualifier should contain all the primers used  
                      for a single PCR reaction. If multiple forward or reverse primers are                   
                      present in a single PCR reaction, multiple sets of fwd_name/fwd_seq 
                      or rev_name/rev_seq values will be  present.
      Value format    /PCR_primers="[fwd_name: XXX1, ]fwd_seq: xxxxx1,[fwd_name: XXX2,]
                      fwd_seq: xxxxx2, [rev_name: YYY1, ]rev_seq: yyyyy1, 
                      [rev_name: YYY2, ]rev_seq: yyyyy2"
                */
        PCRPrimerSet primerSet = this.parent.getDomain().make(PCRPrimerSet.class, this.sample.getResource().getURI() + "/primerset");
        String name = null;
        boolean isFwd = false;
        int count = 0;
        for (String elem : value.split(",")) {
            String[] tmp = elem.split(":");
            String header = tmp[0].trim();
            String val = tmp[1].trim();
            if (header.equals("fwd_name")) {
                name = val;
                isFwd = true;
            } else if (header.equals("rev_name")) {
                name = val;
                isFwd = false;
            } else if (header.equals("fwd_seq")) {
                if (name != null && !isFwd)
                    throw new Exception("expect fwd_name before fwd_seq");
                PCRPrimer primer = this.parent.getDomain().make(PCRPrimer.class, this.sample.getResource().getURI() + "/primer" + count++);
                if (name != null) {
                    primer.setCommonName(name);
                    name = null;
                }
                primer.setSequence(val);
                primerSet.setForwardPrimer(primer);
            } else if (header.equals("rev_seq")) {
                if (name != null && isFwd)
                    throw new Exception("expect rev_name before rev_seq");
                PCRPrimer primer = this.parent.getDomain().make(PCRPrimer.class, this.sample.getResource().getURI() + "/primer" + count++);
                if (name != null) {
                    primer.setCommonName(name);
                    name = null;
                }
                primer.setSequence(val);
                primerSet.setReversePrimer(primer);
            }
        }
        sample.setPCRPrimers(primerSet);
    }

    /**
     * Describes the physical, environmental and/or local geographical source of the biological sample from which the sequence was derived. Use geographicalLocation to denote the geographical location (examples: 'permanent Antarctic sea ice', 'rumen isolates from standard Pelleted ration-fed steer #67', 'denitrifying activated sludge from carbon limited continuous reactor')
     * isolationSource xsd:String?;
     */
    public void handleIsolationSource(String value) {
        sample.setIsolationSource(value);
    }

    /**
     * This denotes that this sample is the result of a bulk NA extraction method, therefore the organism of the sample and associated sequence(s) are not (exactly) known.
     * environmentalSample xsd:boolean?;
     */
    public void handleEnvironmentalSample(String value) {
        sample.setEnvironmentalSample(true);
    }

    /**
     * Altitude of the location from which the sample was collected in metres above or below nominal sea level
     * altitude xsd:Double?;
     */
    public void handleAltitude(String value) {
        // Removing all non valid characters...
        value = value.replaceAll("[^0-9]", "");
        sample.setAltitude(Double.parseDouble(value));
    }

    /**
     * The persons or institute who collected the specimen
     * collectedBy @foaf:Agent*;
     */
    public void handleCollectedBy(String value) {
        value = value.replace("<", "");
        sample.addCollectedBy(((FlatFileEntry) parent).makeAgent(value));
    }

    /**
     * The date and time on which the specimen was collected.
     * collectionDate xsd:dateTime?;
     */
    public void handleCollectionDate(String value) {
//    logger.info("DATETIME: "+value);
        try {
            sample.setCollectionDate(GBOLUtil.dateFormats(value));
        } catch (org.apache.jena.datatypes.DatatypeFormatException e) {

        }
        //System.err.println("Datetime parsing goes wrong according to org.apache.jena.datatypes");
    /*
     * RUNNING: /Users/jasperkoehorst/OneDrive - WageningenUR/Projects/16S/Data/results/GCA_000016945.1_ASM1694v1_genomic.gbff.gz
     * 2017-06-14 07:35:58 INFO  Logger:78 - Reading new entry
2017-06-14 07:35:58 INFO  Logger:80 - 8559
Parsing date: 1966
1966-01-01T00:00
2017-06-14 07:36:28 INFO  Logger:78 - Reading new entry
2017-06-14 07:36:28 INFO  Logger:80 - 273
Parsing date: 1966
2017-06-14 07:36:28 ERROR Logger:61 - InvocationTargetException: java.lang.reflect.InvocationTargetException
2017-06-14 07:36:28 ERROR Logger:62 - Cause: org.apache.jena.datatypes.DatatypeFormatException: Lexical form '1966-01-01T00:00' is not a legal instance of Datatype[http://www.w3.org/2001/XMLSchema#dateTime -> class org.apache.jena.datatypes.xsd.XSDDateTime] null
1966-01-01T00:00
2017-06-14 07:36:28 ERROR Logger:63 - [Ljava.lang.StackTraceElement;@30e868be
Parsing date: 1966
1966-01-01T00:00
2017-06-14 07:36:28 ERROR Logger:61 - InvocationTargetException: java.lang.reflect.InvocationTargetException
2017-06-14 07:36:28 ERROR Logger:62 - Cause: org.apache.jena.datatypes.DatatypeFormatException: Lexical form '1966-01-01T00:00' is not a legal instance of Datatype[http://www.w3.org/2001/XMLSchema#dateTime -> class org.apache.jena.datatypes.xsd.XSDDateTime] null
2017-06-14 07:36:28 ERROR Logger:63 - [Ljava.lang.StackTraceElement;@66c92293
qualifier: handleCollection_date not supported for value: 1966
FAILED: java.lang.NoSuchMethodException: java.lang.Object.handleCollection_date(java.lang.String)
FAILED: handleCollection_date
FAILED: null
FAILED: 1966
     */
    }

    /**
     * The geographical location(country or ocean) from which the sample is collected
     * geographicalLocation @:GeographicalLocation?;
     */
    public void handleCountry(String value) throws Exception {
        //"<country_value>[:<region>][, <locality>]"
        String[] tmp1 = value.split(",");
        String[] tmp2 = tmp1[0].split(":");
        String country = tmp2[0].trim();
        String region = null;
        String locality = null;
        if (tmp2.length > 1)
            region = tmp2[1];
        if (tmp1.length > 1)
            locality = tmp1[1];

        Country countryType = (Country) GBOLUtil.getEnum(Country.class, country);
        if (countryType != null) {
            LandBasedGeographicalLocation loc = this.parent.getDomain().make(LandBasedGeographicalLocation.class, this.sample.getResource().getURI() + "/geoloc");
            loc.setCountry(countryType);
            if (region != null)
                loc.setRegion(region);
            if (locality != null)
                loc.setLocality(locality);
            sample.setGeographicalLocation(loc);
        } else {
            Ocean oceanType = (Ocean) GBOLUtil.getEnum(Ocean.class, country);
            if (oceanType != null) {
                WaterBasedGeographicalLocation loc = this.parent.getDomain().make(WaterBasedGeographicalLocation.class, this.sample.getResource().getURI() + "/geoloc");
                loc.setOcean(oceanType);
                if (region != null)
                    loc.setRegion(region);
                if (locality != null)
                    loc.setLocality(locality);
                sample.setGeographicalLocation(loc);
            } else {
                logger.error("Unknown country field: " + country);
            }
        }
    }

    /**
     * The geographical coordinates(latitude + longtitude) of the location where the specimen was collected
     * geographicalCoordinate @:GeographicalCoordinate?;
     */
    public void handleLatLon(String value) throws Exception {
        //47.94 N 28.12 W
        GeographicalCoordinate coordinate = this.parent.getDomain().make(GeographicalCoordinate.class, this.sample.getResource().getURI() + "/geocoordinate");
        Matcher matcher = Pattern.compile("(\\d+(\\.\\d+)?)\\s([nNsS])\\s(\\d+(\\.\\d+)?)\\s([wWeE])").matcher(value);
        if (matcher.matches()) {
            double latitude = Double.parseDouble(matcher.group(1));
            if (matcher.group(3).equalsIgnoreCase("s"))
                latitude *= -1;
            double longtitude = Double.parseDouble(matcher.group(4));
            if (matcher.group(6).equalsIgnoreCase("w"))
                longtitude *= -1;
            coordinate.setLatitude(latitude);
            coordinate.setLongitude(longtitude);
        } else
            throw new Exception("latitude longtitude format is wrong: " + value);

        sample.setGeographicalCoordinate(coordinate);
    }

    /**
     * The developmental stage at which the organism was when the sample was obtained
     * devStage xsd:String?;
     */
    public void handleDevStage(String value) {
        sample.setDevStage(value);
    }

    /**
     * The name of the expert who identified the specimen taxonomically
     * identifiedBy @foaf:Agent?;
     */
    public void handleIdentifiedBy(String value) {
        sample.setIdentifiedBy(((FlatFileEntry) this.parent).makeAgent(value));
    }

    /**
     * Individual isolate from which the sequence was obtained. For example ('Patient #152','DGGE band PSBAC-13')
     * isolate xsd:String?;
     */
    public void handleIsolate(String value) {
        sample.setIsolate(value);
    }

    /**
     * The tissue library from which the sample was obtained
     * tissueLib xsd:String?;
     */
    public void handleTissueLib(String value) {
        sample.setTissueLib(value);
    }

    /**
     * The tissue type from which the sample was obtained, use term from the 'BRENDA Tissue and Enzyme Source Ontology' whenever possible
     * tissueType xsd:String?;
     */
    public void handleTissueType(String value) {
        sample.setTissueType(value);
    }

    /**
     * Type of membrane-bound intracellular structure from which the sample was obtained. Use a 'cellelar component' term from the GO ontology.
     * organelle IRI?;
     */
    public void handleOrganelle(String value) {
        sample.setOrganelle(compartments.get(value.replaceAll("^.*:", "")));
    }

    /**
     * The serological typing of the species within the sample by its antigenic properties
     * serotype xsd:String?;
     */
    public void handleSerotype(String value) {
        sample.setSerotype(value);
    }

    /**
     * The specific name of the host from which the sample has been collected, where the name indicates to which
     * habitat/ecological enviroment the host has adopted to. Example: 'Columbia', which indicate that this
     * Arabidopsis adopted itself with a hairier phenotype to deal with the especially sunny habitat.
     * ecotype xsd:String?;
     */
    public void handleEcotype(String value) {
        sample.setEcotype(value);
    }

    /**
     * For the host from which the sample was obtained the name for a combination of alleles that are
     * linked together on the same physical chromosome. In the absence of recombination, each haplotype is
     * inherited as a unit, and may be used to track gene flow in populations.
     * haplotype xsd:String?;
     */

    public void handleHaplotype(String value) {
        sample.setHaplotype(value);
    }

    /**
     * For the host from which the sample was obtained the name for a group of similar haplotypes that share some
     * sequence variation. Haplogroups are often used to track migration of population groups.
     * haplogroup xsd:String?;
     */
    public void handleHaplogroup(String value) {
        sample.setHaplogroup(value);
    }

    /**
     * The cultivar (cultivated variety) of plant from which sample was obtained.
     * cultivar xsd:String?;
     */
    public void handleCultivar(String value) {
        sample.setCultivar(value);
    }

    /**
     * The cell line from which the sequence was obtained, please use term from 'Cell Line Ontology' (http://www.clo-ontology.org/) where possible
     * cellLine xsd:String?;
     */
    public void handleCellLine(String value) {
        sample.setCellLine(value);
    }

    /**
     * The cell type from which the sequence was obtained, please use term from 'Cell Ontology' (http://cellontology.org/) where possible
     * cellType xsd:String?;
     */
    public void handleCellType(String value) {
        sample.setCellType(value);
    }

    /**
     * The host of the organism (from which the sample was obtained), which it lives in, upon or attached to in its natural enviroment.
     * host @:TaxonomyRef?;
     */
    public void handleHost(String value) {
        // TODO ! sample.setHost(value);
    }

    /**
     * Scientific name of the laboratory host used to propagate the source organism from which the sample was obtained.
     * The full binomial scientific name of the host organism should be used when known.
     * Extra conditional information relating to the host may also be included.
     * For example '12 year old human with IBD' for a gut bacteria.
     * labHost xsd:String?;
     */
    public void handleLabHost(String value) {
        sample.setLabHost(value);
    }

    /**
     * The mating type of the organism from which the sample was obtained. Mating type is used for prokaryotes, and for eukaryotes that undergo meiosis without sexually dimorphic gametes. For eukaryotic organisms that undergo meiosis with sexually dimorphic gametes please use the sex property.
     * matingType xsd:String?;
     */
    public void handleMatingType(String value) {
        sample.setMatingType(value);
    }

    /**
     * The sex of the organism from which the sample was obtained. Sex is used for eukaryotic organisms that undergo meiosis with sexually dimorphic gametes. For prokaryotes, and for eukaryotes that undergo meiosis without sexually dimorphic gametes please use the mathingType property.
     * sex xsd:String?;
     */
    public void handleSex(String value) {
        sample.setSex(value);
    }

    /**
     * The name of the subpopulation or phenotype from which the sample was collected.
     * popVariant xsd:String?;
     */
    public void handlePopVariant(String value) {
        sample.setPopVariant(value);
    }

    /*
     * The variety of the host from which the sample was derived. Use cultivar if the host is a cultivated plant.
     * variety xsd:String?;
     */
    public void handleVariety(String value) {
        sample.setVariety(value);
    }

    /**
     * The name or identifier of virus or phage (segment) from which the sample was obtained (using PCR).
     * segment xsd:String?;
     */
    public void handleSegment(String value) {
        this.sample.setSegment(value);
    }

    /**
     * Chromosome variable for the Sequence class accessed from the Sample parser...
     * Not to be confusded with Chromosome identifier from PCR which should not be within EMBL files
     * chromosome xsd:String?;
     */
    public void handleChromosome(String value) {
        Chromosome chr = (Chromosome) this.seq;
        chr.setChromosome(value);
        //this.sample.setChromosome(value);
    }

    /**
     * The map location on the chromosome from which the sample was obtained (using PCR).
     * map xsd:String?;
     */
    public void handleMap(String value) {
        this.sample.setMap(value);
    }

    /**
     * The name or identifier of plasmid from which the sample was obtained (using PCR).
     * plasmid xsd:String?;
     */
    public void handlePlasmid(String value) {
        this.sample.setPlasmid(value);
    }


    /**
     * The name or identifier of the strain from which the sample was obtained (using PCR).
     * strain xsd:String?;
     */
    public void handleStrain(String value) {
        this.sample.setStrain(value);
    }

    /**
     * The name or identifier of the sub-clone from which the sample was obtained (using PCR).
     * subClone xsd:String?;
     */
    public void handleSubClone(String value) {
        this.sample.setSubClone(value);
    }

    /**
     * The name or identifier of the sub-species from which the sample was obtained (using PCR).
     * subSpecies xsd:String?;
     */
    public void handleSubSpecies(String value) {
        this.sample.setSubSpecies(value);
    }

    /**
     * The name or identifier of a genetically or otherwise modified strain from which the sample was obtained (using PCR). The parental strain from which it is derived should be annotated with the strain property.
     * subStrain xsd:String?;
     */
    public void handleSubStrain(String value) {
        this.sample.setSubStrain(value);
    }

    /**
     * proviral --> moved to sequence & set proviralExtraction = true on sample
     * <p>
     * Indicate that this sample is an proviral sequence extracted from another organism using PCR.
     * proviralExtraction xsd:boolean?;
     */
    public void handleProviral(String value) {
        this.sample.setProviralExtraction(Boolean.parseBoolean(value));
        this.seq.setProviral(Boolean.parseBoolean(value));
    }

    public void handleNote(String value) {
        // TODO AnnotationResult rootURI not present... Same as Sequence.java Note
        Note note = this.parent.getDomain().make(Note.class, sample.getResource().getURI() + "/note");
        note.setText(value);
        //TODO ! get correct prov object

        Provenance noteProv = this.parent.getDomain().make(Provenance.class, note.getResource().getURI() + "/noteprov");
        this.parent.getDomain().disableCheck();
        AnnotationResult annotResult = this.parent.getDomain().make(AnnotationResult.class, Flatfile.args.annotURI);
        noteProv.setOrigin(annotResult);
        this.parent.getDomain().enableCheck();
        note.setProvenance(noteProv);
        sample.addNote(note);
    }

    /*
     *  Obtained while parsing GFF files
     */

    public void handleGenome(String value) {
        if (value.toLowerCase().matches("^chromosome$"))
            seq.setMacronuclear(true);
    }

    public void handleId(String value) {
        seq.addAccession(value);
    }

    /**
     * For GFF support
     *
     * @param value
     */
    public void handleName(String value) {
        seq.setShortName(value);
    }


    public void handleCitation(String value) {
        int num = Integer.parseInt(value.replaceAll("\\[|\\]", ""));
        Citation ref = this.parent.getRef(num);
        if (ref != null)
            this.sample.addCitation(ref);
        else
            logger.warn("ref with reference number " + num + " not found");
    }
}
