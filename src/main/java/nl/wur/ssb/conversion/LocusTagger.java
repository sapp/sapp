package nl.wur.ssb.conversion;

import life.gbol.domain.Gene;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

import java.util.Iterator;

public class LocusTagger {

    public static void tag(Domain domain) throws Exception {
        Iterable<ResultLine> results = domain.getRDFSimpleCon()
                .runQuery("getGeneWithoutLocusTag.sparql", true);
        Iterator<ResultLine> resultsIter = results.iterator();

        String prefix = "SAPP";
        int count = 1;
        while (resultsIter.hasNext()) {
            ResultLine resultLine = resultsIter.next();
            Gene gene = domain.make(Gene.class, resultLine.getIRI("gene"));
            gene.setLocusTag(prefix + "_" + count);
            count++;
        }
    }
}
