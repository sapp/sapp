package nl.wur.ssb.conversion.reduce;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.conversion.options.CommandOptionsReduce;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.jena.ext.com.google.common.io.Files;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Collection;

public class Reduce {
    private static final Logger logger = LogManager.getLogger(Reduce.class);

    public static void main(CommandOptionsReduce commandOptionsReduce) throws Exception {
        if (commandOptionsReduce.input != null) {
            reduce(commandOptionsReduce.domain, commandOptionsReduce.output);
        } else if (commandOptionsReduce.dir != null) {
            if (!commandOptionsReduce.recursive) {
                if (commandOptionsReduce.output.exists()) {
                    if (commandOptionsReduce.output.isFile()) {
                        throw new Exception("When using dir command output cannot be an existing file");
                    }
                    if (commandOptionsReduce.output.isDirectory()) {
                        System.out.println("Will store results in " + commandOptionsReduce.output.getAbsolutePath());
                    }
                } else {
                    commandOptionsReduce.output.mkdirs();
                }
                for (File file : commandOptionsReduce.dir.listFiles()) {
                    if (file.getName().endsWith(".hdt")) {
                        Domain domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(file);
                        reduce(domain, new File(commandOptionsReduce.output + "/" + file.getName()));
                    }
                }
            } else {
                // List all files recursively
                Collection files = FileUtils.listFiles(
                        commandOptionsReduce.dir,
                        new RegexFileFilter("^(.*?)"),
                        DirectoryFileFilter.DIRECTORY
                );
                for (Object file : files) {
                    System.err.println(file.toString());
                }
            }
        }
    }

    private static void reduce(Domain domain, File output) throws Exception {
        StmtIterator statements = domain.getRDFSimpleCon().getModel().listStatements();

        // Local store to merge into
        File tempDir = Files.createTempDir();
        Domain reduced = new Domain("file://" + tempDir);

        int totalStatementCount = 0;
        int reducedStatementCount = 0;
        while (statements.hasNext()) {
            totalStatementCount = totalStatementCount + 1;
            if (totalStatementCount % 100 == 0)
                System.err.print(totalStatementCount + " statements processed\r");
            Statement statement = statements.next();
            if (!statement.getPredicate().getLocalName().contains("sequence")) {
                reducedStatementCount = reducedStatementCount + 1;
                reduced.getRDFSimpleCon().getModel().add(statement);
            }
        }
        logger.info("Reduced " + totalStatementCount + " to " + reducedStatementCount);
        logger.info("Saving to " + output);
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(reduced, output);
    }
}
