package nl.wur.ssb.sparql;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class SPARQL {
    private static final Logger logger = LogManager.getLogger(SPARQL.class);
    private static final HashSet<Path> outputFiles = new HashSet<Path>();
    private static PrintStream outputStream = System.out;
    private static String queryString;
    private static String format;

    public static void main(String[] args) throws Exception {
        CommandOptionsSPARQL arguments = new CommandOptionsSPARQL(args);

        format = arguments.format;

        if (arguments.output != null) {
            outputStream = new PrintStream(arguments.output);
        }

        // Formatting the query (for galaxy if needed)
        queryString = sparqlFormatting(arguments.queryString);

        // Checking if query is in a file
        if (new File(arguments.queryString).exists()) {
            Scanner queryScan = new Scanner(new File(arguments.queryString));
            queryString = queryScan.useDelimiter("\\Z").next();
            queryScan.close();
        }

        // Testing if query string is a valid SPARQL query
        QueryFactory.create(queryString);

        // File query
        if (arguments.input != null) {
            HashSet<File> files = new HashSet<>();
            String[] HDTFilesSplit = arguments.input.getAbsolutePath().split(",");
            for (String hdtFile : HDTFilesSplit) {
                File folder = new File(hdtFile);
                // Check if it is a file (wildcards are then automatically detected as folder
                if (folder.isFile()) {
                    files.add(folder.toPath().toFile());
                } else if (folder.isDirectory()) {
                    logger.info("# Folder found checking content");
                    logger.info("Listing files from " + folder.getAbsolutePath() + " recursively: " + arguments.recursive);
                    files.addAll(new ArrayList(FileUtils.listFiles(folder, null, arguments.recursive)));
                    logger.info("Found: " + files.size() + " files");
                } else {
                    throw new Exception("This is neither a file or a folder");
                }
            }

            /**
             * Process files
             */

            AtomicInteger count = new AtomicInteger();

//            ForkJoinPool customThreadPool = new ForkJoinPool(arguments.jobs);
//            customThreadPool.submit(() -> files.parallelStream().forEach(rdfFile -> {
            for (File rdfFile : files) {
                count.set(count.get() + 1);
                if (count.get() % 10 == 0) {
                    logger.info("Parsing genome " + count + " " + files.size());
                }
                try {
                    Path tempFile = Files.createTempFile("sapp_sparql_", "." + arguments.format);
                    queryFile(rdfFile, arguments.format, tempFile);
                    outputFiles.add(tempFile);
                } catch (Exception e) {
                    logger.error("Query failed for " + rdfFile);
                    System.err.println(e.getMessage());
                    System.err.println(StringUtils.join(e.getStackTrace(), "\n"));
                }
            }
//            )).get();

//            while (!customThreadPool.isQuiescent()) {
//                logger.info("Current threads active " + customThreadPool.getActiveThreadCount());
//                SECONDS.sleep(1);
//            }

            finish();
        }
    }

    private static void finish() throws IOException {
        // Merge all output files
        byte[] buf = new byte[1 << 20]; // loads 1 MB of the file
        boolean start = true;
        for (Path outputFile : outputFiles) {
            logger.info("Processing " + outputFile.getFileName());
            Scanner scanner = new Scanner(outputFile);
            if (!start && scanner.hasNextLine())
                scanner.nextLine();
            while (scanner.hasNextLine()) {
                outputStream.println(scanner.nextLine());
            }
            scanner.close();
            start = false;
        }
    }

    private static void queryFile(File rdfFile, String format, Path outputFile) throws Exception {
        if (rdfFile.getName().endsWith(".hdt") || rdfFile.getName().endsWith(".dat")) {
            OutputStream outputStream = new PrintStream(outputFile.toFile());

            HDT hdt = HDTManager.mapIndexedHDT(rdfFile.getAbsolutePath(), null);
            HDTGraph graph = new HDTGraph(hdt);
            Model model = ModelFactory.createModelForGraph(graph);

            org.apache.jena.query.Query query = QueryFactory.create(queryString);
            QueryExecution qe = QueryExecutionFactory.create(query, model);
            org.apache.jena.query.ResultSet results = qe.execSelect();

            // Output query results
            if (format.toLowerCase().matches("csv")) {
                ResultSetFormatter.outputAsCSV(outputStream, results);
            } else if (format.toLowerCase().matches("tsv")) {
                ResultSetFormatter.outputAsTSV(outputStream, results);
            } else {
                throw new Exception("Output format unknown: " + format);
            }

            // Important - free up resources used running the query
            qe.close();
            model.close();
            graph.close();
            hdt.close();
            outputStream.close();
        }
    }

    private static String sparqlFormatting(String queryString) {
        queryString = queryString.replaceAll("__oc__", "{");
        queryString = queryString.replaceAll("__sq__", "'");
        queryString = queryString.replaceAll("__tc__", " ");
        queryString = queryString.replaceAll("__cr____cn__", " ");
        queryString = queryString.replaceAll("__dq__", "\"");
        queryString = queryString.replaceAll("__cc__", "}");
        queryString = queryString.replaceAll("__lt__", "<");
        queryString = queryString.replaceAll("__cn__", " ");
        queryString = queryString.replaceAll("__gt__", ">");
        queryString = queryString.replaceAll("__pd__", "#");
        return queryString;
    }

    public int read(Reader reader, char[] chars) throws IOException {
        return reader.read(chars);
    }
}
