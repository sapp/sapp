package nl.wur.ssb.sparql;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import java.io.File;

@Parameters(commandDescription = "Available options: ")
public class CommandOptionsSPARQL {

    @Parameter(names = {"-sparql"}, description = "Application: SPARQL query interface")
    boolean sparql;
    @Parameter(names = {"-i", "-input"}, description = "HDT input file or folder (regex file selection is possible) for querying, multiple folders can be given using , as separator")
    File input;
    @Parameter(names = {"-o", "-output"}, description = "Query result file, otherwise stdout")
    File output;
    @Parameter(names = {"-f", "-format"}, description = "Output format, csv / tsv / json / xml")
    String format = "tsv";
    @Parameter(names = {"-query"}, description = "SPARQL Query or FILE containing the query to execute", required = true)
    String queryString;
    @Parameter(names = {"-r", "-recursive"}, description = "Iteratively goes through sub directories searching for HDT files")
    boolean recursive;
    @Parameter(names = "-jobs", description = "Number of jobs to run in parallel")
    int jobs = 4;
    @Parameter(names = "-debug", description = "Debug mode")
    boolean debug = false;
    @Parameter(names = "-memory", description = "Store intermediate results into memory", hidden = true)
    Boolean memory = false;
    @Parameter(names = "--help")
    Boolean help = false;

    public CommandOptionsSPARQL(String[] args) {
        try {
            JCommander jc = new JCommander(this, args);

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
