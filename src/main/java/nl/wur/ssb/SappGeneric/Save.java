package nl.wur.ssb.SappGeneric;

import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.options.HDTSpecification;
import org.rdfhdt.hdtjena.HDTGraph;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.GZIPOutputStream;

import static nl.wur.ssb.SappGeneric.Generic.decompressGZip;

/**
 * Created by jasperkoehorst on 23/03/2017.
 */

public class Save {
    private static final Logger logger = LogManager.getLogger(Save.class);


    /**
     * Loads the given NTRIPLES (compressed or not) into a given endpoint as used by CommandOptionsGeneric
     *
     * @param commandOptionsGeneric default arguments with the -update
     * @param ntFile                the given rdf file that needs to be loaded into a SPARQL endpoint as shown by CommandOptionsGeneric
     */
    public static void save(CommandOptionsGeneric commandOptionsGeneric, File ntFile) throws IOException {
        Reader reader = new Reader();
        BufferedReader bufferedReader = reader.reader(ntFile);
        String strInsert = "INSERT DATA { ";
        int insertCounter = 0;
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            insertCounter = insertCounter + 1;

            String[] lineSplit = line.split(" ", 3);
            String subject = lineSplit[0];
            String predicate = lineSplit[1];
            String object = lineSplit[2];

            strInsert = strInsert + subject + " " + predicate + " " + object;
            if (insertCounter % 100 == 0) {
                if (insertCounter % 100000 == 0)
                    System.out.println("Loaded : " + insertCounter + " triples into endpoint " + commandOptionsGeneric.update + "\r");
                strInsert = strInsert + " } ";
                UpdateRequest updateRequest = UpdateFactory.create(strInsert);
                UpdateProcessor updateProcessor = UpdateExecutionFactory.createRemote(updateRequest, commandOptionsGeneric.update.toString());
                updateProcessor.execute();
                strInsert = "INSERT DATA { ";
            }
        }

        // Last batch
        System.out.println("Loading last batch \r");
        strInsert = strInsert + " } ";
        UpdateRequest updateRequest = UpdateFactory.create(strInsert);
        UpdateProcessor updateProcessor = UpdateExecutionFactory.createRemote(updateRequest, commandOptionsGeneric.update.toString());
        updateProcessor.execute();
        logger.info("Finished loading " + insertCounter + " triples");
    }


    private static void syncToEndpoint(CommandOptionsGeneric commandOptionsGeneric) {
        StmtIterator statements = commandOptionsGeneric.domain.getRDFSimpleCon().getModel().listStatements();
        String strInsert = "INSERT DATA { ";
        int insertCounter = 0;
        while (statements.hasNext()) {
            insertCounter = insertCounter + 1;

            Statement statement = statements.nextStatement();

            String subject = statement.getSubject().getURI();
            String predicate = statement.getPredicate().getURI();
            String object = statement.getObject().toString();
            if (statement.getObject().isURIResource()) {
                object = "<" + statement.getObject().toString() + ">";
            }

            if (statement.getObject().isLiteral()) {
                object = statement.getObject().asLiteral().getString();
                object = object.replaceAll("\\\\", "\\\\\\\\");
                object = object.replaceAll("\"", "\\\\\"");
            }

            if (object.startsWith("<http")) {
                // object = "<" + object + ">";
            } else {
                object = "\"" + object + "\"";
            }

            strInsert = strInsert + "<" + subject + "> <" + predicate + "> " + object + " . ";
            if (insertCounter % 100 == 0) {
                if (insertCounter % 100000 == 0)
                    System.out.println("Loaded : " + insertCounter + " triples into endpoint " + commandOptionsGeneric.update + "\r");
                strInsert = strInsert + " } ";
                UpdateRequest updateRequest = UpdateFactory.create(strInsert);
                UpdateProcessor updateProcessor = UpdateExecutionFactory.createRemote(updateRequest, commandOptionsGeneric.update.toString());
                updateProcessor.execute();
                strInsert = "INSERT DATA { ";
            }
        }

        // Last batch
        System.out.println("Loading last batch \r");
        strInsert = strInsert + " } ";
        UpdateRequest updateRequest = UpdateFactory.create(strInsert);
        UpdateProcessor updateProcessor = UpdateExecutionFactory.createRemote(updateRequest, commandOptionsGeneric.update.toString());
        updateProcessor.execute();
        System.out.println("Finished loading " + insertCounter + " triples");
    }


    /**
     * Save HDT to File with compression, always appends
     *
     * @param hdt    hdt file
     * @param output output file for .nt.gz
     * @throws IOException for writer
     */
    private static void save(HDT hdt, File output) {
        HDTGraph graph = new HDTGraph(hdt);
        Model model = ModelFactory.createModelForGraph(graph);
        PrintWriter writer = null;
        try {
            writer = getWriter(output);
            model.write(writer, "NTRIPLES");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Save Domain to File with compression, always appends
     *
     * @param domain rdf object to be saved
     * @param output output nt.gz file
     * @throws IOException
     */
    private static void save(Domain domain, File output) {
        // Merging two files as direct save from domain object forces overwrite...
//        System.err.println("save");
//        System.out.println(">>>>"  + domain.getRDFSimpleCon().getModel().size());

        // Saving to temp file
        try {
            File tmpFile = File.createTempFile("domain", ".nt");
//            System.out.println(">>>>"  + domain.getRDFSimpleCon().getModel().size());
            domain.save(tmpFile.getAbsolutePath(), RDFFormat.N_TRIPLE);

            // Merging...
            PrintWriter writer = getWriter(output);
            FileInputStream fis = new FileInputStream(tmpFile);
            BufferedReader in = new BufferedReader(new InputStreamReader(fis));
            String aLine;
            while ((aLine = in.readLine()) != null) {
                writer.println(aLine);
            }
            writer.close();
            tmpFile.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Creating a writer (always appends)
     *
     * @param output creates output writer with compression
     * @return
     * @throws IOException
     */
    private static PrintWriter getWriter(File output) throws IOException {
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(output, true)), StandardCharsets.UTF_8));
        return writer;
    }

    /**
     * Converts (plain/compressed) text ntriple to HDT file
     *
     * @param ntriple input file
     * @param hdt     output file
     */
    public static void toHDT(File ntriple, File hdt) throws Exception {
        toHDT(ntriple, hdt, RDFNotation.NTRIPLES);
    }

    /**
     * Converts any non-compressed to HDT
     *
     * @param input     input non-compressed rdf file
     * @param outputHDT output file for HDT format
     * @param notation  kind of input RDF format
     * @throws Exception ...
     */
    public static void toHDT(File input, File outputHDT, RDFNotation notation) throws Exception {
        System.out.println("Saving to file...");

        if (input.isDirectory()) {
            RDFSimpleCon con = new RDFSimpleCon("file://" + input);
            input = new File(input.getAbsolutePath() + ".tmp");
            con.save(input.getAbsolutePath());
            input.deleteOnExit();
        } else {
            // Check if gz?
            if (Generic.isGZipped(input)) {
                System.out.println("File is compressed, decompressing it...");
                decompressGZip(input, new File(input + ".decompressed"));
                new File(input + ".decompressed").deleteOnExit();
                input = new File(input + ".decompressed");
            }
        }
        String baseURI = "http://gbol.life/0.1/";

        HDT hdt = HDTManager.generateHDT(input.getAbsolutePath(), baseURI, notation, new HDTSpecification(), null);

        hdt.saveToHDT(outputHDT.getAbsolutePath(), null);
    }

    /**
     * Converts Domain to HDT file
     *
     * @param domain
     * @param hdt
     * @throws Exception
     */
    private static void toHDT(Domain domain, File hdt) throws Exception {
        Path tmp = Files.createTempFile("save", ".ttl");
        tmp.toFile().deleteOnExit();
        domain.save(tmp.toFile().getAbsolutePath());
        toHDT(tmp.toFile(), hdt, RDFNotation.TURTLE);
    }
}