package nl.wur.ssb.SappGeneric;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class FASTA {
    private static final Logger logger = LogManager.getLogger(FASTA.class);

    /**
     * @param options according to the commandoptions package
     * @param type    (genome) - (gene) - (protein) required
     * @return list of files
     * @throws Exception when issues arise
     */
    public static List<File> create(CommandOptionsGeneric options, String type) throws Exception {
        String queryFile;
        if (type.matches("gene")) {
//            queryFile = "getGene.txt";
//            return getFastaFromGenesOrProteins(queryFile, options);
            throw new Exception("To be implemented");
        } else if (type.matches("protein")) {
            queryFile = "getProteinsFromSample.txt";
            return getFastaFromGenesOrProteins(queryFile, options);
        } else if (type.matches("genome")) {
            queryFile = "getGenome.txt";
            return getFastaFromGenome(queryFile, options);
        } else {
            throw new Exception("(gene), (protein) or (genome) type string parameter needed");
        }
    }

    /**
     * Test to see if we can create large protein and gene fasta files when using endpoints
     *
     * @param queryFile
     * @param options
     * @return
     * @throws Exception
     */
    private static List<File> getFastaFromGenesOrProteins(String queryFile, CommandOptionsGeneric options) throws Exception {
        Iterator<ResultLine> resultLineIterator;
        // Multiple sample writer store...
        List<File> writerFiles = new ArrayList<>();

        if (options.input != null && options.input.getName().endsWith(".hdt")) {
            HDT hdt = HDTManager.loadIndexedHDT(options.input.getAbsolutePath(), null);
            resultLineIterator = options.domain.getRDFSimpleCon().runQuery(hdt, "getSample.txt").iterator();
        } else {
            resultLineIterator = options.domain.getRDFSimpleCon().runQuery("getSample.txt", false).iterator();
        }

        while (resultLineIterator.hasNext()) {
            ResultLine result = resultLineIterator.next();
            String sample = result.getIRI("sample");

            // Format sample for file
            String sampleName = sample.replaceAll(".*gbol.life/0.1/", "");
            sampleName = sampleName.replace("/", "__#__");
            File sampleFile = new File("./sapp_storage/" + sampleName + ".fasta");

            if (sampleFile.exists()) {
                logger.info("Fasta file already exists: " + sampleFile + " ... skipping creation");
                writerFiles.add(sampleFile);
            } else {
                Iterator<ResultLine> seqIterator;
                if (options.input != null && options.input.getName().endsWith(".hdt")) {
                    HDT hdt = HDTManager.loadIndexedHDT(options.input.getAbsolutePath());
                    seqIterator = options.domain.getRDFSimpleCon().runQuery(hdt, queryFile, sample).iterator();
                } else {
                    System.err.println("Sample: " + sample + " " + queryFile);
                    seqIterator = options.domain.getRDFSimpleCon().runQuery(queryFile, true, sample).iterator();
                }

                PrintWriter printWriter = new PrintWriter(sampleFile);
                while (seqIterator.hasNext()) {
                    ResultLine seq = seqIterator.next();
                    printWriter.println(">" + seq.getIRI("seqobject") + "\n" + seq.getLitString("sequence"));
                }
                printWriter.close();
                if (sampleFile.length() > 0) {
                    writerFiles.add(sampleFile);
                } else {
                    System.err.println("No sequences for " + sampleFile);
                    sampleFile.delete();
                }
            }
        }
        return writerFiles;
    }

    private static List<File> getFastaFromGenome(String queryFile, CommandOptionsGeneric options) throws Exception {
        logger.info("Obtaining sequences");

        HashMap<String, List<String>> sampleObjects = new HashMap<>();
        Iterator<ResultLine> resultLineIterator;

        resultLineIterator = options.domain.getRDFSimpleCon().runQuery(queryFile, false).iterator();

        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            String sample = resultLine.getIRI("sample");
            if (sample == null) {
                throw new NullPointerException("Sample cannot be null...");
            }
            // Format sample
            sample = sample.replaceAll(".*gbol.life/0.1/", "");
            sample = sample.replace("/", "__#__");
            if (sampleObjects.get(sample) == null)
                sampleObjects.put(sample, new ArrayList<>());

            sampleObjects.get(sample).add(resultLine.getIRI("seqobject"));
        }

        // Multiple sample writer store...
        HashMap<String, PrintWriter> writers = new HashMap<>();
        List<File> writerFiles = new ArrayList<>();

        logger.info("Found " + sampleObjects.size() + " sample(s)");

        logger.info("Writing to file");

        for (String sampleKey : sampleObjects.keySet()) {
            int count = 0;
            // Check if fasta file exists...
            logger.info("Creating FASTA file for: " + sampleKey);
            if (new File(new File(sampleKey).getName()).exists()) {
                logger.info("FASTA file exists... skipping: " + new File(new File(sampleKey).getName()).getName());
                count = count + 1;
                writerFiles.add(new File(new File(sampleKey).getName()));
            } else {
                for (String sequenceIRI : sampleObjects.get(sampleKey)) {
                    count++;
                    String sequence;

                    // TODO figure out why domain queries get clogged up and freeze
                    if (options.domain != null) {
                        sequence = SPARQL.getSequence(sequenceIRI, options.domain);
                    } else if (options.endpoint != null) {
                        String sparqlQueryString1 = "PREFIX gbol: <http://gbol.life/0.1/>" + "   SELECT ?sequence " + "   WHERE { " + "       <" + sequenceIRI + "> gbol:sequence ?sequence ." + "   }";
                        org.apache.jena.query.Query query = QueryFactory.create(sparqlQueryString1);
                        QueryExecution qexec = QueryExecutionFactory.sparqlService(options.endpoint.toString(), query);
                        sequence = qexec.execSelect().next().getLiteral("sequence").getString();
                        qexec.close();
                    } else {
                        throw new Exception("Acquiring sequence from input file or endpoint failed");
                    }


                    // Check to which sample it belongs to...
                    if (writers.get(sampleKey) == null) {
                        writers.put(sampleKey, new PrintWriter(new File(sampleKey).getName()));
                        writerFiles.add(new File(new File(sampleKey).getName()));
                    }

                    writers.get(sampleKey).write(">" + sequenceIRI + "\n" + sequence + "\n");

                    if (count % 1000 == 0) {
                        logger.info("Parsed " + count + " of " + sampleObjects.get(sampleKey).size() + " sequences\r");
                    }
                }
            }
            if (count == 0) {
                logger.error("No sequences found... using SPARQL query: " + queryFile + " on sample " + sampleKey);
            }
        }

        // Closing all writers
        for (PrintWriter writer : writers.values()) {
            writer.close();
        }

        logger.info("FASTA file(s) created");

        return writerFiles;
    }
}
