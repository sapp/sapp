package nl.wur.ssb.SappGeneric;

public class XRefDBEntry {
    private final String uriRegexPattern;
    private final String primaryRegexPattern;
    private final String uriSpace;
    private final String id;
    private final String uriLookupEndpoint;
    private final String title;
    private final String description;

    public XRefDBEntry(String uriRegexPattern, String primaryRegexPattern, String uriSpace, String id, String uriLookupEndpoint, String title, String description) {
        this.uriRegexPattern = uriRegexPattern;
        this.primaryRegexPattern = primaryRegexPattern;
        this.uriSpace = uriSpace;
        this.id = id;
        this.uriLookupEndpoint = uriLookupEndpoint;
        this.title = title;
        this.description = description;
    }

    public String getUriRegexPattern() {
        return uriRegexPattern;
    }

    public String getPrimaryRegexPattern() {
        return primaryRegexPattern;
    }

    public String getUriSpace() {
        return uriSpace;
    }

    public String getId() {
        return id;
    }

    public String getUriLookupEndpoint() {
        return uriLookupEndpoint;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
