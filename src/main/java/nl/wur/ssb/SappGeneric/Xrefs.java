package nl.wur.ssb.SappGeneric;

import life.gbol.domain.XRefProvenance;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;

public class Xrefs {

    private static final HashMap<String, XRefDBEntry> dbEntries = new HashMap<>();
    static HashSet<String> warnings =  new HashSet<>();

    static {
        dbEntries.put("<empty>", new XRefDBEntry("No uriRegexPattern available", "No PrimaryRegexPattern available", "",
                "No id available", "", "No title available", "No description available"));
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(Util.getResourceFile("identifiers.tsv")));

            int lineCount = 0;
            while (br.ready()) {
                String line = br.readLine();
                lineCount++;
                String[] lineSplit = line.split("\t");
                if (lineSplit.length > 2) {
                    String databaseName = lineSplit[0].toLowerCase();

                    // String uriRegexPattern = lineSplit[0].toLowerCase().replaceAll("^\"|\"$", "");
                    String PrimaryRegexPattern = lineSplit[1].replaceAll("^\"|\"$", "");
                    String uriSpace = lineSplit[2];
                    String id = lineSplit[3];
                    String uriLookupEndpoint = lineSplit[4];
                    String title = lineSplit[5];
                    String description = lineSplit[6];

                    dbEntries.put(id.toLowerCase(), new XRefDBEntry(databaseName, PrimaryRegexPattern,
                            uriSpace, id, uriLookupEndpoint, title, description));

                } else {
                    throw new RuntimeException("This is an incomplete line: " + lineCount + " " + line);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static life.gbol.domain.XRef create(Domain domain, XRefProvenance xRefProv,
                                               String dbIdentifier, String primary) {
        return create(domain, xRefProv, dbIdentifier, primary, null);
    }

    public static life.gbol.domain.XRef create(Domain domain, XRefProvenance xRefProv,
                                               String dbIdentifier, String primary, String secondary) {
        return create(life.gbol.domain.XRef.class, domain, xRefProv, dbIdentifier, primary, secondary);
    }

    public static life.gbol.domain.XRef create(Class clazztype, Domain domain, XRefProvenance xRefProv, String dbIdentifier, String primary, String secondary) {
        return create(clazztype, domain, xRefProv, dbIdentifier, "x.x", primary, secondary);
    }

    public static life.gbol.domain.XRef create(Class clazztype, Domain domain,
                                               XRefProvenance xRefProv, String dbIdentifier, String dbVersion, String primary, String secondary) {

        XRefDBEntry xRefDBEntry = lookup(dbIdentifier, primary);

        life.gbol.domain.Database database;
        if (dbVersion != null) {
            database = domain.make(life.gbol.domain.Database.class, "http://gbol.life/0.1/db/" + xRefDBEntry.getId() + "/" + dbVersion);
        } else {
            database = domain.make(life.gbol.domain.Database.class,
                    "http://gbol.life/0.1/db/" + xRefDBEntry.getId());
        }

        database.setId(xRefDBEntry.getId());

        if (xRefDBEntry.getDescription().length() != 0) {
            database.setDescription(xRefDBEntry.getDescription());
        }
        if (xRefDBEntry.getUriSpace().length() != 0) {
            database.setUriSpace(xRefDBEntry.getUriSpace());
        }

        if (xRefDBEntry.getUriLookupEndpoint().length() != 0) {
            database.setUriLookupEndpoint(xRefDBEntry.getUriLookupEndpoint());
        }

        if (xRefDBEntry.getTitle().length() != 0) {
            database.setTitle(xRefDBEntry.getTitle());
        }

        if (dbVersion != null) {
            database.setHasVersion(dbVersion);
        }

        // We currently use the primary accession regex to find a hit
        if (xRefDBEntry.getPrimaryRegexPattern() != null) {
            database.setUriRegexPattern(xRefDBEntry.getPrimaryRegexPattern());
        } else {
            // But if there is none we use the url pattern
            database.setUriRegexPattern(xRefDBEntry.getUriRegexPattern());
        }

        String primaryIRI =
                "http://gbol.life/0.1/db/" + xRefDBEntry.getId() + "/" + GBOLUtil.cleanURI(primary);
        // Creation of xref using database and primary accession
        life.gbol.domain.XRef xref = domain.make(clazztype, primaryIRI);

        xref.setDb(database);
        xref.setAccession(primary);
        xref.setProvenance(xRefProv);

        if (secondary != null) {
            xref.addSecondaryAccession(secondary);
        }
        return xref;
    }

    public static XRefDBEntry lookup(String db, String pacc) {
        XRefDBEntry res = dbEntries.get(db.toLowerCase().trim());
        if (res == null) {
            return new XRefDBEntry("No uriRegexPattern available", "No PrimaryRegexPattern available", "",
                    db, "", "No title available", "No description available");
        }
        // Checks if the pattern of the accession number matches if there is one...
        if (!res.getPrimaryRegexPattern().isEmpty()) {
            // Test capitalization... if no match with normal .. lower .. UPPER ..
            if (pacc.matches(res.getPrimaryRegexPattern()))
                return res;
            else if (pacc.toLowerCase().matches(res.getPrimaryRegexPattern()))
                return res;
            else if (pacc.toUpperCase().matches(res.getPrimaryRegexPattern()))
                return res;
            else {
                String warningMessage = "We found the database (" + db + ") but regex pattern: >" + res.getPrimaryRegexPattern() + "< did not match to: ";
                // To prevent spamming the same warning message
                if (!warnings.contains(warningMessage)) {
                    System.err.println(warningMessage + pacc);
                    warnings.add(warningMessage);
                }
            }
        }
        return res;
    }
}
