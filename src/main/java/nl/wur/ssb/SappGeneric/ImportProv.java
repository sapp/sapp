package nl.wur.ssb.SappGeneric;

import life.gbol.domain.AnnotationSoftware;
import life.gbol.domain.AutomaticAnnotationActivity;
import life.gbol.domain.LocalDataFile;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import org.apache.jena.ext.com.google.common.hash.Hashing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3.ns.prov.domain.Agent;
import org.w3.ns.prov.domain.Entity;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ImportProv {

    static final Logger logger = LogManager.getLogger(ImportProv.class);

    private final AnnotationSoftware software;
    private final AutomaticAnnotationActivity activity;
    private LocalDataFile inputFile;
    private LocalDataFile outputFile;

    /**
     * @param domain
     * @param inputs
     * @param output
     * @param commandLine
     * @param toolName
     * @param toolVersion
     * @param repository
     * @param userAgentIri
     * @param starttime
     * @param endtime
     * @throws Exception
     */
    public ImportProv(Domain domain, List<String> inputs, File output, String commandLine, String toolName, String toolVersion, String repository, String userAgentIri, LocalDateTime starttime, LocalDateTime endtime)
            throws Exception {

        List<String> md5s = new ArrayList<>();
        for (String input : inputs) {
            if (new File(input).exists()) {
                md5s.add(Generic.checksum(new File(input), Hashing.md5()));
            } else {
                md5s.add("sparql endpoint");
            }
        }

        String toolId = toolName.replaceAll(" ", "") + "/" + toolVersion.replace(".", "_");
        software = domain.make(AnnotationSoftware.class, "http://gbol.life/0.1/" + toolId);
        software.setName(toolName);
        software.setVersion(toolVersion);
        software.setCodeRepository(repository);

        if (userAgentIri != null) {
            Agent agent = domain.make(Agent.class, userAgentIri);
            software.setActedOnBehalfOf(agent);
        }

        // TODO ! other imported files
        int index = -1;
        for (String input : inputs) {
            index++;
            if (new File(input).exists()) {
                inputFile = GBOLUtil.createLocalFile(domain, new File(input), md5s.get(index));
                inputFile = domain.make(LocalDataFile.class, "http://gbol.life/0.1/file/" + md5s.get(index));
                inputFile.setLocalFileName(new File(input).getName());
                inputFile.setLocalFilePath(new File(input).getAbsolutePath());
                inputFile.setTitle("Input file");
                inputFile.setMd5(md5s.get(index));
                inputFile.setSize(new File(input).length());
                inputFile.setDescription("The input that has been used");
                inputFile.setHasVersion(Generic.checksum(new File(input), Hashing.sha384()));
            }
        }

        String paramLine = "";
        if (output != null) {
            outputFile = GBOLUtil.createLocalFile(domain, output, output.getName());
            outputFile.setTitle("Output rdf file");
            outputFile.setDescription("The result in the GBOL format");
            outputFile.setUriSpace("http://gbol.life/0.1/");

            // TODO ! Include extra rdf files when given
            // TODO fix parameter line as it is now empty
            // Creates extra information on inputFiles
            if (inputFile != null) {
                paramLine = GBOLUtil.createParamString(commandLine, "input", inputFile, "output", outputFile);
            }

            if (paramLine.length() == 0) {
                paramLine = commandLine;
            }
        }

        /*
         * Setting the run time activity
         */

        activity = domain.make(AutomaticAnnotationActivity.class, "http://gbol.life/0.1/" + toolId + "/" + starttime);

        activity.setStartedAtTime(starttime);
        activity.setEndedAtTime(endtime);
        activity.setParameterString(paramLine);

        if (inputFile != null)
            activity.addUsed(inputFile);

        if (outputFile != null) {
            activity.addGenerated(outputFile);
            outputFile.setWasGeneratedBy(activity);
            outputFile.setWasAttributedTo(software);
        }

        if (outputFile != null && inputFile != null)
            outputFile.addWasDerivedFrom(inputFile);
    }

    /**
     * @param domain       RDFSimpleCon object
     * @param inputs       List of files used as input
     * @param output       GBOL output file location
     * @param commandLine  Join of the args
     * @param toolName     Name of the application used
     * @param toolVersion  Version of the application used
     * @param repository   GIT repo if present
     * @param userAgentIri URL of agent if present (e.g. ORCID)
     * @param starttime    Initialization of the code run
     * @param endtime      End of the code run
     * @throws Exception
     */
    public ImportProv(Domain domain, List<String> inputs, File output, String commandLine, String toolName, String toolVersion, String repository, String userAgentIri, LocalDateTime starttime, LocalDateTime endtime, List<String> md5s)
            throws Exception {

        logger.info("Info: " + toolName + " " + toolVersion);

        String toolId = toolName.replaceAll(" ", "") + "/" + toolVersion.replace(".", "_");
        software = domain.make(AnnotationSoftware.class, "http://gbol.life/0.1/" + toolId);
        software.setName(toolName);
        software.setVersion(toolVersion);
        software.setCodeRepository(repository);

        if (userAgentIri != null) {
            Agent agent = domain.make(Agent.class, userAgentIri);
            software.setActedOnBehalfOf(agent);
        }

        // TODO ! other imported files
        int index = -1;
        for (String input : inputs) {
            index++;
            if (new File(input).exists()) {
                inputFile = GBOLUtil.createLocalFile(domain, new File(input), md5s.get(index));
                inputFile = domain.make(LocalDataFile.class, "http://gbol.life/0.1/file/" + md5s.get(index));
                inputFile.setLocalFileName(new File(input).getName());
                inputFile.setLocalFilePath(new File(input).getAbsolutePath());
                inputFile.setTitle("Input file");
                inputFile.setMd5(md5s.get(index));
                inputFile.setSize(new File(input).length());
                inputFile.setDescription("The input that has been used");
                inputFile.setHasVersion(Generic.checksum(new File(input), Hashing.sha384()));
            }
        }

        /*
         * Setting the run time activity
         */

        activity = domain.make(AutomaticAnnotationActivity.class, "http://gbol.life/0.1/" + toolId + "/" + starttime);

        activity.setStartedAtTime(starttime);
        activity.setEndedAtTime(endtime);
        activity.setParameterString(commandLine);
        // activity.setParameterString(paramLine);

        if (inputFile != null)
            activity.addUsed(inputFile);

        if (outputFile != null) {
            activity.addGenerated(outputFile);
            outputFile.setWasGeneratedBy(activity);
            outputFile.setWasAttributedTo(software);
        }

        if (inputFile != null && outputFile != null)
            outputFile.addWasDerivedFrom(inputFile);
    }


    public void linkEntity(Entity entity) {
        entity.setWasAttributedTo(this.software);

        if (this.inputFile != null)
            entity.addWasDerivedFrom(this.inputFile);

        entity.setWasGeneratedBy(this.activity);

        if (outputFile != null)
            outputFile.addContainedEntity(entity);
    }

    public void finished() throws IOException {
        this.activity.setEndedAtTime(LocalDateTime.now());
    }
}
