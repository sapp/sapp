package nl.wur.ssb.SappGeneric;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

/**
 * scanDependencies class which can checks a dependencies file, download dependencies and unpack them.
 * uses Unzip.java(SappGeneric) and RetrieveFromURL(SappGeneric)
 */
public class installToolDeps {
    RetrieveFromURL retrieveFromURL = new RetrieveFromURL();
    URL[] urlArray;
    File FullDir;

    /**
     * Method to scan the dependencies textfile and initiate the download+unpacking
     *
     * @param InputDir     Input directory to download files to.
     * @param dependencies Textfile with the download information
     * @throws Exception All exceptions are caught with this exception
     */

    public void retrieveDeps(String InputDir, Scanner dependencies) throws Exception {
        retrieveDeps(InputDir, dependencies, null);
    }

    /**
     * @param InputDir     Input directory to download files to.
     * @param dependencies Textfile with the download information
     * @param version      optional parameter to restrict the dependencies to a specific line e.g. version number
     * @throws Exception All exceptions are caught with this exception
     */
    public void retrieveDeps(String InputDir, Scanner dependencies, String version) throws Exception {
        //For every line in the file(downloaddirectory + download URL...)
        while (dependencies.hasNextLine()) {
            String line = dependencies.nextLine();
            if (!line.startsWith("#")) {
                String[] lineSplit = line.trim().split("\t");
                String path = lineSplit[0] + lineSplit[1];
                // TODO look for ordered list
                String[] urls = Arrays.copyOfRange(lineSplit, 2, lineSplit.length);
                this.urlArray = new URL[urls.length];
                int count = -1;
                for (String url : urls) {
                    count += 1;
                    this.urlArray[count] = new URL(url);
                }

                this.FullDir = new File(InputDir + path);

                //If the file does not exist, download it and the version option is not used
                if (!this.FullDir.exists() && version == null) {
                    // line remains

                } else if (!this.FullDir.exists() && version != null && line.contains(version)) {
                    // if a version is given and it matches within the line...
                    // line remains
                } else {
                    line = null;
                }

                // Downloading...
                if (line != null) {
                    System.out.println("Downloading: " + line);
                    //Retrieve HTTP/FTP
                    retrieveFromURL.URLToFile(this.urlArray, this.FullDir);
                    if (retrieveFromURL.getConData() == 1) {
                        throw new Exception("The connection could not be established. For the links");
                    }
                    //Unpack
                    Unzip unzip = new Unzip(this.FullDir);
                    unzip.unArchive();
                }
            }
        }
    }
}
