package nl.wur.ssb.SappGeneric;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Unzip {
    private static final Logger logger = LogManager.getLogger(Unzip.class);
    private final File inFile;
    private String unPackDir;

    /**
     * Unzip constructor
     *
     * @param inputFile Input file to extract
     */

    public Unzip(File inputFile) {
        inFile = inputFile;
    }

    /**
     * Method to distinguish which unarchive method to use.
     *
     * @throws Exception All exceptions are thrown
     */

    public void unArchive() throws Exception {
        ///recognise the right extention and call the right unziper.
        // if not necessary return no error, if not possible return error
        if (inFile.getName().endsWith("tar.gz")) {
            tarGzToFile();
        } else if (inFile.getName().endsWith("gz")) {
            faGzToFile();
        } else if (inFile.getName().endsWith("zip")) {
            zipToFile();
        } else {
            logger.info("No unarchive needed or detected");
            return;
        }
        // If file was unpacked.. create 0 byte file for checking
        inFile.createNewFile();
    }

    /**
     * Tar.GZ to file method. To convert tar.gz in folder.
     *
     * @return Returns a List of files.
     * @throws IOException      throws an IOException(fault in input)
     * @throws ArchiveException throws an  ArchiveException(fault in the archive)
     */

    private List<File> tarGzToFile() throws IOException, ArchiveException {
        unPackDir = inFile.getParent();
        logger.info(String.format("Untaring %s to dir %s", inFile.getAbsolutePath(), unPackDir));
        if (inFile.length() / 1024 / 1024 > 1000)
            System.err.println("Untaring huge files can take a while");

        final List<File> untaredFiles = new LinkedList<>();
        final InputStream is = new FileInputStream(inFile);
        GZIPInputStream gis = new GZIPInputStream(is);
        final TarArchiveInputStream debInputStream = (TarArchiveInputStream) new ArchiveStreamFactory().createArchiveInputStream("tar", gis);
        TarArchiveEntry entry = null;
        while ((entry = (TarArchiveEntry) debInputStream.getNextEntry()) != null) {

            final File outputFile = new File(unPackDir, entry.getName());

            if (entry.isDirectory()) {
                if (!outputFile.exists()) {
                    if (!outputFile.mkdirs()) {
                        throw new IllegalStateException(String.format("Couldn't create directory %s.", outputFile.getAbsolutePath()));
                    }
                }
            } else {
                File parent = new File(outputFile.getParent());
                if (!parent.exists())
                    parent.mkdirs();
                final OutputStream outputFileStream = new FileOutputStream(outputFile);
                IOUtils.copy(debInputStream, outputFileStream);
                outputFileStream.close();
            }
//            System.err.println("File: "+outputFile);
            untaredFiles.add(outputFile);
        }
        debInputStream.close();
        Files.delete(inFile.toPath());

        return untaredFiles;
    }

    /**
     * Method to extract fa.gz or fasta.gz files.
     *
     * @throws IOException      throws an IOException(fault in input)
     * @throws ArchiveException throws an  ArchiveException(fault in the archive)
     */

    private void faGzToFile() throws IOException, ArchiveException {
        unPackDir = inFile.getAbsolutePath().replace(".gz", "");
        System.err.printf("Unarchive %s to dir %s%n", inFile.getAbsolutePath(), unPackDir);
        System.err.println("Unarchiving huge files can take a while");
        try {
            final InputStream fis = new FileInputStream(inFile);
            GZIPInputStream gis = new GZIPInputStream(fis);
            byte[] buffer = new byte[1024];
            int length;
            final FileOutputStream fos = new FileOutputStream(unPackDir);
            while ((length = gis.read(buffer)) > 0) {
                fos.write(buffer, 0, length);
            }
            fos.close();
            gis.close();
            fis.close();
            Files.delete(inFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void zipToFile() throws IOException {
        unPackDir = inFile.getAbsolutePath().replace(".zip", "");
        System.err.printf("Unarchive %s to dir %s%n", inFile.getAbsolutePath(), unPackDir);
        System.err.println("Unarchiving huge files can take a while");
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(inFile))) {

            ZipEntry entry = zis.getNextEntry();

            while (entry != null) {

                File file = new File(new File(unPackDir).getParent(), entry.getName());

                if (entry.isDirectory()) {
                    file.mkdirs();
                } else {
                    File parent = file.getParentFile();

                    if (!parent.exists()) {
                        parent.mkdirs();
                    }

                    try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file))) {

                        byte[] buffer = new byte[4096];

                        int location;

                        while ((location = zis.read(buffer)) != -1) {
                            bos.write(buffer, 0, location);
                        }
                    }
                }
                entry = zis.getNextEntry();
            }
        }
        Files.delete(inFile.toPath());
    }
}
