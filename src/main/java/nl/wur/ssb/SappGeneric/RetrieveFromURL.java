package nl.wur.ssb.SappGeneric;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class RetrieveFromURL {
    private static final Logger logger = LogManager.getLogger(RetrieveFromURL.class);

    private int conData = 0;

    /**
     * Method to retrieve files from an URL and store them in an output file.
     *
     * @param URLs   All URL's which can be tried, only http and ftp will be tried to convert.
     * @param output Storing dir/file
     * @return File output file
     * @throws Exception All exceptions are thrown
     */
    public File URLToFile(URL[] URLs, File output) throws Exception {
        boolean worked = false;
        //for every url, check which protocol should be used and download the files.
        for (URL url : URLs) {
            logger.info("Trying to access: " + url);
            if ((url.getProtocol().equals("http") || url.getProtocol().equals("https")) && !worked) {
                logger.info("Protocol: HTTP");
                HttpURLConnection huc = (HttpURLConnection) url.openConnection();
                huc.setRequestMethod("GET");
                try {
                    int codeVal = huc.getResponseCode();
                    huc.connect();
                    if (codeVal == 200) {
                        logger.error("Downloading file, this can take a while");
                        FileUtils.copyURLToFile(url, output);
                        worked = true;
                    }
                } catch (Exception ex) {
                    logger.error(ex.getLocalizedMessage());
                    logger.error("An connection error occured, please check your connection and the URL");
                    this.conData = 1;
                }

            }
            if (url.getProtocol().equals("ftp") && !worked) {
                logger.error("Protocl: FTP");
                URLConnection huc = url.openConnection();
                try {
                    huc.connect();
                    logger.error("Downloading file, this can take a while");
                    FileUtils.copyURLToFile(url, output);
                    worked = true;
                } catch (Exception ex) {
                    logger.error(ex.getLocalizedMessage());
                    logger.error("connection error occured, please check your connection and the URL");
                    this.conData = 1;
                }

            }
        }
        // Can be removed
        return output;
    }

    /**
     * Method to retrieve the errorcode value. If an exception is catch, this value will be increased.
     *
     * @return int Errorvalue
     */
    public int getConData() {
        return this.conData;
    }
}
