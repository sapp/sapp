package nl.wur.ssb.SappGeneric;

import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.io.IOUtils;
import org.apache.jena.ext.com.google.common.hash.HashCode;
import org.apache.jena.ext.com.google.common.hash.HashFunction;
import org.apache.jena.ext.com.google.common.hash.HashingInputStream;
import org.apache.jena.ext.com.google.common.io.Files;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static nl.wur.ssb.RDFSimpleCon.Util.getOs;
import static nl.wur.ssb.RDFSimpleCon.Util.getResourceFile;

public class Generic {
    private static final Logger logger = LogManager.getLogger(Generic.class);

    /**
     * Generates the checksum of the given type for the string content.
     *
     * @param checksumType type of checksum ("MD5" or "SHA384")
     * @return checksum string
     */
    public static String checksum(String forCheckSum, String checksumType) {

        try {
            MessageDigest md = MessageDigest.getInstance(checksumType); // "SHA-384"
            md.update(forCheckSum.getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }

            return (sb.toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Generates the MD5 checksum of the file content.
     *
     * @param file         first file to read for checksum
     * @param checksumType type of checksum ("MD5" or "SHA384")
     * @return checksum string
     */
    public static String checksum(File file, HashFunction checksumType) throws IOException {
        HashCode hc = Files.hash(file, checksumType);
        return hc.toString();
    }

    public static String checksum(InputStream stream, HashFunction checksumType) throws IOException {
        HashingInputStream hashingInputStream = new HashingInputStream(checksumType, stream);
        return hashingInputStream.hash().toString();
    }

    public static void Execute(String command) throws Exception {
        ExecCommand exec = new ExecCommand(command);
        if (exec.getExit() > 0) {
            logger.info(command);
            logger.error("Error: " + exec.getError());
            logger.info("Exit: " + exec.getExit());
            logger.info("Output: " + exec.getOutput());
            throw new Exception("Issues with program");
        }
    }

    /**
     * @param clazz of the main program to obtain the right manifest and versoin
     * @return The {version, and the commit hash} from git in that order
     * @throws MalformedURLException
     * @throws IOException
     */
    public static String[] getVersion(Class<?> clazz) throws MalformedURLException, IOException {
        String className = clazz.getSimpleName() + ".class";
        String classPath = clazz.getResource(className).toString();

        if (classPath.startsWith("jar")) {
            String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) + "/META-INF/MANIFEST.MF";
            Manifest manifest = new Manifest(new URL(manifestPath).openStream());
            Attributes attr = manifest.getMainAttributes();

            String ImplementationVersion = attr.getValue("Implementation-Version");
            String ImplementationSha = attr.getValue("Implementation-Sha");

            String[] versioning;
            if (ImplementationVersion == null && ImplementationSha == null) {
                versioning = new String[]{"2.0", ""};
            } else {
                versioning = new String[]{ImplementationVersion, ImplementationSha};
            }

            return versioning;
        } else {
            String[] versioning = {"test-run", "test-run"};
            return versioning;
        }
    }

    /**
     * Checks if a file is gzipped.
     *
     * @param f
     * @return
     */
    public static boolean isGZipped(File f) {
        int magic = 0;
        try {
            RandomAccessFile raf = new RandomAccessFile(f, "r");
            magic = raf.read() & 0xff | ((raf.read() << 8) & 0xff00);
            raf.close();
        } catch (Throwable e) {
            e.printStackTrace(System.err);
        }
        return magic == GZIPInputStream.GZIP_MAGIC;
    }

    public static void compressGZip(File INPUT_FILE, File OUTPUT_GZIP_FILE) throws IOException {
        logger.info("Compressing " + INPUT_FILE + " to " + OUTPUT_GZIP_FILE);
        // Create a FileInputStream for the input file
        FileInputStream fis = new FileInputStream(INPUT_FILE);

        // Create a GZIPOutputStream for the output file
        GZIPOutputStream gos = new GZIPOutputStream(new FileOutputStream(OUTPUT_GZIP_FILE));

        // Create a buffer to read data from the input file
        byte[] buffer = new byte[1024];

        // Read data from the input file and write it to the GZIPOutputStream
        int len;
        while ((len = fis.read(buffer)) > 0) {
            gos.write(buffer, 0, len);
        }

        // Close the input and output streams
        fis.close();
        gos.finish();
        gos.close();
    }

    public static void decompressGZip(File INPUT_GZIP_FILE, File OUTPUT_FILE) throws IOException {

        byte[] buffer = new byte[1024];
        GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(INPUT_GZIP_FILE));

        FileOutputStream out = new FileOutputStream(OUTPUT_FILE);

        int len;
        while ((len = gzis.read(buffer)) > 0) {
            out.write(buffer, 0, len);
        }
        out.close();
        gzis.close();
    }

    // From RDFSimpleCon

    public static String prepareFileFromJar(String file) throws IOException {
        return prepareFileIntFromJar(file, false, true);
    }

    public static String prepareBinaryFromJar(String file) throws IOException {
        return prepareFileIntFromJar(file, true, true);
    }

    public static String getTempFile(String file) throws IOException {
        return prepareFileIntFromJar(file, false, false);
    }

    public static void cleanTemp() throws IOException {
        String tempPath = getTempPath();
        (new File(tempPath)).delete();
    }

    private static String prepareFileIntFromJar(String file, boolean isBinary, boolean doCreate) throws IOException {
        String tempPath = getTempPath();
        (new File(tempPath)).mkdir();
        File outBinFileName = new File(tempPath + file.substring(Math.max(0, file.lastIndexOf("/"))));
        if (!outBinFileName.exists() && doCreate) {
            PrintStream var10000 = System.out;
            String var10001 = file.replaceAll("\\{OS\\}", getOs());
            var10000.println(var10001 + "\n" + outBinFileName);
            InputStream stream = getResourceFile(file.replaceAll("\\{OS\\}", getOs()));
            FileOutputStream output = new FileOutputStream(outBinFileName);
            IOUtils.copy(stream, output);
            output.close();
            if (isBinary) {
                outBinFileName.setExecutable(true);
            }

            System.out.println("Creating file: " + outBinFileName);
        }

        return outBinFileName.toString();
    }

    private static String getTempPath() throws IOException {
        String path = Util.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String jarPath = URLDecoder.decode(path, StandardCharsets.UTF_8);
        return jarPath.substring(0, jarPath.lastIndexOf("/")) + "/tmp/";
    }

    public static List<File> getProteinsFromRDF(Domain domain, String name) throws Exception {

        boolean stopcodon = false;

        Iterable<ResultLine> queryResults;

        queryResults = domain.getRDFSimpleCon().runQuery("getProteins.txt", true);

        int counter = 0;
        int filePart = 0;

        List<File> proteinFiles = new ArrayList<>();
        File proteinFile = java.nio.file.Files.createTempFile(name + "_" + filePart, "_protein.fasta").toFile();
        // File proteinFile = new File(name + "_" + filePart + "_interpro.fasta");
        proteinFiles.add(proteinFile);
        PrintWriter writer = new PrintWriter(proteinFile, StandardCharsets.UTF_8);

        for (ResultLine item : queryResults) {
            counter++;
            if (counter > 30000) {
                logger.info("Parsing batch: " + filePart);
                // Run generated file
                writer.close();

                // Create new file
                filePart++;
                // Reset the counter and create a new temp protein file
                counter = 0;
                proteinFile = java.nio.file.Files.createTempFile(name + "_" + filePart, "_interpro.fasta").toFile();
                proteinFiles.add(proteinFile);
                writer = new PrintWriter(proteinFile, StandardCharsets.UTF_8);
            }

            String protein = item.getIRI("protein");
            String sequence = item.getLitString("sequence");

            if (!sequence.contains("*")) {
                writer.write(">" + protein + "\n" + sequence + "\n");
            } else {
                sequence = sequence.replaceAll("\\*", "X");
                writer.write(">" + protein + "\n" + sequence + "\n");
                stopcodon = true;
            }
        }

        logger.info("Parsing all batches");

        if (stopcodon) {
            logger.warn("Some proteins contain * in their sequence and have been masked with an X");
        }

        writer.close();

        // Parsing all the files
        return proteinFiles;
    }

    public static HashMap<String, String> getLocusTags(Domain domain) throws Exception {
        // Obtain protein sequences and calculate MD5
        ResultSet resultSet = domain.getRDFSimpleCon().runQueryDirect(
                "PREFIX gbol: <http://gbol.life/0.1/>\n" +
                        "SELECT ?locus ?protein\n" +
                        "WHERE {\n" +
                        "    ?gene a gbol:Gene .\n" +
                        "    ?gene gbol:locusTag ?locus .\n" +
                        "    ?gene gbol:transcript/gbol:feature/gbol:protein ?protein .\n" +
                        "}");

        HashMap<String, String> locusLookup = new HashMap<>();
        while (resultSet.hasNext()) {
            QuerySolution querySolution = resultSet.next();
            String iri = querySolution.get("protein").asResource().getURI();
            String locus = querySolution.getLiteral("locus").getString();
            locusLookup.put(locus.toUpperCase(), iri);
        }
        return locusLookup;
    }
}
