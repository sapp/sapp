package nl.wur.ssb.SappGeneric.GBOL;

import life.gbol.domain.LocalDataFile;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rdfhdt.hdt.hdt.HDT;
import org.w3.ns.prov.domain.Entity;
import uk.ac.ebi.embl.api.validation.helper.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

public class GBOLUtil {

    private static final Logger logger = LogManager.getLogger(GBOLUtil.class);
    static HashMap<String, HashMap<String, String>> enumTypesLookup = new HashMap<>();

    static {
        try {
            // for(String item : FileUtils.readFile(Util.getResourceFile("identifiers.tsv")).split("\n"))
            for (String item : FileUtils.readFile(Util.getResourceFile("EnumProps.tsv")).split("\n")) {
                String[] elems = item.split("\t");
                HashMap<String, String> map = enumTypesLookup.get(elems[0]);
                if (map == null) {
                    map = new HashMap<String, String>();
                    enumTypesLookup.put(elems[0], map);
                }
                map.put(elems[2].toLowerCase(), elems[1]);
                if (elems[4].equals("true")) {
                    map.put("<default>", elems[1]);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static EnumClass getEnum(Class clazz, String val) throws Exception {
        return getEnum(clazz, val, true);
    }

    public static EnumClass getEnum(Class clazz, String val, boolean reportErr) throws Exception {
        String entry = resolveEnum(clazz.getName(), val, reportErr);
        if (entry == null) {
            return null;
        }
        return (EnumClass) clazz.getMethod("make", String.class).invoke(null, entry);
    }

    public static String resolveEnum(String enumType, String val) {
        return resolveEnum(enumType, val, true);
    }

    public static String resolveEnum(String enumType, String val, boolean reportErr) {
        String lookup = enumType.replaceAll(".*\\.", "");
        HashMap<String, String> map = enumTypesLookup.get(lookup);
        String res = map.get(val.toLowerCase().trim());
        if (res == null) {
            if (reportErr) {
                logger.error("Cannot find enumeration type: " + val + " for enumeration: " + enumType);
            }
            res = map.get("<default>");
        }
        return res;
    }

    public static LocalDataFile createLocalFile(Domain domain, File file, String md5) {
        LocalDataFile fileDesc = domain.make(LocalDataFile.class, "http://gbol.life/0.1/file/" + md5);
        fileDesc.setLocalFileName(file.getName());
        fileDesc.setLocalFilePath(file.getAbsolutePath());
        return fileDesc;
    }

    /**
     * @param obj < Args (replace input variable, inputvalue)*
     */
    public static String createParamString(Object obj, Object... replace) throws Exception {
        String res = "";
        HashMap<String, String> replaceMap = new HashMap<>();
        for (int i = 0; i < replace.length; i += 2) {
            replaceMap.put((String) replace[i], "{" + ((Entity) replace[i + 1]).getResource().getURI() + "}");
        }
        for (Field field : obj.getClass().getFields()) {
            if (field.getAnnotationsByType(com.beust.jcommander.Parameter.class).length > 0) {
                String name = field.getName();
                if (replaceMap.containsKey(name)) {
                    res += name + " " + replaceMap.get(name);
                } else {
                    Object fieldVal = field.get(obj);
                    if (fieldVal != null) {
                        res += fieldVal.toString();
                    }
                }
                res += " ";
            }
        }
        return res.trim();
    }

    public static String cleanURI(String in) {
        //TODO ! cleanURIs
        if (in.toLowerCase().matches("cds")) {
            return "cds";
        }
        in = in.replaceAll("\\|", "_");
        // Check if needed before continueing
//        in = in.replaceAll(" ", "_");
//        in = in.replaceAll(":", "_");
//        in = in.replaceAll("\\(", "_");
//        in = in.replaceAll("\\)", "_");
//        in = in.replaceAll("\\[", "_");
//        in = in.replaceAll("\\]", "_");
//        in = in.replaceAll("\\.", "_");
//        in = in.replaceAll(",", "_");
//        in = in.replaceAll(";", "_");
//        in = in.replaceAll("=", "_");
//        in = in.replaceAll(">", "_");
//        in = in.replaceAll("<", "_");
//        in = in.replaceAll("\\+", "_");
//        in = in.replaceAll("-", "_");
//        in = in.replaceAll("\\*", "_");
//
        return in;
    }

    public static LocalDateTime dateFormats(String date) {
        //    System.err.println("Parsing date: " + date);

        List<SimpleDateFormat> knownPatterns = new ArrayList<>();
        // TODO > add a wide variety of standard date parsing formats (grep awk?)
        // It should go from high to low complexity
        // knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm"));
        knownPatterns.add(new SimpleDateFormat("dd-MMM-yyyy"));
        knownPatterns.add(new SimpleDateFormat("MMM-yyyy"));
        knownPatterns.add(new SimpleDateFormat("yyyy"));

        for (SimpleDateFormat pattern : knownPatterns) {
            try {
                // Take a try
                Date dt = new Date(pattern.parse(date).getTime());
                //return dt.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDateTime result = dt.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                //        System.err.println(result);
                return result;
            } catch (ParseException pe) {
                // Loop on
            }
        }
        System.err.println("No known Date format found: " + date);
        return null;
    }

    /**
     * Retrieval of the codon table from a SPARQL endpoint
     *
     * @param rdfResource RDFSimplecon connection object
     * @return codontable value or -1 when no or inconsistent values are detected
     * @throws Exception when query file is not found
     */
    public static int getGeneticCode(RDFSimpleCon rdfResource) throws Exception {
        return getGeneticCode(null, rdfResource);
    }


    /**
     * Retrieval of the codon table within the HDT / RDF object
     *
     * @param hdt         Binary RDF file
     * @param rdfResource RDFSimplecon connection object
     * @return codon table value or -1 when no or inconsistent values are detected
     * @throws Exception when query file is not found
     */
    public static int getGeneticCode(HDT hdt, RDFSimpleCon rdfResource) throws Exception {
        Iterable<ResultLine> geneticCodes;
        if (hdt != null) {
            geneticCodes = rdfResource.runQuery(hdt, "getGeneticCode.txt");
        } else {
            geneticCodes = rdfResource.runQuery("getGeneticCode.txt", true);
        }

        Iterator<ResultLine> geneticCodesIterator = geneticCodes.iterator();
        if (!geneticCodesIterator.hasNext())
            return -1;

        int geneticCode = geneticCodesIterator.next().getLitInt("genetic_code");
        if (geneticCodesIterator.hasNext()) {
            System.err.println("Different genetic codes detected. Will fall back to -codon parameter");
            return -1;
        }
        return geneticCode;
    }
}
