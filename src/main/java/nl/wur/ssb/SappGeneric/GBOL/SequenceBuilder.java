package nl.wur.ssb.SappGeneric.GBOL;

import life.gbol.domain.NASequence;
import life.gbol.domain.Region;
import life.gbol.domain.StrandPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import org.apache.commons.lang.StringUtils;
import uk.ac.ebi.embl.api.entry.XRef;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.location.CompoundLocation;
import uk.ac.ebi.embl.api.entry.location.Location;
import uk.ac.ebi.embl.api.entry.qualifier.Qualifier;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class SequenceBuilder {
    protected static HashMap<String, String> featureMap;

    static {
        featureMap = new HashMap<>();
        featureMap.put("misc_RNA", "MiscRna");
        featureMap.put("mRNA", "mRNA");
        featureMap.put("ncRNA", "ncRNA");
        featureMap.put("prim_transcript", "PrecursorRNA");
        featureMap.put("rRNA", "rRNA");
        featureMap.put("tmRNA", "tmRNA");
        featureMap.put("tRNA", "tRNA");
        featureMap.put("assembly_gap", "AssemblyGap");
        featureMap.put("CDS", "CDS");
        featureMap.put("centromere", "Centromere");
        featureMap.put("C_region", "ConstantRegion");
        featureMap.put("D_segment", "DiversitySegment");
        featureMap.put("D-loop", "DLoop");
        featureMap.put("exon", "Exon");
        featureMap.put("5'UTR", "FivePrimeUTR");
        featureMap.put("N_region", "GapRegion");
        featureMap.put("gene", "Gene");
        featureMap.put("iDNA", "Intron");
        featureMap.put("intron", "Intron");
        featureMap.put("J_segment", "JoiningSegment");
        featureMap.put("mat_peptide", "MaturePeptide");
        featureMap.put("misc_binding", "MiscBinding");
        featureMap.put("misc_feature", "MiscFeature");
        featureMap.put("misc_recomb", "MiscRecomb");
        featureMap.put("misc_structure", "MiscStructure");
        featureMap.put("misc_difference", "MiscVariation");
        featureMap.put("mobile_element", "MobileElement");
        featureMap.put("modified_base", "ModifiedBase");
        featureMap.put("variation", "NaturalVariation");
        featureMap.put("operon", "Operon");
        featureMap.put("polyA_site", "PolyASite");
        featureMap.put("primer_bind", "PrimerBinding");
        featureMap.put("protein_bind", "ProteinBinding");
        featureMap.put("regulatory", "RegulationSite");
        featureMap.put("repeat_region", "RepeatRegion");
        featureMap.put("rep_origin", "ReplicationOrigin");
        featureMap.put("STS", "SequenceTaggedSite");
        featureMap.put("sig_peptide", "SignalPeptide");
        featureMap.put("source", "Source");
        featureMap.put("stem_loop", "StemLoop");
        featureMap.put("S_region", "SwitchRegion");
        featureMap.put("telomere", "Telomere");
        featureMap.put("3'UTR", "ThreePrimeUTR");
        featureMap.put("oriT", "TransferOrigin");
        featureMap.put("gap", "UnknownAssemblyGap");
        featureMap.put("unsure", "UnsureBases");
        featureMap.put("old_sequence", "UpdatedSequence");
        featureMap.put("V_region", "VariableRegion");
        featureMap.put("V_segment", "VariableSegment");
        // special cases of the regulation site (depecrated now in genbank but still encountered)
        featureMap.put("attenuator", "RegulationSite");
        featureMap.put("CAAT_signal", "RegulationSite");
        featureMap.put("DNase_I_hypersensitive_site", "RegulationSite");
        featureMap.put("enhancer", "RegulationSite");
        featureMap.put("enhancer_blocking_element", "RegulationSite");
        featureMap.put("GC_signal", "RegulationSite");
        featureMap.put("imprinting_control_region", "RegulationSite");
        featureMap.put("insulator", "RegulationSite");
        featureMap.put("locus_control_region", "RegulationSite");
        featureMap.put("matrix_attachment_region", "RegulationSite");
        featureMap.put("minus_10_signal", "RegulationSite");
        featureMap.put("minus_35_signal", "RegulationSite");
        featureMap.put("other", "RegulationSite");
        featureMap.put("polyA_signal_sequence", "RegulationSite");
        featureMap.put("promoter", "RegulationSite");
        featureMap.put("recoding_stimulatory_region", "RegulationSite");
        featureMap.put("replication_regulatory_region", "RegulationSite");
        featureMap.put("response_element", "RegulationSite");
        featureMap.put("ribosome_binding_site", "RegulationSite");
        featureMap.put("riboswitch", "RegulationSite");
        featureMap.put("silencer", "RegulationSite");
        featureMap.put("TATA_box", "RegulationSite");
        featureMap.put("terminator", "RegulationSite");
        featureMap.put("transcriptional_cis_regulatory_region", "RegulationSite");
        featureMap.put("TATA_signal", "RegulationSite");
        featureMap.put("-35_signal", "RegulationSite");
        featureMap.put("-10_signal", "RegulationSite");
        featureMap.put("RBS", "RegulationSite");
        featureMap.put("polyA_signal", "RegulationSite");
        featureMap.put("misc_signal", "RegulationSite");
    }

    protected Domain domain;
    protected NASequence dNASequence;
    protected String rootIRI;
    protected String featureURI;
    protected HashMap<Integer, life.gbol.domain.Citation> refs = new HashMap<>();

    protected SequenceBuilder(Domain domain, String rootURI) {
        this.domain = domain;
        this.featureURI = rootURI.replaceAll("\\/$", "");
    }

    public SequenceBuilder(Domain domain, NASequence dNASequence, String rootIRI) {
        this.domain = domain;
        this.dNASequence = dNASequence;
        this.rootIRI = rootIRI;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setSequence(life.gbol.domain.Sequence seqObj, String seq) throws Exception {
        seqObj.setSequence(seq);
        seqObj.setSha384(Generic.checksum(seq, "SHA-384"));
        seqObj.setLength((long) seq.length());
    }

    public life.gbol.domain.Citation getRef(int number) {
        return this.refs.get(number);
    }

    public life.gbol.domain.Location makeLocation(Feature feature, life.gbol.domain.Feature gbolFeature) {
        CompoundLocation<Location> locObj = feature.getLocations();

        boolean isComplement = locObj.isComplement();
        List<Location> locations = locObj.getLocations();

        LinkedList<Region> regions = new LinkedList<>();
        for (int i = 0; i < locations.size(); i++) {
            regions.add(makeRegion(locations.get(i), isComplement, i == 0 && locObj.isLeftPartial(),
                    i == locations.size() - 1 && locObj.isRightPartial(), gbolFeature));
        }
        if (isComplement) {
            Collections.reverse(regions);
        }

        if (regions.size() == 1) {
            return regions.get(0);
        } else {
            life.gbol.domain.ListOfRegions compoundLoc = domain.make(life.gbol.domain.ListOfRegions.class, gbolFeature.getResource().getURI() + "/compoundloc");
            for (Region region : regions)
                compoundLoc.addMembers(region);
            return compoundLoc;
        }
    }

    public Region makeRegion(Location loc, boolean isComplement, boolean leftPartial,
                             boolean rightPartial, nl.wur.ssb.RDFSimpleCon.api.OWLThing gbolFeature) {
        long begin = loc.getBeginPosition();
        long end = loc.getEndPosition();
        boolean isComplementLoc = loc.isComplement();
        return makeRegion(begin, end, isComplementLoc, isComplement, leftPartial, rightPartial,
                gbolFeature);
    }

    public Region makeRegion(long begin, long end, boolean isComplementLoc, boolean isComplement,
                             boolean leftPartial, boolean rightPartial, nl.wur.ssb.RDFSimpleCon.api.OWLThing gbolFeature) {
        return makeRegion(begin, end, isComplementLoc, isComplement, leftPartial, rightPartial);
    }

    public Region makeRegion(long begin, long end, boolean isComplement,
                             boolean leftPartial, boolean rightPartial) {
        return this.makeRegion(begin, end, isComplement, false, leftPartial, rightPartial);
    }

    public Region makeRegion(long begin, long end, boolean isComplementLoc, boolean isComplement,
                             boolean leftPartial, boolean rightPartial) {

        // TODO what if both state they are complement?..
        Region region;
        if (leftPartial && rightPartial)
            region = domain.make(Region.class, "http://gbol.life/0.1/region/partial/" + (isComplementLoc ^ isComplement ? "c" : "") + begin + "-" + end);
        else if (leftPartial)
            region = domain.make(Region.class, "http://gbol.life/0.1/region/leftpartial/" + (isComplementLoc ^ isComplement ? "c" : "") + begin + "-" + end);
        else if (rightPartial)
            region = domain.make(Region.class, "http://gbol.life/0.1/region/rightpartial/" + (isComplementLoc ^ isComplement ? "c" : "") + begin + "-" + end);
        else
            region = domain.make(Region.class, "http://gbol.life/0.1/region/" + (isComplementLoc ^ isComplement ? "c" : "") + begin + "-" + end);

        if (leftPartial)
            region.setBegin(this.makeBeforePosition("http://gbol.life/0.1/beforebegin/" + begin, begin));
        else
            region.setBegin(this.makeExactPosition("http://gbol.life/0.1/exactbegin/" + begin, begin));

        if (rightPartial)
            region.setEnd(this.makeAfterPosition("http://gbol.life/0.1/afterend/" + end, end));
        else
            region.setEnd(this.makeExactPosition("http://gbol.life/0.1/exactend/" + end, end));

        // XOR if master is complemented then complement all childs as master complement is not stored
        if (isComplementLoc ^ isComplement)
            region.setStrand(StrandPosition.ReverseStrandPosition);
        else
            region.setStrand(StrandPosition.ForwardStrandPosition);
        return region;
    }


    public life.gbol.domain.Position makeExactPosition(String iri, long pos) {
        life.gbol.domain.ExactPosition toRet = domain.make(life.gbol.domain.ExactPosition.class, iri);
        toRet.setPosition(pos);
        return toRet;
    }

    public life.gbol.domain.Position makeBeforePosition(String iri, long pos) {
        life.gbol.domain.BeforePosition toRet = domain.make(life.gbol.domain.BeforePosition.class, iri);
        toRet.setPosition(pos);
        return toRet;
    }

    public life.gbol.domain.Position makeAfterPosition(String iri, long pos) {
        life.gbol.domain.AfterPosition toRet = domain.make(life.gbol.domain.AfterPosition.class, iri);
        toRet.setPosition(pos);
        return toRet;
    }

    public Region makeRegion(long begin, long end, String name, nl.wur.ssb.RDFSimpleCon.api.OWLThing gbolFeature) {
        Region region = domain.make(Region.class, "http://gbol.life/0.1/" + name + "/" + begin + "-" + end);
        region.setBegin(this.makeExactPosition("http://gbol.life/0.1/exactbegin/" + begin, begin));
        region.setEnd(this.makeExactPosition("http://gbol.life/0.1/exactend/" + end, end));
        region.setStrand(StrandPosition.ForwardStrandPosition);
        return region;
    }

    public String makeIRIFeature(Feature feature) {
        return this.makeIRIFeature(feature, feature.getName());
    }

    public life.gbol.domain.XRef createXRef(XRef xref) throws Exception {
        return this.createXRef(xref.getDatabase(), xref.getPrimaryAccession(),
                xref.getSecondaryAccession());
    }

    public life.gbol.domain.XRef createXRef(String dbIdentifier, String primary) throws Exception {
        return this.createXRef(dbIdentifier, primary, null);
    }

    public life.gbol.domain.XRef createXRef(String dbIdentifier, String primary, String secondary)
            throws Exception {
        //TODO createXRef for Sequencebuilder, override present in flatfileentry
   /* life.gbol.domain.XRef toRet =
        Xrefs.create(this.domain, this.xrefProv, dbIdentifier, primary, secondary);
    this.annotResult.addTarget(toRet.getDb());
    dataset.addLinkedDataBases(toRet.getDb());
    return toRet;*/
        return null;
    }

    public String makeIRIFeature(Feature feature, String name) {
        CompoundLocation<Location> locObj = feature.getLocations();
        boolean isComplement = locObj.isComplement();

        LinkedList<String> iriParts = new LinkedList<>();
        for (Location loc : locObj.getLocations())
            iriParts.add((loc.isComplement() ^ isComplement ? "c" : "") + loc.getBeginPosition() + "-"
                    + loc.getEndPosition());
        if (isComplement) {
            Collections.reverse(iriParts);
        }
//    return this.rootIRI + GBOLUtil.cleanURI(name) + "/" + (locObj.isLeftPartial() ? "~" : "")
//        + StringUtils.join(iriParts, "_") + (locObj.isRightPartial() ? "~" : "");

        return this.featureURI + "/" + GBOLUtil.cleanURI(name) + "/" + (locObj.isLeftPartial() ? "~" : "")
                + StringUtils.join(iriParts, "_") + (locObj.isRightPartial() ? "~" : "");

    }

    public void makeProtein(life.gbol.domain.CDS cds, Feature feature, String sequence) throws Exception {
        boolean pseudo = feature.getQualifiers("pseudo").size() > 0;
        List<Qualifier> translation = feature.getQualifiers("translation");
        String aa;
        if (translation.size() == 0) {
            if (feature.getQualifiers("transl_table").size() == 0) {
                feature.addQualifier("transl_table", "1");
            }
            int table = Integer.parseInt(feature.getQualifiers("transl_table").get(0).getValue());
            aa = Translate.make(sequence, table, true, pseudo);
        } else {
            aa = translation.get(0).getValue();
        }

        String sha384 = Generic.checksum(aa, "SHA-384");
        life.gbol.domain.Protein protein = domain.make(life.gbol.domain.Protein.class, "http://gbol.life/0.1/protein/" + sha384);
        protein.setSequence(aa);
        protein.setSha384(sha384);
        protein.setLength((long) aa.length());
        cds.setProtein(protein);
    }
}
