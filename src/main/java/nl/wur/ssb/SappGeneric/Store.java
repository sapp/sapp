package nl.wur.ssb.SappGeneric;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.ext.com.google.common.io.Files;

import java.io.File;

public class Store {
    public static Domain createMemoryStore() throws Exception {
        return new Domain("");
    }

    public static Domain createDiskStore() throws Exception {
        File diskstore = Files.createTempDir();
        diskstore.deleteOnExit();
        return createDiskStore(diskstore);
    }

    public static Domain createDiskStore(File dir) throws Exception {
        return new Domain("file://" + dir.getAbsolutePath());
    }

    public static Domain connectEndpoint(String endpoint) throws Exception {
        return new Domain(endpoint);
    }

    public static Domain connectEndpoint(String endpoint, String username, String password) throws Exception {
        Domain domain = new Domain(endpoint);
        domain.getRDFSimpleCon().setAuthen(username, password);
        return domain;
    }

}
