package nl.wur.ssb.SappGeneric;

import org.apache.jena.ext.com.google.common.hash.Hashing;

import java.io.Reader;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;

public class SequenceFile {

    public static SequenceFileObject reader(File seqFile) throws IOException {
        System.out.println(seqFile);
        // SequenceFileObject.setType(FileType.FASTA);

        BufferedReader buffered;
        try {
            InputStream fileStream = new FileInputStream(seqFile.getAbsolutePath());
            InputStream gzipStream = new GZIPInputStream(fileStream);
            Reader decoder = new InputStreamReader(gzipStream, StandardCharsets.UTF_8);
            buffered = new BufferedReader(decoder);
        } catch (ZipException e) {
            FileReader in = new FileReader(seqFile.getAbsolutePath());
            buffered = new BufferedReader(in);
        }

        String content = buffered.readLine();
        SequenceFileObject fileInfo = null;
        if (content.startsWith("@"))
            fileInfo = FastQ(buffered, content);
        else if (content.startsWith(">"))
            fileInfo = FastA(buffered, content);
        else {
            System.out.println("Unknown file format");
        }

        buffered.close();
        fileInfo.setMD5(Generic.checksum(seqFile, Hashing.md5()));
        return fileInfo;
    }

    private static SequenceFileObject FastQ(BufferedReader buffered, String content) throws IOException {
        // Header is already read at first attempt
        long lines = 1;
        long bases = 0;
        long length = 0;

        while ((content = buffered.readLine()) != null) {
            lines = lines + 1;
            if (lines % 4 == 0) {
                length = length + content.length();
            }
        }

        SequenceFileObject seqFile = new SequenceFileObject();

        // 4 lines for a fastq entry
        lines = lines / 4;
        // Average read length
        length = length / lines;

        seqFile.setLines(lines);
        seqFile.setLength(length);

        return seqFile;
    }

    private static SequenceFileObject FastA(BufferedReader buffered, String content) throws IOException {
        // Header is already read at first attempt
        long lines = 1;
        long bases = 0;
        long length = 0;

        while ((content = buffered.readLine()) != null) {
            lines = lines + 1;
            if (lines % 2 == 0) {
                length = length + content.length();
            }
        }

        // 4 lines for a fastq entry
        lines = lines / 2;

        // Average read length
        length = length / lines;

        SequenceFileObject seqFile = new SequenceFileObject();
        seqFile.setLines(lines);
        seqFile.setLength(length);

        return seqFile;
    }
}
