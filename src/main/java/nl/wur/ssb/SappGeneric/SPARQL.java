package nl.wur.ssb.SappGeneric;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.rdfhdt.hdt.hdt.HDT;

import java.util.ArrayList;
import java.util.List;

public class SPARQL {

    private static RDFSimpleCon RDFResource = null;

    /**
     * @param hdt
     * @return
     * @throws Exception
     */
    public static List<String> getSampleIRI(HDT hdt) throws Exception {
        RDFResource = new RDFSimpleCon("");
        Iterable<ResultLine> samples = RDFResource.runQuery(hdt, "getSample.txt");
        List<String> list = new ArrayList<>();

        for (ResultLine sampleobj : samples) {
            String sample = sampleobj.getIRI("sample");
            list.add(sample);
        }

        RDFResource.close();
        return list;
    }

    /**
     * @param resource
     * @return
     * @throws Exception
     */
    public static List<String> getSampleIRI(RDFSimpleCon resource) throws Exception {
        Iterable<ResultLine> samples = resource.runQuery("getSample.txt", true);
        List<String> list = new ArrayList<>();

        for (ResultLine sampleobj : samples) {
            String sample = sampleobj.getIRI("sample");
            list.add(sample);
        }
        return list;
    }

    /**
     * @param iri
     * @param domain
     * @return
     * @throws Exception
     */
    public static String LocusTag(String iri, Domain domain) throws Exception {
        RDFSimpleCon RDFResource = domain.getRDFSimpleCon();
        String locus =
                RDFResource.runQuery("getLocusTag.txt", true, iri).iterator().next().getLitString("locus");
        return locus;
    }

    /**
     * @param iri
     * @param hdt
     * @return
     * @throws Exception
     */
    public static String getLocusTag(String iri, HDT hdt) throws Exception {
        RDFSimpleCon RDFResource = new RDFSimpleCon("");
        String locus = RDFResource.runQuery(hdt, "getLocusTag.txt", iri).iterator().next().getLitString("locus");
        RDFResource.close();
        return locus;
    }

    /**
     * @param iri
     * @param endpoint
     * @return
     * @throws Exception
     */
    public static String getLocusTag(String iri, RDFSimpleCon endpoint) throws Exception {
        String locus = endpoint.runQuery("getLocusTag.txt", true, iri).iterator().next().getLitString("locus");
        return locus;
    }

    /**
     * @param iri
     * @param domain
     * @return
     * @throws Exception
     */
    public static String getType(String iri, Domain domain) throws Exception {
        String type = domain.getRDFSimpleCon().runQuery("getType.txt", true, iri).iterator().next().getIRI("type");
        return type;
    }

    /**
     * @param iri
     * @param hdt
     * @return
     * @throws Exception
     */
    public static String getType(String iri, HDT hdt) throws Exception {
        RDFSimpleCon RDFResource = new RDFSimpleCon("");
        String type = RDFResource.runQuery(hdt, "getType.txt", iri).iterator().next().getIRI("type");
        RDFResource.close();
        return type;
    }

    /**
     * @param iri
     * @param endpoint
     * @return
     * @throws Exception
     */
    public static String getType(String iri, RDFSimpleCon endpoint) throws Exception {
        String type =
                endpoint.runQuery("getType.txt", true, iri).iterator().next().getIRI("type");
        return type;
    }

    /**
     * @param iri
     * @param domain
     * @return
     * @throws Exception
     */
    public static Iterable<ResultLine> getTranscripts(String iri, Domain domain) throws Exception {
        Iterable<ResultLine> transcripts = null;
        RDFSimpleCon RDFResource = domain.getRDFSimpleCon();
        transcripts = RDFResource.runQuery("getTranscripts.txt", true, iri);
        return transcripts;
    }

    /**
     * @param iri
     * @param hdt
     * @return
     * @throws Exception
     */
    public static Iterable<ResultLine> getTranscripts(String iri, HDT hdt)
            throws Exception {
        Iterable<ResultLine> transcripts;
        RDFSimpleCon RDFResource = new RDFSimpleCon("");
        transcripts = RDFResource.runQuery(hdt, "getTranscripts.txt", iri);
        RDFResource.close();
        return transcripts;
    }

    /**
     * @param locus_tag
     * @param RDFResource
     * @return
     * @throws Exception
     */
    public static String getIriFromLocus(String locus_tag, RDFSimpleCon RDFResource)
            throws Exception {
        String iri = RDFResource.runQuery("getIRIfromLocus.txt", true, locus_tag).iterator().next()
                .getIRI("iri");
        return iri;
    }

    /**
     * @param locus_tag
     * @param domain
     * @return
     * @throws Exception
     */
    public static String getIriFromLocus(String locus_tag, Domain domain) throws Exception {
        RDFSimpleCon RDFResource = domain.getRDFSimpleCon();
        String iri = getIriFromLocus(locus_tag, RDFResource);
        return iri;
    }

    /**
     * @param locus_tag
     * @param hdt
     * @return
     * @throws Exception
     */
    public static String getIriFromLocus(String locus_tag, HDT hdt)
            throws Exception {
        String iri;
        RDFSimpleCon RDFResource = new RDFSimpleCon("");
        iri =
                RDFResource.runQuery(hdt, "getIRIfromLocus.txt", locus_tag).iterator().next().getIRI("iri");
        RDFResource.close();
        return iri;
    }

    /**
     * @param locus_tag
     * @param type
     * @param RDFResource
     * @return
     * @throws Exception
     */
    public static String getIriFromLocusOfType(String locus_tag, String type,
                                               RDFSimpleCon RDFResource) throws Exception {
        String iri = RDFResource.runQuery("getIRIfromLocusOfType.txt", true, locus_tag, type).iterator()
                .next().getIRI("iri");
        return iri;
    }

    /**
     * @param locus_tag
     * @param type
     * @param domain
     * @return
     * @throws Exception
     */
    public static String getIriFromLocusOfType(String locus_tag, String type, Domain domain)
            throws Exception {
        RDFSimpleCon RDFResource = domain.getRDFSimpleCon();
        String iri = getIriFromLocusOfType(locus_tag, type, RDFResource);
        return iri;
    }

    /**
     * @param locus_tag
     * @param type
     * @param hdt
     * @return
     * @throws Exception
     */
    public static String getIriFromLocusOfType(String locus_tag, String type, HDT hdt) throws Exception {
        String iri;
        RDFSimpleCon RDFResource = new RDFSimpleCon("");
        iri = RDFResource.runQuery(hdt, "getIRIfromLocusOfType.txt", locus_tag, type).iterator().next()
                .getIRI("iri");
        RDFResource.close();
        return iri;
    }

    /**
     * Retrieves sequence from a given uri
     *
     * @param seqobject sequence URI
     * @param domain    domain object
     * @return sequence as string
     * @throws Exception
     */
    public static String getSequence(String seqobject, Domain domain) throws Exception {
        return domain.getRDFSimpleCon().runQuery("getSequence.txt", false, seqobject).iterator().next().getLitString("sequence");
    }

    /**
     * Retrieves sequence from a given uri
     *
     * @param seqobject sequence url
     * @param hdt       HDT object
     * @return sequence as string
     * @throws Exception
     */
    public static String getSequence(String seqobject, HDT hdt) throws Exception {
        RDFSimpleCon rdfSimpleCon = new RDFSimpleCon("");
        return rdfSimpleCon.runQuery(hdt, "getSequence.txt", seqobject).iterator().next().getLitString("sequence");
    }
}
