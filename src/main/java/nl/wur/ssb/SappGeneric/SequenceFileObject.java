package nl.wur.ssb.SappGeneric;

import life.gbol.domain.FileType;

public class SequenceFileObject {

    private static FileType type;
    private long lines;
    private long length;
    private String MD5;

    public static void setType(FileType type) {
        SequenceFileObject.type = type;
    }

    public long getLines() {
        return lines;
    }

    public void setLines(long lines) {
        this.lines = lines;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public String getMD5() {
        return MD5;
    }

    public void setMD5(String MD5) {
        this.MD5 = MD5;
    }
}
