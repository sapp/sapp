package nl.wur.ssb.SappGeneric;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;

public class Reader {

    public BufferedReader reader(File file) throws IOException {
        BufferedReader buffered;
        try {
            InputStream fileStream = new FileInputStream(file.getAbsolutePath());
            InputStream gzipStream = new GZIPInputStream(fileStream);
            java.io.Reader decoder = new InputStreamReader(gzipStream, StandardCharsets.UTF_8);
            buffered = new BufferedReader(decoder);
        } catch (ZipException e) {
            FileReader in = new FileReader(file.getAbsolutePath());
            buffered = new BufferedReader(in);
        }
        return buffered;
    }
}
