package nl.wur.ssb.SappGeneric.InputOutput;

import jena.riot;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.exceptions.ParserException;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.options.HDTSpecification;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;

import static nl.wur.ssb.SappGeneric.InputOutput.Input.RDFFormat.*;
import static nl.wur.ssb.SappGeneric.InputOutput.Input.formatDetector;

public class Output {

    private static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(Output.class);
    /**
     * @param domain
     * @param output
     */
    public static void save(Domain domain, File output) throws Exception {
        // Set common prefixes
        domain.getRDFSimpleCon().setNsPrefix("gbol", "http://gbol.life/0.1/");
        domain.getRDFSimpleCon().setNsPrefix("terms", "http://purl.org/dc/terms/");
        domain.getRDFSimpleCon().setNsPrefix("prov", "http://www.w3.org/ns/prov#");
        domain.getRDFSimpleCon().setNsPrefix("void", "http://rdfs.org/ns/void#");
        domain.getRDFSimpleCon().setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
        domain.getRDFSimpleCon().setNsPrefix("end", "http://gbol.life/0.1/exactend/");
        domain.getRDFSimpleCon().setNsPrefix("begin", "http://gbol.life/0.1/exactbegin/");
        domain.getRDFSimpleCon().setNsPrefix("protein", "http://gbol.life/0.1/protein/");
        domain.getRDFSimpleCon().setNsPrefix("ipr", "http://gbol.life/0.1/db/interproscan/");
        domain.getRDFSimpleCon().setNsPrefix("sup", "http://gbol.life/0.1/db/superfamily/");
        domain.getRDFSimpleCon().setNsPrefix("metacyc", "http://gbol.life/0.1/db/metacyc/");
        domain.getRDFSimpleCon().setNsPrefix("go", "http://gbol.life/0.1/db/go/GO:");
        domain.getRDFSimpleCon().setNsPrefix("pfam", "http://gbol.life/0.1/db/pfam/");

        if (output.isDirectory()) {
            // Jena directory load and done...
        } else {
            boolean compressed = false;
            if (output.getName().endsWith(".gz")) {
                compressed = true;
                output = new File(output.getAbsolutePath().replaceAll(".gz$", ""));
            }

            //Create temporary store
            Input.RDFFormat rdfFormat = formatDetector(output);
            if (rdfFormat.equals(DIR)) {
                // Move the domain folder?
                output.mkdirs();
                Domain domainFolder = new Domain("file://" + output.getAbsolutePath());
                domain.getRDFSimpleCon().getModel().listStatements().forEachRemaining(e -> domainFolder.getRDFSimpleCon().getModel().add(e));

                domain.close();
                domainFolder.close();
            }
            // Create directory to store to database in temporarily
            if (rdfFormat.equals(HDT)) {
                saveHDT(domain, output);
            } else {
                saveRDF(domain, output, rdfFormat);
            }

            if (compressed) {
                // Compress file
                Generic.compressGZip(output, new File(output + ".gz"));
                // Delete uncompressed file
                output.delete();
            }
        }
    }

    private static void saveRDF(Domain domain, File output, Input.RDFFormat rdfFormat) throws IOException {
        if (rdfFormat.equals(N3)) {
            domain.save(output.getAbsolutePath(), RDFFormat.N3);
        } else if (rdfFormat.equals(JSONLD)) {
            domain.save(output.getAbsolutePath(), RDFFormat.JSONLD);
        } else if (rdfFormat.equals(RDF_XML)) {
            domain.save(output.getAbsolutePath(), RDFFormat.RDF_XML);
        } else if (rdfFormat.equals(N_TRIPLE)) {
            domain.save(output.getAbsolutePath(), RDFFormat.N_TRIPLE);
        } else if (rdfFormat.equals(TURTLE)) {
            domain.save(output.getAbsolutePath(), RDFFormat.TURTLE);
        } else if (rdfFormat.equals(N3)) {
            domain.save(output.getAbsolutePath(), RDFFormat.N3);
        } else {
            throw new IOException("File format not recognised " + rdfFormat.name());
        }
    }

    private static void saveHDT(Domain domain, File output) throws IOException, ParserException {
        String baseURI = "http://gbol.life/0.1/";
        File tempFile = Files.createTempFile("HDT_turtle", "").toFile();
        domain.save(tempFile.getAbsolutePath(), RDFFormat.TURTLE);

        org.rdfhdt.hdt.hdt.HDT hdt = HDTManager.generateHDT(tempFile.getAbsolutePath(), baseURI, RDFNotation.TURTLE, new HDTSpecification(), null);

        hdt.saveToHDT(output.getAbsolutePath(), null);

    }

    /**
     * @param ntFiles list of files to merge
     * @param output output file
     * @throws Exception when something goes wrong
     */
    public static void save(ArrayList<File> ntFiles, File output) throws Exception {
        logger.info("Saving all stores to " + output);

        // Using Apache riot to merge the files
        ArrayList<String> riotArgsList = new ArrayList<>();
        // Prevents File: to be printed in the header...
        riotArgsList.add("-q");
        // Detect file extension format
        String name = output.getName();
        if (name.endsWith(".ttl")) {
            riotArgsList.add("--output=TURTLE");
        } else if (name.endsWith(".nt")) {
            riotArgsList.add("--output=NTRIPLES");
        } else if (name.endsWith(".nq")) {
            riotArgsList.add("--output=NQUADS");
        } else if (name.endsWith(".jsonld")) {
            riotArgsList.add("--output=JSONLD");
        } else if (name.endsWith(".rdf")) {
            riotArgsList.add("--output=RDF/XML");
        } else if (name.endsWith(".hdt")) {
            riotArgsList.add("--output=TURTLE");
            logger.warn("HDT is not supported for this function, converting to turtle instead: " + output.getName() + " -> " + output.getName().replace(".hdt", ".ttl"));
            output = new File(output.getAbsolutePath().replace(".hdt", ".ttl"));
        } else {
            throw new Exception("Unknown output format, please use .ttl, .nt, .nq, .jsonld or .rdf");
        }
        // Create parent directory if it does not exist
        if (output.getParentFile() != null)
            output.getParentFile().mkdirs();
        // Redirect output stream
        OutputStream myOutputStream = new FileOutputStream(output); // or any other OutputStream
        PrintStream printStream = new PrintStream(myOutputStream);
        System.setOut(printStream);


        ntFiles.forEach(ntFile -> riotArgsList.add(ntFile.getAbsolutePath()));
        String[] riotArgs = riotArgsList.toArray(new String[0]);
        riot.main(riotArgs);

        // Close the streams and reset out to system out again
        printStream.close();
        myOutputStream.close();
        System.setOut(System.out);

        // If HDT is requested, convert the file to HDT
        if (name.endsWith(".hdt")) {
            logger.info("Converting to HDT, warning need to load the entire file into memory");
            nl.wur.ssb.RDFSimpleCon.api.Domain domain = new nl.wur.ssb.RDFSimpleCon.api.Domain("file://" + output);
            output = new File(output.getAbsolutePath().replace(".ttl", ".hdt"));
            logger.info("Saving HDT to " + output);
            Output.save(domain, output);
        }
    }
}
