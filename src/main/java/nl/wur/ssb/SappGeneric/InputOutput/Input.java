package nl.wur.ssb.SappGeneric.InputOutput;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static nl.wur.ssb.SappGeneric.InputOutput.Input.RDFFormat.*;


public class Input {
    private static final Logger logger = LogManager.getLogger(Input.class);

    public static Domain load(File input) throws Exception {
        logger.debug("Loading " + input.getAbsolutePath());
        List<File> inputFile = new ArrayList<>();
        inputFile.add(input);
        return load(inputFile);
    }

    public static Domain load(List<File> inputs) throws Exception {
        logger.info("Loading " + inputs.size() + " rdf files");
        File dir = null;
        Domain dirDomain = null;
        // If ends with gz its compressed
        for (File input : inputs) {
            if (input.getName().endsWith(".gz")) {
                File tempFile = File.createTempFile("decompress_" + input, "." + FilenameUtils.getExtension(input.getAbsolutePath().replaceAll(".gz$", "")));
                tempFile.deleteOnExit();
                logger.info("GZip detected, decompressing now to " + tempFile);
                Generic.decompressGZip(input, tempFile);
                input = tempFile;
            }

            //Create temporary store
            RDFFormat rdfFormat = formatDetector(input);
            if (rdfFormat.equals(DIR)) {
                if (inputs.size() > 1) {
                    throw new Exception("Multiple input files AND directories not yet supported");
                }
                return new Domain("file://" + input);
            }
            // Create directory to store to database in temporarily
            if (dir == null) {
                logger.info("Loading files into " + "RDF_" + inputs.hashCode());
                dir = Files.createTempDirectory("RDF_" + inputs.hashCode()).toFile();
                dirDomain = new Domain("file://" + dir);
                // Delete on exit
                File finalDir = dir;

                Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                    FileUtils.deleteQuietly(finalDir);
                }));
            }

            if (rdfFormat.equals(HDT)) {
                loadHDT(input, dirDomain);
            } else {
                logger.info("RDFDataMgr loading the file " + input.getName());
                RDFDataMgr.read(dirDomain.getRDFSimpleCon().getModel(), input.getAbsolutePath());
            }
        }
        return dirDomain;
    }

    /**
     * HDT load into jena directory
     *
     * @param input
     * @param dir
     * @return
     */
    private static File loadHDT(File input, Domain dir) throws IOException {

        HDT hdt = HDTManager.mapIndexedHDT(input.getAbsolutePath(), null);
        HDTGraph graph = new HDTGraph(hdt);
        Model model = ModelFactory.createModelForGraph(graph);
        dir.getRDFSimpleCon().getModel().add(model.listStatements());
        return input;
    }

    /**
     * Function to determine the output format based on the file extension or if not return as a folder
     *
     * @param input
     * @return
     */
    public static RDFFormat formatDetector(File input) throws IOException {
        if (input.isDirectory()) {
            return DIR;
        } else if (input.getName().endsWith("hdt")) {
            return HDT;
        } else if (input.getName().endsWith("jsonld")) {
            return JSONLD;
        } else if (input.getName().endsWith("ttl")) {
            return TURTLE;
        } else if (input.getName().endsWith("nt")) {
            return N_TRIPLE;
        } else if (input.getName().endsWith("n3")) {
            return N3;
        } else if (input.getName().endsWith("rdf")) {
            return RDF_XML;
        } else if (input.getName().endsWith("dat")) {
            return TURTLE;
        } else if (input.getName().endsWith("gz")) {
            // Format detection by removing the .gz extension and detect again
            return formatDetector(new File(input.getName().replaceAll(".gz$", "")));
        } else {
            throw new IOException("File format unknown: " + input.getName() + " (" + FilenameUtils.getExtension(input.getName()) + ")");
        }
    }

    public enum RDFFormat {
        RDF_XML,
        N_TRIPLE,
        TURTLE,
        N3,
        HDT,
        JSONLD,
        DIR
    }
}
