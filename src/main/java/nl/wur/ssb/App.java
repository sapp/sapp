package nl.wur.ssb;

import nl.wur.ssb.conversion.Conversion;
import nl.wur.ssb.diamond.Diamond;
import nl.wur.ssb.eggnog.Eggnog;
import nl.wur.ssb.infernal.Infernal;
import nl.wur.ssb.interproscan.InterProScan;
import nl.wur.ssb.kofamscan.KoFamScan;
import nl.wur.ssb.prodigal.Prodigal;
import nl.wur.ssb.sparql.SPARQL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;


public class App {
    private static final Logger logger = LogManager.getLogger(App.class);
    public static final String storageDir = "./sapp_storage/";
    public static void main(String[] args) throws Exception {
        CommandOptions commandOptions = new CommandOptions(args);

        // Create storage directory
        if (!new File(storageDir).exists()) {
            new File(storageDir).mkdirs();
        }
        // Query module
        if (commandOptions.sparql) {
            logger.info("Performing SPARQL query");
            SPARQL.main(args);
            logger.info("SPARQL query completed");
            // Annotation modules
        } else if (commandOptions.prodigal) {
            logger.info("Performing Prodigal analysis");
            new Prodigal(args);
            logger.info("Prodigal analysis completed");
        } else if (commandOptions.infernal) {
            logger.info("Performing Infernal analysis");
            new Infernal(args);
            logger.info("Infernal analysis completed");
        } else if (commandOptions.interpro) {
            logger.info("Performing InterProScan analysis");
            new InterProScan(args);
            logger.info("InterProScan analysis completed");
        } else if (commandOptions.eggnog) {
            logger.info("Performing EggNOG analysis");
            new Eggnog(args);
            logger.info("EggNOG analysis completed");
        } else if (commandOptions.kofamscan) {
            logger.info("Performing KoFamScan analysis");
            new KoFamScan(args);
            logger.info("KoFamScan analysis completed");
        } else if (commandOptions.diamond) {
            logger.info("Performing Diamond analysis");
            new Diamond(args);
            logger.info("Diamond analysis completed");
        } else if (commandOptions.embl2rdf ||
                commandOptions.genbank2rdf ||
                commandOptions.fasta2rdf ||
                commandOptions.rdf2fasta ||
                commandOptions.rdf2embl ||
                commandOptions.rdf2gtf ||
                commandOptions.gtf2rdf ||
                commandOptions.gff2rdf ||
                commandOptions.gff2embl ||
                commandOptions.convert ||
                commandOptions.reduce ||
                commandOptions.merge
        ) {
            logger.info("Performing conversion");
            Conversion.main(args);
            logger.info("Conversion completed");
        } else {
            logger.warn("No arguments given");
            new CommandOptions(new String[]{"--help"});
        }
    }
}
