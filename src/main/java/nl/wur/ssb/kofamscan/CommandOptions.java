package nl.wur.ssb.kofamscan;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import life.gbol.domain.AnnotationResult;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.CommandOptionsGeneric;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.time.LocalDateTime;

@Parameters(commandDescription = "Available options: ")
public class CommandOptions extends CommandOptionsGeneric {

    public final String repository = "https://gitlab.com/sapp/sapp";
    // public String description = "KO detection module from SAPP";
    public final LocalDateTime starttime = LocalDateTime.now();
    final String tool = "KOFAMSCAN";
    public String toolversion = "1.3.0";

    @Parameter(names = {"-kofamscan"}, description = "Application: Running in KoFamScan mode")
    public boolean kofamscan = false;

    @Parameter(names = {"-profile"}, description = "Profile folder with hmm files or hal file", required = false)
    public File profile;

    @Parameter(names = {"-r", "-resultFile"}, description = "KofamScan (extended tsv) result file")
    File resultFile;

    @Parameter(names = {"-kolist"}, description = "File containing the ko list", required = false)
    public File kolist;

    @Parameter(names = {"-exec_annotation"}, description = "Executable by default using exec_annotation system variable")
    public String binary = "exec_annotation";

    @Parameter(names = {"-threads"}, description = "Number of cpu threads to use")
    public int cpu = 2;

    public String annotResultIRI;
    public String commandLine;

    public int locus = 1;
    // final String sappversion =  nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptions.class)[0];
    // final String sappcommit = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptions.class)[1];
    AnnotationResult annotResult;

    CommandOptions(String[] args) throws Exception {
        try {
            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            if (this.help) {
                throw new ParameterException("");
            }

            // TODO make this default in all modules
            // Setting the AnnotationProvenance
            String[] files = new String[0];

            if (this.input != null) {
                this.domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(this.input);
            } else if (this.endpoint != null) {
                this.domain = new Domain(this.endpoint.toString());
                if (this.username != null) {
                    this.domain.getRDFSimpleCon().setAuthen(this.username, this.password);
                }
            } else {
                throw new ParameterException("-input is missing");
            }


            if (this.input == null && this.endpoint == null) {
                throw new ParameterException("Required either -input or -endpoint");
            }

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help || args.length == 0) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}

