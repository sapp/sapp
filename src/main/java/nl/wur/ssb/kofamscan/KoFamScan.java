package nl.wur.ssb.kofamscan;

import life.gbol.domain.*;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.ExecCommand;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.SappGeneric.Xrefs;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.*;
import java.util.zip.GZIPInputStream;

import static nl.wur.ssb.SappGeneric.Generic.getLocusTags;
import static org.apache.jena.rdf.model.ResourceFactory.createProperty;

public class KoFamScan extends SequenceBuilder {

    final UUID xrefprovID = UUID.randomUUID();
//    private final XRefProvenance xrefProv;
    private final CommandOptions arguments;
    private final File name;
    private final Logger logger = LogManager.getLogger(KoFamScan.class);
    private final XRefProvenance xRefProvenance;

    public KoFamScan(String[] args) throws Exception {
        super(null, "http://gbol.life/0.1/");

        arguments = new CommandOptions(args);

        this.domain = arguments.domain;

        if (arguments.input != null)
            name = arguments.input;
        else
            name = new File(new File(arguments.endpoint.toString()).getName());

        this.rootIRI = "http://gbol.life/0.1/" + xrefprovID + "/";
        // xrefProv = arguments.domain.make(XRefProvenance.class, rootIRI + "XRefProv");
        // xrefProv.setOrigin(arguments.annotResult);

        if (arguments.debug)
            logger.atLevel(Level.DEBUG);

        // Generic xref provenance
        xRefProvenance = domain.make(XRefProvenance.class, "http://gbol.life/0.1/kofamscan/" + arguments.toolversion + "/prov/xref");
        ImportProv origin = new ImportProv(domain, List.of(arguments.input.getAbsolutePath()), arguments.output, arguments.commandLine, arguments.tool, arguments.toolversion, arguments.repository, null, arguments.starttime, LocalDateTime.now());
        arguments.annotResult = domain.make(AnnotationResult.class, "http://gbol.life/0.1/" + UUID.randomUUID());
        origin.linkEntity(arguments.annotResult);
        xRefProvenance.setOrigin(arguments.annotResult);

        // If no sequences are found the analysis is obviously skipped
        // Everything is now from loadRDF as we have set a limit to 5.000 per
        // analysis run...
        if (arguments.resultFile != null) {
            logger.info("Parsing KofamScan results from " + arguments.resultFile);
            kofamscanParser(arguments.resultFile);
        } else {
            logger.info("No KofamScan results found, running KofamScan");
            loadRDF();
        }

        logger.info("Results are saved in: " + arguments.output.getAbsolutePath());
        nl.wur.ssb.SappGeneric.InputOutput.Output.save(arguments.domain, arguments.output);

        arguments.domain.close();
    }

    private void kofamscanParser(File resultFile) throws Exception {
        // Parse if gzip
        BufferedReader reader;
        if (resultFile.getName().endsWith(".gz")) {
            reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(resultFile))));
        } else {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(resultFile)));
        }

        // Get locus tags
        HashMap<String, String> locusTags = getLocusTags(arguments.domain);
        int counter = 0;
        while (reader.ready()) {
            counter++;
            if (counter % 1000 == 0) {
                logger.info("Parsing line: " + counter);
            }
            String line = reader.readLine();
            if (line.startsWith("#")) continue;
            String[] lineSplit = line.split("\t");
            String something = lineSplit[0];
            String uri = lineSplit[1];
            // Check if uri is not a locusTag
            if (locusTags.containsKey(uri)) {
                uri = locusTags.get(uri);
            }
            String KO = lineSplit[2];
            String thrshld = lineSplit[3];
            String score = lineSplit[4];
            String evalue = lineSplit[5];
            String definition = lineSplit[6].replaceAll("\"", "");
            String ec = "";
            if (definition.matches(".*EC:[0-9]+.[0-9]+.[0-9]+.[0-9]+]")) {
                ec = definition.replaceAll(".*EC:([0-9]+.[0-9]+.[0-9]+.[0-9]+)]", "$1");
            }

            Protein protein = domain.make(Protein.class, uri);
            ProteinHomology pFeature = domain.make(ProteinHomology.class, uri + "/" + KO);
            // pFeature.setSignature("https://www.kegg.jp/entry/ko:" + KO);
            pFeature.addAccession(KO);
            pFeature.setFunction(definition);
            pFeature.setHomologousTo("https://www.kegg.jp/entry/ko:" + KO);

            FeatureProvenance fprov = domain.make(FeatureProvenance.class, uri + "/" + KO + "/fprov");
            fprov.setOrigin(arguments.annotResult);

            ProvenanceApplication provenanceApplication = domain.make(ProvenanceApplication.class, uri + "/" + KO + "/annotation");
            if (!score.isEmpty())
                provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/score"), Float.valueOf(score));
            if (!evalue.isEmpty())
                provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/evalue"), Float.valueOf(evalue));
            if (!thrshld.isEmpty())
                provenanceApplication.getResource().addLiteral(createProperty("http://gbol.life/0.1/threshold"), Float.valueOf(thrshld));

            fprov.setAnnotation(provenanceApplication);

            pFeature.addProvenance(fprov);

            if (!ec.isEmpty()) {
                XRef xref = Xrefs.create(XRef.class, domain, xRefProvenance, "ec", "0.0", ec, null);
                pFeature.addXref(xref);
            }
            pFeature.setLocation(this.makeRegion(1, protein.getSequence().length(), true, false, false));
            protein.addFeature(pFeature);
        }
        reader.close();
    }

    private void kofamscan(File proteinFile, File output) throws Exception {
        logger.info("Processing " + output.getAbsolutePath());
        // Execute kofamscan
        if (!output.exists()) {
            String command = arguments.binary + " --profile=" + arguments.profile + " --ko-list=" + arguments.kolist + " --cpu=" + arguments.cpu + " --format=detail-tsv -o " + output + " " + proteinFile.getAbsolutePath();
            System.out.println("Command: " + command);
            ExecCommand execCommand = new ExecCommand(command);
            System.err.println(execCommand.getOutput());
        }
    }

    private void loadRDF() throws Exception {

        boolean stopcodon = false;

        Iterable<ResultLine> queryResults;

        queryResults = arguments.domain.getRDFSimpleCon().runQuery("getProteins.txt", true);

        int counter = 0;
        int filePart = 0;

        List<File> proteinFiles = new ArrayList<>();
        File proteinFile = File.createTempFile(name + "_" + filePart, ".kofamscan.fasta");
        proteinFiles.add(proteinFile);
        PrintWriter writer = new PrintWriter(proteinFile, StandardCharsets.UTF_8);

        for (ResultLine item : queryResults) {
            counter++;
            if (counter > 30000) {
                logger.info("Parsing batch: " + filePart);
                // Run generated file
                writer.close();

                // Create new file
                filePart++;
                // Reset the counter and create a new temp protein file
                counter = 0;
                proteinFile = new File(name + "_" + filePart + ".kofamscan.fasta");
                proteinFiles.add(proteinFile);
                writer = new PrintWriter(proteinFile, StandardCharsets.UTF_8);
            }

            String protein = item.getIRI("protein");
            String sequence = item.getLitString("sequence");

            if (!sequence.contains("*")) {
                writer.write(">" + protein + "\n" + sequence + "\n");
            } else {
                sequence = sequence.replaceAll("\\*", "X");
                writer.write(">" + protein + "\n" + sequence + "\n");
                stopcodon = true;
            }
        }

        logger.info("Parsing all batches");

        if (stopcodon) {
            logger.warn("Some proteins contain * in their sequence and have been masked with an X");
        }

        writer.close();

        // Parsing all the files
        for (File proteinFileX : proteinFiles) {
            if (proteinFileX.length() == 0) {
                throw new IllegalArgumentException("Your input file " + proteinFileX + " seems to have no proteins. Check your input .hdt file for proteins and add them in case of absence.");
            }
            File output = new File(proteinFile + ".out");
            kofamscan(proteinFileX, output);
        }

        // Collect all proteinFile + ".out" files and merge them into one
        PrintWriter printWriter = new PrintWriter("kofamscan.out", StandardCharsets.UTF_8);
        for (File proteinFileX : proteinFiles) {
            File kofamScanOutputFile = new File(proteinFileX + ".out");
            BufferedReader bufferedReader = new BufferedReader(new FileReader(kofamScanOutputFile));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                printWriter.println(line);
            }
            bufferedReader.close();
        }
        printWriter.close();
    }
}
