SELECT DISTINCT ?xrefs ?pa ?db ?xrefs2 ?pa2 ?db2
WHERE { 
	OPTIONAL {
		<%1$s> a gbol:CDS .
		<%1$s> gbol:xref ?xrefs .
		?xrefs gbol:PrimaryAccession ?pa .
		?xrefs gbol:db ?dbiri .
		?dbiri gbol:shortName ?db .
		}
	OPTIONAL {
		<%1$s> gbol:protein ?protein .
		?protein gbol:xref ?xrefs2 .
		?xrefs2 gbol:PrimaryAccession ?pa2 .
		?xrefs2 gbol:db ?dbiri2 .
		?dbiri2 gbol:shortName ?db2 .
	}
}