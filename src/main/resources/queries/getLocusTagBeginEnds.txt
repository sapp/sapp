SELECT DISTINCT ?beginpos ?endpos
WHERE {
	?gene gbol:locusTag "%1$s" .
	?gene gbol:location ?loc .
	?loc gbol:begin ?begin .
	?loc gbol:end ?end .
	?begin gbol:position ?beginpos .
	?end gbol:position ?endpos .
}
