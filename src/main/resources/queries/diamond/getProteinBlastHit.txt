SELECT ?desc
WHERE {
	<%1$s> a gbol:Protein .
	<%1$s> gbol:feature ?feature .
	?feature a gbol:ProteinHomology .
	?feature gbol:homologousToDesc ?desc .
	} 