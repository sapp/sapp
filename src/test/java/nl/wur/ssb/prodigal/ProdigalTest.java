package nl.wur.ssb.prodigal;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;


public class ProdigalTest {

    @Test
    public void testMeta() throws Throwable {
        File input = new File("./src/test/resources/input/prodigal/testfile3.hdt");
        File output = new File("./src/test/resources/output/testfile3.ttl");
        output.delete();

        String[] args = {"-input", input.getAbsolutePath(), "-output", output.getAbsolutePath(), "-prodigal", "-meta", "-debug"};
        App.main(args);
        File GFFfile = new File("AFCG01000000__#__sample_genecaller.gff");
        File nucleotideFile = new File("AFCG01000000__#__sample_nucleotide.fasta");
        File proteinFile = new File("AFCG01000000__#__sample_proteins.fasta");
        new validate(GFFfile.getAbsolutePath(), nucleotideFile.getAbsolutePath(), proteinFile.getAbsolutePath(), output.getAbsolutePath(), "getProdigalHits.txt");
    }

    @Test
    public void testGFF() throws Throwable {
        testMeta();

        // Test if GFF file is created and use direct parsing
        File input = new File("./src/test/resources/input/prodigal/testfile3.hdt");
        File output = new File("./src/test/resources/output/testfile3_parser.ttl");
        File GFFfile = new File("AFCG01000000__#__sample_genecaller.gff");
        File nucleotideFile = new File("AFCG01000000__#__sample_nucleotide.fasta");
        File proteinFile = new File("AFCG01000000__#__sample_proteins.fasta");

        String[] args = {"-input", input.getAbsolutePath(), "-output", output.getAbsolutePath(), "-prodigal", "-faa", proteinFile.getAbsolutePath(), "-ffa", nucleotideFile.getAbsolutePath(), "-gff", GFFfile.getAbsolutePath()};
        App.main(args);
    }

    @Test
    public void testSingle() throws Throwable {
        String input = "./src/test/resources/input/prodigal/GCA_001296815.1.hdt";
        input = "./tick/Neo_06.hdt";
        String output = "./src/test/resources/output/GCA_001296815.1.ttl";
        String[] args = {"-input", input, "-output", output, "-prodigal"};
        while (new File(output).exists()) {
            new File(output).delete();
        }

        if (new File(input).exists())
            App.main(args);
    }
}
