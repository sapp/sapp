package nl.wur.ssb.infernal;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;


public class InfernalTest {

    @Test
    public void testInfernal() throws Throwable {
        String cm = "./Rfam.cm";
        String clanin = "./Rfam.clanin";
        String input = "./src/test/resources/input/infernal/example.hdt";
        File output = new File("./src/test/resources/output/infernal.ttl");
        output.delete();
        String[] args = {
                "-infernal",
                "-input", input,
                "-output", output.getAbsolutePath(),
                "-cm", cm,
                "-clanin", clanin,
                "-cmscan", "/usr/local/bin/cmscan"
        };
        App.main(args);
    }
}
