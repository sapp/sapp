package nl.wur.ssb.sparql;

import nl.wur.ssb.App;

public class SPARQLTest {
    public void testSPARQL() throws Throwable {
        String[] args = {
                "-sparql",
                "-i", "taxonomy.hdt", // ""./src/test/resources/input/conversion/HDT",
                "-query", "SELECT * WHERE { ?S ?P ?O } LIMIT 10",
                "-format", "tsv",
                "-jobs", "4"
        };
        App.main(args);
    }
}
