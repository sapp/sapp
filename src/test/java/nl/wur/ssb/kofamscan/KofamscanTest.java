package nl.wur.ssb.kofamscan;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;

public class KofamscanTest {
    @Test
    public void testKoFamScanTest() throws Throwable {
        File input = new File("./src/test/resources/input/kofamscan/Lactobacillus_apis.SAPP.hdt");
        File resultFile = new File("./src/test/resources/input/kofamscan/Lactobacillus_apis.KofamKOALA.txt");
        File output = new File("./src/test/resources/output/kofamscan.ttl");
        String[] args = new String[]{
                "-kofamscan",
                "-input", input.getAbsolutePath(),
                "-resultFile", resultFile.getAbsolutePath(),
                "-output", output.getAbsolutePath(),
                "-exec_annotation", "/Users/jasperk/mambaforge/envs/kofamscan_v1.3.0/bin/exec_annotation",
                "-kolist", "/Users/jasperk/kegg/ko_list",
                "-profile", "/Users/jasperk/kegg/profiles",
                "-threads", "3"
        };
        App.main(args);
    }


}
