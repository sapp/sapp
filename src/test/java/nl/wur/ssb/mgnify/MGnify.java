package nl.wur.ssb.mgnify;

import nl.wur.ssb.App;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.query.ResultSet;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class MGnify {

    private final String PREFIX = """
            PREFIX gbol: <http://gbol.life/0.1/>
                        
            """;
    private final String identifier = "MGYG000290000";

    @Test
    public void test() throws Throwable {
//        String input = "hello\t\tworld\t!\t";
//        String[] parts = input.split("(?<=\t)|(?=\t)");
//        for (String part : parts) {
//            System.err.println(">" + part);
//        }
//        // Standard genome to RDF
//        conversion();
//        // Prodigal de-novo gene prediction
//        prodigal();
//        // Protein conversion
//        conversionProtein();
//        // Protein conversion
        conversionGFF();
        eggnog();
        interproscan();
    }

    private void interproscan() throws Exception {
        String input = "./src/test/resources/output/" + identifier + "_eggnog.ttl";
        String output = "./src/test/resources/output/" + identifier + "_interpro.ttl";
        String resultFile = "./src/test/resources/input/mgnify/MGYG000290000_InterProScan.tsv";
        new File(output).delete();

        String[] args = {"-i", input, "-o", output, "-resultFile", resultFile, "-interpro", "-debug"};

        App.main(args);
    }

    private void eggnog() throws Exception {
        String input = "./src/test/resources/output/" + identifier + "_protein.ttl";
        String output = "./src/test/resources/output/" + identifier + "_eggnog.ttl";
        String resultFile = "./src/test/resources/input/mgnify/MGYG000290000_eggNOG.tsv";
        new File(output).delete();

        String[] args = {"-i", input, "-o", output, "-resultFile", resultFile, "-eggnog", "-debug"};

        App.main(args);

    }

    private void conversionGFF() throws Exception {
        String input = "./src/test/resources/input/mgnify/MGYG000290000.gff";
        String fasta = "./src/test/resources/input/mgnify/MGYG000290000.fna";
        String output = "./src/test/resources/output/" + identifier + "_protein.ttl";
        new File(output).delete();

        String[] args = {"-i", input, "-f", fasta, "-o", output, "-gff2rdf", "-codon", "11", "-identifier", identifier, "-debug"};

        App.main(args);

        Domain domain = new Domain("file://" + output);
        ResultSet resultSet = domain.getRDFSimpleCon().runQueryDirect(PREFIX + "SELECT * WHERE { ?contig a gbol:Contig }");
        assertTrue(resultSet.hasNext());
        resultSet = domain.getRDFSimpleCon().runQueryDirect(PREFIX + "SELECT * WHERE { ?protein a gbol:Protein }");
        assertTrue(resultSet.hasNext());
    }

    private void conversionProtein() throws Exception {
        String input = "./src/test/resources/input/mgnify/MGYG000290000.faa";
        String output = "./src/test/resources/output/" + identifier + "_protein.ttl";
        new File(output).delete();

        String[] args = {"-i", input, "-o", output, "-fasta2rdf", "-codon", "11", "-identifier", identifier, "-protein", "-debug"};

        App.main(args);

        Domain domain = new Domain("file://" + output);
        ResultSet resultSet = domain.getRDFSimpleCon().runQueryDirect(PREFIX + "SELECT * WHERE { ?contig a gbol:Contig }");
        assertFalse(resultSet.hasNext());
        resultSet = domain.getRDFSimpleCon().runQueryDirect(PREFIX + "SELECT * WHERE { ?protein a gbol:Protein }");
        assertTrue(resultSet.hasNext());
    }

    private void prodigal() throws Exception {
        String input = "./src/test/resources/output/" + identifier + ".ttl";
        String output = "./src/test/resources/output/" + identifier + "_prodigal.ttl";

        new File(output).delete();

        String[] args = {"-input", input, "-output", output, "-prodigal", "-meta", "-debug"};
        App.main(args);

        Domain domain = new Domain("file://" + output);
        ResultSet resultSet = domain.getRDFSimpleCon().runQueryDirect(PREFIX + "SELECT * WHERE { ?protein a gbol:Protein }");
        assertTrue(resultSet.hasNext());
    }

    public void conversion() throws Throwable {
        String input = "./src/test/resources/input/mgnify/MGYG000290000.fna";
        String output = "./src/test/resources/output/" + identifier + ".ttl";
        new File(output).delete();

        String[] args = {"-i", input, "-o", output, "-fasta2rdf", "-codon", "11", "-identifier", identifier, "-genome", "-contig", "-debug"};

        App.main(args);

        Domain domain = new Domain("file://" + output);
        ResultSet resultSet = domain.getRDFSimpleCon().runQueryDirect(PREFIX + "SELECT * WHERE { ?contig a gbol:Contig }");
        assertTrue(resultSet.hasNext());
    }
}
