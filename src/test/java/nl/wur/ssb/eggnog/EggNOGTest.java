package nl.wur.ssb.eggnog;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;

public class EggNOGTest {
    @Test
    public void testEggNOG() throws Exception {
        File input = new File("./src/test/resources/input/eggnog/testfile3.ttl");
        File output = new File("./src/test/resources/output/testfile3_eggnog.ttl");
        String[] args = new String[]{
                "-eggnog",
                "-input", input.getAbsolutePath(),
                "-output", output.getAbsolutePath(),
                "-path", "/scratch/koeho006/git/unlock/infrastructure/binaries/eggnog-mapper-2.1.12"
        };
        App.main(args);
    }
}
