package nl.wur.ssb.local;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;

public class LocalTest {
    @Test
    public void testWorkflow() throws Throwable {
        File input = new File("./src/test/resources/input/conversion/EMBL/GCA_000520275.1.embl.gz");
        File output = new File("./GCA_000520275.1.ttl");

        String[] args;
        if (input.exists()) {
            // Conversion
            args = new String[]{"-embl2rdf", "-i", input.getAbsolutePath(), "-o", output.getAbsolutePath(), "-id", "GCA_000520275.1"};
            App.main(args);
            // Prodigal
            args = new String[]{"-prodigal", "-i", output.getAbsolutePath(), "-o", output.getAbsolutePath()};
            App.main(args);
            // rRNA / infernal
            args = new String[]{"-infernal", "-input", output.getAbsolutePath(), "-output", output.getAbsolutePath(), "-cm", "/scratch/koeho006/git/unlock/references/databases/infernal/Rfam.cm", "-clanin", "/scratch/koeho006/git/unlock/references/databases/infernal/Rfam.clanin", "-cmscan", "/scratch/koeho006/git/unlock/infrastructure/binaries/infernal/infernal-1.1.4-linux-intel-gcc/binaries/cmscan"};
            App.main(args);
            // Interproscan
            args = new String[]{"-interpro", "-input", output.getAbsolutePath(), "-output", output.getAbsolutePath(), "-a", "PFAM", "-path", "/scratch/koeho006/git/unlock/infrastructure/binaries/interproscan-5.65-97.0"};
            App.main(args);
            // Eggnog
            args = new String[]{"-eggnog"};
            App.main(args);
        } else {
            System.err.println("Input does not exist");
        }
    }

    @Test
    public void testPrefix() throws Throwable {
        File output = new File("./src/test/resources/output/issues/GCA_003835015.1.ttl");
        String[] args = {"-convert", "-i", output.getAbsolutePath(), "-o", output.getAbsolutePath() + "2.ttl"};
        App.main(args);
    }
}
