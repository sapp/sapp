package nl.wur.ssb.main;

import nl.wur.ssb.App;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

public class AppTest {
    private static final Logger logger = LogManager.getLogger();

    @Test
    public void testApp() {
        String[] args = {""};
        try {
            App.main(args);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Test
    public void testLog() {
        logger.info("INFO MESSAGE");
        logger.warn("WARN MESSAGE");
        logger.error("ERROR MESSAGE");
    }
}
