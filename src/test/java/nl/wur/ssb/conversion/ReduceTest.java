package nl.wur.ssb.conversion;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

/**
 * SAPP Conversion test cases
 */

public class ReduceTest {

    @Test
    public void testFileReduce() throws Exception {
        String inputFile = "./src/test/resources/input/conversion/HDT/testfile3.txt.hdt";
        String outputFile = "./src/test/resources/output/reduced.hdt";
        String[] args = {
                "-i", inputFile,
                "-o", outputFile,
                "-reduce"

        };
        App.main(args);
    }

    @Test
    public void testFolderReduce() throws Exception {
        String inputDir = "./src/test/resources/input/conversion/HDT";
        String outputDir = "./src/test/resources/output/HDT/output/";
        String[] args = {
                "-i", inputDir,
                "-o", outputDir,
                "-reduce"
        };
        App.main(args);
    }
}
