package nl.wur.ssb.conversion;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.InputOutput.Input;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class testForFail {

    String inputGff;
    String inputNucleotide;
    String inputProtein;
    String inputRDF;
    String queryFile;
    Domain domain;

    /**
     * Augustus constructor for gff vs rdf files test
     *
     * @param input     input file containing tool output in gff3 format
     * @param inputRDF  input file containing rdf output
     * @param queryFile queries to run on rdf
     * @throws Exception
     */

    public testForFail(String input, String inputRDF, String queryFile) throws Exception {
        this.inputGff = input;
        this.inputRDF = inputRDF;
        this.queryFile = queryFile;
        checkGFF3();
    }

    /**
     * Prodigal constructor for gff vs rdf files test
     *
     * @param inputGff        input Gff file
     * @param inputNucleotide input nucleotide.fasta file
     * @param inputProtein    input protein.fasta file
     * @param inputRDF        created RDF output file for prodigal
     * @param queryFile       file with the query to run
     * @throws Exception
     */
    public testForFail(String inputGff, String inputNucleotide, String inputProtein, String inputRDF, String queryFile) throws Exception {
        this.inputGff = inputGff;
        this.inputNucleotide = inputNucleotide;
        this.inputProtein = inputProtein;
        this.inputRDF = inputRDF;
        this.queryFile = queryFile;
        checkMeta();
    }


    /**
     * Check for Meta test
     *
     * @throws Exception
     */
    private void checkMeta() throws Exception {
        System.out.println("test for faults");
        //parse gff
        List<List<String>> geneCollection = parseGFF();
        //parse nucleotide
        List<List<String>> nucleotideCollection = parseFasta(inputNucleotide);
        //parse protein
        List<List<String>> proteinCollection = parseFasta(inputProtein);
        // Load RDF
        domain = Input.load(new File(inputRDF));
        //first checks
        assertEquals(geneCollection.size(), nucleotideCollection.size(), proteinCollection.size());
        boolean checker = false;
        for (List<String> gene : geneCollection) {
            for (List<String> nucleotide : nucleotideCollection) {
                boolean a = gene.get(0).equals(nucleotide.get(0).replaceAll("_\\d ", ""));
                boolean b = gene.get(1).equals(nucleotide.get(1).replace(" ", ""));
                boolean c = gene.get(2).equals(nucleotide.get(2).replace(" ", ""));
                if (a && b && c) {
                    checker = true;
                    break;
                }
            }
        }
        assertTrue(checker);
        //checkRDF
        new checkRDF(domain, nucleotideCollection, proteinCollection, queryFile);


    }

    /**
     * Check for augustus gff3 file
     *
     * @throws Exception
     */
    private void checkGFF3() throws Exception {
        System.out.println("test for faults");

        domain = Input.load(new File(inputRDF));
        //parse gff3
        BufferedReader buffered;
        InputStream fileStream = new FileInputStream(this.inputGff);
        Reader decoder = new InputStreamReader(fileStream, StandardCharsets.UTF_8);
        buffered = new BufferedReader(decoder);
        String line = "";
        boolean geneStart = false;
        boolean proteinStart = false;
        String proteinSequence = "";
        String proteinShakey;
        String beginPos;
        String endPos;
        List<String> geneParts = new ArrayList<>();
        //for every line check if it is start, end, protein of gene position and store it if needed
        //at the end of a predicted gene it preforms checkRDF
        while (line != null) {
            if (line.startsWith("# start gene")) {
                geneStart = true;
            } else if (line.startsWith("# end gene")) {
                geneStart = false;
                proteinStart = false;
                proteinSequence = proteinSequence.replace("X", "*");
                geneParts.add(proteinSequence);
                proteinShakey = nl.wur.ssb.SappGeneric.Generic.checksum(proteinSequence, "SHA-384");
                geneParts.add(proteinShakey);
                //checks geneParts with the RDF file
                new checkRDF(domain, geneParts, this.queryFile);
                geneParts.clear();
            } else if (geneStart) {
                if (line.startsWith("# protein sequence")) {
                    proteinStart = true;
                    //replaces all wrong characters which occur in the first sequence line
                    proteinSequence = line.replace("\n", "").replace("# protein sequence = [", "").replace("]", "");
                } else if (proteinStart) {
                    proteinSequence = proteinSequence + line.replace("# ", "").replace("]", "");
                }
                if (line.contains("AUGUSTUS\tgene")) {
                    String[] items = line.split("\t");
                    if (items.length > 0) {
                        beginPos = items[3];
                        endPos = items[4];
                        geneParts.add(beginPos);
                        geneParts.add(endPos);
                    }
                }
            }

            line = buffered.readLine();
        }
        buffered.close();
        decoder.close();
        fileStream.close();
    }

    /**
     * Parser for the prodigal GFF file
     *
     * @return ArrayList<List < String>> geneCollection containing the parsed genes
     * @throws Exception
     */
    private List<List<String>> parseGFF() throws Exception {
        System.out.println("Parsing gff");
        BufferedReader buffered;
        InputStream fileStream = new FileInputStream(inputGff);
        Reader decoder = new InputStreamReader(fileStream, StandardCharsets.UTF_8);
        buffered = new BufferedReader(decoder);
        String line = "";
        List<List<String>> geneCollection = new ArrayList<>();
        while (line != null) {
            if (!line.startsWith("#") && !line.equals("")) {
                String[] elements = line.split("\t");
                List<String> geneParts = new LinkedList<>(Arrays.asList(elements));
                if (geneParts.size() == 9) {
                    geneParts.remove(1);
                    geneParts.remove(1);
                    geneCollection.add(geneParts);
                }
            }
            line = buffered.readLine();
        }
        buffered.close();
        decoder.close();
        fileStream.close();
        return geneCollection;
    }

    /**
     * Parser for the prodigal fasta files
     *
     * @return List<List < String>> fastaCollection containing the parsed genes/proteins
     * @throws Exception
     */
    private List<List<String>> parseFasta(String inputStream) throws Exception {
        System.out.println("Parsing .fasta");
        InputStream fileStream = new FileInputStream(inputStream);
        Reader decoder = new InputStreamReader(fileStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(decoder);
        String line = "";
        List<String> fastaParts;
        List<List<String>> fastaCollection = new ArrayList<>();
        boolean start = false;
        int countStart = -1;
        int countList = -1;
        fastaParts = new ArrayList<>();
        while (line != null) {
            if (line.startsWith(">") && countList > -1) {
                String sequence = fastaParts.get(fastaParts.size() - 1);
                String shakey = nl.wur.ssb.SappGeneric.Generic.checksum(sequence, "SHA-384");
                fastaParts.add(shakey);
                fastaCollection.add(fastaParts);
                countList++;
                countStart = 0;
                String[] elements = line.split("#");
                elements[0] = elements[0].replace(">", "");
                fastaParts = new ArrayList<>(Arrays.asList(elements));
            } else if (line.startsWith(">") && countList == -1) {
                countList++;
                countStart = 0;
                String[] elements = line.split("#");
                elements[0] = elements[0].replace(">", "");
                fastaParts = new ArrayList<>(Arrays.asList(elements));
            } else if (!bufferedReader.ready()) {
                int length = fastaParts.size();
                String data = fastaParts.get(length - 1);
                data = data + line.replace("\n", "").replace("*", "");
                fastaParts.remove(length - 1);
                fastaParts.add(data);
                String sequence = fastaParts.get(fastaParts.size() - 1);
                String shakey = nl.wur.ssb.SappGeneric.Generic.checksum(sequence, "SHA-384");
                fastaParts.add(shakey);
                fastaCollection.add(fastaParts);
            } else if (countStart == 0) {
                countStart++;
                line = line.replace("\n", "").replace("*", "");
                fastaParts.add(line);
            } else if (countStart > 0) {
                int length = fastaParts.size();
                String data = fastaParts.get(length - 1);
                data = data + line.replace("\n", "").replace("*", "");
                fastaParts.remove(length - 1);
                fastaParts.add(data);
            }

            line = bufferedReader.readLine();
        }
        bufferedReader.close();
        decoder.close();
        fileStream.close();
        return fastaCollection;
    }
}
