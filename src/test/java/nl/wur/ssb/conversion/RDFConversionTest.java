package nl.wur.ssb.conversion;

import nl.wur.ssb.App;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.InputOutput.Input;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Iterator;

public class RDFConversionTest {

    @Test
    public void testToHDT() throws Exception {
        new File("chr_3_2_A_niger_ATCC_1015").mkdirs();

        String[] args = {
                "-convert",
                "-input", "./src/test/resources/input/conversion/RDF/chr_3_2_A_niger_ATCC_1015.ttl",
                "-output", "chr_3_2_A_niger_ATCC_1015"};

        App.main(args);

        // Query
        Domain domain = new Domain("file://chr_3_2_A_niger_ATCC_1015");
        System.err.println(domain.getRDFSimpleCon().getModel().size());
        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("getProteins.txt", true).iterator();
        boolean success = false;
        while (resultLineIterator.hasNext()) {
            String proteinIRI = resultLineIterator.next().getIRI("protein");
            System.err.println(proteinIRI);
            success = true;
            break;
        }
        if (!success) {
            throw new Exception("Test failed, get was not a success");
        }
    }

    @Test
    public void testSmallFile() throws Exception {
        String[] args = {
                "-convert",
                "-input", "./src/test/resources/input/conversion/RDF/chr_3_2_A_niger_ATCC_1015.ttl",
                "-output", "./src/test/resources/output/chr_3_2_A_niger_ATCC_1015.hdt"
        };

        App.main(args);
        Input.load(new File("./src/test/resources/input/conversion/RDF/test.ttl"));
    }
}
