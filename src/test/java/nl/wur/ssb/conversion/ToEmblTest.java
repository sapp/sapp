package nl.wur.ssb.conversion;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * SAPP Conversion test cases
 */

public class ToEmblTest {

    @Test
    public void testFile4() throws Exception {
        File inputFile = new File("./src/test/resources/input/conversion/HDT2EMBL/example2.hdt");
        File outputFile = new File("./src/test/resources/output/example.embl");
        outputFile.delete();
        String[] args = {"-i", inputFile.getAbsolutePath(), "-o", outputFile.getAbsolutePath(), "-rdf2embl"};
        if (inputFile.exists())
            App.main(args);
    }

    @Test
    public void testFileFullInterProAndEnzdp() throws Exception {
        File tmp = new File("./src/test/resources/output/exampleFull.embl");
        tmp.delete();
        File inputFile = new File("./src/test/resources/input/conversion/HDT2EMBL/sphingo_FULL_IPR.hdt");
        String[] args = {"-i", inputFile.getCanonicalPath(), "-o",
                "./src/test/resources/output/exampleFull.embl", "-rdf2embl", "-codon", "1", "-sha", "-locus", "LOCUSTEST_"};
        if (inputFile.exists())
            App.main(args);
    }

    @Test
    public void testFileBlastSwissOnly() throws Exception {
        File output = new File("./src/test/resources/output/exampleBlast.embl");
        output.delete();
        File inputFile = new File("./src/test/resources/input/conversion/HDT2EMBL/swiss_blast.hdt");
        String[] args = {"-i", inputFile.getCanonicalPath(), "-o", output.getAbsolutePath(), "-rdf2embl", "-codon", "1", "-sha", "-locus", "BLAST_"};
        if (inputFile.exists())
            App.main(args);
    }

    @Test
    public void testFasta2Embl() throws Exception {
        File input = new File("./src/test/resources/input/conversion/fasta/testfile1.fasta");
        File output = new File("./src/test/resources/output/testfile1.fasta.hdt");
        String[] args = {"-i", input.getAbsolutePath(), "-o", output.getAbsolutePath(), "-fasta2rdf", "-codon", "11", "-identifier", "Testfile1", "-genome", "-contig", "-debug"};
        App.main(args);

        String[] args2 = {"-i", output.getAbsolutePath(), "-o", output.getAbsolutePath() + ".embl", "-rdf2embl"};
        App.main(args2);
    }
}
