package nl.wur.ssb.conversion;


import org.junit.jupiter.api.Test;

import java.io.File;

//
public class RemoteTests {

    @Test
    public void testFastaEndpoint() throws Exception {
        File input = new File("./src/test/resources/input/conversion/FASTA/testfile2.fasta");
        File output = new File("./src/test/resources/output/testfile2.fasta.hdt");
        String update = "http://10.117.11.77:7200/repositories/Test/statements";
        String[] args = {
                "-i", "./src/test/resources/input/conversion/FASTA/testfile2.fasta",
                "-o", "./src/test/resources/output/testfile2.fasta.hdt",
                "-fasta2rdf",
                "-codon", "11",
                "-identifier", "TestFile2",
                "-genome", "-chromosome",
                "-update", update};
        if (!new File("/.dockerenv").exists()) {
//            App.main(args);
        }
    }
}

