package nl.wur.ssb.conversion;

import nl.wur.ssb.App;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.InputOutput.Input;
import nl.wur.ssb.SappGeneric.Reader;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.Test;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.validation.Origin;
import uk.ac.ebi.embl.api.validation.ValidationMessage;
import uk.ac.ebi.embl.api.validation.ValidationResult;
import uk.ac.ebi.embl.fasta.reader.FastaFileReader;
import uk.ac.ebi.embl.fasta.reader.FastaLineReader;

import java.io.BufferedReader;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

/**
 * SAPP conversion testcase. It extends the super class TestCase.
 */
public class FastaTest {
    static org.apache.logging.log4j.Logger logger = LogManager.getLogger(App.class);

    /**
     * Method to check whether the conversion from fasta2rdf is going right. Takes the sequence from the fastafile and converts it to
     * an SHA-384 checksum. The RDF checksum is compared with this one and if there are problems, then they will be reported.
     *
     * @param input     Input fastafile
     * @param output    Output RDFfile
     * @param args      Arguments for the Fasta app
     * @param type      The type of sequence. Now checking for DNA or Protein.
     * @param queryFile The queryfile to use for getting the RDF data.
     * @throws Exception Main exception.
     */
    public static void validation(File input, File output, String[] args, String type, String queryFile) throws Exception {
        if (!input.exists()) {
            logger.warn("Input file does not exists " + input);
            return;
        }
        while (output.exists()) {
            if (output.isDirectory()) {
                FileUtils.deleteDirectory(output);
            } else {
                FileUtils.forceDelete(output);
            }
            logger.info("Removing test output file " + output);
        }

        App.main(args);

        /**
         * Private RDFResource variable, for connecting to the local RDF database.
         */
        Domain domain = Input.load(output);

        //Queries to execute through RDFResource.
        Iterable<ResultLine> contigs = domain.getRDFSimpleCon().runQuery(queryFile, true);

        if (type == "DNA") {
            // FASTA reader for input validation of DNA type files.
            Map fasta = new HashMap();

            BufferedReader bufreader = new Reader().reader(input);
            FastaFileReader fastaReader = new FastaFileReader(new FastaLineReader(bufreader));

            ValidationResult result = fastaReader.read();
            Collection<ValidationMessage<Origin>> messages = result.getMessages();
            for (ValidationMessage<Origin> message : messages) {
                logger.info(message.getMessage());
            }

            while (fastaReader.isEntry()) {
                Entry entry = fastaReader.getEntry();
                //Getting the right formatted sequence of the fasta file
                String seq = new String(entry.getSequence().getSequenceByte()).toUpperCase().trim();
                String primAcc = entry.getPrimaryAccession();
                //Create SHA-key out of the Fasta file sequence
                String shakey = Generic.checksum(seq, "SHA-384");
                fasta.put(shakey, seq);
                // Reading the next entry
                fastaReader.read();
            }

            for (ResultLine contig : contigs) {
                String seq = contig.getLitString("sequence");
                //Creating SHA-key out of RDF sequence
                String seqkey = Generic.checksum(seq.trim(), "SHA-384");
                //Get the SHA-key of the RDF database
                String shakeyRDF = contig.getLitString("shakey");
                //Checks is SHA-key of the RDF database is present in the hashmap from the fasta file and if its Null
                assertNotNull(fasta.get(shakeyRDF));
                //Checks if the SHA-key from the RDF sequence is equal to the RDF database SHA-key
                assertEquals(seqkey, shakeyRDF);
            }
            bufreader.close();

        } else if (type == "Protein") {
            String sequence = "";
            String fileCheckSum = "";
            Scanner scanner = new Scanner(input);
            //Protein file reader
            Map<String, String> fastaTemp = new HashMap<>();
            Map<String, String> fasta = new HashMap<>();
            String header = null;
            int count = -1;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.startsWith(">")) {
                    header = line;
                    fastaTemp.put(header, "");
                } else {
                    fastaTemp.put(header, fastaTemp.get(header) + line);
                }
            }
            scanner.close();
            //Reforms the fasta Hashmap. Now checksum and serquence are stored.
            for (String key : fastaTemp.keySet()) {

                String checkSum = Generic.checksum(fastaTemp.get(key), "SHA-384");
                fasta.put(checkSum, fastaTemp.get(key));
            }
            //For every contig in the RDF database, compare it to the contig in the fastafile.
            for (ResultLine contig : contigs) {
                String seq = contig.getLitString("sequence");
                //Creating SHA-key out of RDF sequence
                String seqkey = Generic.checksum(seq, "SHA-384");
                //Get the SHA-key of the RDF database
                String shakey = contig.getLitString("shakey");
                //Checks is SHA-key of the RDF database is present in the hashmap from the fasta file and if its Null
                assertNotNull(fasta.get(shakey));
                //Checks if the SHA-key from the RDF sequence is equal to the RDF database SHA-key
                assertEquals(seqkey, shakey);
            }
            scanner.close();
        }
    }

    @Test
    public void testInvalidFasta() {
        String input = "./src/test/resources/input/conversion/FASTA/invalid.fasta";
        String output = "./src/test/resources/output/testfile1.fasta.hdt";
        String[] args = {"-i", input, "-o", output, "-fasta2rdf", "-codon", "11", "-identifier", "Testfile1", "-genome", "-contig", "-debug"};
        try {
            validation(new File(input), new File(output), args, "DNA", "getFastaTestContigs.txt");
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("java.io.IOException: Invalid Sequence:Failed to read the Sequence at line :null"));
        }
        try {
            validation(new File(input), new File(output), args, "Protein", "getFastaTestProteins.txt");
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("java.io.IOException: Invalid Sequence:Failed to read the Sequence at line :null"));
        }
    }

    /**
     * Testcase 1, Genes
     *
     * @throws Exception
     */
    @Test
    public void testFile1() throws Exception {
        File input = new File("./src/test/resources/input/conversion/fasta/testfile1.fasta");
        File output = new File("./src/test/resources/output/testfile1.fasta.hdt");
        String[] args = {"-i", input.getAbsolutePath(), "-o", output.getAbsolutePath(), "-fasta2rdf", "-codon", "11", "-identifier", "Testfile1", "-genome", "-contig", "-debug"};
        // Cleanup output file if exists
        while (output.exists()) {
            output.delete();
        }

        // Cleanup temp dir if exists
        while (new File(output + "dir").exists()) {
            new File(output + "dir").delete();
        }

        if (input.exists()) {
            validation(input, output, args, "DNA", "getFastaTestContigs.txt");
        } else {
            logger.warn("Input file does not exists " + input);
        }
    }

    /**
     * Testcase 2, Genomes
     *
     * @throws Exception
     */
    @Test
    public void testFile2() throws Exception {
        String input = "./src/test/resources/input/conversion/FASTA/testfile2.fasta";
        String output = "./src/test/resources/output/testfile2.fasta.hdt";
        String[] args = {"-i", input, "-o", output, "-fasta2rdf", "-codon", "11",
                "-identifier", "TestFile2", "-genome", "-chromosome"};
        new File(output + ".index.v1-1").delete();
        if (new File(input).exists())
            validation(new File(input), new File(output), args, "DNA", "getFastaTestContigs.txt");
    }

    /**
     * Compression test
     *
     * @throws Exception
     */
    @Test
    public void testFile2Gz() throws Exception {
        String input = "./src/test/resources/input/conversion/FASTA/testfile2.fasta.gz";
        String output = "./src/test/resources/output/testfile2.fasta.hdt";
        String[] args = {
                "-i", input,
                "-o", output,
                "-fasta2rdf",
                "-codon", "11",
                "-identifier", "TestFile2",
                "-genome",
                "-chromosome"
        };
        if (new File(input).exists())
            validation(new File(input), new File(output), args, "DNA", "getFastaTestContigs.txt");
    }

    /**
     * Testcase 3, Protein.
     *
     * @throws Exception
     */
    @Test
    public void testProteinFastaMycoplasma() throws Exception {
        String input = "./src/test/resources/input/conversion/FASTA/testfile3.fasta";
        String output = "./src/test/resources/output/testfile3.fasta.hdt";
        String[] args = {"-i", input, "-o", output, "-fasta2rdf", "-codon", "11", "-identifier", "TestFile3", "-protein"};
        validation(new File(input), new File(output), args, "Protein", "getFastaTestProteins.txt");
    }

    /**
     * Testcase 4, Debug.
     *
     * @throws Exception
     */
    @Test
    public void testDebug() throws Exception {
        String input = "./src/test/resources/input/conversion/DEBUG/Mus_musculus.GRCm38.dna.primary_assembly.head.fa";
        String output = "./src/test/resources/output/debug.hdt";
        String[] args = {"-i", input, "-o", output, "-fasta2rdf", "-codon", "11", "-identifier", "TestFile4", "-genome", "-chromosome"};
        validation(new File(input), new File(output), args, "Genome", "getFastaTestContigs.txt");
    }
}
