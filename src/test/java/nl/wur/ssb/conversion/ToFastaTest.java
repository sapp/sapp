package nl.wur.ssb.conversion;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * SAPP Conversion test cases
 */

public class ToFastaTest {


    @Test
    public void testFile3() throws Exception {
        String[] args = {"-i",
                "./src/test/resources/input/conversion/RDF/chr_3_2_A_niger_ATCC_1015.ttl",
                "-rdf2fasta",
                "-transcript",
                "transcript.fasta",
                "-protein", "protein.fasta",
                "-genome", "genome.fasta",
                "-trna", "trna.fasta",
                "-rrna", "rrna.fasta"
        };

        App.main(args);

        System.err.println(new File("transcript.fasta").length());
        System.err.println(new File("protein.fasta").length());
        System.err.println(new File("trna.fasta").length());
        System.err.println(new File("genome.fasta").length());
    }
}
