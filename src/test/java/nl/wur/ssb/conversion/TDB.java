package nl.wur.ssb.conversion;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.io.FileUtils;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.system.ErrorHandlerFactory;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;

public class TDB {
    private final static org.apache.logging.log4j.Logger logger = LogManager.getLogger(TDB.class);

    @Test
    public void testReload() throws Exception {
        // Test to create TDB
        String file = "./src/test/resources/input/conversion/RDF/chr_3_2_A_niger_ATCC_1015.ttl";
        FileUtils.forceMkdir(new File("test"));

        Domain domain = new Domain("file://test");

        logger.info(domain.getRDFSimpleCon().getModel().size());

        // The parsers will do the necessary character set conversion.
        try (
                InputStream in = new FileInputStream(file)) {
            RDFParser.create()
                    .source(in)
                    .lang(RDFLanguages.TURTLE)
                    .errorHandler(ErrorHandlerFactory.errorHandlerStrict)
                    .base("http://gbol.life/0.1/")
                    .parse(domain.getRDFSimpleCon().getModel());
        }

        domain.getRDFSimpleCon().getModel().getNsPrefixMap().put("gbol", "http://gbol.life/0.1/");

        for (String key : domain.getRDFSimpleCon().getModel().getNsPrefixMap().keySet()) {
            System.err.println(key + " " + domain.getRDFSimpleCon().getModel().getNsPrefixMap().get(key));
        }

        System.err.println(domain.getRDFSimpleCon().getModel().size());

        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("getProteins.txt", true).iterator();
        while (resultLineIterator.hasNext()) {
            String proteinIRI = resultLineIterator.next().getIRI("protein");
            System.err.println(proteinIRI);
            break;
        }

        resultLineIterator = domain.getRDFSimpleCon().runQuery("getProteinInformation.txt", true).iterator();
        while (resultLineIterator.hasNext()) {
            String proteinIRI = resultLineIterator.next().getIRI("protein");
            System.err.println(proteinIRI);
        }

        domain.close();
    }
}
