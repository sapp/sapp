package nl.wur.ssb.conversion;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;


/**
 * SAPP Conversion test cases
 */

public class GFF3Test {

    @Test
    public void testSaccharomyces() {
        String output = "./src/test/resources/output/Saccharomyces_cerevisiae.dir";
        String fasta = "./src/test/resources/input/conversion/GFF3/Saccharomyces_cerevisiae.R64-1-1.dna.chromosome.I.fa";
        String gff3 = "./src/test/resources/input/conversion/GFF3/Saccharomyces_cerevisiae.R64-1-1.35.chromosome.I.gff3";
        String[] args = {"-f", fasta, "-gff2rdf", "-i", gff3, "-o", output, "-id", "Saccharomyces_cerevisiae.R64-1-1.35", "-debug", "-codon", "1", "-topology", "linear"};
        // TODO looking into Object is external ref or has no type defined
        //    App.main(args);

        //    HDT hdt = HDTManager.loadIndexedHDT(output, null);
        //    HDTGraph graph = new HDTGraph(hdt);
        //    Model model = new ModelCom(graph);
        //    domain.getRDFSimpleCon().getModel().add(model);
        //    domain.save(output + ".ttl");
    }

    @Test
    public void testNiger() throws Exception {
        String output = "./src/test/resources/output/chr_3_2_A_niger_ATCC_1015.ttl";
        String fasta = "./src/test/resources/input/conversion/GFF3/chr_3_2_A_niger_ATCC_1015.hdt.fasta";
        String gff3 = "./src/test/resources/input/conversion/GFF3/chr_3_2_A_niger_ATCC_1015.hdt.gff3";
        String[] args = {
                "-f", fasta,
                "-gff2rdf",
                "-i", gff3,
                "-o", output,
                "-id", "Saccharomyces_cerevisiae.R64-1-1.35",
                "-debug",
                "-codon", "1",
                "-topology", "linear"
        };

        if (new File(gff3).exists() && !new File("/.dockerenv").exists()) {
            App.main(args);
        }

        new testForFail(gff3, output, "getAugustusHits.txt");
    }

    @Test
    public void testTranslation() throws Exception {
        String fasta = "./src/test/resources/input/conversion/Translation/genome.fa";
        String gff3 = "./src/test/resources/input/conversion/Translation/weird_scaffold_7.gff";
        String output = "./src/test/resources/output/scaffold_09_test_2.ttl";
        String[] args = {
                "-f", fasta,
                "-gff2rdf",
                "-i", gff3,
                "-o", output,
                "-id", "scaffold_09_test",
                "-debug",
                "-codon", "1",
                "-topology", "linear",
                "-force"
        };
        while (new File(output).exists()) {
            new File(output).delete();
        }
        if (new File(gff3).exists()) {
            App.main(args);
        } else {
            System.out.println("test skipped");
        }
    }

}
