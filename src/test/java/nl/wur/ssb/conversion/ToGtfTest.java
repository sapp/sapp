package nl.wur.ssb.conversion;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

/**
 * SAPP Conversion test cases
 */

public class ToGtfTest {

    @Test
    public void testFile3() throws Exception {
        String inputFile1 = "./src/test/resources/input/conversion/HDT/testfile3.txt.hdt";
        String[] args = {
                "-i", inputFile1,
                "-o", "./src/test/resources/output/testfiles.txt.hdt.gtf",
                "-rdf2gtf"
        };
        App.main(args);
    }

}
