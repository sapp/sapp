package nl.wur.ssb.conversion;

import nl.wur.ssb.App;

/**
 * SAPP conversion testcase. It extends the super class TestCase.
 */
public class HelpTest {

    public void testHelp() throws Exception {
        // Causes an exit 0 which makes it not possible to have @Test enabled...
        String[] args = {};
        App.main(args);
    }
}
