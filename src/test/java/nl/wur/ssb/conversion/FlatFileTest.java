package nl.wur.ssb.conversion;

import nl.wur.ssb.App;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.junit.jupiter.api.Test;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.qualifier.Qualifier;
import uk.ac.ebi.embl.api.validation.Origin;
import uk.ac.ebi.embl.api.validation.ValidationMessage;
import uk.ac.ebi.embl.api.validation.ValidationResult;
import uk.ac.ebi.embl.flatfile.reader.EntryReader;
import uk.ac.ebi.embl.flatfile.reader.embl.EmblEntryReader;
import uk.ac.ebi.embl.flatfile.reader.genbank.GenbankEntryReader;

import java.io.BufferedReader;
import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

//import org.rdfhdt.hdt.hdt.HDTManager;
//import org.rdfhdt.hdtjena.HDTGraph;

/**
 * Unit test for FlatFile conversion
 */

public class FlatFileTest {

    /**
     * Method to check whether or not the input sequence is correctly transformed into an rdf file format.
     *
     * @param input     The input file(Genbank or EMBL)
     * @param output    The output file(HDT)
     * @param args      Argument line to start the conversion into RDF
     * @param fileType  The file type which is used.(GENBANK or EMBL)
     * @param queryFile The queryfile for querying the RDF database.
     * @throws Exception Main exception if something wents wrong.
     */
    public void validation(File input, File output, String[] args, String fileType, String queryFile) throws Exception {
        Conversion.main(args);

        System.err.println("FINISHED conversion, performing tests...");

        /*
        RDF connection for quering of the rdf database.
         */
        /**
         * RDF connection for querying of the rdf database.
         */
        Domain domain = nl.wur.ssb.SappGeneric.InputOutput.Input.load(output);
        System.err.println("Triples: " + domain.getRDFSimpleCon().getModel().size());

        //Queries to execute through RDFResource.
        Iterable<ResultLine> results = domain.getRDFSimpleCon().runQuery(queryFile, true);

        //Setting up the EMBL/GENBANK file reader
        BufferedReader reader = Conversion.getStreamFromZip(input);
        EntryReader emblReader = null;
        if (fileType.equals("EMBL")) {
            emblReader = new EmblEntryReader(reader);
        } else if (fileType.equals("GENBANK")) {
            emblReader = new GenbankEntryReader(reader);
        }
        try {
            //Read embl and genbank files manual. Initiate reading the EMBL/GENBANK file
            ValidationResult validations = emblReader.read();

            if (!validations.getMessages().isEmpty()) {
                Iterator<ValidationMessage<Origin>> valiter = validations.getMessages().iterator();
                while (valiter.hasNext()) {
                    ValidationMessage<Origin> validation = valiter.next();
                }
            }

            //Start reading embl file
            HashMap<String, String> emblProtMap = new HashMap<>();
            String NASHA = "";
            String protSHA;
            String emblProtein;
            boolean containsProt = false;
            while (emblReader.isEntry()) {
                //Get .NA seq
                Entry entry = emblReader.getEntry();
                String emblSequence = new String(entry.getSequence().getSequenceByte()).toUpperCase();
                NASHA = nl.wur.ssb.SappGeneric.Generic.checksum(emblSequence, "SHA-384");
                //Extract the protein seq('s) out of the entry and store them in an hashmap
                for (int i = 0; i < entry.getFeatures().size(); i++) {
                    Feature feature = entry.getFeatures().get(i);
                    for (int j = 0; j < feature.getQualifiers().size(); j++) {
                        Qualifier qualifier = feature.getQualifiers().get(j);
                        if (qualifier.getName().equals("translation")) {
                            containsProt = true;
                            emblProtein = qualifier.getValue();
                            protSHA = nl.wur.ssb.SappGeneric.Generic.checksum(emblProtein, "SHA-384");
                            emblProtMap.put(protSHA, emblProtein);
                        }
                    }
                }
                emblReader.read();
            }

            //Checks for every result in the db:
            for (ResultLine result : results) {

                // Getting the protein results
                life.gbol.domain.Protein protein = domain.make(life.gbol.domain.Protein.class, result.getIRI("protein"));

                String protSeq = protein.getSequence();
                String protShakey = protein.getSha384();

                // Getting the contig (repetitive)
                life.gbol.domain.NASequence dnaobject = domain.make(life.gbol.domain.NASequence.class, result.getIRI("dnaobject"));
                String shakey = dnaobject.getSha384();
                String seq = dnaobject.getSequence();

                //Creating SHA-key out of RDF sequence
                String seqkey = nl.wur.ssb.SappGeneric.Generic.checksum(seq, "SHA-384");
                String protkey = nl.wur.ssb.SappGeneric.Generic.checksum(protSeq, "SHA-384");

                // For non pseudo genes
                if (result.getIRI("pseudo") == null && containsProt) {
                    //Checks if the SHA-key from the RDF sequence is equal to the RDF database SHA-key
                    assertEquals(seqkey, shakey);
                    //Checks if the RDF database protein SHA-key is equal to the RDF protein sequence SHA-key
                    assertEquals(protShakey, protkey);
                    //Checks if the RDF database protein SHA-key is present in the map with the protein SHA-key's from the file
                    assertNotNull(emblProtMap.get(protShakey));
                    //Checks if the file sequence SHA-key equals to the RDF sequence SHA-key
                    // Disable when testing a file with more than one contig as it only works on files with 1 contig...
                    // assertEquals(NASHA, shakey);
                }
            }
            //When there is no protein sequence in the file
        } catch (ArrayIndexOutOfBoundsException aex) {
            System.err.println("There is no protein detected.");
            System.exit(1);
        }
    }

    /**
     * Test 1 with testfile1
     */
    @Test
    public void testFile1() throws Exception {
//        File input = new File("GCA_001518815.1.gz");
        File input = new File("./src/test/resources/input/conversion/EMBL/testfile1.txt.gz");
        File output = new File("./src/test/resources/output/testfile1.txt.ttl");
        while (output.exists()) {
            output.delete();
        }
        String[] args = {"-i", input.getAbsolutePath(), "-embl2rdf", "-o", output.getAbsolutePath(), "-id", "AFCG01000000", "-codon", "11"};

        // Open files checking
        validation(input, output, args, "EMBL", "getEmblTest.txt");
    }

    /**
     * Test 1A with testfile1A
     */
    @Test
    public void testFile1A() throws Exception {
        File input = new File("./src/test/resources/input/conversion/EMBL/testfile1A.txt.gz");
        File output = new File("./src/test/resources/output/testfile1A.txt.ttl");
        while (output.exists()) {
            output.delete();
        }
        String[] args = {"-i", input.getAbsolutePath(), "-embl2rdf", "-o", output.getAbsolutePath(), "-id", "AFCG01000000", "-codon", "1"};
        validation(input, output, args, "EMBL", "getEmblTest.txt");
    }

    /**
     * Test 2 with testfile2
     */
    @Test
    public void testFile2() throws Exception {
        File input = new File("./src/test/resources/input/conversion/EMBL/testfile2.txt.gz");
        File output = new File("./src/test/resources/output/testfile2.txt.hdt");
        while (output.exists()) {
            output.delete();
        }
        String[] args = {
                "-embl2rdf",
                "-i", input.getAbsolutePath(),
                "-o", output.getAbsolutePath(),
                "-id", "AFCG01000000",
                "-codon", "11"
        };
        validation(input, output, args, "EMBL", "getEmblTest.txt");
        output.delete();
    }

    /**
     * Test 3 with testfile3
     */
    @Test
    public void testFile3() throws Exception {
        File input = new File("./src/test/resources/input/conversion/EMBL/testfile3.txt");
        File output = new File("./src/test/resources/output/testfile3.ttl");
        while (output.exists()) {
            output.delete();
        }
        String[] args = {"-i", input.getAbsolutePath(), "-embl2rdf", "-o", output.getAbsolutePath(), "-id", "AFCG01000000"};
        validation(input, output, args, "EMBL", "getEmblTest.txt");
    }


    /**
     * Test 4 with testfile4
     */
    @Test
    public void testFile4() throws Exception {
        File input = new File("./src/test/resources/input/conversion/EMBL/testfile4.txt.gz");
        File output = new File("./src/test/resources/output/testfile4.txt.hdt");
        while (output.exists()) {
            output.delete();
        }
        String[] args = {
                "-embl2rdf",
                "-i", input.getAbsolutePath(),
                "-o", output.getAbsolutePath(),
                "-id", "GCA_000007125",
                "-codon", "11",
        };

        validation(input, output, args, "EMBL", "getEmblTest.txt");
        output.delete();
    }


    /**
     * Test 5 with testfile5
     */
    @Test
    public void testFile5() throws Exception {
        File input = new File("./src/test/resources/input/conversion/EMBL/testfile5.txt.gz");
        File output = new File("./src/test/resources/output/testfile5.txt.hdt");
        while (output.exists()) {
            output.delete();
        }
        // Duplication test
        String[] args = {"-i", input.getAbsolutePath(), "-embl2rdf", "-o", output.getAbsolutePath(), "-id", "GCA_000007125"};
        validation(input, output, args, "EMBL", "getEmblTest.txt");
        output.delete();
    }

    /**
     * Test 6 with pseudo genes
     */
    @Test
    public void testFilePseudo() throws Exception {
        File input = new File("./src/test/resources/input/conversion/EMBL/pseudo.gz");
        File output = new File("./src/test/resources/output/pseudo.txt.hdt");
        while (output.exists()) {
            output.delete();
        }
        // Pseudo test
        String[] args = {"-i", input.getAbsolutePath(), "-embl2rdf", "-o", output.getAbsolutePath(), "-id", "pseudo"};
        while (output.exists()) {
            output.delete();
        }
        while (new File(output + ".dir").exists()) {
            new File(output + ".dir").delete();
        }

        validation(input, output, args, "EMBL", "getEmblTest.txt");
        output.delete();
    }

    @Test
    public void testFile5Endpoint() throws Exception {
        File input = new File("./src/test/resources/input/conversion/EMBL/testfile5.txt.gz");
        String update = "http://10.117.11.77:7200/repositories/Test/statements";
        // Duplication test
        String[] args = {"-i", input.getAbsolutePath(), "-embl2rdf", "-update", update, "-id", "testFile5Endpoint"};
        if (!new File("/.dockerenv").exists() && input.exists()) {
//            App.main(args);
        }
    }

    @Test
    public void testFileLocal() throws Exception {
        File genomeFolder = new File("/Users/koeho006/Downloads/genomes");
        // List files recursively in a folder and subfolders
        Files.find(genomeFolder.toPath(), 999, (p, bfa) -> bfa.isRegularFile()).forEach(file -> {
            // Only process files that are not yet converted
            if (file.toString().endsWith(".dat.gz") && !new File(file.toFile().getAbsolutePath() + ".ttl").exists()) {
                String[] args = {"-i", file.toString(), "-embl2rdf", "-id", file.toFile().getParentFile().getName(), "-output", file.toFile().getAbsolutePath() + ".ttl", "-codon", "1"};
                try {
                    validation(file.toFile(), new File(file + ".ttl"), args, "EMBL", "getEmblTest.txt");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.err.println("Skipping " + file);
            }
        });

//        File input = new File("/Users/koeho006/Downloads/Sus_scrofa.Sscrofa11.1.110.dat");
//        // File input = new File("/Users/jasperk/failed/GCA_001653015.1.embl.gz");
////        File input = new File("GCA_900536895.1.embl.gz");
//        File output = new File(input.getName() + ".ttl");
//        if (input.exists()) {
//            // Duplication test
//            String[] args = {"-i", input.getAbsolutePath(), "-embl2rdf", "-o", output.getAbsolutePath(), "-id", "TEST",  "-debug", "-codon", "11"}; // "-strict",
//            validation(input, output, args, "EMBL", "getEmblTest.txt");
//        } else {
//            System.err.println("Input file not found");
//        }
//        // output.delete();
    }

    /**
     * Test Genbank with the genbank testfile
     */
    @Test
    public void testGenbank() throws Exception {
        File input = new File("./src/test/resources/input/conversion/GENBANK/GCA_001007995.1_ASM100799v1_genomic.gbff.gz");
        File output = new File("./src/test/resources/output/GCA_001007995.1_ASM100799v1_genomic.hdt");
        String[] args = {"-i", input.getAbsolutePath(), "-genbank2rdf", "-o", output.getAbsolutePath(), "-id", "genbank", "-debug"};
        //        isThereAProblem(input, output, args, "GENBANK", "getEmblTest.txt");
//        output.delete();
    }

    /**
     * BAKTA EMBL test
     */
    @Test
    public void testBAKTA() throws Exception {
        File input = new File("./src/test/resources/input/conversion/BAKTA/GCA_000154365.fasta.embl.gz");
        File output = new File("./src/test/resources/output/BAKTA/GCA_000154365.fasta.embl.ttl");
        String[] args = {"-i", input.getAbsolutePath(), "-embl2rdf", "-o", output.getAbsolutePath(), "-id", "GCA_000154365"};
        App.main(args);
    }
}
























