package nl.wur.ssb.conversion;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Main class to check RDF files on inconsistencies.
 */
public class checkRDF {
    List<String> checkParts;
    List<List<String>> nucleotideParts;
    List<List<String>> proteinParts;
    Domain inputRDF;
    String queryFile;

    /**
     * Constructor for the augustus prediction test.
     *
     * @param domain
     * @param checkParts The parts to check against the RDF file
     * @param queryFile  The query which need to be executed.
     * @throws Exception
     */
    public checkRDF(Domain domain, List<String> checkParts, String queryFile) throws Exception {
        this.inputRDF = domain;
        this.checkParts = checkParts;
        this.queryFile = queryFile;
        checkAugustus();
    }

    /**
     * w
     * Constructor for the prodigal prediction test.
     *
     * @param domain          RDF file where augustus output was writen to
     * @param nucleotideParts The nucleotide parts to check against the RDF file
     * @param proteinParts    The protein parts to check against the RDF file
     * @param queryFile       The query which need to be executed.
     * @throws Exception
     */
    public checkRDF(Domain domain, List<List<String>> nucleotideParts, List<List<String>> proteinParts, String queryFile) throws Exception {
        this.inputRDF = domain;
        // this.inputRDF = nl.wur.ssb.SappGeneric.InputOutput.Input.load(inputRDF);
        this.nucleotideParts = nucleotideParts;
        this.proteinParts = proteinParts;
        this.queryFile = queryFile;
        checkProdigal();
    }

    /**
     * Method to check the RDF database
     *
     * @throws Exception
     */
    private void checkAugustus() throws Exception {
        //get rdf hits and check for inconsistency
        Iterable<ResultLine> results = this.inputRDF.getRDFSimpleCon().runQuery(this.queryFile, true, checkParts.get(2), checkParts.get(3));
        ResultLine result = results.iterator().next();
        assertEquals(result.getLitInt("start"), Integer.parseInt(checkParts.get(0)));
        assertEquals(result.getLitInt("end"), Integer.parseInt(checkParts.get(1)));
    }

    private void checkProdigal() throws Exception {
        //get rdf hits and check for inconsistency. For every 'gene' in the nucleotide file.
        for (int i = 0; i < nucleotideParts.size(); i++) {
            List<String> nPart = nucleotideParts.get(i);
            List<String> pPart = proteinParts.get(i);
            String nseq = nPart.get(nPart.size() - 2);
            String nshakey = nPart.get(nPart.size() - 1);
            String pseq = pPart.get(pPart.size() - 2);
            String pshakey = pPart.get(pPart.size() - 1);

            RDFSimpleCon rdfSimpleCon = new RDFSimpleCon("");
            assertEquals(nPart.get(1), pPart.get(1));
            assertEquals(nPart.get(2), pPart.get(2));

//            System.out.println(pseq + "\n" +  pshakey+ "\n" +  nseq+ "\n" +  nshakey+ "\n" +  nPart.get(1)+ "\n" +  nPart.get(2));
//            System.out.println(hdt);
            // No results yields a nullpointer...
            Iterable<ResultLine> results = this.inputRDF.getRDFSimpleCon().runQuery(this.queryFile, true, pseq, pshakey, nseq, nshakey, nPart.get(1), nPart.get(2));
            assertTrue(results.iterator().hasNext());
            rdfSimpleCon.close();
        }
    }
}
