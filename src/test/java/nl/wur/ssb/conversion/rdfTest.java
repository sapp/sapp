package nl.wur.ssb.conversion;

import jena.riot;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.impl.ModelCom;
import org.junit.jupiter.api.Test;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.options.HDTSpecification;
import org.rdfhdt.hdtjena.HDTGraph;

import java.io.File;
import java.util.Scanner;

public class rdfTest {

    @Test
    public void testInit() throws Exception {
        Domain domain = new Domain("");
        domain.getRDFSimpleCon().addLit("http://example.com", "http://example.com", "test");
        domain.save("./src/test/resources/test.ttl");
        Scanner scanner = new Scanner(new File("./src/test/resources/test.ttl"));
        while (scanner.hasNextLine()) {
            System.out.println(scanner.nextLine());
        }
    }

    @Test
    public void testRiot() throws Exception{
        String rdfInput = "./src/test/resources/input/conversion/RDF/test.ttl";
        riot.main("--output=TURTLE", "-q", rdfInput);
//        riot.main("--help");
    }

    @Test
    public void testRDFSimpleConHDT() throws Exception {
        testInit();

        String baseURI = "http://example.com/mydataset";
        String rdfInput = "./src/test/resources/input/conversion/RDF/test.ttl";
        String inputType = "turtle";
        String hdtOutput = "./src/test/resources/test.hdt";

        // Create HDT from RDF file
        HDT hdt = HDTManager.generateHDT(
                rdfInput,         // Input RDF File
                baseURI,          // Base URI
                RDFNotation.parse(inputType), // Input Type
                new HDTSpecification(),   // HDT Options
                null              // Progress Listener
        );

        // Save generated HDT to a file
        hdt.saveToHDT(hdtOutput, null);

        hdt = HDTManager.loadIndexedHDT("./src/test/resources/test.hdt", null);
        HDTGraph graph = new HDTGraph(hdt);
        Model model = new ModelCom(graph);
        model.listStatements().forEachRemaining(System.err::println);
    }
}
