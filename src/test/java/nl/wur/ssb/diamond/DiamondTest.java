package nl.wur.ssb.diamond;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;

import java.io.File;

public class DiamondTest {

    @Test
    public void testDiamondBlast() throws Exception {
        File input = new File("./src/test/resources/input/interpro/testfile1.txt.hdt");
        File output = new File("./src/test/resources/output/diamond/testfile1.ttl");
        String[] args = new String[]{
                "-diamond",
                "-db", "./groEL.dmnd",
                "-binary", "/Users/jasperk/mambaforge/bin/diamond",
                "-input", input.getAbsolutePath(),
                "-output", output.getAbsolutePath(),
        };
        App.main(args);
    }

    @Test
    public void testDiamondEndpoint() throws Exception {
        File output = new File("./src/test/resources/output/diamond/endpoint.ttl");
        String[] args = new String[]{
                "-diamond",
                "-db", "groEL.dmnd",
                "-binary", "/Users/jasperk/mambaforge/bin/diamond",
                "-endpoint", "http://localhost:7200/repositories/Anaplasma",
                "-output", output.getAbsolutePath(),
        };
        App.main(args);
    }

    private void isSomethingWrong(File inputFile, File outputFile, String queryFile, String[] args) throws Exception {
        outputFile.getParentFile().mkdirs();
        App.main(args);
    }
}
