package nl.wur.ssb.external;

import nl.wur.ssb.App;
import org.junit.jupiter.api.Test;


public class ExternalTest {
    // External annotation implementing the following modules:
    // - Conversion
    // - Prodigal
    // - InterProScan

    @Test
    public void external() throws Exception {
/*
./src/test/resources/external/GCA_000005845.2.fasta.gz
./src/test/resources/external/GCA_000005845.2.prodigal-Pfam.interproscan.json.gz
./src/test/resources/external/GCA_000005845.2.prodigal.faa.txt
./src/test/resources/external/GCA_000005845.2.prodigal.ffn.txt
./src/test/resources/external/GCA_000005845.2.prodigal.gff.txt
 */

        // Perform fasta converion
        App.main(new String[]{
                "-input", "./src/test/resources/input/external/GCA_000005845.2.fasta.gz",
                "-output", "./src/test/resources/output/external/GCA_000005845.2.ttl",
                "-id", "GCA_000005845.2",
                "-codon", "11",
                "-genome",
                "-fasta2rdf",
                "-debug"
        });

        // Perform prodigal annotation
        App.main(new String[]{
                "-input", "./src/test/resources/output/external/GCA_000005845.2.ttl",
                "-output", "./src/test/resources/output/external/GCA_000005845.2.prodigal.ttl",
                "-prodigal",
                "-gff", "./src/test/resources/input/external/GCA_000005845.2.prodigal.gff.txt",
                "-faa", "./src/test/resources/input/external/GCA_000005845.2.prodigal.faa.txt",
                "-ffa", "./src/test/resources/input/external/GCA_000005845.2.prodigal.ffn.txt"
        });

        // Perform interproscan annotation
        App.main(new String[]{
                "-input", "./src/test/resources/output/external/GCA_000005845.2.prodigal.ttl",
                "-output", "./src/test/resources/output/external/GCA_000005845.2.prodigal.interproscan.ttl",
                "-interpro",
                "-debug",
                "-resultFile", "./src/test/resources/input/external/GCA_000005845.2.prodigal-Pfam.interproscan.json.gz"
        });
    }
}
