package nl.wur.ssb.interpro;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.wur.ssb.App;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.interproscan.objects.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.assertTrue;

public class InterProTest {
    @Test
    public void testInterProScan() throws Exception {
        File input = new File("./src/test/resources/input/interpro/testfile1.txt.hdt");
        File output = new File("./src/test/resources/output/testfile1.txt.ttl");
        String resultFile = "./src/test/resources/input/interpro/testfile1.txt.hdt_0_interpro.fasta_interpro.json";
        String[] args = new String[]{
                "-interpro",
                "-input", input.getAbsolutePath(),
                "-output", output.getAbsolutePath(),
                "-resultFile", resultFile,
                "-applications", "PFAM,TIGRFAM,SMART",
                "-path", "/unlock/infrastructure/binaries/interproscan/interproscan-5.53-87.0/"
        };
        isSomethingWrong(input, output, "getTestPredicts.txt", args);
    }

    @Test
    public void testLactobacillus() throws Exception {
        File resultFile = new File("./src/test/resources/input/interpro/Lactobacillus_apis.faa.json.gz");
            String content;
            // Check if gzip compression
            if (resultFile.getName().endsWith(".gz")) {
                GZIPInputStream gzip = new GZIPInputStream(Files.newInputStream(resultFile.toPath()));
                content = new String(gzip.readAllBytes());
            } else {
                content = Files.readString(resultFile.toPath());
            }

            // JSON string mapper to object with several configuration options
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

            // https://json2csharp.com/json-to-pojo to generate the objects
            Root root = objectMapper.readValue(content, Root.class);
            System.err.println("Loaded " + root.getInterproscanVersion());
    }

    private void isSomethingWrong(File inputFile, File outputFile, String queryFile, String[] args) throws Exception {
        App.main(args);
        File JSON = new File(inputFile + "_0_interpro.fasta_interpro.json");
        HashMap<String, String> fileMap = parser(JSON);

        //Making sparql query engine
        //Queries to execute through RDFResource.
        Domain domain = new Domain("file://" + outputFile.getAbsolutePath());
        Iterable<ResultLine> contigs = domain.getRDFSimpleCon().runQuery(queryFile, true);
        for (ResultLine contig : contigs) {
            String key = contig.getLitString("key");
            String name = contig.getLitString("name");
            assertTrue(fileMap.containsKey(key));
            assertTrue(fileMap.containsValue(name));
        }
    }

    private HashMap<String, String> parser(File outputfile) throws Exception {
        HashMap<String, String> interProKeyNameMap = new HashMap();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

            // https://json2csharp.com/json-to-pojo to generate the objects
            String content = Files.readString(outputfile.toPath());
            Root root = objectMapper.readValue(content, Root.class);

            for (Result result : root.getResults()) {
                for (Match match : result.getMatches()) {
                    Signature signature = match.getSignature();
                    for (Location location : match.getLocations()) {
//                            JSONObject model = (JSONObject) models.get(element.toString());
//                            String modelName = (String) model.get("name");
//                            String modelKey = (String) model.get("key");
//                            interProKeyNameMap.put(modelKey, modelName);
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Interpro's JSON data file not found.");
            System.exit(0);
        }
        return interProKeyNameMap;
    }
}
