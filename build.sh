#!/bin/bash
#============================================================================
#title          :SAPP-2.0
#description    :The SAPP application build script
#author         :Jasper Koehorst
#date           :2023
#version        :2.0
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR

git -C $DIR pull

rm -f $DIR/*.jar

if [ "$1" == "test" ]; then
    ./gradlew build --info
else
    echo "Skipping tests, run './build.sh test' to perform tests"
    ./gradlew build -x test --stacktrace
fi

FILE=`ls $DIR/build/libs/ | tail -n 1` 

echo "File "$FILE" selected"

# Test if startup works
java -jar $DIR/build/libs/$FILE --help

# Rename file and move to local directory for upload
cp $DIR/build/libs/$FILE $DIR/$FILE
